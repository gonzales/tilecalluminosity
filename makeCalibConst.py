#!/usr/bin/env python

import sys, os
import ROOT

from optparse import OptionParser

def get_comma_separated_args(option, opt, value, parser):
    setattr(parser.values, option.dest, value.split(','))


parser = OptionParser()

parser.add_option('-r','--runNumber', type=int, help='Run number', dest="runNumber")
parser.add_option('--doPedestal', dest = "doPedestal", action="store_true", help='Compute the pedestal', default=False)
parser.add_option('-l', '--LBRange', type='string', help='Comma separated LBRange', action='callback', callback=get_comma_separated_args)
parser.add_option('-x','--filePathIndex', type=int, help='Index of path for Benedetto file used in get_anchor.sh', dest="filePathIndex")
parser.add_option('-g','--inGapLaser', type=int, help='Index for inGap Laser', dest="inGapLaser", default = 0)
parser.add_option('--doSACorrection', dest = "doSACorrection", action="store_true", help='Do SA correction', default=False)
parser.add_option('-s', '--suffix', type='string', help='Suffix of the calibConst file', dest = "suffix",default="")
parser.add_option('--override-pedestal', type='string', help='Override pedestal method', dest = "overridePedestal", default="")



(options, args) = parser.parse_args()

ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine(".L MakeCalibConst.C")
ROOT.MakeCalibConst(options.runNumber,0,options.doPedestal,(int)(options.LBRange[0]),(int)(options.LBRange[1]),options.filePathIndex,options.inGapLaser,options.doSACorrection,options.suffix,-1,options.overridePedestal)
