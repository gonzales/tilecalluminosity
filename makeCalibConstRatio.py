#!/usr/bin/env python

import sys, os
import ROOT

from optparse import OptionParser

def get_comma_separated_args(option, opt, value, parser):
    setattr(parser.values, option.dest, value.split(','))


parser = OptionParser()

parser.add_option('--convConstantIndex1', type=int, help='Index for the calibration constants file in the numerator', dest="convConstantIndex1")
parser.add_option('--convConstantIndex2', type=int, help='Index for the calibration constants file in the denominator', dest="convConstantIndex2")
parser.add_option('-c','--cellsType', type=int, help='Index of the cells', dest="cellsType")
parser.add_option('--legend1', type='string', help='Legend used for the numerator file', dest = "legend1", default="")
parser.add_option('--legend2', type='string', help='Legend used for the denominator file', dest = "legend2", default="")
parser.add_option('--inGapLaser1', type=int, help='Index for inGap Laser used in the numerator file', dest="inGapLaser1", default = 0)
parser.add_option('--inGapLaser2', type=int, help='Index for inGap Laser used in the denominator file', dest="inGapLaser2", default = 0)
parser.add_option('-r', '--ratioRange', type='string', help='Comma separated ratioRange for the ratio panel', action='callback', callback=get_comma_separated_args, default=["-1","-1"])
parser.add_option('-y', '--yRange', type='string', help='Comma separated ratioRange for the constants panel', action='callback', callback=get_comma_separated_args, default=["-1","-1"])


(options, args) = parser.parse_args()

ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine(".L macroRatioCalibConst.C")
ROOT.macroRatioCalibConst(options.convConstantIndex1,options.convConstantIndex2,options.cellsType,options.legend1,options.legend2,options.inGapLaser1,0,options.inGapLaser2,0,float((options.ratioRange[0])),float((options.ratioRange[1])),0,float((options.yRange[0])),float((options.yRange[1])))
