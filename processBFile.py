#!/usr/bin/env python

import sys, os
import ROOT

from optparse import OptionParser

def get_comma_separated_args(option, opt, value, parser):
    setattr(parser.values, option.dest, value.split(','))

parser = OptionParser()

parser.add_option('--plotRatioFiles', dest = "plotRatioFiles", action="store_true", help='Use macroPlotRatioFiles.C', default=False)

parser.add_option('-r','--runNumber', type=int, help='Run number', dest="runNumber")
parser.add_option('-c','--cellsType', type=int, help='Index of the cells', dest="cellsType",default=-1)
parser.add_option('-s', '--suffix', type='string', help='Suffix of the BFile', dest = "suffix")
parser.add_option('--suffix2', type='string', help='Suffix of another BFile to use in double ratios', dest = "suffix2", default="")
parser.add_option('--label1', type='string', help='Label for suffix1 data', dest = "label1", default="")
parser.add_option('--label2', type='string', help='Label for suffix2 data', dest = "label2", default="")
parser.add_option('-x','--filePathIndex', type=int, help='Index of path for Benedetto file used in get_anchor.sh', dest="filePathIndex")
parser.add_option('--doPlotPerChannel', dest = "doPlotPerChannel", action="store_true", help='Make per channel plots', default=False)
parser.add_option('--doPlotPerChannelProfile', dest = "doPlotPerChannelProfile", action="store_true", help='Make per channel profile plots', default=False)
parser.add_option('--doPlotPerChannelRR', dest = "plotPerChannelRR", action="store_true", help='Make RR per channel plots', default=False)
parser.add_option('--doPlotRMS', dest = "doPlotRMS", action="store_true", help='Make RMS vs LB plot', default=False)
parser.add_option('--skipJoinChannelPlot', dest = "skipJoinChannelPlot", action="store_true", help='Skip join channel plot', default=False)
parser.add_option('-v','--avg_every', type=int, help='Step for average', dest="avg_every",default=1)
parser.add_option('-y', '--yRange', type='string', help='Comma separated yRange', action='callback', callback=get_comma_separated_args, default=["-1","-1"])
parser.add_option('--yRangePerChannel', type='string', help='Comma separated yRangePerChannel', action='callback', callback=get_comma_separated_args, default=["-1","-1"])
parser.add_option('--yRangeRMS', type='string', help='Comma separated yRangeRMS', action='callback', callback=get_comma_separated_args, default=["-1","-1"])
parser.add_option('--yRangeRMSPerChannel', type='string', help='Comma separated yRangeRMS perChannel version', action='callback', callback=get_comma_separated_args, default=["-1","-1"])
parser.add_option('--yRangeRMSJoinChannel', type='string', help='Comma separated yRangeRMS JoinChannel version', action='callback', callback=get_comma_separated_args, default=["-1","-1"])
parser.add_option('-l', '--LBRange', type='string', help='Comma separated LBRange', action='callback', callback=get_comma_separated_args, default=["-1","-1"])
parser.add_option('--ProfileRange', type='string', help='Comma separated ProfileRange', action='callback', callback=get_comma_separated_args, default=["-1","-1"])
parser.add_option('--ProfileYRange', type='string', help='Comma separated ProfileYRange', action='callback', callback=get_comma_separated_args, default=["-1","-1"])
parser.add_option('-g','--gainGroup', type=int, help='Gain group', dest="gainGroup", default=-1)
parser.add_option('--prune_factor', type=int, help='Factor to apply in pruning function', dest="prune_factor", default=-1)
parser.add_option('--txtIndex', type=int, help='Index for txt output', dest="txtIndex", default=-1)
parser.add_option('--txtName', type='string', help='Name for txt output', dest = "txtName", default="")
parser.add_option('--writeLineFit', dest = "writeLineFit", action="store_true", help='Write the linear fit parameters to a file', default=False)
parser.add_option('--columnToUse', type=int, help='Column to use in Benedetto file', dest="columnToUse",default=83)
parser.add_option('-f','--folderName', type='string', help='Output folder', dest = "folderName", default="")
parser.add_option('--doIncludeErrors', dest = "doIncludeErrors", action="store_true", help='Include errors in plot', default=False)
parser.add_option('--labelLBRange', dest = "labelLBRange", action="store_true", help='Use LB range as x-labels', default=False)
parser.add_option('--useCells', type='string', help='Comma separated cells', dest = "useCells", default="")
parser.add_option('--useParts', type='string', help='Comma separated parts', dest = "useParts", default="")
parser.add_option('--plotProjectedRatio', dest = "plotProjectedRatio", action="store_true", help='Plot the projected ratio to the y axis as a histogram', default=False)
parser.add_option('--plotProjectedProfile', dest = "plotProjectedProfile", action="store_true", help='Plot the projected profile to the y axis as a histogram', default=False)
parser.add_option('--freqCutoff', dest = "freqCutoff", action="store_true", help='Execute macroFrecCutoff', default=False)
parser.add_option('-b','--bfiles-folder', type='string', help='Folder for the BFiles', dest = "bfiles_folder", default="BFiles")
parser.add_option('-o','--out-folder', type='string', help='Folder for the outputs', dest = "out_folder", default="macros/Plots/PerChannelRatioRatios")



(options, args) = parser.parse_args()

override_cells = ROOT.vector('TString')()
if options.useCells != "": 
    for f in options.useCells.split(","): override_cells.push_back(f)
override_parts = ROOT.vector('TString')()
if options.useParts != "": 
    for f in options.useParts.split(","): override_parts.push_back(f)

ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine(".L macroPerChannelRatioRatios.C")
ROOT.gROOT.ProcessLine(".L macroPlotRatioFiles.C")
ROOT.gROOT.ProcessLine(".L macroFreqCutoff.C")

if options.txtName != "" and options.txtIndex == -1 : options.txtIndex = 0
if options.plotProjectedRatio: options.doPlotPerChannel = True
if options.plotProjectedProfile: options.doPlotPerChannelProfile = True

if not options.plotRatioFiles and not options.freqCutoff:
    ROOT.macroPerChannelRatioRatios(options.runNumber, 
                                    options.cellsType,
                                    options.suffix,
                                    options.suffix2,
                                    options.filePathIndex,
                                    options.doPlotPerChannel,
                                    options.avg_every,
                                    float(options.yRange[0]),
                                    float(options.yRange[1]),
                                    float(options.LBRange[0]),
                                    float(options.LBRange[1]),
                                    options.gainGroup,
                                    options.prune_factor,
                                    options.txtIndex,
                                    options.txtName,
                                    options.writeLineFit,
                                    options.columnToUse,
                                    options.folderName,
                                    options.doIncludeErrors,
                                    float(options.ProfileRange[0]),
                                    float(options.ProfileRange[1]),
                                    float(options.ProfileYRange[0]),
                                    float(options.ProfileYRange[1]),
                                    options.doPlotPerChannelProfile,
                                    options.skipJoinChannelPlot,
                                    options.plotPerChannelRR,
                                    options.label1,
                                    options.label2,
                                    float(options.yRangePerChannel[0]),
                                    float(options.yRangePerChannel[1]),
                                    override_cells,
                                    override_parts,
                                    options.doPlotRMS,
                                    float(options.yRangeRMS[0]),
                                    float(options.yRangeRMS[1]),
                                    float(options.yRangeRMSPerChannel[0]),
                                    float(options.yRangeRMSPerChannel[1]),
                                    float(options.yRangeRMSJoinChannel[0]),
                                    float(options.yRangeRMSJoinChannel[1]),
                                    options.plotProjectedRatio,
                                    options.plotProjectedProfile,
                                    options.bfiles_folder,
                                    options.out_folder,
                        )
elif options.plotRatioFiles:

    txtNames = ROOT.vector('TString')()
    for f in options.txtName.split(","): txtNames.push_back(f)

    ROOT.macroPlotRatioFiles(
                                    options.cellsType,
                                    txtNames,
                                    options.columnToUse,
                                    float(options.yRange[0]),
                                    float(options.yRange[1]),
                                    options.labelLBRange,
                                    options.folderName,
                            )
elif options.freqCutoff:

    ROOT.macroFreqCutoff(
                                    options.runNumber,
                                    options.cellsType,
                                    options.suffix,
                                    options.folderName,
                                    float(options.ProfileRange[0]),
                                    float(options.ProfileRange[1]),
                                    float(options.ProfileYRange[0]),
                                    float(options.ProfileYRange[1]),
                                    override_cells,
                                    override_parts,
                            )

