#include "MBLumi/IntegratorGainTool.h"

float IntegratorGainTool::GetGain(int part, int mod, int pmt, int gg) {
    return gains[part][mod][Utils::GetLaserIndex(pmt, part)][gg];
}

float IntegratorGainTool::GetGainError(int part, int mod, int pmt, int gg) {
    return gains_err[part][mod][Utils::GetLaserIndex(pmt, part)][gg];
}