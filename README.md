Cleaned version of the repo in [here](https://gitlab.cern.ch/gonzales/mblumi/-/tree/sgonzalez_20210824)

Contact: sergio.gonzalez.fernandez@cern.ch

# Setup

source setup.sh

# MBLumi

To get the Signal above pedestal / currents / luminosity for a given run, use the [MBLumi.py](MBLumi.py) script. The main parameters are:

* **-r** The run number
* **-c** Index for the cells to process. Defined in [Global::GetCellsToCompute](https://gitlab.cern.ch/gonzales/tilecalluminosity/-/blob/master/MBLumi/configuration.h#L1000). For example:
	* 0 -> E3, E4
	* 2 -> D5, D6
	* 1314 -> A13, A14
* **-s** Suffix label for the outputs. Can contain special content:
	* SignalAbovePed: Will not divide by the gain
	* refGain354124: The gain grouping will be done in reference to 354174
* **-a** The anchor runNumber to use for the calibration constants. If zero, the constants will be 1 and the outputs will be currents. The correspondence to the file with the constants is defined in [Global::GetConvConstantFilename](https://gitlab.cern.ch/gonzales/tilecalluminosity/-/blob/master/MBLumi/configuration.h#L844)
* **-l** Index for the laser inter-run corrections. Defined in [Global::GetLaserDirectory](https://gitlab.cern.ch/gonzales/tilecalluminosity/-/blob/master/MBLumi/configuration.h#L1132)
* **-g** Index for the inGap laser corrections. Zero if not applied. Defined in [Global::GetInGapInputFile](https://gitlab.cern.ch/gonzales/tilecalluminosity/-/blob/master/MBLumi/configuration.h#L1222)
* **--doPedestal** Even if there is already pedestal data to be loaded, this command will override that and the pedestal will be recomputed.
* **--doPedestalPlots** Produce the plots for the pedestal computation.
* **--override-pedestal** Use a pedestal method different than the one defined in [Global::getPedestalType](https://gitlab.cern.ch/gonzales/tilecalluminosity/-/blob/master/MBLumi/configuration.h#L761). Options are defined in [Global::getOverridenPedestalType](https://gitlab.cern.ch/gonzales/tilecalluminosity/-/blob/master/MBLumi/configuration.h#L832)
* **--useTime** Output file will be indexed by timestamp rather than by LB.
* **--anchor-file** Set the file with the calib constants to override the -a parameter.

This script will process mb files and produce txt files containing per channel signal above pedestal / current / luminosity for the determined cells. Also the averaged values per cell family will be stored in a different txt file. The per channel files can be parsed with a function defined in macros/functions.h [getLumiInformation](https://gitlab.cern.ch/gonzales/tilecalluminosity/-/blob/master/macros/functions.h#L2095).

# Ratios with other detector's luminosity, profile plots, etc

The output of MBLumi.py can then be processed to produce a variety of plots with [processBFile.py](processBFile.py). The main parameters are:

* **-r** The run number
* **-c** Index for the cells to process. Defined in [Global::GetCellsToCompute](https://gitlab.cern.ch/gonzales/tilecalluminosity/-/blob/master/MBLumi/configuration.h#L1000)
* **-s** The suffix label of the file to process including the laser labels (i.e. the text in the perChannel file between the run Number and the cells, for example Lumi_{run}{suffix}_ECells.dat)
* **-x** Index for the path to retrieve the Benedetto file that contains other detector's luminosity. Defined in [get_anchor.sh](get_anchor.sh)
* **--doPlotPerChannel** Produce the ratio plots for each channel. Will take a bit more time.
* **--doPlotPerChannelProfile** Produce the profile ratio plots for each channel. Will take a bit more time.
* **--doPlotRMS** Produce the RMS vs LB plot.
* **-g** If this option is used with 1,2 or 3; only the channels from that gain group will be used.

More input parameters are accepted by this script to customize the y axis range among other things.

# Produce Calibration Constants

The MBLumi.py script can be used to produce TileCal luminosity but needs the calibration constants to convert the current to luminosity. These information can be produced by the [makeCalibConst.py](makeCalibConst.py) script, which produces the per channel calib constants by cross calibrating the Tile channel current to tracking luminosity inside a LB range of a particular run. The main parameters are:

* **-r** The run number to use for the cross calibration.
* **--doPedestal** Even if there is already pedestal data to be loaded, this command will override that and the pedestal will be recomputed.
* **-l** The LB range coma-separated where the cross-calibration will take place.
* **-x** Index for the path to retrieve the Benedetto file that contains other detector's luminosity. Defined in [get_anchor.sh](get_anchor.sh)
* **-g** Index for the inGap laser corrections. Defined in [Global::GetInGapInputFile](https://gitlab.cern.ch/gonzales/tilecalluminosity/-/blob/master/MBLumi/configuration.h#L1222)
* **--override-pedestal** Use a pedestal method different than the one defined in [Global::getPedestalType](https://gitlab.cern.ch/gonzales/tilecalluminosity/-/blob/master/MBLumi/configuration.h#L761). Options are defined in [Global::getOverridenPedestalType](https://gitlab.cern.ch/gonzales/tilecalluminosity/-/blob/master/MBLumi/configuration.h#L832)

This script will produce a file with the calibration constants in a share/ folder that can be used in the MBLumi process.

# Ratio of Calibration Constants

In order to test the gain dependency of a channel to the current, one can study the ratio between calibration constants produced in a LB range with large current  and the ones produced at a LB range with low current. This can be produced with the [makeCalibConstRatio.py](makeCalibConstRatio.py). The main parameters are:

* **--convConstantIndex1** Index for the calibration constants file in the numerator. Can be defined in [Global::GetConvConstantFilename](https://gitlab.cern.ch/gonzales/tilecalluminosity/-/blob/master/MBLumi/configuration.h#L844)
* **--convConstantIndex1** Index for the calibration constants file in the denominator. Can be defined in [Global::GetConvConstantFilename](https://gitlab.cern.ch/gonzales/tilecalluminosity/-/blob/master/MBLumi/configuration.h#L844)
* **-c** Index for the cells to process. Defined in [Global::GetCellsToCompute](https://gitlab.cern.ch/gonzales/tilecalluminosity/-/blob/master/MBLumi/configuration.h#L1000)
* **--legend1** Legend text for the numerator
* **--legend2** Legend text for the denominator
* **--inGapLaser1** Index for the inGap laser corrections used for the numerator. Zero if not applied. Defined in [Global::GetInGapInputFile](https://gitlab.cern.ch/gonzales/tilecalluminosity/-/blob/master/MBLumi/configuration.h#L1222)
* **--inGapLaser2** Index for the inGap laser corrections used for the denominator. Zero if not applied. Defined in [Global::GetInGapInputFile](https://gitlab.cern.ch/gonzales/tilecalluminosity/-/blob/master/MBLumi/configuration.h#L1222)
* **-r** The range coma-separated for the ratio panel.
* **-y** The range coma-separated for the constants panel.

# Plot MEAN laser-in-gap corrections

The plots of the inGap correction as a function of current are produced with [macroMeanLasGap.C](macroMeanLasGap.C). The input parameter is just the laserInGap index that corresponds to the file with the inGap corrections. These are defined in [Global::GetInGapInputFile](https://gitlab.cern.ch/gonzales/tilecalluminosity/-/blob/master/MBLumi/configuration.h#L1222).
