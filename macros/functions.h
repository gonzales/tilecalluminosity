#ifndef functions_h
#define functions_h
#include <cstdlib>
#include <sys/stat.h>
#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"

namespace PARAMS {
    typedef enum {
        LHC_FREQ = 11246, //Hz
        XSEC_INE = 80, //mb
    } PARAMSID;
}

struct histParams {
    TF1 * f_gaus;
    double fit_mean;
    double goodness_of_fit;
    double skewness;
};

map<TString,int> cell_colors = {{"E3_EBA",kBlue},{"E3_EBC",kRed},{"E4_EBA",kGreen},{"E4_EBC",kMagenta},
                                {"D5L_EBA",kBlue},{"D5L_EBC",kRed},{"D6L_EBA",kGreen},{"D6L_EBC",kMagenta},
                                {"D5R_EBA",kBlue+2},{"D5R_EBC",kRed+2},{"D6R_EBA",kGreen+2},{"D6R_EBC",kMagenta+2},
                                {"D5 EBA",kBlue},{"D5 EBC",kRed},{"D6 EBA",kGreen},{"D6 EBC",kMagenta},
                                {"D1L_LBA",kBlue},{"D1L_LBC",kRed},{"D2L_LBA",kGreen},{"D2L_LBC",kMagenta},
                                {"D1R_LBA",kBlue+2},{"D1R_LBC",kRed+2},{"D2R_LBA",kGreen+2},{"D2R_LBC",kMagenta+2},
                                {"D1 LBA",kBlue},{"D1 LBC",kRed},{"D2 LBA",kGreen},{"D2 LBC",kMagenta},
                                {"A12L_EBA",kBlue},{"A12L_EBC",kRed},{"A13L_EBA",kGreen},{"A13L_EBC",kMagenta},
                                {"A12R_EBA",kBlue+2},{"A12R_EBC",kRed+2},{"A13R_EBA",kGreen+2},{"A13R_EBC",kMagenta+2},
                                {"A14L_EBA",kBlue},{"A14L_EBC",kRed},{"A14R_EBA",kBlue+2},{"A14R_EBC",kRed+2},
                                {"E1_EBA",kCyan},{"E1_EBC",kOrange+10},{"E2_EBA",kPink+10},{"E2_EBC",kViolet+7},
                                };

vector<TString> getFilesList(TString folder, vector<TString> v_mustContain) {
    TSystemDirectory dir(folder,folder);
    TList * files = dir.GetListOfFiles();

    TSystemFile * file;
    TIter nxt(files);

    vector<TString> output;

    while ((file=(TSystemFile*)nxt())) {
        TString fileName = file->GetName();
        if (fileName == "." || fileName == "..") continue;
        bool pass_contain = true;
        for (TString mustContain : v_mustContain) {
            if (!fileName.Contains(mustContain)) pass_contain = false;
        }
        if (!pass_contain) continue;
        output.push_back(fileName);
    }

    return output;
}

vector<TString> getFilesList(TString folder, TString mustContain = "") {
    TSystemDirectory dir(folder,folder);
    TList * files = dir.GetListOfFiles();

    TSystemFile * file;
    TIter nxt(files);

    vector<TString> output;

    while ((file=(TSystemFile*)nxt())) {
        TString fileName = file->GetName();
        if (fileName == "." || fileName == "..") continue;
        if (mustContain != "" && !fileName.Contains(mustContain)) continue;
        output.push_back(fileName);
    }

    return output;
}

double GetXNDC(double x) {
  gPad->Update();//this is necessary!
  return (x - gPad->GetX1())/(gPad->GetX2()-gPad->GetX1());
}

double GetYNDC(double y) {
  gPad->Update();//this is necessary!
  return (y - gPad->GetY1())/(gPad->GetY2()-gPad->GetY1());
}

template <typename T>
vector<T> joinVectors(vector<T> A, vector<T> B) {
    vector<T> AB = A;
    AB.insert(AB.end(), B.begin(), B.end());
    return AB;
}

template <typename T>
void add(T value, vector<T> & v) {
    if (find(v.begin(),v.end(),value) == v.end()) v.push_back(value);
}

template <typename T>
int getIndex(T value, vector<T> v) {
    int index = find(v.begin(),v.end(),value) - v.begin();
    return index;
}

template <typename T>
bool contains(T value, vector<T> v) {
    return find(v.begin(),v.end(),value) < v.end();
}

bool fileExists(const TString name) {
  struct stat buffer;   
  return (stat (name, &buffer) == 0); 
}

bool fileExists(const std::string& name) {
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}

void coutError(TString error) {
    cout << "\033[1;31m" << error << "\033[0m" << endl;
}

template <typename T>
int indexOf(int value, vector<T> v) {
    if (v.size() == 0) {coutError("The size in indexOf is 0"); return -1;}
    for (int i = 0 ; i < v.size() - 1; i++) {
        if (v.at(i+1) > value && value >= v.at(i)) {
            return i;
        }
        if (v.at(i+1) == value) return i+1;
    }
    return -1;
}

int getDSID(TString fileName) {
    if (fileName.Index("anch") != -1) {
        TString removeThis = fileName(fileName.Index("anch"),10);
        fileName.ReplaceAll(removeThis,"");
    }
    TPRegexp re("(\\d\\d\\d\\d\\d\\d)");
    TObjArray *sub = re.MatchS(fileName);

    if (sub->Last() == nullptr) {
        coutError("Unable to extract DSID from " + fileName);
        return 0;
    } else {
        TString dsid = ((TObjString *)sub->At(1))->GetString();
        return dsid.Atoi();
    }

    return 0;
}

int getFromLB(TString fileName) {
    TPRegexp re("_from(\\d+)");
    TObjArray *sub = re.MatchS(fileName);

    if (sub->Last() == nullptr) {
        coutError("Unable to extract fromLB from " + fileName);
        return 0;
    } else {
        TString LB = ((TObjString *)sub->At(1))->GetString();
        return LB.Atoi();
    }

    return 0;
}

int getToLB(TString fileName) {
    TPRegexp re("_to(\\d+)");
    TObjArray *sub = re.MatchS(fileName);

    if (sub->Last() == nullptr) {
        coutError("Unable to extract toLB from " + fileName);
        return 0;
    } else {
        TString LB = ((TObjString *)sub->At(1))->GetString();
        return LB.Atoi();
    }

    return 0;
}

TString getCellsName(int cells_type) {
    if (cells_type == 0 || cells_type == 1) return "ECells";
    else if (cells_type == 2) return "DCells";
    else if (cells_type == 3) return "YCells";
    else if (cells_type == 4) return "BCells";
    else if (cells_type == 5) return "e2Cells";
    else if (cells_type == 6) return "a2Cells";
    else if (cells_type == 7) return "ACells";
    else if (cells_type == 10) return "ACells";
    else if (cells_type == 1314) return "A13A14";
    else if (cells_type == 13) return "A13LA13R";
    else if (cells_type == 14) return "BCCells";
    else if (cells_type == 15) return "A15Cell";
    else if (cells_type == 16) return "D6Cell";
    else if (cells_type == 21) return "D5Cells";
    else if (cells_type == 22) return "D6Cells";
    else if (cells_type == 31) return "D1Cells";
    else if (cells_type == 32) return "D2Cells";
    else if (cells_type == 41) return "BC3Cells";
    else if (cells_type == 42) return "BC4Cells";
    else if (cells_type == 61) return "A2Cells";
    else if (cells_type == 62) return "A9Cells";
    else if (cells_type == 71) return "A12Cells";
    else if (cells_type == 72) return "A15Cells";
    else if (cells_type == 1234) return "E1E2E3E4";
    else if (cells_type > 100) return "ECells_ByGainGroup";
    return "";
}

pair<TString,TString> getCellName(int cells_type){
    TString cell_1 = "E3";
    TString cell_2 = "E4";
    if (cells_type == 2) {
        cell_1 = "D5";
        cell_2 = "D6";
    } else if (cells_type == 3) {
        cell_1 = "D1";
        cell_2 = "D2";
    } else if (cells_type == 4) {
        cell_1 = "BC3";
        cell_2 = "BC4";
    } else if (cells_type == 5) {
        cell_1 = "E1";
        cell_2 = "E2";
    } else if (cells_type == 6) {
        cell_1 = "A2";
        cell_2 = "A9";
    } else if (cells_type == 7) {
        cell_1 = "A12";
        cell_2 = "A15";
    } else if (cells_type == 10) {
        cell_1 = "A12";
        cell_2 = "A13";
    }
    else if (cells_type == 101) {
        cell_1 = "E3_g1";
        cell_2 = "E4_g1";
    } else if (cells_type == 102) {
        cell_1 = "E3_g2";
        cell_2 = "E4_g2";
    } else if (cells_type == 103) {
        cell_1 = "E3_g3";
        cell_2 = "E4_g3";
    }
    return make_pair(cell_1,cell_2);
}

TString getBFilePath(int runNumber, TString runSuffix, int cell_type) {
    return TString::Format("BFiles/Lumi_%d%s_%s.dat", runNumber, runSuffix.Data(), getCellsName(cell_type).Data());
}

double *HSVtoRGB(double h)
{
    float fR = 0., fG = 0., fB = 0.;
    float fV = 1., fS = 1.;
    double *rgb = new double[3];
    float fC = fV * fS; // Chroma
    float fHPrime = fmod(h / 60.0, 6);
    float fX = fC * (1 - fabs(fmod(fHPrime, 2) - 1));
    float fM = fV - fC;

    if (0 <= fHPrime && fHPrime < 1)
    {
        fR = fC;
        fG = fX;
        fB = 0;
    }
    else if (1 <= fHPrime && fHPrime < 2)
    {
        fR = fX;
        fG = fC;
        fB = 0;
    }
    else if (2 <= fHPrime && fHPrime < 3)
    {
        fR = 0;
        fG = fC;
        fB = fX;
    }
    else if (3 <= fHPrime && fHPrime < 4)
    {
        fR = 0;
        fG = fX;
        fB = fC;
    }
    else if (4 <= fHPrime && fHPrime < 5)
    {
        fR = fX;
        fG = 0;
        fB = fC;
    }
    else if (5 <= fHPrime && fHPrime < 6)
    {
        fR = fC;
        fG = 0;
        fB = fX;
    }
    else
    {
        fR = 0;
        fG = 0;
        fB = 0;
    }

    rgb[0] = fR + fM;
    rgb[1] = fG + fM;
    rgb[2] = fB + fM;

    rgb[1] = rgb[0]>0.75 && rgb[1]>0.75 ? rgb[1] - 0.25 : rgb[1];

    return rgb;
}

pair<TString,TString> getTxtInfo(TString txtName) {
    TString txtFolderName = "";
    TString txtKeyName = txtName;
    TObjArray * tx = txtName.Tokenize("/");
    if (tx->GetEntries() > 1) {
        txtFolderName = ((TObjString*)tx->At(0))->String();
        txtKeyName = ((TObjString*)tx->At(1))->String();
    }
    return make_pair(txtFolderName, txtKeyName);
}

vector<int> getColors(int N) {
    vector<int> colors;
    for (int j = 0; j < N + 1; j++){
        double hue = 240. * (double)j / (double)N;
        double *rgb = HSVtoRGB(hue);
        int ci = TColor::GetFreeColorIndex();
        TColor *c = new TColor(ci, rgb[0], rgb[1], rgb[2]);
        colors.push_back(ci);
    }
    return colors;
}

vector<int> getMarkerStyles(int N) {
    vector<int> markerStyles;
    for (int j = 0 ; j < N + 1; j++) {
        markerStyles.push_back(20 + (j % 30));
    }
    return markerStyles;
}

TFile * getFile(TString path) {
    return new TFile(path);
}

pair<float,float> GetMinMax(vector<float> v) {
    float v_min = *min_element(v.begin(),v.end());
    float v_max = *max_element(v.begin(),v.end());
    return make_pair(v_min,v_max);
}

pair<double,double> GetMinMax(vector<double> v) {
    double v_min = *min_element(v.begin(),v.end());
    double v_max = *max_element(v.begin(),v.end());
    return make_pair(v_min,v_max);
}

pair<int,int> GetMinMax(vector<int> v) {
    int v_min = *min_element(v.begin(),v.end());
    int v_max = *max_element(v.begin(),v.end());
    return make_pair(v_min,v_max);
}

double Lerp(double x0, double y0, double x1, double y1, double x) {
    return (y0*(x1-x) + y1*(x-x0))/(x1-x0);
}

TH1D* GetHistFromVector(vector<double> v, int n_bins = 100) {
    auto minMax = GetMinMax(v);
    double scale = minMax.second - minMax.first;
    TH1D * hist = new TH1D("","",n_bins,(minMax.first-scale*0.1),(minMax.second+scale*0.1));
    for (double val : v) {hist->Fill(val);}
    return hist;
}

TH1D* GetHistFromVector(vector<float> v, int n_bins = 100) {
    auto minMax = GetMinMax(v);
    float scale = minMax.second - minMax.first;
    TH1D * hist = new TH1D("","",n_bins,(minMax.first-scale*0.1),(minMax.second+scale*0.1));
    for (float val : v) hist->Fill(val);
    return hist;
}

template <typename T>
TH1D* GetHistFromVector(vector<T> v, T min, T max, int n_bins = 100) {
    TH1D * hist = new TH1D("","",n_bins,min,max);
    for (T val : v) hist->Fill(val);
    return hist;
}

vector<double> GetXVector(TGraph * g) {
    vector<double> x(g->GetX(),g->GetX()+g->GetN());
    return x;
}

vector<double> GetYVector(TGraph * g) {
    vector<double> y(g->GetY(),g->GetY()+g->GetN());
    return y;
}

vector<double> GetErrorYVector(TGraph * g) {
    coutError("No errors defined for a TGraph");
    return {};
}

vector<double> GetErrorYVector(TGraphErrors * g) {
    vector<double> ey(g->GetEY(),g->GetEY()+g->GetN());
    return ey;
}

TH1D* GetHistFromGraph(TGraph * g, double start = -1, double end = -1) {
    vector<double> x = GetXVector(g);
    vector<double> y = GetYVector(g);

    auto minMax = GetMinMax(x);
    if (start == -1) start = minMax.first;
    if (end == -1) end = minMax.second;

    TH1D * hist = new TH1D("","",g->GetN(),start,end);

    for (int i = 0 ; i < x.size() ; i++) {
        int bin = hist->GetBin(x.at(i)) +1;  // ADDED +1 so it works with plotLabelGraph
        hist->SetBinContent(bin,y.at(i));
    }

    return hist;
}

TH1D* GetHistFromGraphProjection(TGraph * g, bool noEmptyBins, int min = -1) {
    vector<double> y = GetYVector(g);
    double ymin = g->GetYaxis()->GetXmin();
    double ymax = g->GetYaxis()->GetXmax();
    int n_bins = sqrt(g->GetN());
    if ((ymax - ymin) / n_bins  < 1) n_bins = 100;

    bool hasEmptyBins = true;
    TH1D * hist;
    while (hasEmptyBins && noEmptyBins) {
        hist = new TH1D("","",n_bins,ymin,ymax);
        hist->Sumw2(true);

        for (int i = 0 ; i < y.size() ; i++) {
            hist->Fill(y.at(i));
        }
        if (min != -1 && n_bins == min) break;

        hasEmptyBins = false;

        for (int i_bin = 1 ; i_bin <= hist->GetNbinsX() ; i_bin++) {
            if (hist->GetBinContent(i_bin) == 0) {
                hasEmptyBins = true;
                n_bins -= 1;
                delete hist;
                break;
            }
        }


    }

    return hist;
}

TH1D* GetHistFromGraphProjection(TGraph * g, int n_bins = -1) {
    vector<double> y = GetYVector(g);
    double ymin = g->GetYaxis()->GetXmin();
    double ymax = g->GetYaxis()->GetXmax();
    if (n_bins == -1) n_bins = sqrt(g->GetN());
    if ((ymax - ymin) / n_bins  < 1) n_bins = 20;

    TH1D * hist = new TH1D("","",n_bins,ymin,ymax);
    hist->Sumw2(true);

    for (int i = 0 ; i < y.size() ; i++) {
        hist->Fill(y.at(i));
    }

    return hist;
}

TH1D* GetHistFromGraphProjection(TGraph * g, vector<double> xRange, int n_bins = -1) {
    vector<double> x = GetXVector(g);
    vector<double> y = GetYVector(g);

    vector<double> x_filtered;
    vector<double> y_filtered;

    for (int i_point = 0 ; i_point < y.size() ; i_point++) {
        if (x.at(i_point) < xRange.at(0) || x.at(i_point) > xRange.at(1)) continue;
        x_filtered.push_back(x.at(i_point));
        y_filtered.push_back(y.at(i_point));
    }
    if (x_filtered.size() == 0) {
        coutError("Empty graph in GetHistFromGraphProjection");
        return nullptr;
    }

    TGraph * graph_inRange = new TGraph(x_filtered.size(), &x_filtered.at(0), &y_filtered.at(0));

    return GetHistFromGraphProjection(graph_inRange, n_bins);
}

vector<float> getVectorFromMiddles(vector<float> v) {
    vector<float> u;
    for (int i = 0 ; i < v.size() -1 ; i++) {
        u.push_back(v.at(i) + (v.at(i+1) - v.at(i))/2.);
    }
    u.push_back(v.back());
    return u;
}

int DeltaNFunction_inMacros(vector<float> &v, int index) {
    float val = v.at(index);
    int Np, Nm;
    for (float c_val : v) {
        if (c_val <= val) Nm++;
        else Np++;
    }
    return fabs(Np - Nm);
}

vector<float> pruningFunction_inMacros(vector<float> &v) {
    vector<float> f_mapped;
    auto minMax_pair = GetMinMax(v);
    float scale = minMax_pair.second - minMax_pair.first;
    for (int i = 0 ; i < v.size() - 1 ; i++){
        float DeltaL = (v.at(i + 1) - v.at(i))/scale;
        int DeltaN = DeltaNFunction_inMacros(v,i);
        float f_val = DeltaN * pow(DeltaL,3);
        f_mapped.push_back(f_val);
    }
    return f_mapped;
}

TF1 * GetLine(double x1, double y1, double x2, double y2, double xStart, double xEnd) {
    TF1 * line = new TF1("line","[0]*(x-[1])/([2]-[1]) + [3]*(x-[2])/([1]-[2])",xStart,xEnd);
    line->SetParameter(0,y2);
    line->SetParameter(1,x1);
    line->SetParameter(2,x2);
    line->SetParameter(3,y1);
    return line;
}

void makeDirectory(TString dir) {
    TString command = "mkdir -p " + dir;
    system(command);
}

void removeFile(TString path) {
    TString command = "rm " + path;
    system(command);
}

string getCMDoutput(const std::string& mStr) {
    string result, file;
    FILE* pipe{popen(mStr.c_str(), "r")};
    char buffer[256];

    while(fgets(buffer, sizeof(buffer), pipe) != NULL)
    {
        file = buffer;
        result += file.substr(0, file.size() - 1);
    }

    pclose(pipe);
    return result;
}

string to_string_with_precision(double num, const int n = 6)
{
    ostringstream out;
    out.precision(n);
    out << fixed << num;
    return out.str();
}

string zero_padding(int num, const int n) {
    ostringstream out;
    out.fill('0');
    out.width(n);
    out << num;
    return out.str();
}

int getScientificFigures(double num) {
    double abs_num = fabs(num);
    int n = 0;
    while (abs_num < 1) {
        abs_num *= 10;
        n += 1;
    }
    return n;
}

TString getInScientific(double num) {
    int n = getScientificFigures(num);
    if (n == 0) return to_string_with_precision(num,2);
    return to_string_with_precision(num * pow(10,n),2) + " #upoint 10^{-" + to_string(n) + "}";
}

int getSigFigures(double num) {
    if (num > 1) {
        for (int i = 1 ; i < 10 ; i++) {
            if (num*pow(10,-1*i) < 1) return -1*i;
        }
    } else {
        for (int i = 1 ; i < 10 ; i++) {
            if (num >= pow(10,-1*i)) return i;
        }
    }
    return 2;
}

TString ResultWithFigures(double result, double unc) {
    // cout << "-------- Result with figures: " << result << " " << unc << " --------" << endl;
    int fig = getSigFigures(unc);
    if (fig > 0) {
        return to_string_with_precision(result, fig);
    } else {
        if (abs(fig) == 1) return to_string((int)result);
        else return to_string_with_precision(result*pow(10,fig),1) + " #times 10^{" + to_string(-1*fig) + "}";
    }
}

TString ResultAndUncertaintyWithFigures(double result, double unc) {
    int fig = getSigFigures(unc);
    if (fig > 0) {
        return "(" + to_string_with_precision(result,fig) + " #pm " + to_string_with_precision(unc,fig) + ")";
    } else {
        if (abs(fig) == 1) return "(" + to_string((int)result) + " #pm " + to_string((int)unc) + ")";
        return "(" + to_string_with_precision(result*pow(10,fig),1) + " #pm " + to_string_with_precision(unc*pow(10,fig),1) + ") #times 10^{" + to_string(-1*fig) + "}";
    }
}

TString GetLinearFitString(TF1 * fit_linear, TString dep_var = "x") {

    TString sign = (fit_linear->GetParameter(1) > 0) ? "+" : "-";
    TString indep_par = ResultWithFigures(fit_linear->GetParameter(0), fit_linear->GetParError(0));
    TString dep_par = ResultWithFigures(fabs(fit_linear->GetParameter(1)), fit_linear->GetParError(1));
    return indep_par + " " + sign + " " + dep_par + " #upoint " + dep_var;

}

pair<string,string> split(string str, string del) {
    int idx = str.find(del);
    return make_pair(str.substr(0,idx), str.substr(idx+1,str.length()));
}

double getValueFromTree(TTree * tree, TString varName, int entry_val = 0) {
    int entry = (entry_val == -1) ? tree->GetEntries() - 1 : entry_val;
    tree->GetEntry(entry);
    return tree->GetLeaf(varName)->GetValue();
}

double getValueFromTree(TTree * tree, TString varName, TString selection) {
    tree->Draw(varName,selection,"goff");
    if (tree->GetSelectedRows() != 1) {
        coutError("Not a valid selection for getValueFromTree");
        return -1;
    }
    return tree->GetV1()[0];
}

vector<double> getVectorFromTree(TTree * tree, TString varName, TString cut = "") {
    tree->Draw(varName,cut,"goff");
    return *(new vector<double>(   tree->GetV1()    ,    tree->GetV1() +  tree->GetSelectedRows()  ));
}

TH1D* getHistFromDrawTree(TTree * tree, TString varName, TString cut, TString label) {
    TFile * tmp_file = new TFile("TMP.root","RECREATE");
    tmp_file->cd();
    TString command = varName + " >> hist" + label;
    tree->Draw(command,cut,"goff");
    TH1D * hist = (TH1D*)tmp_file->Get("hist" + label);
    // tmp_file->Close();
    return hist;
}

TH1D* getHistFromTree(TTree * tree, TString varName, TString cut = "", int n_bins = 100) {
    return GetHistFromVector(getVectorFromTree(tree, varName, cut), n_bins);
}

vector<float> getFloatVectorFromTree(TTree * tree, TString varName, TString cut = "") {
    tree->Draw(varName,cut,"goff");
    return *(new vector<float>(   tree->GetV1()    ,    tree->GetV1() +  tree->GetSelectedRows()   ));
}


template <typename T>
T getMedian(vector<T> v) {
    size_t size = v.size();

    if (size == 0) return -999;

    sort(v.begin(),v.end());
    if (size % 2 == 0) return (v[size/2 - 1] + v[size/2]) / 2.;
    else return v[size / 2];
}

pair<double, double> GetMeanErr(vector<double> v) {
    if (v.size() == 0) return make_pair(0,0);

    double sum = accumulate(v.begin(), v.end(), 0.0);
    double mean = sum / v.size();

    double sq_sum = 0;
    for (auto val : v) {
        sq_sum += (val - mean)*(val - mean);
    }
    double err = sqrt(sq_sum/(v.size() - 1))/sqrt(v.size());

    mean = !isnan(mean) ? mean : -999;
    err = !isnan(err) ? err : -999;

    return make_pair(mean,err);
}

pair<float, float> GetMeanErr(vector<float> v) {
    if (v.size() == 0) return make_pair(0,0);

    float sum = accumulate(v.begin(), v.end(), 0.0);
    float mean = sum / v.size();

    float sq_sum = 0;
    for (auto val : v) {
        sq_sum += (val - mean)*(val - mean);
    }
    float err = sqrt(sq_sum/(v.size() - 1))/sqrt(v.size());

    mean = !isnan(mean) ? mean : -999;
    err = !isnan(err) ? err : -999;

    return make_pair(mean,err);
}

template <typename T>
T GetErrorFromAMean(vector<T> v) {
    if (v.size() == 0) return 0;

    T sq_sum = inner_product(v.begin(), v.end(), v.begin(), 0.0);
    T error = sqrt(sq_sum) / v.size();

    error = !isnan(error) ? error : -999;

    return error;
}

template <typename T>
T GetRMS(vector<T> v) {
    if (v.size() == 0) return 0;

    T sum = accumulate(v.begin(), v.end(), 0.0);
    T mean = sum / v.size();

    T sq_sum = 0;
    for (auto val : v) {
        sq_sum += (val - mean)*(val - mean);
    }
    T rms = sqrt(sq_sum/v.size());

    return rms;
}

template <typename T>
pair<vector<T>,vector<T>> averageEveryN(vector<T> v, int step) {

    if (step <= 0) step = 1;

    vector<T> averaged;
    vector<T> averaged_err;
    vector<T> grouped_values;

    for (int i = 0 ; i < v.size() ; i++) {
        T val = v.at(i);
        grouped_values.push_back(val);
        if (grouped_values.size() == step || i == v.size() - 1) {
            auto mean_err = GetMeanErr(grouped_values);
            averaged.push_back(mean_err.first);
            averaged_err.push_back(mean_err.second);
            grouped_values.clear();
        }
    }

    return make_pair(averaged,averaged_err);
}

template <typename T>
vector<T> ErrorEveryN(vector<T> v, int step) {

    if (step <= 0) step = 1;

    vector<T> averaged_err;
    vector<T> grouped_values;

    for (int i = 0 ; i < v.size() ; i++) {
        T val = v.at(i);
        grouped_values.push_back(val);
        if (grouped_values.size() == step || i == v.size() - 1) {
            T err = GetErrorFromAMean(grouped_values);
            averaged_err.push_back(err);
            grouped_values.clear();
        }
    }

    return averaged_err;
}

template <typename T>
vector<T> keepEveryN(vector<T> v, int step) {

    if (step <= 0) step = 1;

    vector<T> keeping;

    for (int i = 0 ; i < v.size() ; i+= step) {
        keeping.push_back(v.at(i));
    }

    return keeping;

}

TGraph * GetGraphAverageEveryN(TGraph * g, int N) {
    vector<double> x_vector = GetXVector(g);
    vector<double> y_vector = GetYVector(g);
    x_vector = averageEveryN(x_vector,N).first;
    y_vector = averageEveryN(y_vector,N).first;
    TGraph * result = new TGraph(x_vector.size(), &x_vector.at(0), &y_vector.at(0));
    return result;
}

TGraphErrors * GetGraphAverageEveryN(TGraphErrors * g, int N) {
    vector<double> x_vector = GetXVector(g);
    vector<double> y_vector = GetYVector(g);
    vector<double> yErr_vector = GetErrorYVector(g);
    x_vector = averageEveryN(x_vector,N).first;
    y_vector = averageEveryN(y_vector,N).first;
    yErr_vector = ErrorEveryN(yErr_vector,N);
    TGraphErrors * result = new TGraphErrors(x_vector.size(), &x_vector.at(0), &y_vector.at(0), 0, &yErr_vector.at(0));
    return result;
}

TGraph * GetGraphKeepEveryN(TGraph * g, int N) {
    vector<double> x_vector = GetXVector(g);
    vector<double> y_vector = GetYVector(g);
    x_vector = keepEveryN(x_vector,N);
    y_vector = keepEveryN(y_vector,N);
    TGraph * result = new TGraph(x_vector.size(), &x_vector.at(0), &y_vector.at(0));
    return result;
}

TGraph* GetGraphAveraged(vector<TGraph*> graphs) {
    TGraph * averagedGraph = new TGraph();
    map<double,vector<double>> m_forAvg;
    for (TGraph * graph : graphs) {
        for (int i_point = 0 ; i_point < graph->GetN() ; i_point++) {
            Double_t x,y;
            graph->GetPoint(i_point, x, y);
            m_forAvg[x].push_back(y);
        }
    }
    for (auto kv : m_forAvg) {
        double x = kv.first;
        double y_avg = GetMeanErr(m_forAvg[x]).first;
        averagedGraph->SetPoint(averagedGraph->GetN(), x, y_avg);
    }
    return averagedGraph;
}

TGraphErrors* GetGraphAveraged(vector<TGraphErrors*> graphs) {
    TGraphErrors * averagedGraph = new TGraphErrors();
    map<double,vector<double>> m_forAvg;
    map<double,vector<double>> m_forAvg_err;
    for (TGraphErrors * graph : graphs) {
        vector<double> ey_vector = GetErrorYVector(graph);
        for (int i_point = 0 ; i_point < graph->GetN() ; i_point++) {
            Double_t x,y;
            graph->GetPoint(i_point, x, y);
            m_forAvg[x].push_back(y);
            m_forAvg[x].push_back(ey_vector.at(i_point));
        }
    }
    for (auto kv : m_forAvg) {
        double x = kv.first;
        double y_avg = GetMeanErr(m_forAvg[x]).first;
        double y_avg_err = GetErrorFromAMean(m_forAvg_err[x]);
        averagedGraph->SetPoint(averagedGraph->GetN(), x, y_avg);
        averagedGraph->SetPointError(averagedGraph->GetN()-1, 0, y_avg_err);
    }
    return averagedGraph;
}


double GetGraphAverageValue(TGraph* graph) {
    vector<double> y_vector = GetYVector(graph);
    return GetMeanErr(y_vector).first;
}

TGraph * GetGraphRatio(TGraph * graph1, TGraph * graph2) {
    vector<double> x_vector1 = GetXVector(graph1);
    vector<double> y_vector1 = GetYVector(graph1);
    vector<double> x_vector2 = GetXVector(graph2);
    vector<double> y_vector2 = GetYVector(graph2);

    vector<double> x_ratio, y_ratio;
    for (int i_index = 0 ; i_index < x_vector1.size() ; i_index++) {
        int i_index2 = indexOf(x_vector1.at(i_index), x_vector2);
        if (i_index2 == -1) continue;
        x_ratio.push_back(x_vector1.at(i_index));
        y_ratio.push_back(y_vector1.at(i_index) / y_vector2.at(i_index2));
    }

    TGraph * graph_ratio = new TGraph(x_ratio.size(), &x_ratio.at(0), &y_ratio.at(0));
    return graph_ratio;
}


pair<vector<TGraph*>,vector<TString>> GetCellGraphs(map<TString,TGraph*> map_graphs, vector<TString> cells_to_print, vector<int> parts_to_compute, bool mean_in_legend = true) {
    vector<TString> legends;
    vector<TGraph*> graphs;

    vector<TString> cell_names;
    for (TString cell_pair_name : cells_to_print) {
        TObjArray * tx = cell_pair_name.Tokenize("_");
        if (tx->GetEntries() > 1) {
            TString cell_name = ((TObjString*)tx->At(0))->String();
            if (!contains(cell_name, cell_names)) cell_names.push_back(cell_name);
        }
    }

    for (TString cell_name : cell_names) {
        int pmt = Utils::GetPMTFromString(cell_name);
        for (int part : parts_to_compute) {
            TString part_str = Utils::GetPartString(part);
            if (Utils::HasLeftRight(pmt,part)) {
                vector<TGraph*> graphs_to_average;
                if (map_graphs.count(cell_name + "L_" + part_str) == 0) {coutError("Graph with key: " + cell_name + "L_" + part_str + " not found"); continue;}
                graphs_to_average.push_back(map_graphs[cell_name + "L_" + part_str]);
                if (map_graphs.count(cell_name + "R_" + part_str) == 0) {coutError("Graph with key: " + cell_name + "R_" + part_str + " not found"); continue;}
                graphs_to_average.push_back(map_graphs[cell_name + "R_" + part_str]);
                TGraph* graph_avg = GetGraphAveraged(graphs_to_average);
                double mean_val = GetGraphAverageValue(graph_avg);

                graphs.push_back(graph_avg);
                if (mean_in_legend) legends.push_back(TString::Format("%s %s (%s)", cell_name.Data(), part_str.Data(), to_string_with_precision(mean_val,4).c_str()));
                else legends.push_back(cell_name + " " + part_str);
            }
        }
    }

    return make_pair(graphs, legends);
}

pair<vector<TGraph*>, vector<TString>> GetCellGraphs(vector<TGraph*> graphs, vector<TString> labels, vector<TString> cells_to_print, vector<int> parts_to_compute, bool mean_in_legend = true) {
    map<TString,TGraph*> map_graphs;
    for (int i_graph = 0 ; i_graph < graphs.size() ; i_graph++) {
        TString label = labels.at(i_graph);
        label.ReplaceAll(" ","_");
        map_graphs[label] = graphs.at(i_graph);
    }
    return GetCellGraphs(map_graphs, cells_to_print, parts_to_compute, mean_in_legend);
}

pair<vector<TGraph*>,vector<TString>> GetCellGraphsPartsAveraged(map<TString,TGraph*> map_graphs, vector<TString> cells_to_print, vector<int> parts_to_compute, bool mean_in_legend = true) {
    vector<TString> legends;
    vector<TGraph*> graphs;

    vector<TString> cell_names;
    for (TString cell_pair_name : cells_to_print) {
        TObjArray * tx = cell_pair_name.Tokenize("_");
        if (tx->GetEntries() > 1) {
            TString cell_name = ((TObjString*)tx->At(0))->String();
            if (!contains(cell_name, cell_names)) cell_names.push_back(cell_name);
        }
    }

    for (TString cell_name : cell_names) {
        int pmt = Utils::GetPMTFromString(cell_name);
        for (int part : parts_to_compute) {
            TString part_str = Utils::GetPartString(part);
            if (part_str(2) == 'C') continue;

            vector<TGraph*> graphs_to_average;
            if (Utils::HasLeftRight(pmt,part)) {
                if (map_graphs.count(cell_name + "L_" + part_str) > 0) graphs_to_average.push_back(map_graphs[cell_name + "L_" + part_str]);
                if (map_graphs.count(cell_name + "R_" + part_str) > 0) graphs_to_average.push_back(map_graphs[cell_name + "R_" + part_str]);
                if (map_graphs.count(cell_name + "L_" + part_str(0,2) + "C") > 0) graphs_to_average.push_back(map_graphs[cell_name + "L_" + part_str(0,2) + "C"]);
                if (map_graphs.count(cell_name + "R_" + part_str(0,2) + "C") > 0) graphs_to_average.push_back(map_graphs[cell_name + "R_" + part_str(0,2) + "C"]);
            } else {
                if (map_graphs.count(cell_name + "_" + part_str) > 0) graphs_to_average.push_back(map_graphs[cell_name + "_" + part_str]);
                if (map_graphs.count(cell_name + "_" + part_str(0,2) + "C") > 0) graphs_to_average.push_back(map_graphs[cell_name + "_" + part_str(0,2) + "C"]);
            }
            if (graphs_to_average.size() == 0) continue;
            TGraph* graph_avg = GetGraphAveraged(graphs_to_average);
            double mean_val = GetGraphAverageValue(graph_avg);
            graphs.push_back(graph_avg);
            if (mean_in_legend) legends.push_back(TString::Format("%s (%s)", cell_name.Data(), to_string_with_precision(mean_val,4).c_str()));
            else legends.push_back(cell_name);
        }
    }

    return make_pair(graphs, legends);
}

TH1D* GetHistFromGraphErrors(TGraphErrors* graph, double min, double max, const int n_bins = 100, bool printErrors = false) {
    TH1D * hist = new TH1D("","",n_bins,min,max);
    vector<double> x_vector = GetXVector(graph);
    vector<double> y_vector = GetYVector(graph);
    vector<double> ey_vector = GetErrorYVector(graph);
    int N[n_bins];
    map<int,vector<double>> errors;
    map<int,vector<double>> yValues;
    for (int i_bin = 1 ; i_bin <= n_bins ; i_bin++) {
        N[i_bin-1] = 0;
    }

    for (int i_point = 0 ; i_point < x_vector.size() ; i_point++) {
        int bin = hist->GetXaxis()->FindBin(x_vector.at(i_point));
        hist->SetBinContent(bin, hist->GetBinContent(bin) + y_vector.at(i_point));
        N[bin-1] += 1;
        if (ey_vector.size() > 0) {
            errors[bin-1].push_back(ey_vector.at(i_point));
            yValues[bin-1].push_back(y_vector.at(i_point));
            if (printErrors) cout << ey_vector.at(i_point) << endl;
        }
    }
    for (int i_bin = 1 ; i_bin <= n_bins ; i_bin++) {
        if (N[i_bin-1] == 0) continue;
        // hist->SetBinContent(i_bin, hist->GetBinContent(i_bin) / N[i_bin-1]);
        hist->SetBinContent(i_bin, GetMeanErr(yValues[i_bin-1]).first);
        // if (ey_vector.size() > 0) hist->SetBinError(i_bin, GetErrorFromAMean(errors[i_bin-1]));
        if (ey_vector.size() > 0) hist->SetBinError(i_bin, GetMeanErr(yValues[i_bin-1]).second);
        else hist->SetBinError(i_bin,1e-4);
    }
    return hist;
}

TString GetChebyshevFormula(int N) {
    TString formula = "[0]";
    if (N >= 1) formula += " + [1]*(x)";
    if (N >= 2) formula += " + [2]*(2*x^2-1)";
    if (N >= 3) formula += " + [3]*(4*x^3-3*x)";
    if (N >= 4) formula += " + [4]*(8*x^4-8*x^2+1)";
    if (N >= 5) formula += " + [5]*(16*x^5-20*x^3+5*x)";
    if (N >= 6) formula += " + [6]*(32*x^6-48*x^4+18*x^2-1)";
    if (N >= 7) formula += " + [7]*(64*x^7-112*x^5+56*x^3-7*x)";
    if (N >= 8) formula += " + [8]*(128*x^8-256*x^6+160*x^4-32*x^2+1)";
    if (N >= 9) formula += " + [9]*(256*x^9-576*x^7+432*x^5-120*x^3+9*x)";
    if (N >= 10) formula += " + [10]*(512*x^10-1280*x^8+1120*x^6-400*x^4+50*x^2-1)";
    return formula;
}


TF1 * RescaleFunction(TF1 * f, double newStart, double newEnd, double oldStart, double oldEnd) {
    TString formula = f->GetExpFormula();
    formula.ReplaceAll("[p","[");
    // double oldStart = f->GetXmin();
    // double oldEnd = f->GetXmax();
    TString newX = "(x - " + to_string(newStart) + ")*" + to_string(oldEnd-oldStart) + "/" + to_string(newEnd-newStart) + " + " + to_string(oldStart);
    formula.ReplaceAll("x",newX);
    TF1 * newF = new TF1("",formula,newStart,newEnd);
    for (int i_param = 0 ; i_param < f->GetNumberFreeParameters() ; i_param++) {
        newF->SetParameter(i_param, f->GetParameter(i_param));
        newF->SetParError(i_param, f->GetParError(i_param));
    }
    newF->SetLineColor(f->GetLineColor());
    return newF;
}

template <typename T>
void sortTwoLists(vector<double> & list_base, vector<T> & other_list){

    vector<double> sorted_1;
    vector<T> sorted_2;

    for (int i = 0 ; i < list_base.size() ; i++) {
        sorted_1.push_back(list_base.at(i));
    }

    sort(sorted_1.begin(), sorted_1.end());

    // sort(sorted_1.begin(), sorted_1.end(), [&](unsigned i, unsigned j){return i < j;});

    for (int i = 0 ; i < sorted_1.size() ; i++) {
        for (int j = 0 ; j < sorted_1.size() ; j++) {
            if (sorted_1.at(i) == list_base.at(j)) {
                sorted_2.push_back(other_list.at(j));
                break;
            }
        }
    }

    list_base = sorted_1;
    other_list = sorted_2;
}

template<typename T>
void remove(vector<T> &v, vector<int> toRemove) {
    vector<T> prunned;
    for (int i = 0 ; i < v.size() ; i++) {
        if (find(toRemove.begin(),toRemove.end(),i) == toRemove.end()) {
            prunned.push_back(v.at(i));
        } 
    }
    v = *(new vector<T>(prunned));
}


TF1 * getGaussian(vector<float> v) {
    float v_min = *min_element(v.begin(),v.end());
    float v_max = *max_element(v.begin(),v.end());
    if (v_max == v_min) return 0;

    float extra_margin = (v_max - v_min)*0.15;
    int NBins = fabs(v_max) + fabs(v_min) + 2*extra_margin;
    if (NBins <= 0) return 0;

    TH1D * hist = new TH1D("","", NBins, v_min - extra_margin, v_max + extra_margin);
    for (auto val : v) hist->Fill(val);
    TF1 * f_gaus = new TF1("fit","gaus", v_min - extra_margin, v_max + extra_margin);

    hist->Fit(f_gaus,"MNSQR");
    delete hist;

    return f_gaus;
}

TF1 * getGaussian(vector<double> v) {
    double v_min = *min_element(v.begin(),v.end());
    double v_max = *max_element(v.begin(),v.end());
    if (v_max == v_min) return 0;

    float extra_margin = (v_max - v_min)*0.15;
    int NBins = fabs(v_max) + fabs(v_min) + 2*extra_margin;
    if (NBins <= 0) return 0;

    TH1D * hist = new TH1D("","", NBins, v_min - extra_margin, v_max + extra_margin);
    for (auto val : v) hist->Fill(val);
    TF1 * f_gaus = new TF1("fit","gaus", v_min - extra_margin, v_max + extra_margin);

    auto params = GetMeanErr(v);
    cout << params.first << "   " << params.second << endl;
    f_gaus->SetParameter(0,1);
    f_gaus->SetParameter(1,params.first);
    f_gaus->SetParameter(2,params.second);

    hist->Fit(f_gaus,"MNSQR");
    delete hist;

    return f_gaus;
}

pair<float,float> GetMeanErr_Gaus(vector<float> v)
{
    TF1 * f_gaus = getGaussian(v);
    if (f_gaus == 0) return make_pair(-999,-999);
    return make_pair(f_gaus->GetParameter(1),f_gaus->GetParameter(2));
}

vector<int> pruneGaussian(vector<float> &v, double prune_factor = 1, bool docout = false) {
    vector<int> toRemove{};
    auto params = GetMeanErr_Gaus(v);
    if (params.first == -999 && params.second == -999) {return toRemove;}
    float mean = params.first;
    float sigma = params.second;
    for (int i = 0 ; i < v.size() ; i++) {
        if (  fabs(v.at(i) - mean) > sigma*prune_factor  ) toRemove.push_back(i);
    }

    remove(v, toRemove);
    return toRemove;
}

template<typename T>
vector<int> prune(vector<T> &v, double prune_factor = 1, bool docout = false) {
    vector<int> toRemove{};
    if (prune_factor == -1) return toRemove;
    vector<float> distances{};
    vector<int> screw_avg{};
    double sum = accumulate(v.begin(),v.end(),0.);
    double sq_sum = inner_product(v.begin(),v.end(),v.begin(),0.);
    int k = 0;
    do {
        toRemove.clear();
        if (k > 0) {
            vector<float> s_distances = *(new vector<float>(distances));
            sort(s_distances.begin(),s_distances.end());
            float value_to_add = s_distances.at(s_distances.size() - k);
            int idx_value_to_add = find(distances.begin(),distances.end(),value_to_add) - distances.begin();
            screw_avg.push_back(idx_value_to_add);
        }
        for (int i = 0 ; i < v.size() ; i++) {
            if (find(screw_avg.begin(),screw_avg.end(),i) != screw_avg.end()) {
                toRemove.push_back(i);
                continue;
            }
            double subs_sum = sum -v.at(i);
            double subs_sq_sum = sq_sum - v.at(i)*v.at(i);
            for (int j : screw_avg) {subs_sum -= v.at(j); subs_sq_sum -= v.at(j)*v.at(j);}
            double mean = (subs_sum)/(v.size() - 1 - screw_avg.size());
            double stdev = sqrt((subs_sq_sum)/(v.size() - 1 - screw_avg.size()) - mean * mean);
            if (k == 0) distances.push_back(fabs(mean - v.at(i)));
            if (fabs(mean - v.at(i)) > stdev*prune_factor) {toRemove.push_back(i);}
        }
        k++;
    } while (toRemove.size() == v.size());
    remove(v, toRemove);
    return toRemove;
}

vector<vector<double>> SigmaPruning(int n, vector<double> x_vector, vector<double> y_vector, vector<double> yErr_vector) {
    if (n == -1) return {x_vector, y_vector, yErr_vector};
    vector<double> x_pruned, y_pruned, yErr_pruned;
    auto params = GetMeanErr(y_vector);
    // cout << "----------------" << endl;
    for (int i_point = 0 ; i_point < x_vector.size() ; i_point++) {
        // cout << y_vector.at(i_point) << " " << params.first << " " << params.second << " " << abs(y_vector.at(i_point) - params.first)/params.second << endl;
        if (abs(y_vector.at(i_point) - params.first)/params.second > n) continue;
        x_pruned.push_back(x_vector.at(i_point));
        y_pruned.push_back(y_vector.at(i_point));
        if (yErr_vector.size() > 0) yErr_pruned.push_back(yErr_vector.at(i_point));
    }
    return {x_pruned,y_pruned,yErr_pruned};
}

vector<double> standarize(vector<double> v) {
    vector<double> v_std;
    auto params = GetMeanErr(v);
    for (auto val : v) v_std.push_back((val - params.first)/params.second);
    return v_std;
}

vector<double> normalize(vector<double> v) {
    vector<double> v_norm;
    double v_max = *max_element(v.begin(),v.end());
    double v_min = *min_element(v.begin(),v.end());
    double max_value = (fabs(v_min) > v_max) ? v_min : v_max; 

    for (auto val : v) v_norm.push_back(   val / max_value   );
    return v_norm;
}

vector<float> standarize(vector<float> v) {
    vector<float> v_std;
    auto params = GetMeanErr(v);
    for (auto val : v) v_std.push_back((val - params.first)/params.second);
    return v_std;
}

vector<float> normalize(vector<float> v) {
    vector<float> v_norm;
    float v_max = *max_element(v.begin(),v.end());
    float v_min = *min_element(v.begin(),v.end());
    float max_value = (fabs(v_min) > v_max) ? v_min : v_max; 

    for (auto val : v) v_norm.push_back(   val / max_value   );
    return v_norm;
}

void readFile(TString path, int lineStart, vector<int> columnIndexes, vector<vector<string>*> vectorsToFill) {
    ifstream file1(path);
    if (!file1.is_open()) return;
    string str;
    int line = 0;
    auto params = GetMinMax(columnIndexes);
    while (getline(file1, str))
    {
        line++;
        if (line < lineStart) continue;

        istringstream iss(str);
        vector<string> subs;

        do {
            string s;
            iss >> s;
            if (s.size() > 0) subs.push_back(s);
        } while (iss);

        if (subs.size() <= params.second) continue;

        for (int i = 0 ; i < vectorsToFill.size() ; i++) {
            vectorsToFill.at(i)->push_back(subs[columnIndexes.at(i)]);
        }
    }
}

map<TString,double> getMapFromFile(TString path) {
    map<TString,double> outMap;
    vector<string> keys, values;
    readFile(path,1,{0,1},{&keys,&values});
    for (int i_index = 0 ; i_index < keys.size() ; i_index++) {
        outMap[keys.at(i_index)] = stod(values.at(i_index));
    }
    return outMap;
}


void readFile(TString path, int lineStart, vector<int> columnIndexes, vector<vector<float>*> vectorsToFill) {
    ifstream file1(path);
    if (!file1.is_open()) return;
    string str;
    int line = 0;
    auto params = GetMinMax(columnIndexes);
    // cout << "Getting columns ";
    // for (int col : columnIndexes) cout << col << " ";
    // cout << "starting from line " << lineStart << endl;
    while (getline(file1, str))
    {
        line++;
        if (line < lineStart) continue;

        istringstream iss(str);
        vector<string> subs;

        do {
            string s;
            iss >> s;
            if (s.size() > 0) subs.push_back(s);
        } while (iss);

        if (subs.size() <= params.second) continue;

        for (int i = 0 ; i < vectorsToFill.size() ; i++) {
            // cout << columnIndexes.at(i) << " " << stod(subs[columnIndexes.at(i)]) << "; ";
            vectorsToFill.at(i)->push_back(stod(subs[columnIndexes.at(i)]));
        }
        // cout << endl;
    }
}

void readFile(TString path, int lineStart, vector<int> columnIndexes, vector<vector<double>*> vectorsToFill) {
    ifstream file1(path);
    if (!file1.is_open()) return;
    string str;
    int line = 0;
    auto params = GetMinMax(columnIndexes);
    while (getline(file1, str))
    {
        line++;
        if (line < lineStart) continue;

        istringstream iss(str);
        vector<string> subs;

        do {
            string s;
            iss >> s;
            if (s.size() > 0) subs.push_back(s);
        } while (iss);

        if (subs.size() <= params.second) continue;

        for (int i = 0 ; i < vectorsToFill.size() ; i++) {
            vectorsToFill.at(i)->push_back(stod(subs[columnIndexes.at(i)]));
        }
    }
}

void SaveChebyshev(TFitResultPtr theFit, int N, double start, double end, TString outFile) {
    ofstream outfile; outfile.open(outFile);
    for (int i = 0 ; i <= N ; i++) outfile << theFit->GetParams()[i] << endl;
    outfile << start << endl;
    outfile << end << endl;
    outfile.close();
    cout << "Chebyshev fit stored in " << outFile << endl;
}

TF1 * ReadChebyshev(int N, TString inFile) {
    vector<double> params;
    readFile(inFile, 1, {0}, (vector<vector<double>*>){&params});
    TString formula = GetChebyshevFormula(N);
    double start = params.at(params.size() - 2);
    double end = params.at(params.size() - 1);
    TF1 * f = new TF1("cheby",formula,start,end);
    for (int i = 0 ; i <= N ; i++) f->SetParameter(i,params.at(i));
    return f;
}


TF1 * GetChebyshev(TFitResultPtr theFit, int N, double start, double end) {
    if (N < 0 || N > 10) {coutError("GetChebyshev:: Not implemented up to this order"); return 0;}
    TString formula = GetChebyshevFormula(N);

    TF1 * f = new TF1("cheby",formula,start,end);
    for (int i = 0 ; i <= N ; i++) f->SetParameter(i,theFit->GetParams()[i]);
    return f;
}

TF1 * GetChebyshev(int N, double start, double end) {
    if (N < 0 || N > 10) {coutError("GetChebyshev:: Not implemented up to this order"); return 0;}
    TString formula = GetChebyshevFormula(N);

    TF1 * f = new TF1("cheby",formula,start,end);
    return f;
}

long long time_stoll(string time) {
    bool inDecimals = false;
    int decimalsPrinted = 0;
    string to_mus;
    for (int i = 0  ; i < time.size() ; i++){
        if (inDecimals) decimalsPrinted++; 
        if (time[i] != '.') to_mus += time[i];
        else {
            inDecimals = true;
        }
    }
    for (int i = decimalsPrinted ; i < 6 ; i++) {
        to_mus += '0';
    }
    return stoll(to_mus);
}

void readTime(TString path, int lineStart, int columnIndex, vector<long long> * times) {
    ifstream file1(path);
    if (!file1.is_open()) return;
    string str;
    int line = 0;
    while (getline(file1, str))
    {
        line++;
        if (line < lineStart) continue;

        istringstream iss(str);
        vector<string> subs;

        do {
            string s;
            iss >> s;
            if (s.size() > 0) subs.push_back(s);
        } while (iss);

        times->push_back(time_stoll(subs[columnIndex]));

    }
}



void readFile_vsTime(TString path, int lineStart, vector<int> columnIndexes, vector<long long> * times, vector<vector<double>*> vectorsToFill) {
    ifstream file1(path);
    if (!file1.is_open()) return;
    string str;
    int line = 0;
    while (getline(file1, str))
    {
        line++;
        if (line < lineStart) continue;

        istringstream iss(str);
        vector<string> subs;

        do {
            string s;
            iss >> s;
            if (s.size() > 0) subs.push_back(s);
        } while (iss);

        times->push_back(stoll(subs[columnIndexes.at(0)]));

        for (int i = 1 ; i < columnIndexes.size() ; i++) {
            vectorsToFill.at(i-1)->push_back(stod(subs[columnIndexes.at(i)]));
        }
    }
}



void readPLBFile(TString path, int lineStart, vector<double> * rows, vector<long long> * timesStart, vector<long long> * timesEnd) {
    ifstream file1(path);
    if (!file1.is_open()) return;
    string str;
    int line = 0;
    while (getline(file1, str))
    {
        line++;
        if (line < lineStart) continue;

        istringstream iss(str);
        vector<string> subs;

        do {
            string s;
            iss >> s;
            if (s.size() > 0) subs.push_back(s);
        } while (iss);

        rows->push_back(stod(subs[1]));
        timesStart->push_back(time_stoll(subs[3]));
        timesEnd->push_back(time_stoll(subs[5]));
    }
}

void readPrunnedMods(TString path, vector<int> & runNums, vector<double> & LBs, vector<vector<pair<int,int>>> & prunned_A, vector<vector<pair<int,int>>> & prunned_C) {
    ifstream file1(path);
    if (!file1.is_open()) return;
    string str;
    int line = 0;
    while (getline(file1, str))
    {
        line++;
        if (line < 2) continue;

        istringstream iss(str);
        vector<string> subs;

        do {
            string s;
            iss >> s;
            if (s.size() > 0) subs.push_back(s);
        } while (iss);

        if (stoi(subs.at(0)) == 0) continue;

        runNums.push_back(stoi(subs.at(0)));
        LBs.push_back(stod(subs.at(1)));
        vector<pair<int,int>> mods_A, mods_C;
        for (int i = 2 ; i < subs.size() ; i++) {
            auto channel = split(subs.at(i).substr(4,subs.at(i).length()), ".");
            if (subs.at(i).substr(3,1) == "A") mods_A.push_back(make_pair(  stoi(channel.first) , stoi(channel.second)  ));
            if (subs.at(i).substr(3,1) == "C") mods_C.push_back(make_pair(  stoi(channel.first) , stoi(channel.second)  ));
        }
        prunned_A.push_back(mods_A);
        prunned_C.push_back(mods_C);
    }

}

void readPerChannelFile(TString path, vector<float> LBs[64], vector<float> perChannel[64], float LBStart, float LBEnd) {
    ifstream file(path);
    if (!file.is_open()) {coutError(path.Data()); return ;}
    string str;
    int line = 0;
    cout << "=================================" << endl;
    cout << "Reading " << path << endl;
    while (getline(file, str))
    {
        line++;
        istringstream iss(str);
        vector<string> subs;

        do {
            string s;
            iss >> s;
            if (s.size() > 0) subs.push_back(s);
        } while (iss);

        float LB = stof(subs[1]);
        if (LBStart != LBEnd && (LB < LBStart || LB > LBEnd)) continue;

        vector<int> filledMods = {};
        for (int i = 2 ; i < subs.size() -1 ; i++) {
            auto pair_info = split(subs.at(i), ":");
            int mod = stoi(pair_info.first);
            float lumi = stof(pair_info.second);
            perChannel[mod].push_back(lumi);
            LBs[mod].push_back(LB);
            filledMods.push_back(mod);
        }

        for (int mod = 0 ; mod < 64 ; mod++) {
            if (!contains(mod,filledMods)) {
                perChannel[mod].push_back(-999);
                LBs[mod].push_back(-999);
            }
        }
    }
    cout << "=================================" << endl;

}

void readPerChannelFile(TString path, vector<float> perChannel[64], double LBStart, double LBEnd) {
    ifstream file(path);
    if (!file.is_open()) {coutError(path.Data()); return ;}
    string str;
    int line = 0;
    cout << "=================================" << endl;
    cout << "Reading " << path << endl;
    while (getline(file, str))
    {
        line++;
        istringstream iss(str);
        vector<string> subs;

        do {
            string s;
            iss >> s;
            if (s.size() > 0) subs.push_back(s);
        } while (iss);

        double LB = stod(subs[1]);
        if (LB < LBStart || LB > LBEnd) continue;

        for (int i = 2 ; i < subs.size() ; i++) {
            auto pair_info = split(subs.at(i), ":");
            int mod = stoi(pair_info.first);
            float lumi = stof(pair_info.second);
            perChannel[mod].push_back(lumi);
        }
    }
    cout << "=================================" << endl;

}

void readPerChannelFile(TString path, vector<long long> times[64], vector<float> perChannel[64], long long timeStart, long long timeEnd) {
    ifstream file(path);
    if (!file.is_open()) {coutError(path.Data()); return ;}
    string str;
    int line = 0;
    cout << "=================================" << endl;
    cout << "Reading " << path << endl;
    while (getline(file, str))
    {
        line++;
        istringstream iss(str);
        vector<string> subs;

        do {
            string s;
            iss >> s;
            if (s.size() > 0) subs.push_back(s);
        } while (iss);

        long long time = stoll(subs[1]);
        if (time < timeStart || time > timeEnd) continue;

        for (int i = 2 ; i < subs.size() ; i++) {
            auto pair_info = split(subs.at(i), ":");
            int mod = stoi(pair_info.first);
            float lumi = stof(pair_info.second);
            perChannel[mod].push_back(lumi);
            times[mod].push_back(time);
        }
    }
    cout << "=================================" << endl;
}

void readPerChannelFile(TString path, vector<float> perChannel[64], long long timeStart, long long timeEnd) {
    ifstream file(path);
    if (!file.is_open()) {coutError(path.Data()); return ;}
    string str;
    int line = 0;
    cout << "=================================" << endl;
    cout << "Reading " << path << endl;
    while (getline(file, str))
    {
        line++;
        istringstream iss(str);
        vector<string> subs;

        do {
            string s;
            iss >> s;
            if (s.size() > 0) subs.push_back(s);
        } while (iss);

        long long time = stoll(subs[1]);
        if (time < timeStart || time > timeEnd) continue;

        for (int i = 2 ; i < subs.size() ; i++) {
            auto pair_info = split(subs.at(i), ":");
            int mod = stoi(pair_info.first);
            float lumi = stof(pair_info.second);
            perChannel[mod].push_back(lumi);
        }
    }
    cout << "=================================" << endl;
}

TString GetColumnSuffix(int columnToUse) {
    if (columnToUse == 83) return "TRACKs";
    if (columnToUse == 81) return "TRACKs_TIGHT";
    if (columnToUse == 113) return "LUCID";
    if (columnToUse == 79) return "EMEC";
    if (columnToUse == 77) return "EMEC_C";
    if (columnToUse == 75) return "EMEC_A";
    if (columnToUse == 73) return "FCAL";
    if (columnToUse == 71) return "FCAL_C";
    if (columnToUse == 69) return "FCAL_A";
    if (columnToUse == 0) return "ECells";
    if (columnToUse == -2) return "DCells";
    if (columnToUse == -4) return "B3B4Cells";
    if (columnToUse == -101) return "ECells_A";
    if (columnToUse == -102) return "ECells_C";
    if (columnToUse == -103) return "E3A";
    if (columnToUse == -104) return "E3C";
    if (columnToUse == -105) return "E4A";
    if (columnToUse == -106) return "E4C";
    if (columnToUse == -201) return "DCells_A";
    if (columnToUse == -202) return "DCells_C";
    if (columnToUse == -203) return "D5A";
    if (columnToUse == -204) return "D5C";
    if (columnToUse == -205) return "D6A";
    if (columnToUse == -206) return "D6C";
    if (columnToUse == -307) return "E1AC";
    if (columnToUse == -401) return  "B3B4Cells_A";
    if (columnToUse == -402) return  "B3B4Cells_C";
    if (columnToUse == -403) return  "BC3A";
    if (columnToUse == -404) return  "BC3C";
    if (columnToUse == -405) return  "BC4A";
    if (columnToUse == -406) return  "BC4C";
    coutError("functions::GetColumnSuffix -- columnToUse " + to_string(columnToUse) + " not defined");
    return "";
}

TString GetColumnOutDir(int columnToUse) {
    if (columnToUse == 83) return "";
    if (columnToUse == 81) return "_TRACKs_TIGHT";
    if (columnToUse == 113) return "_LUCID";
    if (columnToUse == 79) return  "_EMEC";
    if (columnToUse == 77) return  "_EMEC_CSide";
    if (columnToUse == 75) return  "_EMEC_ASide";
    if (columnToUse == 73) return "_FCAL";
    if (columnToUse == 71) return "_FCAL_CSide";
    if (columnToUse == 69) return "_FCAL_ASide";
    if (columnToUse == -1) return  "_RatioWithECells";
    if (columnToUse == -2) return  "_RatioWithDCells";
    if (columnToUse == -4) return "_RatioWithB3B4Cells";
    if (columnToUse == -101) return  "_RatioWithECellsASide";
    if (columnToUse == -102) return  "_RatioWithECellsCSide";
    if (columnToUse == -103) return  "_RatioWithE3A";
    if (columnToUse == -104) return  "_RatioWithE3C";
    if (columnToUse == -105) return  "_RatioWithE4A";
    if (columnToUse == -106) return  "_RatioWithE4C";
    if (columnToUse == -201) return  "_RatioWithDCellsASide";
    if (columnToUse == -202) return  "_RatioWithDCellsCSide";
    if (columnToUse == -203) return  "_RatioWithD5A";
    if (columnToUse == -204) return  "_RatioWithD5C";
    if (columnToUse == -205) return  "_RatioWithD6A";
    if (columnToUse == -206) return  "_RatioWithD6C";
    if (columnToUse == -307) return  "_RatioWithE1AC";
    if (columnToUse == -401) return  "_RatioWithB3B4CellsASide";
    if (columnToUse == -402) return  "_RatioWithB3B4CellsCSide";
    if (columnToUse == -403) return  "_RatioWithBC3A";
    if (columnToUse == -404) return  "_RatioWithBC3C";
    if (columnToUse == -405) return  "_RatioWithBC4A";
    if (columnToUse == -406) return  "_RatioWithBC4C";
    coutError("functions::GetColumnOutDir -- columnToUse " + to_string(columnToUse) + " not defined");
    return "";
}

TString GetColumnDenom(int columnToUse) {
    if (columnToUse == 83) return "TRACKs_{TIGHTMOD}";
    if (columnToUse == 81) return "TRACKs_{TIGHT}";
    if (columnToUse == 113) return "LUCID";
    if (columnToUse == 79) return "EMEC";
    if (columnToUse == 77) return "EMEC_{C}";
    if (columnToUse == 75) return "EMEC_{A}";
    if (columnToUse == 73) return "FCAL";
    if (columnToUse == 71) return "FCAL_{C}";
    if (columnToUse == 69) return "FCAL_{A}";
    if (columnToUse == -1) return "ECells";
    if (columnToUse == -2) return "DCells";
    if (columnToUse == -4) return "B3B4Cells";
    if (columnToUse == -101) return "ECells_{A}";
    if (columnToUse == -102) return "ECells_{C}";
    if (columnToUse == -103) return "E3A";
    if (columnToUse == -104) return "E3C";
    if (columnToUse == -105) return "E4A";
    if (columnToUse == -106) return "E4C";
    if (columnToUse == -201) return "DCells_{A}";
    if (columnToUse == -202) return "DCells_{C}";
    if (columnToUse == -203) return "D5A";
    if (columnToUse == -204) return "D5C";
    if (columnToUse == -205) return "D6A";
    if (columnToUse == -206) return "D6C";
    if (columnToUse == -307) return "E1AC";
    if (columnToUse == -401) return  "B3B4Cells_{A}";
    if (columnToUse == -402) return  "B3B4Cells_{C}";
    if (columnToUse == -403) return  "BC3A";
    if (columnToUse == -404) return  "BC3C";
    if (columnToUse == -405) return  "BC4A";
    if (columnToUse == -406) return  "BC4C";
    coutError("functions::GetColumnDenom -- columnToUse " + to_string(columnToUse) + " not defined");
    return "";
}

string getLumiFile(int filePathIndex, int runNum){
    string filePath = getCMDoutput("bash get_anchor.sh " + to_string(runNum) + " " + to_string(filePathIndex));
    filePath.erase(std::remove(filePath.begin(), filePath.end(), '\"'), filePath.end());
    TString tstr_filePath = filePath;
    if (filePathIndex == 11){ // UPDATED 2016 TRACKs data
        tstr_filePath.ReplaceAll("_LS","");
    }  
    return tstr_filePath.Data();
}

void readTRACKsFile(int runNumber, int filePathIndex, int columnToUse, bool vsTime, vector<float>& LB_TRACKS, vector<float>& LB_duration, vector<float>& TRACKs_Lumi, vector<float>& TRACKs_Lumi_error, vector<float>& TRACKs_InstLumi, vector<float>& LCD_Lumi, vector<float>& Filled_BCID_save, vector<long long>& TRACKs_timeStart, vector<float>& TRACKs_InstLumi_error, string filePath_TRACKS = "", bool verbose = true) {
    if (filePath_TRACKS == "") filePath_TRACKS = getLumiFile(filePathIndex, runNumber);

    vector<vector<float>*> toFill = {&LB_TRACKS, &LB_duration, &TRACKs_Lumi, &TRACKs_Lumi_error, &LCD_Lumi, &Filled_BCID_save};

    int startLine_TRACKS = 4;
    vector<int> columnIndexes_TRACKS{2,5,columnToUse,columnToUse+1,113,3}; // 83: Int_Luminosity_TRACKS[TIGHTMOD][LBNum],  79: EMEC, 113: Int_Luminosity_LCD[BI][HITOR][LBNum]
    
    // if (columnToUse != 82 && columnToUse != 83) coutError("WARNING: The errors are not properly handed if ratios are not with TRACKs");

    if (runNumber == 331085) {
        coutError("Changing the TRACKS path by hardcode");
        filePath_TRACKS = "/afs/cern.ch/atlas/project/LumiWG/CoolScanNtuple/2017/2017SigmaVis/2017NtuplesProduction/PerLB/2corrections/PerLB_IntegratedLumi_331085_LCDMuCorrToTracksTightMod_BCMMuCorrToTracksTightMod.dat";
        startLine_TRACKS = 4; 
        columnIndexes_TRACKS = {1,4,82,83,112,2};
    } else if (runNumber == 340634) {
        coutError("Changing the TRACKS path by hardcode");
        filePath_TRACKS = "/afs/.cern.ch/atlas/project/LumiWG/CoolScanNtuple/2017/2017SigmaVis/2018NtuplesProduction/5TeV/PerLB/NoMuCorr/PerLB_IntegratedLumi_340634.dat";
        startLine_TRACKS = 4; 
    }

    if (verbose) cout << "Reading file " << filePath_TRACKS << endl;
    if (filePathIndex == 0) {columnIndexes_TRACKS = {1,4,82,83,112,2};}
    if (filePathIndex == 9) {
        toFill = {&LB_TRACKS, &TRACKs_InstLumi, &TRACKs_InstLumi_error, &TRACKs_Lumi};
        columnIndexes_TRACKS = {0,3,4,3};
        startLine_TRACKS = 2;
        vsTime = false;
    }
    if (filePathIndex == 10) { // OLD 2016 TRACKs data
        toFill = {&LB_TRACKS, &LB_duration, &TRACKs_Lumi, &TRACKs_Lumi_error, &Filled_BCID_save};
        columnIndexes_TRACKS = {1,4,84,85,2};
        if (columnToUse == 79) columnIndexes_TRACKS = {1,4,78,79,2};
        startLine_TRACKS = 1;
    }
    if (filePathIndex == 11 || filePathIndex == 12) { // UPDATED 2016 TRACKs data
        TString updated_filePath = filePath_TRACKS;
        updated_filePath.ReplaceAll("_LS","");
        vector<float> LB_updated, mu_TRACKs;
        toFill = {&LB_updated, &mu_TRACKs};
        columnIndexes_TRACKS = {0, 3};
        vector<float> test;
        if (runNumber == 280520) columnIndexes_TRACKS = {0, 4};
        startLine_TRACKS = 2;
        readFile(updated_filePath, startLine_TRACKS, columnIndexes_TRACKS, toFill);
        if (runNumber == 280520) readFile(updated_filePath, startLine_TRACKS, {1}, {&test});

        string file_for_other_info = getLumiFile((filePathIndex == 11) ? 10 : 13, runNumber); 
        vector<float> LB_TRACKS_other_info, LB_duration_other_info, Filled_BCID_save_other_info;
        vector<int> columnIndexes_TRACKS_other_info{1,4,2};
        vector<vector<float>*> toFill_other_info = {&LB_TRACKS_other_info, &LB_duration_other_info, &Filled_BCID_save_other_info};
        readFile(file_for_other_info, 1, columnIndexes_TRACKS_other_info, toFill_other_info);
        if (file_for_other_info == "") {
            coutError("Missing Benedetto info for " + to_string(runNumber) + " in pathIndex " + ((filePathIndex == 11) ? 10 : 13));
        }
        for (int i_LB = 0 ; i_LB < LB_updated.size() ; i_LB++) {
            int index_LB = indexOf(LB_updated.at(i_LB), LB_TRACKS_other_info);
            if (index_LB == -1) continue;
            LB_TRACKS.push_back(LB_updated.at(i_LB));
            LB_duration.push_back(LB_duration_other_info.at(index_LB));
            // if (runNumber == 280520) Filled_BCID_save.push_back(test.at(i_LB));
            // else Filled_BCID_save.push_back(Filled_BCID_save_other_info.at(index_LB));
            Filled_BCID_save.push_back(Filled_BCID_save_other_info.at(index_LB));
            float lumi = (mu_TRACKs.at(i_LB) / 1.7021) * LB_duration.back() * PARAMS::LHC_FREQ * Filled_BCID_save.back() / (PARAMS::XSEC_INE * 1000); // Calibration from rlbq macroGetTRACKsCalibration.C 299390,430,560
            // float lumi = mu_TRACKs.at(i_LB) * LB_duration.back() * PARAMS::LHC_FREQ * Filled_BCID_save.back() / 136087.671875; // Calibration from rlbq macroGetTRACKsCalibration.C 299390,430,560
            // cout << lumi << " = " << LB_updated.at(i_LB) << " " << mu_TRACKs.at(i_LB) << " " << LB_duration.back() << " " << PARAMS::LHC_FREQ << " " << Filled_BCID_save.back() << " " << PARAMS::XSEC_INE * 1000 << endl;
            TRACKs_Lumi.push_back(lumi);
            TRACKs_Lumi_error.push_back(0); //TODO
        }
    } else {
        readFile(filePath_TRACKS, startLine_TRACKS, columnIndexes_TRACKS, toFill);
        if (vsTime) readTime(filePath_TRACKS, startLine_TRACKS, columnIndexes_TRACKS.at(1)-1, &TRACKs_timeStart);
    }

    if (GetColumnSuffix(columnToUse).Contains("FCAL")) {
        if (runNumber >= 325713 && runNumber <= 341649) {
            vector<float> FCAL_LB, FCAL_mu_A, FCAL_mu_C;
            TString path_to_FCAL_file = "/eos/user/g/gonzales/FCal/" + to_string(runNumber) + ".dat";
            readFile(path_to_FCAL_file, 1, {0,1,2}, {&FCAL_LB, &FCAL_mu_A, &FCAL_mu_C});
            if (FCAL_LB.size() > 0) {
                TRACKs_Lumi.clear();
                for (int i_LB = 0 ; i_LB < LB_TRACKS.size() ; i_LB++) {
                    int FCAL_index = find(FCAL_LB.begin(),FCAL_LB.end(),LB_TRACKS.at(i_LB)) - FCAL_LB.begin();
                    float mu_FCAL_A = FCAL_mu_A.at(FCAL_index);
                    float mu_FCAL_C = FCAL_mu_C.at(FCAL_index);
                    float mu_avg = 0.5*(mu_FCAL_A + mu_FCAL_C);
                    float FCAL_Lumi = mu_avg * LB_duration.at(i_LB) * PARAMS::LHC_FREQ * Filled_BCID_save.at(i_LB) / (PARAMS::XSEC_INE * 1000);
                    TRACKs_Lumi.push_back(FCAL_Lumi);
                }
            }
        }
    }

    for (int i = 0 ; i < LB_TRACKS.size() ; i++) {
        if (filePathIndex == 9) {
            TRACKs_InstLumi.at(i) *= 100;
            LB_duration.push_back(1); //These files use instantaneous lumi. Putting one here for lumi/LB_duration be redundant
        } else {
            TRACKs_InstLumi.push_back(TRACKs_Lumi.at(i)/LB_duration.at(i));
            // cout << LB_TRACKS.at(i) << " " << TRACKs_InstLumi.back() << endl;
        }
    } 

}


void readTileFile(int runNumber, TString runSuffix, int cell_type, int columnToUse, int filePathIndex, vector<float>& LB_DENOM, vector<float>& LB_duration, vector<float>& DENOM_InstLumi, vector<float>& DENOM_InstLumi_error, vector<float>& LCD_Lumi, vector<float>& Filled_BCID_save) {
    TString filePath_TILE = getBFilePath(runNumber, runSuffix, cell_type);
    TString filePath_TILE_error = getBFilePath(runNumber, runSuffix + "_error", cell_type);
    vector<float> RUN_TILE, LB_TILE, cell_1_A_TILE,	cell_1_C_TILE,	cell_2_A_TILE, cell_2_C_TILE;
    vector<float> cell_1_error_A_TILE, cell_1_error_C_TILE, cell_2_error_A_TILE, cell_2_error_C_TILE;
    cout << "Reding Tile File: " << filePath_TILE << endl;
    readFile(filePath_TILE, 3, {0,1,2,3,4,5}, (vector<vector<float>*>){&RUN_TILE, &LB_TILE, &cell_1_A_TILE, &cell_1_C_TILE, &cell_2_A_TILE, &cell_2_C_TILE});
    readFile(filePath_TILE_error, 3, {2,3,4,5}, (vector<vector<float>*>){&cell_1_error_A_TILE, &cell_1_error_C_TILE, &cell_2_error_A_TILE, &cell_2_error_C_TILE});

    vector<float> LB_TRACKs, LB_durationTRACKs, TRACKs_Lumi, TRACKs_Lumi_error, TRACKs_InstLumi_error, TRACKs_InstLumi, LCD_Lumi_fromTRACKs, Filled_BCID_save_fromTRACKs;
    vector<long long> TRACKs_timeStart; // in mu_s
    readTRACKsFile(runNumber, filePathIndex, 83, false, LB_TRACKs, LB_durationTRACKs, TRACKs_Lumi, TRACKs_Lumi_error, TRACKs_InstLumi, LCD_Lumi_fromTRACKs, Filled_BCID_save_fromTRACKs, TRACKs_timeStart, TRACKs_InstLumi_error);

    bool use_both_sides = columnToUse < 100;
    bool use_A_side = columnToUse > 100 && (columnToUse - 1) % 100 == 0;
    bool use_C_side = columnToUse > 100 && (columnToUse - 2) % 100 == 0;
    bool use_cell1_A_side = columnToUse > 100 && (columnToUse - 3) % 100 == 0;
    bool use_cell1_C_side = columnToUse > 100 && (columnToUse - 4) % 100 == 0;
    bool use_cell2_A_side = columnToUse > 100 && (columnToUse - 5) % 100 == 0;
    bool use_cell2_C_side = columnToUse > 100 && (columnToUse - 6) % 100 == 0;
    bool use_cell1_AC_side = columnToUse > 100 && (columnToUse - 7) % 100 == 0;
    bool use_cell2_AC_side = columnToUse > 100 && (columnToUse - 8) % 100 == 0;

    for (int i_LB = 0 ; i_LB < LB_TILE.size() ; i_LB++) {
        if (use_both_sides && (cell_1_A_TILE.at(i_LB) == -999 || cell_1_C_TILE.at(i_LB) == -999 || cell_2_A_TILE.at(i_LB) == -999 || cell_2_C_TILE.at(i_LB) == -999)) continue;
        if (use_A_side && (cell_1_A_TILE.at(i_LB) == -999 || cell_2_A_TILE.at(i_LB) == -999)) continue;
        if (use_C_side && (cell_1_C_TILE.at(i_LB) == -999 || cell_2_C_TILE.at(i_LB) == -999)) continue;
        if (use_cell1_A_side && (cell_1_A_TILE.at(i_LB) == -999)) continue;
        if (use_cell1_C_side && (cell_1_C_TILE.at(i_LB) == -999)) continue;
        if (use_cell2_A_side && (cell_2_A_TILE.at(i_LB) == -999)) continue;
        if (use_cell2_C_side && (cell_2_C_TILE.at(i_LB) == -999)) continue;
        if (use_cell1_AC_side && (cell_1_A_TILE.at(i_LB) == -999 || cell_1_C_TILE.at(i_LB) == -999)) continue;
        if (use_cell2_AC_side && (cell_2_A_TILE.at(i_LB) == -999 || cell_2_C_TILE.at(i_LB) == -999)) continue;
        float LB = LB_TILE.at(i_LB);
        int track_index = find(LB_TRACKs.begin(),LB_TRACKs.end(),LB) - LB_TRACKs.begin();

        if (track_index < TRACKs_Lumi.size()) {
            LB_DENOM.push_back(LB_TILE.at(i_LB));
            vector<float> vals_to_get_fromLB;
            vector<float> errors_to_get_fromLB;

            if (use_both_sides) {vals_to_get_fromLB.push_back(cell_1_A_TILE.at(i_LB)); vals_to_get_fromLB.push_back(cell_1_C_TILE.at(i_LB)); vals_to_get_fromLB.push_back(cell_2_A_TILE.at(i_LB)); vals_to_get_fromLB.push_back(cell_2_C_TILE.at(i_LB));}
            else if (use_A_side) {vals_to_get_fromLB.push_back(cell_1_A_TILE.at(i_LB)); vals_to_get_fromLB.push_back(cell_2_A_TILE.at(i_LB));}
            else if (use_C_side) {vals_to_get_fromLB.push_back(cell_1_C_TILE.at(i_LB)); vals_to_get_fromLB.push_back(cell_2_C_TILE.at(i_LB));}
            else if (use_cell1_A_side) {vals_to_get_fromLB.push_back(cell_1_A_TILE.at(i_LB));}
            else if (use_cell1_C_side) {vals_to_get_fromLB.push_back(cell_1_C_TILE.at(i_LB));}
            else if (use_cell2_A_side) {vals_to_get_fromLB.push_back(cell_2_A_TILE.at(i_LB));}
            else if (use_cell2_C_side) {vals_to_get_fromLB.push_back(cell_2_C_TILE.at(i_LB));}
            else if (use_cell1_AC_side) {vals_to_get_fromLB.push_back(cell_1_A_TILE.at(i_LB)); vals_to_get_fromLB.push_back(cell_1_C_TILE.at(i_LB));}
            else if (use_cell2_AC_side) {vals_to_get_fromLB.push_back(cell_2_A_TILE.at(i_LB)); vals_to_get_fromLB.push_back(cell_2_C_TILE.at(i_LB));}

            if (use_both_sides) {errors_to_get_fromLB.push_back(cell_1_error_A_TILE.at(i_LB)); errors_to_get_fromLB.push_back(cell_1_error_C_TILE.at(i_LB)); errors_to_get_fromLB.push_back(cell_2_error_A_TILE.at(i_LB)); errors_to_get_fromLB.push_back(cell_2_error_C_TILE.at(i_LB));}
            else if (use_A_side) {errors_to_get_fromLB.push_back(cell_1_error_A_TILE.at(i_LB)); errors_to_get_fromLB.push_back(cell_2_error_A_TILE.at(i_LB));}
            else if (use_C_side) {errors_to_get_fromLB.push_back(cell_1_error_C_TILE.at(i_LB)); errors_to_get_fromLB.push_back(cell_2_error_C_TILE.at(i_LB));}
            else if (use_cell1_A_side) {errors_to_get_fromLB.push_back(cell_1_error_A_TILE.at(i_LB));}
            else if (use_cell1_C_side) {errors_to_get_fromLB.push_back(cell_1_error_C_TILE.at(i_LB));}
            else if (use_cell2_A_side) {errors_to_get_fromLB.push_back(cell_2_error_A_TILE.at(i_LB));}
            else if (use_cell2_C_side) {errors_to_get_fromLB.push_back(cell_2_error_C_TILE.at(i_LB));}
            else if (use_cell1_AC_side) {errors_to_get_fromLB.push_back(cell_1_error_A_TILE.at(i_LB)); errors_to_get_fromLB.push_back(cell_1_error_C_TILE.at(i_LB));}
            else if (use_cell2_AC_side) {errors_to_get_fromLB.push_back(cell_2_error_A_TILE.at(i_LB)); errors_to_get_fromLB.push_back(cell_2_error_C_TILE.at(i_LB));}

            DENOM_InstLumi.push_back(GetMeanErr(vals_to_get_fromLB).first);
            DENOM_InstLumi_error.push_back(GetErrorFromAMean(errors_to_get_fromLB));

            LB_duration.push_back(LB_durationTRACKs.at(track_index));
            if (track_index < LCD_Lumi_fromTRACKs.size()) LCD_Lumi.push_back(LCD_Lumi_fromTRACKs.at(track_index));
            if (track_index < Filled_BCID_save_fromTRACKs.size()) Filled_BCID_save.push_back(Filled_BCID_save_fromTRACKs.at(track_index));
        } 
    }
}

bool checkUseLBByPath(TString path, float LB) {
    if (path.Contains("340072") && ((LB > 1220 && LB < 1260) || (LB > 1281 && LB < 1303))) return false;
    if (path.Contains("331085") && ((LB == 251) || (LB == 862))) return false;
    return true;
}

void makePerChannelRatio(TString path, vector<float> perChannel_ratio[64], vector<float> &LB_TRACKS, vector<float> &LB_duration, vector<float> &TRACKs_Lumi) {
    ifstream file(path);
    if (!file.is_open()) {coutError(path.Data()); return;}
    string str;
    int line = 0;
    cout << "=================================" << endl;
    cout << "Reading " << path << endl;
    while (getline(file, str))
    {
        line++;
        istringstream iss(str);
        vector<string> subs;

        do {
            string s;
            iss >> s;
            if (s.size() > 0) subs.push_back(s);
        } while (iss);

        float LB = stof(subs[1]);
        if (!checkUseLBByPath(path,LB)) continue;

        int track_index = find(LB_TRACKS.begin(),LB_TRACKS.end(),LB) - LB_TRACKS.begin();
        if (track_index >= TRACKs_Lumi.size()) continue;
        float tracks_lumi = TRACKs_Lumi.at(track_index) / LB_duration.at(track_index);
        if (tracks_lumi == 0 )continue;

        for (int i = 2 ; i < subs.size()-1 ; i++) {
            auto pair_info = split(subs.at(i), ":");
            int mod = stoi(pair_info.first);
            float lumi = stof(pair_info.second);
            perChannel_ratio[mod].push_back(lumi / tracks_lumi);
        }
    }
    cout << "=================================" << endl;
}

void makePerChannelRatio(TString path, vector<float> perChannel_ratio[64], vector<float> &LB_TRACKS, vector<float> &LB_duration, vector<float> &TRACKs_Lumi, vector<float> &LBs) {
    ifstream file(path);
    if (!file.is_open()) {coutError(path.Data()); return;}
    string str;
    int line = 0;
    cout << "=================================" << endl;
    cout << "Reading " << path << endl;
    while (getline(file, str))
    {
        line++;
        istringstream iss(str);
        vector<string> subs;

        do {
            string s;
            iss >> s;
            if (s.size() > 0) subs.push_back(s);
        } while (iss);

        float LB = stof(subs[1]);
        if (!checkUseLBByPath(path,LB)) continue;
        
        int track_index = find(LB_TRACKS.begin(),LB_TRACKS.end(),LB) - LB_TRACKS.begin();
        if (track_index >= TRACKs_Lumi.size()) continue;
        float tracks_lumi = TRACKs_Lumi.at(track_index) / LB_duration.at(track_index);
        if (tracks_lumi == 0 )continue;

        LBs.push_back(LB);
        for (int i = 2 ; i < subs.size()-1 ; i++) {
            auto pair_info = split(subs.at(i), ":");
            int mod = stoi(pair_info.first);
            float lumi = stof(pair_info.second);
            perChannel_ratio[mod].push_back(lumi / tracks_lumi);
        }
    }
    cout << "=================================" << endl;
}

void getTileLumiInformation(TString path, vector<float> perChannel_lumi[64], bool fillLBs, vector<float> &LBs) {
    ifstream file(path);
    if (!file.is_open()) {coutError(path.Data()); return;}
    string str;
    int line = 0;
    cout << "=================================" << endl;
    cout << "Reading " << path << endl;
    while (getline(file, str))
    {
        line++;
        istringstream iss(str);
        vector<string> subs;

        do {
            string s;
            iss >> s;
            if (s.size() > 0) subs.push_back(s);
        } while (iss);

        float LB = stof(subs[1]);
        if (!checkUseLBByPath(path,LB)) continue;

        if (fillLBs) LBs.push_back(LB);
        vector<int> filledMods;
        for (int i = 2 ; i < subs.size()-1 ; i++) {
            auto pair_info = split(subs.at(i), ":");
            int mod = stoi(pair_info.first);
            float lumi = stof(pair_info.second);
            filledMods.push_back(mod);
            perChannel_lumi[mod].push_back(lumi);
        }
        for (int mod = 0 ; mod < 64 ; mod++) {
            if (!contains(mod,filledMods)) {
                perChannel_lumi[mod].push_back(-999);
            }
        }
    }
    cout << "=================================" << endl;
}

void getLumiInformation(TString path, vector<float> perChannel_lumi[64], vector<float> perChannel_IntLumi[64], vector<float> &LB_TRACKS, vector<float> &LB_duration, vector<float> &TRACKs_Lumi, vector<float> intTRACKs_Lumi, vector<float> perChannel_ratio[64], vector<float> perChannel_IntRatio[64], bool fillLBs, vector<float> &LBs) {
    ifstream file(path);
    if (!file.is_open()) {coutError(path.Data()); return;}
    string str;
    int line = 0;
    cout << "=================================" << endl;
    cout << "Reading " << path << endl;
    while (getline(file, str))
    {
        line++;
        istringstream iss(str);
        vector<string> subs;

        do {
            string s;
            iss >> s;
            if (s.size() > 0) subs.push_back(s);
        } while (iss);

        float LB = stof(subs[1]);
        if (!checkUseLBByPath(path,LB)) continue;

        int track_index = find(LB_TRACKS.begin(),LB_TRACKS.end(),LB) - LB_TRACKS.begin();
        float tracks_lumi = -999;
        float LB_dur = 0;
        float intTracks = -999;
        if (track_index < TRACKs_Lumi.size()) {
            LB_dur = LB_duration.at(track_index);
            tracks_lumi = TRACKs_Lumi.at(track_index) / LB_duration.at(track_index);
            intTracks = intTRACKs_Lumi.at(track_index);
        }
        // if (tracks_lumi == 0 ) continue;

        if (fillLBs) LBs.push_back(LB);
        vector<int> filledMods;
        for (int i = 2 ; i < subs.size()-1 ; i++) {
            auto pair_info = split(subs.at(i), ":");
            int mod = stoi(pair_info.first);
            float lumi = stof(pair_info.second);
            filledMods.push_back(mod);
            perChannel_lumi[mod].push_back(lumi);
            if (perChannel_IntLumi[mod].size() == 0) perChannel_IntLumi[mod].push_back(lumi * LB_dur * 1e-36 * 1e30);
            else perChannel_IntLumi[mod].push_back(perChannel_IntLumi[mod].back() + lumi * LB_dur * 1e-36 * 1e30);
            // perChannel_IntLumi[mod].push_back(lumi * LB_dur);
            // if (mod == 2) {
            //     cout << LB << " " << intTracks << " " << tracks_lumi << endl;
            // }
            if (intTracks != -999 && tracks_lumi != 0) {
                perChannel_IntRatio[mod].push_back(perChannel_IntLumi[mod].back() / intTRACKs_Lumi.at(track_index));
                perChannel_ratio[mod].push_back(lumi / tracks_lumi);
            } else {
                perChannel_ratio[mod].push_back(-999);
                perChannel_IntRatio[mod].push_back(0);
            }
        }
        for (int mod = 0 ; mod < 64 ; mod++) {
            if (!contains(mod,filledMods)) {
                perChannel_ratio[mod].push_back(-999);
                perChannel_lumi[mod].push_back(-999);
                if (perChannel_IntLumi[mod].size() == 0) perChannel_IntLumi[mod].push_back(0);
                else perChannel_IntLumi[mod].push_back(perChannel_IntLumi[mod].back() + 0);
                // perChannel_IntLumi[mod].push_back(lumi * LB_dur);
                perChannel_IntRatio[mod].push_back(0);
            }
        }
    }
    cout << "=================================" << endl;
}

void getLumiInformationUsingTile(TString path, vector<float> perChannel_lumi[64], vector<float> &LB_DENOM, vector<float> DENOM_InstLumi, vector<float> perChannel_ratio[64], bool fillLBs, vector<float> &LBs) {
    ifstream file(path);
    if (!file.is_open()) {coutError(path.Data()); return;}
    string str;
    int line = 0;
    cout << "=================================" << endl;
    cout << "Reading " << path << endl;
    while (getline(file, str))
    {
        line++;
        istringstream iss(str);
        vector<string> subs;

        do {
            string s;
            iss >> s;
            if (s.size() > 0) subs.push_back(s);
        } while (iss);

        float LB = stof(subs[1]);
        if (!checkUseLBByPath(path,LB)) continue;
        
        int LB_index = find(LB_DENOM.begin(),LB_DENOM.end(),LB) - LB_DENOM.begin();
        float denom_lumi = -999;
        if (LB_index < DENOM_InstLumi.size()) {
            denom_lumi = DENOM_InstLumi.at(LB_index);
        }

        if (fillLBs) LBs.push_back(LB);
        vector<int> filledMods;
        for (int i = 2 ; i < subs.size()-1 ; i++) {
            auto pair_info = split(subs.at(i), ":");
            int mod = stoi(pair_info.first);
            float lumi = stof(pair_info.second);
            filledMods.push_back(mod);
            perChannel_lumi[mod].push_back(lumi);
            if (denom_lumi != -999) {
                perChannel_ratio[mod].push_back(lumi / denom_lumi);
            } else {
                perChannel_ratio[mod].push_back(-999);
            }
        }
        for (int mod = 0 ; mod < 64 ; mod++) {
            if (!contains(mod,filledMods)) {
                perChannel_ratio[mod].push_back(-999);
                perChannel_lumi[mod].push_back(-999);
            }
        }
    }
    cout << "=================================" << endl;
}

void readPerChannelRatio(TString path, vector<float> run_vector[64], vector<float> mod_ratio[64], vector<int> filterRuns, int runStart, int runEnd, vector<int> & runs_threshold, float threshold) {
    ifstream file(path);
    if (!file.is_open()) return *(new vector<int>());
    string str;
    int line = 0; 
    cout << "Reading " << path << endl;
    while (getline(file, str))
    {
        line++;
        istringstream iss(str);
        vector<string> subs;

        do {
            string s;
            iss >> s;
            if (s.size() > 0) subs.push_back(s);
        } while (iss);

        int run = stoi(subs[0]);

        if (runStart != -1 && runEnd != -1 && (run < runStart || run > runEnd)) continue;
        if (find(filterRuns.begin(),filterRuns.end(),run) != filterRuns.end()) continue;


        for (int i = 1 ; i < subs.size() ; i++) {
            auto pair_info = split(subs.at(i), ":");
            int mod = stoi(pair_info.first);
            float ratio = stof(pair_info.second);
            if (ratio == 0) {continue;}
            if (fabs(ratio - 1) > threshold) {add(run,runs_threshold);}
            run_vector[mod].push_back(stof(subs[0]));
            mod_ratio[mod].push_back(ratio);
        }
    }
}

double getRunIntegratedLuminosity(int runNumber, int filePathIndex) { // In pb
    vector<float> LB_TRACKS, LB_duration, TRACKs_Lumi, TRACKs_Lumi_error, TRACKs_InstLumi, LCD_Lumi, Filled_BCID_save, intDENOM_Lumi;
    vector<float> TRACKs_InstLumi_error;
    vector<long long> TRACKs_timeStart; // in mu_s
    readTRACKsFile(runNumber, filePathIndex, 83, false, LB_TRACKS, LB_duration, TRACKs_Lumi, TRACKs_Lumi_error, TRACKs_InstLumi, LCD_Lumi, Filled_BCID_save, TRACKs_timeStart, TRACKs_InstLumi_error, "", false);
    // for (int i_LB = 0 ; i_LB < LB_TRACKS.size() ; i_LB++) cout << LB_TRACKS.at(i_LB) << " " << TRACKs_Lumi.at(i_LB) << endl;
    double sum = accumulate(TRACKs_Lumi.begin(),TRACKs_Lumi.end(),0.0);
    return sum * 1e-6;
}

double getRunNBunches(int runNumber, int filePathIndex) {
    vector<float> LB_TRACKS, LB_duration, TRACKs_Lumi, TRACKs_Lumi_error, TRACKs_InstLumi, LCD_Lumi, Filled_BCID_save, intDENOM_Lumi;
    vector<float> TRACKs_InstLumi_error;
    vector<long long> TRACKs_timeStart; // in mu_s
    readTRACKsFile(runNumber, filePathIndex, 83, false, LB_TRACKS, LB_duration, TRACKs_Lumi, TRACKs_Lumi_error, TRACKs_InstLumi, LCD_Lumi, Filled_BCID_save, TRACKs_timeStart, TRACKs_InstLumi_error, "", false);
    return GetMeanErr(Filled_BCID_save).first;
}

double getRunPeakLumi(int runNumber, int filePathIndex) { // x10^{30} cm^{-2} s^{-1}
    vector<float> LB_TRACKS, LB_duration, TRACKs_Lumi, TRACKs_Lumi_error, TRACKs_InstLumi, LCD_Lumi, Filled_BCID_save, intDENOM_Lumi;
    vector<float> TRACKs_InstLumi_error;
    vector<long long> TRACKs_timeStart; // in mu_s
    readTRACKsFile(runNumber, filePathIndex, 83, false, LB_TRACKS, LB_duration, TRACKs_Lumi, TRACKs_Lumi_error, TRACKs_InstLumi, LCD_Lumi, Filled_BCID_save, TRACKs_timeStart, TRACKs_InstLumi_error, "", false);
    return GetMinMax(TRACKs_InstLumi).second;
}
 
double getIntegratedLumiAtRun(int runNumber) { // In pb
    TString lumiFile = "share/lumitable2018.dat";
    vector<double> v_runNumber, Lumi_Prescale_Corrected;
    vector<vector<double>*> to_fill = {&v_runNumber,&Lumi_Prescale_Corrected};
    readFile(lumiFile, 2, {0,6}, to_fill);
    double sum_lumi = 0;
    for (int i_run = 0 ; i_run < v_runNumber.size() ; i_run++) {
        sum_lumi += Lumi_Prescale_Corrected.at(i_run);
        if (v_runNumber.at(i_run) == runNumber) break;
    }
    return sum_lumi;
}



void getIntegratedTRACKsLumi(vector<float> TRACKs_Lumi, vector<float> &intTRACKs_Lumi) { // In pb
    if (TRACKs_Lumi.size() == 0) return;
    intTRACKs_Lumi.push_back(TRACKs_Lumi.at(0) * 1e-36 * 1e30);
    for (int i_trk = 1 ; i_trk < TRACKs_Lumi.size() ; i_trk++) {
        intTRACKs_Lumi.push_back(intTRACKs_Lumi.at(i_trk-1) + TRACKs_Lumi.at(i_trk) * 1e-36 * 1e30);
    }
}

double getIntegratedTRACKsLumi(int runNum, int filePathIndex) { // In pb

    TString file = getLumiFile(filePathIndex, runNum);

    vector<float> TRACKs_Lumi;

    readFile(file, 4, {83}, {&TRACKs_Lumi});

    double sum = accumulate(TRACKs_Lumi.begin(),TRACKs_Lumi.end(),0.0);

    return sum * 1e-6;

}

double getIntegratedTRACKsLumi(int runNum_upTo, int filePathIndex, vector<float> runList) { // In pb
    double intLumi = 0;

    for (int runNumber : runList) {
        intLumi += getIntegratedTRACKsLumi(runNumber, filePathIndex);
        if (runNumber == runNum_upTo) break;
    }

    return intLumi;
    
}

double getIntegratedLumi(int runNum, int filePathIndex, int columnToUse) { // In pb

    TString file = getLumiFile(filePathIndex, runNum);

    vector<float> Lumi;

    readFile(file, 4, {columnToUse}, {&Lumi});

    double sum = accumulate(Lumi.begin(),Lumi.end(),0.0);

    return sum * 1e-6;

}

double getIntegratedLumiAtRun(int runNumber, int filePathIndex, int columnToUse = 83) { // In pb
    TString loadFileName = "share/lumiIntegratedFile_" + to_string(columnToUse) + ".dat";
    double sum_lumi = 0;
    if (fileExists(loadFileName)) {
        vector<double> runNumbers, v_intLumi;
        readFile(loadFileName, 1, {0,1}, (vector<vector<double>*>){&runNumbers,&v_intLumi});
        for (int i_run = 0 ; i_run < runNumbers.size() ; i_run++) {
            int runNum = runNumbers.at(i_run);
            sum_lumi = v_intLumi.at(i_run);
            if (runNum >= runNumber) break;
        }
    } else {
        TString Benedetto_directory = "";
        if (filePathIndex == 4) Benedetto_directory = "/afs/cern.ch/atlas/project/LumiWG/CoolScanNtuple/2018/PerLB/1correction/";
        else {coutError("getIntegratedLumiAtRun not implemented for filePathIndex = " + to_string(filePathIndex)); return 0;}
        vector<TString> fileList = getFilesList(Benedetto_directory);
        vector<int> runNumbers;
        for (TString file : fileList) runNumbers.push_back(getDSID(file));
        sort(runNumbers.begin(), runNumbers.end());
        for (int runNum : runNumbers)  {
            sum_lumi += getIntegratedLumi(runNum, filePathIndex, columnToUse);
            if (runNum == runNumber) break;
        }
    }
    return sum_lumi;
}   

void readIntegratedLumiFile (int filePathIndex, int runNum, int algo, vector<vector<float>*> vectorsToFill) {
    string filePath = getLumiFile(filePathIndex, runNum);

    int column;
    if (algo == 1) { //TRACKS
        column = 83;
    } else if (algo == 2) { //LCD Int_Luminosity_LCD[BI][HITOR][LBNum]
        column = 113;
    }

    int startLine = 4;
    vector<int> columnIndexes{2,5,column};
    if (filePathIndex == 0) {columnIndexes = {1,4,column-1};}

    cout << "Reading " << filePath << endl;
    readFile(filePath, startLine, columnIndexes, vectorsToFill);
}

void printLumiColumn(int filePathIndex, int runNum, int algo) {

    vector<float> LB, LB_duration, Lumi;
    vector<vector<float>*> toFill = {&LB, &LB_duration, &Lumi};
    readIntegratedLumiFile(filePathIndex, runNum, algo, toFill);

    for (int i = 0 ; i < LB.size() ; i++) {
        cout << LB.at(i) << "\t" << Lumi.at(i) / LB_duration.at(i) << endl;
    }

}

Double_t getTimeFromLB(int filePathIndex, int runNum, int LB) {
    string filePath = getLumiFile(filePathIndex, runNum);

    int startLine = 4;
    vector<int> columnIndexes{2,4};
    if (filePathIndex == 0) {columnIndexes = {1,3};}

    vector<float> LBs, times;
    vector<vector<float>*> toFill = {&LBs,&times};

    readFile(filePath, startLine, columnIndexes, toFill);

    int closestTime = -1;
    int deltaLB = -1;
    for (int i = 0 ; i < LBs.size() ; i++) {
        if (fabs(LBs.at(i) - LB) < fabs(deltaLB) || deltaLB == -1) {
            deltaLB = LBs.at(i) - LB;
            closestTime = times.at(i);
        }
        if (LBs.at(i) == LB) return times.at(i);
    }
    

    return closestTime - 60 * deltaLB;

}

bool isInside(double eta, double phi, double eta1, double phi1, double eta2, double phi2) {
    return eta > eta1 && eta < eta2 && phi > phi1 && phi < phi2;
}

TGraph* getGraphFromTree(TTree * tree, TString x, TString y, TString cut = "") {
    tree->Draw(y + ":(" + x + ")", cut, "goff");
    // cout << y + ":(" + x + ")" << endl;
    TGraph *gr = new TGraph(tree->GetSelectedRows(), tree->GetV2(), tree->GetV1());
    return gr;
}

TCanvas * getDefaultCanvas(TString xLabel, TString yLabel, bool square, vector<TString> legends, bool grid, bool logScale) {
    TCanvas * c = new TCanvas("canvas","",600,600);
    if (!square) c->SetWindowSize(1500,600);
    if (grid) c->SetGrid();
    c->SetTitle("");
    c->SetBottomMargin(0.15);
    if (xLabel.Contains("frac")) c->SetBottomMargin(0.20);
    if (yLabel != "") c->SetLeftMargin(0.15);
    if (yLabel.Contains("#int")) c->SetLeftMargin(0.2);
    if (legends.size() > 0) c->SetRightMargin(0.15);
    for (TString legend : legends) {
        if (legend.Length() >= 12) c->SetRightMargin(0.18);
        if (legend.Length() >= 16) c->SetRightMargin(0.25);
    }
    if (legends.size() > 30) c->SetRightMargin(0.2);
    if (logScale) c->SetLogy();
    return c;
}

TCanvas * getDefaultDividedCanvas(TString yLabel, vector<TString> legends) {
    TCanvas * c = new TCanvas("","",600,700);
    c->SetGrid();
    c->SetTitle("");
    c->Divide(2);
    c->GetPad(1)->SetPad(0.,0.4,1.,1.);
    c->GetPad(2)->SetPad(0.,0.,1.,0.395);
    c->GetPad(1)->SetBottomMargin(0.01);
    if (yLabel != "") c->GetPad(1)->SetLeftMargin(0.15);
    if (yLabel != "") c->GetPad(2)->SetLeftMargin(0.15);
    c->GetPad(1)->SetRightMargin(0.15);
    c->GetPad(2)->SetRightMargin(0.15);
    if (legends.at(0).Length() >= 12) c->GetPad(1)->SetRightMargin(0.17);
    if (legends.at(0).Length() >= 12) c->GetPad(2)->SetRightMargin(0.17);
    // c->GetPad(2)->SetTopMargin(0);
    c->GetPad(2)->SetBottomMargin(0.25);
    c->GetPad(1)->SetGrid();
    c->GetPad(2)->SetGrid();
    return c;
}

TLegend * getDefaultLegend(vector<TString> legends) {
    double yStart = 0.4;
    double yEnd = 0.6;
    int sizeMultiplier = (legends.size() > 0 && legends.at(0).Contains("splitline")) ? 2 : 1;
    // if (legends.size()*sizeMultiplier > 5) legend = new TLegend(0.86,0.25,1.1,0.75);
    // if (legends.size()*sizeMultiplier > 15) legend = new TLegend(0.86,0.14,1.1,0.9);
    // if (legends.size()*sizeMultiplier > 30) legend = new TLegend(0.81,0.14,0.99,0.9);
    if (legends.size()*sizeMultiplier > 5) {
        yStart = (sizeMultiplier == 1) ? 0.25 : 0.15; 
        yEnd = (sizeMultiplier == 1) ? 0.74 : 0.95;
    }
    if (legends.size()*sizeMultiplier > 15) {yStart = 0.14; yEnd = 0.9;}
    if (legends.size()*sizeMultiplier > 30) {yStart = 0.14; yEnd = 0.9;}
    TLegend * legend = new TLegend(0.86,yStart,1.1,yEnd);
    for (TString text : legends) {
            if (text.Length() >= 12) legend = new TLegend(0.83,yStart,0.99,yEnd);
            if (text.Length() >= 16) legend = new TLegend(0.76,yStart,0.99,yEnd);
    }
    if (legends.size()*sizeMultiplier > 30) legend = new TLegend(0.82,yStart,1,yEnd);
    legend->SetMargin(0.05);
    legend->SetBorderSize(0.00);
    legend->SetFillColor(10);
    legend->SetTextSize(0.045);
    for (TString text : legends) {
        if (text.Length() >= 12) legend->SetTextSize(0.04);
        if (text.Length() >= 16) legend->SetTextSize(0.045);
    }
    if (legends.size() > 15) legend->SetTextSize(0.035);
    if (legends.size() > 30) legend->SetTextSize(0.034);
    if (legends.size() > 30) legend->SetNColumns(2);
    return legend;
}

// plotLabelGraph(graph, xLabels, labels, xLabel, yLabel, fileName, kBlue, 1, 20, {}, {}, square, strs, xStrings, yStrings, strColors, 0.04, lines, atlas_x, atlas_y, xLabelSize = 0.05, f1s, coloredLabels, "ap");
template<typename T>
pair<double,double> plotLabelGraph(T graph, vector<double> xLabels, vector<TString> labels, TString xLabel, TString yLabel, TString fileName, int color = kBlue, int markerSize = 1, int markerStyle = 20, vector<double> xRange = {}, vector<double> yRange = {}, bool square = false, vector<TString> strs = {}, vector<double> xStrings = {}, vector<double> yStrings = {}, vector<int> strColors = {}, double textSize = 0.03, vector<TLine*> lines = {}, double atlas_x = -1, double atlas_y = -1, double xLabelSize = 0.05, vector<TF1*> f1s = {}, vector<TString> coloredLabels = {}, TString drawStyle = "ap", double y_label_offset = 0.9) {
    gStyle->SetOptStat(0);
    if (xLabels.size() == 0) {
        for (int i = 0 ; i < labels.size() ; i++) xLabels.push_back(i);
    }

    TCanvas * c = getDefaultCanvas("", "", square, {}, true,false);
    // c->SetBottomMargin(0.21);
    // for (TString label : labels) if (label.Length() >= 10) c->SetBottomMargin(0.25);
    TH1D * hist = GetHistFromGraph(graph, xLabels.front()-0.5, xLabels.back()+0.5);
    hist->SetTitle("");
    hist->SetMarkerColor(color);
    hist->SetMarkerSize(markerSize);
    hist->SetMarkerStyle(markerStyle);
    hist->SetLineWidth(3);

    hist->Draw(drawStyle);

    if (lines.size() > 0) {
        for (int i = 0 ; i < lines.size() ; i++) {
            lines.at(i)->Draw("l same");
        }
    }

    for (auto f : f1s) f->Draw("same");

    TAxis * xAxis = hist->GetXaxis();

    for (int i = 0 ; i < labels.size() ; i++) {
        int bin = xAxis->FindBin(xLabels.at(i));
        // int bin = xAxis->FindBin(xLabels.at(i)) - 1;
        if (contains(labels.at(i), coloredLabels)) {
            xAxis->SetBinLabel(bin, Form("#color[%d]{" + labels.at(i) + "}", kRed));
        } else {
            xAxis->SetBinLabel(bin, labels.at(i));
        }
    }
    if (labels.size() > 25) xAxis->LabelsOption("v");
    // xAxis->SetNdivisions(0,0,0);

    TAxis * yAxis = hist->GetYaxis();

    if (yRange.size() == 2) { 
        hist->SetMinimum(yRange[0]);
        hist->SetMaximum(yRange[1]); 
    }
    xAxis->SetTitle(xLabel);

    xAxis->SetLabelSize(xLabelSize);
    xAxis->SetTitleSize(0.05);
    yAxis->SetTitleSize(0.055);
    if (yLabel.Length() > 20) yAxis->SetTitleSize(0.04);
    yAxis->SetTitle(yLabel);
    yAxis->SetTitleOffset(y_label_offset);
    yAxis->SetLabelSize(0.045);
    
    if (xRange.size() == 2) xAxis->SetLimits(xRange[0],xRange[1]);
    // else {
    //     double extra_margin = (xAxis->GetXmax() - xAxis->GetXmin()) * 0.15;
    //     xAxis->SetRangeUser(xAxis->GetXmin() - extra_margin,xAxis->GetXmax() + extra_margin);
    // }

    if (atlas_x != -1) ATLASLabel(atlas_x,atlas_y,"Internal");

    if (strs.size() > 0) {
        TLatex latex; latex.SetNDC(); latex.SetTextSize(textSize); latex.SetTextAlign(13);
        for (int i = 0 ; i < strs.size() ;  i++){ latex.SetTextColor(strColors.at(i));latex.DrawLatex(xStrings.at(i),yStrings.at(i),strs.at(i));}
    }
    
    // c->Print(fileName + ".eps");
    c->Print(fileName + ".png");

    delete c;

    return make_pair(yAxis->GetXmin(),yAxis->GetXmax());
}

// Works with just one graph without legend!
// plotLabelMultigraph( graphs, xLabels, labels, legends, "xLabel", "yLabel", fileName, colors, {markersSizes}, {markerStyles}, {xRange}, {yRange}, square false, {strs}, {xStrings}, {yStrings}, {strColors}, 0.03, {TLines}, atlas_x, atlas_y, xLabelSize 0.05, f1s, coloredLabels, y_label_offset 0.9, "p");
template<typename T>
pair<double,double> plotLabelMultigraph(vector<T> graphs, vector<double> xLabels, vector<TString> labels, vector<TString> legends, TString xLabel, TString yLabel, TString fileName, vector<int> colors, vector<int> markersSizes = {}, vector<int> markerStyles = {}, vector<double> xRange = {}, vector<double> yRange = {}, bool square = false, vector<TString> strs = {}, vector<double> xStrings = {}, vector<double> yStrings = {}, vector<int> strColors = {}, double textSize = 0.03, vector<TLine*> lines = {}, double atlas_x = -1, double atlas_y = -1, double xLabelSize = 0.05, vector<TF1*> f1s = {}, vector<TString> coloredLabels = {}, double y_label_offset = 0.9, TString drawStyle = "p") {

    if (xLabels.size() == 0) {
        for (int i = 0 ; i < labels.size() ; i++) xLabels.push_back(i);
    }
    TCanvas * c = getDefaultCanvas("", "", square, legends, true,false);
    // c->SetBottomMargin(0.21);
    // for (TString label : labels) if (label.Length() >= 10) c->SetBottomMargin(0.25);
    TLegend * legend = getDefaultLegend(legends);

    THStack * hs = new THStack("","");
    // TMultiGraph * mg = new TMultiGraph();
    for (int i = 0 ; i < graphs.size() ; i++) {
        auto graph = graphs.at(i);
        TH1D * hist = GetHistFromGraph(graph, xLabels.front()-0.5, xLabels.back()+0.5);
        hist->SetMarkerColor(colors.at(i));
        hist->SetLineColor(colors.at(i));
        hist->SetLineWidth(3);
        if (markersSizes.size() > 0) hist->SetMarkerSize(markersSizes.at(i)); else hist->SetMarkerSize(1);
        if (markerStyles.size() > 0) hist->SetMarkerStyle(markerStyles.at(i)); else hist->SetMarkerStyle(20);
        if (markerStyles.size() > 0) hist->SetLineStyle(markerStyles.at(i));
        // mg->Add(graph);
        hs->Add(hist);
        // if (legends.size() > 0) legend->AddEntry(graph,legends[i],"p");
        if (legends.size() > 0) legend->AddEntry(hist,legends[i], (drawStyle == "hist") ? "l" : "p");
    }
    // mg->Draw("ap");
    hs->Draw(drawStyle + " nostack");

    if (lines.size() > 0) {
        for (int i = 0 ; i < lines.size() ; i++) {
            if (colors.size() > legends.size() && legends.size() > 0) {
                lines.at(i)->SetLineColor(colors.at(graphs.size() + i));
                lines.at(i)->SetLineWidth(4);
                legend->AddEntry(lines.at(i),legends[graphs.size() + i],"l");
            } 
            lines.at(i)->Draw("l same");
        }
    }

    for (auto f : f1s) f->Draw("same");

    TAxis * xAxis = hs->GetXaxis();

    for (int i = 0 ; i < labels.size() ; i++) {
        int bin = xAxis->FindBin(xLabels.at(i));
        if (contains(labels.at(i), coloredLabels)) {
            xAxis->SetBinLabel(bin, Form("#color[%d]{" + labels.at(i) + "}", kRed));
        } else {
            xAxis->SetBinLabel(bin, labels.at(i));
        }
    }
    if (labels.size() > 25) xAxis->LabelsOption("v");
    // xAxis->LabelsOption("i");
    xAxis->SetNdivisions(0,0,0);

    TAxis * yAxis = hs->GetYaxis();

    if (yRange.size() == 2) { 
        hs->SetMinimum(yRange[0]);
        hs->SetMaximum(yRange[1]); 
    }
    // yAxis->SetRangeUser(0.9,1.1); 
    hs->GetXaxis()->SetTitle(xLabel);

    xAxis->SetLabelSize(xLabelSize);
    xAxis->SetTitleSize(0.05);
    yAxis->SetTitleSize(0.055);
    if (yLabel.Length() > 20) yAxis->SetTitleSize(0.04);
    yAxis->SetTitle(yLabel);
    yAxis->SetTitleOffset(y_label_offset);
    yAxis->SetLabelSize(0.045);
    
    if (xRange.size() == 2) xAxis->SetLimits(xRange[0],xRange[1]);
    // else {
    //     double extra_margin = (xAxis->GetXmax() - xAxis->GetXmin()) * 0.15;
    //     xAxis->SetRangeUser(xAxis->GetXmin() - extra_margin,xAxis->GetXmax() + extra_margin);
    // }

    if (legends.size() > 0) legend->Draw();

    if (atlas_x != -1) ATLASLabel(atlas_x,atlas_y,"Internal");

    if (strs.size() > 0) {
        TLatex latex; latex.SetNDC(); latex.SetTextSize(textSize); latex.SetTextAlign(13);
        for (int i = 0 ; i < strs.size() ;  i++){ latex.SetTextColor(strColors.at(i));latex.DrawLatex(xStrings.at(i),yStrings.at(i),strs.at(i));}
    }
    
    // c->Print(fileName + ".eps");
    c->Print(fileName + ".png");

    delete c;

    return make_pair(yAxis->GetXmin(),yAxis->GetXmax());
}

template<typename T>
TMultiGraph * prepareMultigraph(vector<T> graphs, vector<TString> legends, TLegend * legend, vector<int> colors, vector<int> markersSizes, vector<int> markerStyles) {
    TMultiGraph * mg = new TMultiGraph();
    for (int i = 0 ; i < graphs.size() ; i++) {
        auto graph = graphs.at(i);
        if (graph->GetN() == 0) continue;
        graph->SetMarkerColor(colors.at(i));
        graph->SetLineColor(colors.at(i));
        graph->SetLineWidth(3);
        if (markersSizes.size() > 0) graph->SetMarkerSize(markersSizes.at(i)); else graph->SetMarkerSize(1);
        if (markerStyles.size() > 0) graph->SetMarkerStyle(markerStyles.at(i)); else graph->SetMarkerStyle(20);
        mg->Add(graph);
        legend->AddEntry(graph," " + legends[i],"p");
    }
    return mg;
}

template<typename T>
TMultiGraph * prepareMultigraphDoubleSized(vector<T> graphs, vector<int> colors, vector<double> markersSizes, vector<int> markerStyles) {
    TMultiGraph * mg = new TMultiGraph();
    for (int i = 0 ; i < graphs.size() ; i++) {
        auto graph = graphs.at(i);
        if (graph->GetN() == 0) continue;
        graph->SetMarkerColor(colors.at(i));
        graph->SetLineColor(colors.at(i));
        graph->SetLineWidth(3);
        if (markersSizes.size() > 0) graph->SetMarkerSize(markersSizes.at(i)); else graph->SetMarkerSize(1);
        if (markerStyles.size() > 0) graph->SetMarkerStyle(markerStyles.at(i)); else graph->SetMarkerStyle(20);
        mg->Add(graph);
    }
    return mg;
}

template<typename T>
TGraph * getResidueGraph(T graph, vector<double> valueForResidueVector) {
    TGraph * res_graph = new TGraph(); 
    for (int i_point = 0 ; i_point < graph->GetN() ; i_point++) {
        res_graph->SetPoint(i_point, graph->GetX()[i_point], graph->GetY()[i_point] - valueForResidueVector.at(i_point));
    }
    return res_graph;
}

template<typename T>
TMultiGraph * prepareResidueMultigraph(vector<T> graphs, vector<vector<double>> valueForResidueVectors, vector<int> colors, vector<int> markersSizes, vector<int> markerStyles) {
    TMultiGraph * mg = new TMultiGraph();
    for (int i = 0 ; i < graphs.size() ; i++) {
        T res_graph = getResidueGraph(graphs.at(i), valueForResidueVectors.at(i));
        res_graph->SetMarkerColor(colors.at(i));
        if (markersSizes.size() > 0) res_graph->SetMarkerSize(markersSizes.at(i)); else res_graph->SetMarkerSize(1);
        if (markerStyles.size() > 0) res_graph->SetMarkerStyle(markerStyles.at(i)); else res_graph->SetMarkerStyle(20);
        mg->Add(res_graph);
    }
    return mg;
}

TAxis * prepareMultiGraphAxis(TMultiGraph * mg, vector<double> xRange, vector<double> yRange, TString xLabel, TString yLabel, double xTitleSize, double yTitleSize, double xLabelSize, double yLabelSize, double yOffset = 0, bool logScale = false) {
    TAxis * xAxis = mg->GetXaxis();
    TAxis * yAxis = mg->GetYaxis();
    // if (xRange.size() == 2) xAxis->SetLimits(xRange[0],xRange[1]);
    if (xRange.size() == 2) xAxis->SetRangeUser(xRange[0],xRange[1]);
    else {
        double extra_margin = (xAxis->GetXmax() - xAxis->GetXmin()) * 0.25;
        xAxis->SetRangeUser(xAxis->GetXmin() - extra_margin,xAxis->GetXmax() + extra_margin);
    }

    if (yRange.size() == 2) {mg->GetYaxis()->SetRangeUser(yRange[0],yRange[1]); }
    else if (!logScale) {
        double margin = (yAxis->GetXmax() - yAxis->GetXmin()) * 0.1;
        mg->GetYaxis()->SetRangeUser(yAxis->GetXmin() - margin, yAxis->GetXmax() + margin);
    }

    xAxis->SetTitle(xLabel);
    xAxis->SetTitleSize(xTitleSize);
    xAxis->SetLabelSize(xLabelSize);
    yAxis->SetTitle(yLabel);
    yAxis->SetTitleSize(yTitleSize);
    yAxis->SetLabelSize(yLabelSize);
    if (yOffset != 0) yAxis->SetTitleOffset(yOffset);

    return yAxis;
}


void drawStrs(vector<TString> strs, vector<double> xStrings, vector<double> yStrings, vector<int> strColors, double textSize, vector<int> strCoordInNDC = {}) {
    if (strs.size() > 0) {
        TLatex latex; latex.SetNDC(); latex.SetTextAlign(13);
        for (int i = 0 ; i < strs.size() ;  i++){ 
            latex.SetTextSize(textSize);
            if (strs.at(i).Length() > 30 && strs.at(i).Contains("#[]")) latex.SetTextSize(0.03);
            latex.SetTextColor(strColors.at(i));
            if (strCoordInNDC.size() == 0) latex.DrawLatex(xStrings.at(i),yStrings.at(i),strs.at(i));
            else {
                if (strCoordInNDC.at(i) == 1) {latex.DrawLatex(xStrings.at(i),yStrings.at(i),strs.at(i));}
                else {latex.DrawLatex(GetXNDC(xStrings.at(i)), GetYNDC(yStrings.at(i)),strs.at(i));}
            }
        }
    }
}

template<typename T>
void plotResidueMultigraph(vector<T> graphs, vector<vector<double>> valueForResidueVectors, vector<TString> legends, TString xLabel, TString yLabel, TString fileName, vector<int> colors, vector<int> markersSizes = {}, vector<int> markerStyles = {}, vector<double> xRange = {}, vector<double> yRange = {}, vector<TString> strs = {}, vector<double> xStrings = {}, vector<double> yStrings = {}, vector<int> strColors = {}, double textSize = 0.03, float atlas_x = -1, float atlas_y = -1) {

    TCanvas * c = getDefaultDividedCanvas(yLabel, legends);
    TLegend * legend = getDefaultLegend(legends);
    TMultiGraph * mg = prepareMultigraph(graphs, legends, legend, colors, markersSizes, markerStyles);
    TMultiGraph * mg_res = prepareResidueMultigraph(graphs, valueForResidueVectors, colors, markersSizes, markerStyles);

    c->cd(1);
    mg->Draw("ap");
    prepareMultiGraphAxis(mg, xRange, yRange, "", yLabel, 0, 0.055, 0, 0.07);
    mg->Draw("ap");
    legend->Draw();

    drawStrs(strs, xStrings, yStrings, strColors, textSize, {});
    if (atlas_x != -1) ATLASLabel(atlas_x,atlas_y,"Internal");

    c->cd(2);
    mg_res->Draw("ap");
    prepareMultiGraphAxis(mg_res, xRange, {-0.003,0.003}, xLabel, "Residues", 0.07, 0.075, 0.07, 0.095,1.5);
    mg_res->Draw("ap");




    c->Update();

    // c->Print(fileName + ".eps");
    c->Print(fileName + ".png");

    delete c;

}



// plotMultigraph(graphs, legends, xLabel, yLabel, fileName, colors, markersSizes, markerStyles, xRange, yRange, square, strs, xStrings, yStrings, strColors, 0.03, f1s, atlas_x, atlas_y, {}, "ap", logScale, useDate);
template <typename T>
pair<double,double> plotMultigraph(vector<T> graphs, vector<TString> legends, TString xLabel, TString yLabel, TString fileName, vector<int> colors, vector<int> markersSizes = {}, vector<int> markerStyles = {}, vector<double> xRange = {}, vector<double> yRange = {}, bool square = false, vector<TString> strs = {}, vector<double> xStrings = {}, vector<double> yStrings = {}, vector<int> strColors = {}, double textSize = 0.03, vector<TF1*> f1s = {}, float atlas_x = -1, float atlas_y = -1, vector<int> strCoordInNDC = {}, TString drawStyle = "ap", bool logScale = false, bool useDate = false, T extra_graph = nullptr, TString extra_legend = "", TString extra_drawStyle = "p", int extra_color = kBlack, int extra_style = 20, double extra_size = 1) {
    // cout << "THIS ONE" << endl;
    if (extra_graph) legends.push_back(extra_legend);
    TCanvas * c = getDefaultCanvas(xLabel, yLabel, square, legends, true, logScale);
    TLegend * legend = getDefaultLegend(legends);
    TMultiGraph * mg = prepareMultigraph(graphs, legends, legend, colors, markersSizes, markerStyles);

    mg->Draw(drawStyle);
    TAxis * yAxis = prepareMultiGraphAxis(mg, xRange, yRange, xLabel, yLabel, 0.055, 0.055, 0.045, 0.05, 1.25, logScale);
    mg->Draw(drawStyle);

    if (extra_graph) {
        extra_graph->SetMarkerColor(extra_color);
        extra_graph->SetMarkerStyle(extra_style);
        extra_graph->SetMarkerSize(extra_size);
        extra_graph->Draw(extra_drawStyle + " same");
        legend->AddEntry(extra_graph, extra_legend,"p");
    }

    legend->Draw();

    drawStrs(strs, xStrings, yStrings, strColors, textSize, strCoordInNDC);

    for (auto f1 : f1s) f1->Draw("same");

    if (xRange.size() == 2) {
        mg->GetXaxis()->SetLimits(xRange.at(0),xRange.at(1));
    }

    if (useDate) {
        mg->GetXaxis()->SetTimeDisplay(1);
        mg->GetXaxis()->SetNdivisions(-503);
        mg->GetXaxis()->SetTimeFormat("%b %d");
        mg->GetXaxis()->SetTimeOffset(0,"gmt");
    }

    if (atlas_x != -1) ATLASLabel(atlas_x,atlas_y,"Internal");

    c->Update();

    if (fileName != "TMP") {
        c->Print(fileName + ".png");
    }

    delete c;

    return make_pair(yAxis->GetXmin(),yAxis->GetXmax());
}

// plotMultigraph( graphs, legends, "xLabel", "yLabel", fileName, colors, {markerSizes}, {markerStyles}, {xRange}, {yRange}, square false, {str}, {xStrings}, {yStrings}, {strColors}, testSize 0.03, vector<TF1*> f1s = {}, vector<int> f1_colors = {}, vector<TString> f1_legends = {}, bool extend, f2s = {});
pair<double,double> plotMultigraph(vector<TGraph*> graphs, vector<TString> legends, TString xLabel, TString yLabel, TString fileName, vector<int> colors, vector<int> markersSizes = {}, vector<int> markerStyles = {}, vector<double> xRange = {}, vector<double> yRange = {}, bool square = false, vector<TString> strs = {}, vector<double> xStrings = {}, vector<double> yStrings = {}, vector<int> strColors = {}, double textSize = 0.03, vector<TF1*> f1s = {}, vector<int> f1_colors = {}, vector<TString> f1_legends = {}, bool extend = false, vector<TF1*> f2s = {}) {
    if (legends.size() == 0) {coutError("No graphs passed"); return make_pair(0,0);}

    TCanvas * c = getDefaultCanvas(xLabel, yLabel, square, legends, true, false);
    TLegend * legend = getDefaultLegend(legends);
    TMultiGraph * mg = prepareMultigraph(graphs, legends, legend, colors, markersSizes, markerStyles);


    mg->Draw("ap");
    TAxis * xAxis = mg->GetXaxis();
    TAxis * yAxis = mg->GetYaxis();
    if (xRange.size() == 2) xAxis->SetLimits(xRange[0],xRange[1]);
    else {
        double extra_margin = (xAxis->GetXmax() - xAxis->GetXmin()) * 0.25;
        xAxis->SetRangeUser(xAxis->GetXmin() - extra_margin,xAxis->GetXmax() + extra_margin);
    }

    if (yRange.size() == 2) {mg->GetYaxis()->SetRangeUser(yRange[0],yRange[1]); }
    else {
        double margin = (yAxis->GetXmax() - yAxis->GetXmin()) * 0.1;
        mg->GetYaxis()->SetRangeUser(yAxis->GetXmin() - margin, yAxis->GetXmax() + margin);
    }
    mg->Draw("ap");
    xAxis->SetTitle(xLabel);
    xAxis->SetTitleSize(0.05);
    xAxis->SetLabelSize(0.03);
    yAxis->SetTitle(yLabel);
    yAxis->SetTitleSize(0.055);
    yAxis->SetLabelSize(0.07);

    if (strs.size() > 0) {
        TLatex latex; latex.SetNDC(); latex.SetTextSize(textSize); latex.SetTextAlign(13);
        for (int i = 0 ; i < strs.size() ;  i++){ latex.SetTextColor(strColors.at(i));latex.DrawLatex(xStrings.at(i),yStrings.at(i),strs.at(i));}
    }

    if (f1s.size() != 0) {
        for (int i = 0 ; i < f1s.size() ; i++) {
            f1s.at(i)->SetLineColor(f1_colors.at(i));
            if (f1_legends.size() != 0) legend->AddEntry(f1s.at(i),f1_legends.at(i), "l");
            f1s.at(i)->Draw("same");

            if (extend) {
                TF1 * f_extend = new TF1(*(f1s.at(i)));
                f_extend->SetRange(f1s.at(i)->GetXmin()-10000,f1s.at(i)->GetXmax()+100000);
                f_extend->SetLineStyle(5);
                f_extend->SetLineWidth(1);
                f_extend->SetLineColor(f1_colors.at(i));
                f_extend->Draw("same");
            }

            for(auto f2 : f2s) {
                f2->SetLineColor(f1_colors.at(i));
                f2->Draw("same");
            }

        }
    }

    legend->Draw();

    if (fileName != "TMP") {
        // c->Print(fileName + ".eps");
        c->Print(fileName + ".png");
    }

    delete c;

    return make_pair(yAxis->GetXmin(),yAxis->GetXmax());
}


void plotLines(vector<TF1*> lines, vector<TString> legends, TString xLabel, TString yLabel, TString fileName, vector<double> yRange ={}, vector<int> lineStyles = {}, vector<TString> styleLegends = {}, bool square = false) {
    TCanvas * c = getDefaultCanvas(xLabel,yLabel,square,legends,true,false);
    TLegend * legend = getDefaultLegend(legends);
    for (int i_line = 0 ; i_line < lines.size() ; i_line++) {
        TF1 * line = lines.at(i_line);
        if (i_line < legends.size()) legend->AddEntry(line, legends.at(i_line),"l");
        if (yRange.size() != 0) {
            line->SetMaximum(yRange.at(1));
            line->SetMinimum(yRange.at(0));
        }
        if (i_line == 0) line->Draw();
        else             line->Draw("same");
    }
    legend->Draw();

    if (lineStyles.size() > 0) {
        TLegend * legend_style = new TLegend(0.86,0.8,1.1,0.9);
        for (int i_style = 0 ; i_style < lineStyles.size() ; i_style++) {
            TLine * line_place = new TLine(0,0,0,0); line_place->SetLineWidth(0); line_place->SetLineColor(kBlack);
            line_place->SetLineStyle(lineStyles.at(i_style));
            legend_style->AddEntry(line_place,styleLegends.at(i_style),"l");
        }
        legend_style->Draw();
    }

    c->Print(fileName + ".png");

    delete c;
}

pair<double,double> plotGraphTH2D(TH2D * graph, TString xLabel, TString yLabel, TString fileName, int color = kBlue, double markerSize = 1, int markerStyle = 20, double xOffset = 0, double yOffset = 0, vector<double> xRange={}, vector<double> yRange={}, TF1 * f1 = 0, vector<string> strs = {}, vector<double>xStrings = {}, vector<double> yStrings = {}, vector<int> strColors = {}, TLine * line = 0, bool square = false, double textSize = 0.03) {
    TCanvas * c = new TCanvas("","",600,600); c->SetWindowSize(600,600);
    if (!square) c->SetWindowSize(1500,600);
    c->SetGrid();
    c->SetTitle("");
    graph->SetTitle("");
    graph->SetMarkerColor(color);
    graph->SetMarkerStyle(markerStyle);
    graph->SetMarkerSize(markerSize);
    graph->GetXaxis()->SetTitleOffset(xOffset);
    if (xOffset > 0) c->SetBottomMargin(0.15);
    if (yOffset > 0) c->SetLeftMargin(yOffset);
    TAxis * yAxis = graph->GetYaxis();
    if (xRange.size() == 2) graph->GetXaxis()->SetLimits(xRange[0],xRange[1]);
    graph->Draw("p");
    if (yRange.size() == 2) {yAxis->SetLimits(yRange[0],yRange[1]);}
    graph->Draw("p");
    graph->GetXaxis()->SetTitle(xLabel);
    yAxis->SetTitle(yLabel);
    graph->GetXaxis()->SetTitleSize(0.05);
    yAxis->SetTitleSize(0.055);

    graph->GetXaxis()->SetLabelSize(0.06);
    yAxis->SetLabelSize(0.06);

    if (f1 != 0) f1->Draw("same");
    if (strs.size() > 0) {
        TLatex latex; latex.SetNDC(); latex.SetTextSize(textSize); latex.SetTextAlign(13);
        for (int i = 0 ; i < strs.size() ;  i++){ latex.SetTextColor(strColors.at(i));latex.DrawLatex(xStrings.at(i),yStrings.at(i),strs.at(i).c_str());}
    }
    if (line != 0) line->Draw("same");
    c->Modified();

    // c->Print(fileName + ".eps");
    c->Print(fileName + ".png");

    delete c;
    return make_pair(yAxis->GetXmin(),yAxis->GetXmax());
}

// plotGraph(graph, xLabel, yLabel, fileName, kBlue, 1, 20, 0, 0, xRange, yRange, f1, {}, {}, {}, {}, line, square, 0.045, logScale, atlas_x, atlas_y, -999, useDate, "ap");
template <typename T>
pair<double,double> plotGraph(T graph, TString xLabel, TString yLabel, TString fileName, int color = kBlue, int markerSize = 1, int markerStyle = 20, double xOffset = 0, double yOffset = 0, vector<double> xRange={}, vector<double> yRange={}, TF1 * f1 = 0, vector<TString> strs = {}, vector<double>xStrings = {}, vector<double> yStrings = {}, vector<int> strColors = {}, TLine * line = 0, bool square = false, double textSize = 0.03, bool logScale = false, double atlas_x = -1, double atlas_y = -1, double horizontalLine = -999, bool useDate = false, TString drawStyle = "ap") {
    TCanvas * c = new TCanvas("","",600,600); c->SetWindowSize(600,600);
    if (!square) c->SetWindowSize(1500,600);
    c->SetGrid();
    c->SetTitle("");
    if (logScale) c->SetLogy();
    graph->SetTitle("");
    graph->SetMarkerColor(color);
    graph->SetLineColor(color);
    graph->SetMarkerStyle(markerStyle);
    graph->SetMarkerSize(markerSize);
    graph->GetXaxis()->SetTitleOffset(xOffset);

    if (useDate) {
        graph->GetXaxis()->SetTimeDisplay(1);
        graph->GetXaxis()->SetNdivisions(-503);
        graph->GetXaxis()->SetTimeFormat("%b %d");
        graph->GetXaxis()->SetTimeOffset(0,"gmt");
    }

    if (xLabel != "") c->SetBottomMargin(0.15);
    if (yLabel != "") c->SetLeftMargin(0.18);
    //graph->GetYaxis()->SetTitleOffset(yOffset);
    // TString drawStyle = (markerStyle == 1) ? "apl" : "ap";
    graph->Draw(drawStyle);
    TAxis * yAxis = graph->GetYaxis();
    if (yOffset > 0) c->SetLeftMargin(yOffset);
    if (xRange.size() == 2) graph->GetXaxis()->SetLimits(xRange[0],xRange[1]);
    if (yRange.size() == 2) {yAxis->SetLimits(yRange[0],yRange[1]);graph->GetHistogram()->SetMinimum(yRange[0]);graph->GetHistogram()->SetMaximum(yRange[1]);}
    graph->Draw(drawStyle);
    graph->GetXaxis()->SetTitle(xLabel);
    yAxis->SetTitle(yLabel);
    graph->GetXaxis()->SetTitleSize(0.05);
    yAxis->SetTitleSize(0.055);
    // graph->Draw(drawStyle);
    if (f1 != 0) f1->Draw("same");
    if (strs.size() > 0) {
        TLatex latex; latex.SetNDC(); latex.SetTextSize(textSize); latex.SetTextAlign(13);
        for (int i = 0 ; i < strs.size() ;  i++){ latex.SetTextColor(strColors.at(i));latex.DrawLatex(xStrings.at(i),yStrings.at(i),strs.at(i));}
    }
    if (line != 0) line->Draw("same");

    if (horizontalLine != -999) {
        TLine * horLine = new TLine(graph->GetXaxis()->GetXmin(), horizontalLine, graph->GetXaxis()->GetXmax(), horizontalLine);
        horLine->SetLineStyle(9);
        horLine->SetLineWidth(2);
        horLine->Draw("same");
    }

    c->Update();

    if (atlas_x != -1) ATLASLabel(atlas_x,atlas_y,"Internal");

    if (graph->GetN() > 20000) {
        c->Print(fileName + ".eps");
    } else {
        c->Print(fileName + ".png");
    }

    delete c;

    

    return make_pair(yAxis->GetXmin(),yAxis->GetXmax());
}


template <typename T>
void plotRMSvsX(vector<T> graphs, TString xLabel, TString yLabel, TString fileName, int color, int markerSize = 1, int markerStyle = 20, vector<double> xRange = {}, vector<double> yRange = {}, bool square = false, vector<TString> strs = {}, vector<double> xStrings = {}, vector<double> yStrings = {}, vector<int> strColors = {}, double textSize = 0.03, TString drawStyle = "ap", double atlas_x = -1, double atlas_y = -1) {

    if (graphs.size() == 0) return;

    vector<double> x_values = GetXVector(graphs.at(0));
    vector<double> y_rms;

    for (int i_x = 0 ; i_x < x_values.size() ; i_x++) {
        vector<double> y_values_at_x;
        for (auto graph : graphs) {
            for (int i_point = 0 ; i_point < graph->GetN() ; i_point++) {
                Double_t x,y;
                graph->GetPoint(i_point, x, y);
                if (x == x_values.at(i_x)) y_values_at_x.push_back(y);
            }
        }
        double rms = GetRMS(y_values_at_x);
        y_rms.push_back(rms);
    }
    TGraph * graph_rms = new TGraph(x_values.size(), &x_values.at(0), &y_rms.at(0));
    plotGraph(graph_rms, xLabel, yLabel, fileName, kBlue, markerSize, markerStyle, 0, 0, xRange, yRange, 0, strs, xStrings, yStrings, strColors, 0, square, textSize, false, atlas_x, atlas_y, -999, false, drawStyle);
}

pair<double,double> plotTimeAxisGraph(TGraph * graph, TString xLabel, TString yLabel, TString fileName, int color = kBlue, int markerSize = 1, int markerStyle = 20, double xOffset = 0, double yOffset = 0, vector<double> xRange={}, vector<double> yRange={}, TF1 * f1 = 0, vector<TString> strs = {}, vector<double>xStrings = {}, vector<double> yStrings = {}, vector<int> strColors = {}, TLine * line = 0, bool square = false, double textSize = 0.03) {
    TCanvas * c = getDefaultCanvas(xLabel, yLabel, square, {}, true, false);
    graph->SetTitle("");
    graph->SetMarkerColor(color);
    graph->SetMarkerStyle(markerStyle);
    graph->SetMarkerSize(markerSize);
    graph->GetXaxis()->SetTitleOffset(xOffset);
    TString drawStyle = (markerStyle == 1) ? "apl" : "ap";
    graph->Draw(drawStyle);
    TAxis * yAxis = graph->GetYaxis();
    if (yOffset > 0) c->SetLeftMargin(yOffset);
    if (xRange.size() == 2) graph->GetXaxis()->SetLimits(xRange[0],xRange[1]);
    if (yRange.size() == 2) {yAxis->SetLimits(yRange[0],yRange[1]);graph->GetHistogram()->SetMinimum(yRange[0]);graph->GetHistogram()->SetMaximum(yRange[1]);}
    graph->Draw(drawStyle);
    graph->GetXaxis()->SetTimeDisplay(1);
    graph->GetXaxis()->SetNdivisions(-503);
    graph->GetXaxis()->SetTimeFormat("%b %d");
    graph->GetXaxis()->SetTimeOffset(0,"gmt");
    graph->GetXaxis()->SetTitle(xLabel);
    yAxis->SetTitle(yLabel);
    graph->GetXaxis()->SetTitleSize(0.05);
    yAxis->SetTitleSize(0.055);
    if (f1 != 0) f1->Draw("same");
    if (strs.size() > 0) {
        TLatex latex; latex.SetNDC(); latex.SetTextSize(textSize); latex.SetTextAlign(13);
        for (int i = 0 ; i < strs.size() ;  i++){ latex.SetTextColor(strColors.at(i));latex.DrawLatex(xStrings.at(i),yStrings.at(i),strs.at(i));}
    }
    if (line != 0) line->Draw("same");

    c->Update();

    // c->Print(fileName + ".eps");
    c->Print(fileName + ".png");

    delete c;

    

    return make_pair(yAxis->GetXmin(),yAxis->GetXmax());
}

// stackHist( hists,  legends,  colors, "xLabel", fileName, logScale false, "yLabel", {lineStyle}, normalise true, {xRange}, {yRange}, square true, {strs}, {xStrings}, {yStrings}, {strColors}, f1 0, textSize 0.03, "e1");
pair<double,double>  stackHist(vector<TH1D*> hists, vector<TString> legends, vector<int> colors, TString xLabel, TString fileName, bool logScale = false, TString yLabel = "", vector<int> lineStyle = {}, bool normalise = true, vector<double> xRange = {}, vector<double> yRange = {}, bool square = true, vector<TString> strs = {}, vector<double>xStrings = {}, vector<double> yStrings = {}, vector<int> strColors = {}, TF1 * f1 = 0, double textSize = 0.03, TString drawStyle = "hist", double atlas_x = -1, double atlas_y = -1) {
    TCanvas * c = getDefaultCanvas(xLabel, yLabel, square, legends, true, false);
    THStack * hs = new THStack("","");
    TLegend * legend = getDefaultLegend(legends);
    for (int i = 0 ; i < hists.size() ; i++) {
        TH1D * clone = (TH1D*)hists[i]->Clone();
        clone->SetLineColor(colors[i]);
        clone->SetTitle("");
        if (normalise) clone->Scale(1./clone->Integral("width"));
        if (lineStyle.size() > 0) clone->SetLineStyle(lineStyle[i]);
        clone->SetLineWidth(3);
        if (drawStyle == "p") clone->SetMarkerStyle(20);
        if (drawStyle == "p") clone->SetMarkerColor(colors[i]);
        // if (xRange.size() == 2) {clone->SetAxisRange(xRange[0],xRange[1], "X");}
        if (xRange.size() == 2) {clone->GetXaxis()->SetRangeUser(xRange[0],xRange[1]);}
        hs->Add(clone);
        legend->AddEntry(clone,legends[i],"le");
    }
    hs->Draw(drawStyle + " nostack");
    TAxis * xAxis = hs->GetXaxis();
    hs->GetXaxis()->SetTitle(xLabel);
    hs->GetYaxis()->SetTitle(yLabel);
    hs->GetXaxis()->SetTitleSize(0.06);
    // if (xLabel.Contains("frac")) hs->GetXaxis()->SetTitleSize(0.045);
    hs->GetXaxis()->SetTitleOffset(1);
    if (xLabel.Contains("frac")) hs->GetXaxis()->SetTitleOffset(1.2);
    hs->GetXaxis()->SetNdivisions(5);
    hs->GetXaxis()->SetLabelSize(0.06);
    hs->GetYaxis()->SetTitleSize(0.06);
    hs->GetYaxis()->SetLabelSize(0.06);
    hs->GetYaxis()->SetTitleOffset(1.2);

    if (logScale) c->SetLogy();
    if (xRange.size() == 2) { hs->GetXaxis()->SetLimits(xRange[0],xRange[1]);}
    if (yRange.size() > 0) {
        hs->SetMinimum(yRange[0]);
        if (yRange.size() == 2) hs->SetMaximum(yRange[1]);
    }
    c->Modified();
    legend->Draw();

    if (atlas_x != -1) ATLASLabel(atlas_x,atlas_y,"Internal");

    if (strs.size() > 0) {
        TLatex latex; latex.SetNDC(); latex.SetTextSize(textSize); latex.SetTextAlign(13);
        for (int i = 0 ; i < strs.size() ;  i++){ latex.SetTextColor(strColors.at(i));latex.DrawLatex(xStrings.at(i),yStrings.at(i),strs.at(i));}
    }

    if (f1 != 0) f1->Draw("same");

    // c->Print(fileName + ".eps");
    c->Print(fileName + ".png");
    // c->Print(fileName + ".root");

    delete c;

    return make_pair(xAxis->GetXmin(),xAxis->GetXmax());
}

void stackHistGraph(TH1D * hist, TGraph * graph, vector<TString> legends, TString fileName, vector<int> colors = {kBlue,kRed}, TString xLabel = "", TString yLabel = "", bool logScale = false, vector<int> lineStyle = {}, vector<double> xRange = {}, vector<double> yRange = {}, bool square = true, vector<string> strs = {}, vector<double>xStrings = {}, vector<double> yStrings = {}, vector<int> strColors = {}, TF1 * f1 = 0, double textSize = 0.03) {
    TCanvas * c = new TCanvas("","",700,500);
    if (square) c->SetWindowSize(600,600);
    c->SetGrid();
    if (yLabel.Length() > 0) c->SetLeftMargin(0.15);
    c->SetRightMargin(0.2);
    TLegend * legend = new TLegend(0.83,0.4,0.9,0.6);
    legend->SetBorderSize(0.00);
    legend->SetFillColor(10);
    legend->SetTextSize(0.04);
    legend->SetMargin(0.05);

    TH1D * clone = (TH1D*)hist->Clone("hist");
    clone->SetLineColor(colors[0]);
    clone->SetLineWidth(3);
    clone->SetTitle("");
    if (lineStyle.size() > 0) clone->SetLineStyle(lineStyle[0]);
    legend->AddEntry(clone,legends[0],"le");

    float maxHistValue = clone->GetBinContent(clone->GetMaximumBin());

    graph->SetTitle("");
    graph->SetMarkerColor(colors[1]);
    graph->SetMarkerStyle(20);
    graph->SetMarkerSize(1);
    // for (int i=0;i<graph->GetN();i++) graph->GetY()[i] *= maxHistValue/TMath::MaxElement(graph->GetN(),graph->GetY());
    legend->AddEntry(graph, legends[1],"p");

    float maxGraphValue = TMath::MaxElement(graph->GetN(),graph->GetY());

    clone->Scale(maxGraphValue/maxHistValue);

    graph->Draw("ap");
    clone->Draw("hist same");

    graph->GetXaxis()->SetTitle(xLabel);
    graph->GetYaxis()->SetTitle(yLabel);
    legend->Draw();
    if (logScale) c->SetLogy();
    if (xRange.size() == 2) graph->GetXaxis()->SetRangeUser(xRange[0],xRange[1]);
    if (yRange.size() > 0) {
        graph->SetMinimum(yRange[0]);
        if (yRange.size() == 2) graph->SetMaximum(yRange[1]);
    }

    if (strs.size() > 0) {
        TLatex latex; latex.SetNDC(); latex.SetTextSize(textSize); latex.SetTextAlign(13);
        for (int i = 0 ; i < strs.size() ;  i++){ latex.SetTextColor(strColors.at(i));latex.DrawLatex(xStrings.at(i),yStrings.at(i),strs.at(i).c_str());}
    }

    if (f1 != 0) f1->Draw("same");

    // c->Print(fileName + ".eps");
    c->Print(fileName + ".png");

    delete c;
}

Int_t Get2DBin(TH2D * hist, double x, double y) {
    TAxis *xaxis = hist->GetXaxis();
    TAxis *yaxis = hist->GetYaxis();
    Int_t binx = xaxis->FindBin(x);
    Int_t biny = yaxis->FindBin(y);
    return hist->GetBin(binx,biny);
}

void stack2DHist(TH2D * h1, TH2D *h2, int color1, int color2, TString xLabel, TString yLabel, TString name) {
    TCanvas * c = new TCanvas("","",600,600);
    THStack * hs = new THStack("","");

    h1->SetFillColor(color1);
    h1->SetLineColor(color1);
    h2->SetFillColor(color2);
    h2->SetLineColor(color2);

    hs->Add(h2);
    hs->Add(h1);

    hs->Draw("hist nostack");
    hs->GetXaxis()->SetTitle(xLabel);
    hs->GetYaxis()->SetTitle(yLabel);

    c->Print(name + ".png");
    // c->Print(name + ".eps");
    delete c;
}

void plotRatio(TGraph* graph1, TGraph* graph2, TString legend1, TString legend2, TString xLabel, TString yLabel, TString fileName, bool logScale = true, vector<double> ratioRange = {}, TString drawStyle = "ap", vector<TString> strs = {}, vector<double>xStrings = {}, vector<double> yStrings = {}, vector<int> strColors = {}, double textSize = 0.04 , vector<double> yRange = {}, int color1 = kRed, int color2 = kBlue)
{
    TCanvas *c = getDefaultDividedCanvas(yLabel, {legend1,legend2});
    TLegend * legend = getDefaultLegend({legend1,legend2});

    c->cd(1);
    TMultiGraph * mg = new TMultiGraph();
    graph1->SetMarkerColor(color1);
    graph2->SetMarkerColor(color2);
    graph1->SetTitle("");
    graph1->Draw("ap");
    mg->Add(graph1); mg->Add(graph2);

    mg->GetYaxis()->SetLabelSize(0.045);
    if (logScale) c->GetPad(1)->SetLogy();

    mg->Draw("p");
    if (yLabel != "") {
        graph1->GetYaxis()->SetTitle(yLabel);
        graph1->GetYaxis()->SetTitleSize(0.06);
        graph1->Draw("ap");
        mg->Draw("p");
    }
    if (yRange.size() != 0) {
        mg->SetMinimum(yRange.at(0));
        mg->SetMaximum(yRange.at(1));
    }

    legend->SetBorderSize(0.00);
    legend->SetFillColor(10);
    legend->AddEntry(graph1,legend1,"lep");
    legend->AddEntry(graph2,legend2,"lep");
    legend->SetTextSize(0.05);
    legend->Draw();

    if (strs.size() > 0) {
        TLatex latex; latex.SetNDC(); latex.SetTextSize(textSize); latex.SetTextAlign(13);
        for (int i = 0 ; i < strs.size() ;  i++){ latex.SetTextColor(strColors.at(i));latex.DrawLatex(xStrings.at(i),yStrings.at(i),strs.at(i));}
    }

    c->cd(2);

    TGraph * graph_ratio = GetGraphRatio(graph1, graph2);

    graph_ratio->SetTitle("");
    graph_ratio->GetXaxis()->SetTitle(xLabel);
    graph_ratio->GetXaxis()->SetTitleSize(0.11);
    graph_ratio->GetXaxis()->SetLabelSize(0.06);
    graph_ratio->GetXaxis()->SetLabelOffset(0.01);
    graph_ratio->GetYaxis()->SetTitle("Ratio");
    graph_ratio->GetYaxis()->SetTitleSize(0.09);
    graph_ratio->GetYaxis()->SetTitleOffset(0.8);
    graph_ratio->GetYaxis()->SetLabelSize(0.06);
    graph_ratio->GetYaxis()->SetLabelOffset(0.01);
    graph_ratio->SetMarkerStyle(kFullCircle);
    graph_ratio->SetMarkerColor(kBlack);
    graph_ratio->SetMarkerSize(0.75);
    graph_ratio->SetLineColor(1);
    graph_ratio->SetLineWidth(1);
    // graph_ratio->GetYaxis()->SetRangeUser(0,2);
    graph_ratio->Draw("AP");

    if (ratioRange.size() > 0) {
        graph_ratio->SetMinimum(ratioRange[0]);
        if (ratioRange.size() == 2) graph_ratio->SetMaximum(ratioRange[1]);
    }
    c->Modified();

    TLatex latex; latex.SetNDC(); latex.SetTextSize(0.08); latex.SetTextAlign(13);
    if (legend1.Length() > 6 || legend2.Length() > 6) {latex.SetTextColor(kBlack);latex.DrawLatex(0.86,0.6,"#frac{" + legend1 + "}{" + legend2 + "}");}
    else {latex.SetTextColor(kBlack);latex.DrawLatex(0.86,0.7,"#frac{" + legend1 + "}{" + legend2 + "}");}

    // c->Print(fileName + ".eps");
    c->Print(fileName + ".png");

    delete c;
}


void plotRatio(TH1D* h1, TH1D* h2, TString legend1, TString legend2, TString xLabel, TString yLabel, TString fileName, bool logScale = true, bool normalise = true, vector<double> ratioRange = {}, TString drawStyle = "e1", vector<TString> strs = {}, vector<double>xStrings = {}, vector<double> yStrings = {}, vector<int> strColors = {}, double textSize = 0.04 , vector<double> yRange = {}, int color1 = kRed, int color2 = kBlue)
{
    TH1D * h1Clone = (TH1D*)h1->Clone("h1");
    TH1D * h2Clone = (TH1D*)h2->Clone("h2");

    TCanvas *c = getDefaultDividedCanvas(yLabel, {legend1,legend2});
    TLegend * legend = getDefaultLegend({legend1,legend2});

    c->cd(1);
    THStack * hs = new THStack("","");
    h2Clone->SetTitle("");
    h2Clone->GetYaxis()->SetLabelSize(0.045);
    h2Clone->SetLineColor(color2);
    h2Clone->SetMarkerColor(color2);
    h2Clone->SetLineWidth(3);
    h2Clone->SetMarkerStyle(8);
    if (normalise) h2Clone->Scale(1./h2Clone->Integral());
    hs->Add(h2Clone);
    h1Clone->SetLineColor(color1);
    h1Clone->SetMarkerColor(color1);
    h1Clone->SetLineWidth(3);
    h1Clone->SetMarkerStyle(4);
    if (normalise) h1Clone->Scale(1./h1Clone->Integral());
    hs->Add(h1Clone);
    if (logScale) c->GetPad(1)->SetLogy();
    hs->Draw(drawStyle + " nostack");
    if (yLabel != "") {
        hs->GetYaxis()->SetTitle(yLabel);
        hs->GetYaxis()->SetTitleSize(0.06);
        hs->Draw(drawStyle + " nostack");
    }
    if (yRange.size() != 0) {
        hs->SetMinimum(yRange.at(0));
        hs->SetMaximum(yRange.at(1));
    }
    

    legend->SetBorderSize(0.00);
    legend->SetFillColor(10);
    if (drawStyle == "hist p") {
        legend->AddEntry(h1Clone,legend1,"p");
        legend->AddEntry(h2Clone,legend2,"p");
    } else {
        legend->AddEntry(h1Clone,legend1,"lep");
        legend->AddEntry(h2Clone,legend2,"lep");
    }
    legend->SetTextSize(0.05);
    legend->Draw();

    if (strs.size() > 0) {
        TLatex latex; latex.SetNDC(); latex.SetTextSize(textSize); latex.SetTextAlign(13);
        for (int i = 0 ; i < strs.size() ;  i++){ latex.SetTextColor(strColors.at(i));latex.DrawLatex(xStrings.at(i),yStrings.at(i),strs.at(i));}
    }

    c->cd(2);


    TH1D * hRatio = (TH1D*)h1Clone->Clone("hRatioMetS");
    hRatio->Divide(h2Clone);

    hRatio->SetTitle("");
    hRatio->GetXaxis()->SetTitle(xLabel);
    hRatio->GetXaxis()->SetTitleSize(0.11);
    // hRatio->GetXaxis()->SetTitleOffset(1.2);
    hRatio->GetXaxis()->SetLabelSize(0.06);
    hRatio->GetXaxis()->SetLabelOffset(0.01);
    hRatio->GetYaxis()->SetTitle("");
    hRatio->GetYaxis()->SetTitleSize(0.09);
    // hRatio->GetYaxis()->SetTitleOffset(0.9);
    hRatio->GetYaxis()->SetLabelSize(0.06);
    hRatio->GetYaxis()->SetLabelOffset(0.01);
    // hRatio->GetYaxis()->CenterTitle();
    hRatio->SetMarkerStyle(kFullCircle);
    hRatio->SetMarkerColor(kBlack);
    hRatio->SetMarkerSize(0.75);
    hRatio->SetLineColor(1);
    hRatio->SetLineWidth(1);
    hRatio->GetYaxis()->SetRangeUser(0,2);
    // hRatio->Draw("hist p");
    hRatio->Draw("P");

    if (ratioRange.size() > 0) {
        hRatio->SetMinimum(ratioRange[0]);
        if (ratioRange.size() == 2) hRatio->SetMaximum(ratioRange[1]);
    }
    c->Modified();

    TLatex latex; latex.SetNDC(); latex.SetTextSize(0.08); latex.SetTextAlign(13);
    if (legend1.Length() > 6 || legend2.Length() > 6) {latex.SetTextColor(kBlack);latex.DrawLatex(0.86,0.6,"#frac{" + legend1 + "}{" + legend2 + "}");}
    else {latex.SetTextColor(kBlack);latex.DrawLatex(0.86,0.7,"#frac{" + legend1 + "}{" + legend2 + "}");}

    // c->Print(fileName + ".eps");
    c->Print(fileName + ".png");

    delete c;
}


void plotPieChart(vector<TString> labels, vector<double> vals, vector<int> colors, TString title, TString name, vector<int> offsetSlices={})
{
    TCanvas *cpie = new TCanvas("","",600,600);
    // cpie->SetMargin(0,50,0,0);
    // cpie->SetLeftMargin(0.03);
    // cpie->SetRightMargin(0.00001);
    // cpie->SetBottomMargin(0.03);
    // cpie->SetTopMargin(0.03);

    TPie *pie = new TPie("pie",title,vals.size(),&vals[0],&colors[0]);

    pie->SetLabelsOffset(.01);
    pie->SetX(.32);
    pie->SetY(.4);
    pie->SetRadius(.22);
    pie->SetLabelFormat("(%perc)");
    for (int i = 0 ; i < labels.size() ; i++) {
        pie->SetEntryLabel(i, labels[i]);
    }
    for (int i = 0 ; i < offsetSlices.size() ; i++) {
        pie->SetEntryRadiusOffset(i,.05);
    }
    pie->Draw("rsc");
    TLegend *pieleg = pie->MakeLegend();
    // pieleg->SetX1(.55); pieleg->SetX2(8);
    // pieleg->SetY1(.4); pieleg->SetY2(.8);
    pieleg->SetBorderSize(0.00);pieleg->SetFillColor(10);
    // pieleg->SetTextSize(0.05);

    cpie->Print(name + ".png");
    // cpie->Print(name + ".eps");
    delete cpie;
}

void plotHist(TH1D* hist, TString xLabel, TString fileName, bool logScale = false, TString yLabel = "", bool normalise = true, vector<double> xRange = {}, vector<double> yRange = {}, bool square = true, vector<TString> strs = {}, vector<double>xStrings = {}, vector<double> yStrings = {}, vector<int> strColors = {}, vector<TF1*> f1 = {}, double textSize = 0.03, TString drawStyle = "e1", vector<TLine*> lines = {}, int color = kBlue) {
    gStyle->SetOptStat(0);
    TCanvas * c = getDefaultCanvas(xLabel,yLabel, square, {}, true, logScale);

    TH1D * clone = (TH1D*)hist->Clone(hist->GetName());
    clone->SetLineWidth(3);
    clone->SetTitle("");
    clone->SetLineColor(color);
    if (normalise) clone->Scale(1./clone->Integral("width"));

    clone->Draw(drawStyle);
    clone->GetXaxis()->SetTitle(xLabel);
    clone->GetYaxis()->SetTitle(yLabel);
    clone->GetYaxis()->SetTitleSize(0.05);

    if (logScale) c->SetLogy();
    if (xRange.size() == 2) clone->GetXaxis()->SetRangeUser(xRange[0],xRange[1]);
    if (yRange.size() > 0) {
        clone->SetMinimum(yRange[0]);
        if (yRange.size() == 2) clone->SetMaximum(yRange[1]);
    } else if (f1.size() > 0) {
        double f1_max = f1.at(0)->GetMaximum();
        double hist_maxContent = hist->GetMaximum();
        if (f1_max > hist_maxContent) clone->SetMaximum(f1_max * 1.2);
    }

    if (strs.size() > 0) {
        TLatex latex; latex.SetNDC(); latex.SetTextSize(textSize); latex.SetTextAlign(13);
        for (int i = 0 ; i < strs.size() ;  i++){ latex.SetTextColor(strColors.at(i));latex.DrawLatex(xStrings.at(i),yStrings.at(i),strs.at(i));}
    }

    for (auto f : f1) f->Draw("same");
    for (auto l : lines) l->Draw("same");


    // c->Print(fileName + ".eps");
    c->Print(fileName + ".png");

    delete c;
}

void plotHist(TH1* hist, TString name, TString xLabel, bool logScale = false, double xOffset = 0, TString title = "")
{
    TCanvas * c = new TCanvas("","",600,600);
    TH1D * clone = (TH1D*)hist->Clone("hist");
    clone->SetLineWidth(3);
    clone->SetTitle(title);
    clone->Draw("hist");
    clone->GetXaxis()->SetTitle(xLabel);
    // clone->GetXaxis()->SetTitleOffset(xOffset);
    // if (xOffset > 0) c->SetBottomMargin(0.15);
    if (logScale) c->SetLogy();
    c->Print(name + ".png");
    // c->Print(name + ".eps");
    delete c;
}

void plot2DHist(TH2* hist, TString name, TString xLabel, TString yLabel, TString title = "", vector<double> levels = {}, vector<int> colors = {})
{
    TCanvas * c = new TCanvas("","",1000,600);
    TH2 * clone = (TH2*)hist->Clone();
    c->SetRightMargin(0.15);

    clone->SetTitle(title);

    if (levels.size() > 0) {
        gStyle->SetPalette(colors.size(), &colors[0]);
        clone->SetContour(levels.size(), &levels[0]);
        clone->Draw("col");        
    } else {
        clone->Draw("colz");
    }

    clone->GetXaxis()->SetTitle(xLabel);
    clone->GetYaxis()->SetTitle(yLabel);
    clone->GetXaxis()->SetLabelSize(0.05);
    clone->GetXaxis()->SetTitleSize(0.05);
    clone->GetYaxis()->SetTitleSize(0.055);
    clone->GetYaxis()->SetLabelSize(0.06);

    c->Print(name + ".png");
    // c->Print(name + ".eps");
    delete c;
}

void plotProfile2D(TProfile2D * prof, TString xLabel, TString yLabel, TString name) {
    TCanvas * c = new TCanvas("","",600,600);
    prof->Draw("lego");
    prof->GetXaxis()->SetTitle(xLabel);
    prof->GetYaxis()->SetTitle(yLabel); 

    c->Print(name + ".png");
    // c->Print(name + ".eps");
    delete c;  
}

void plotHist(TProfile* hist, TString name, TString xLabel, TString yLabel, TString title)
{
    TCanvas * c = new TCanvas("","",600,600);
    hist->SetTitle(title);
    hist->Draw("colz");
    hist->GetXaxis()->SetTitle(xLabel);
    hist->GetYaxis()->SetTitle(yLabel);

    c->Print(name + ".png");
    // c->Print(name + ".eps");
    delete c;
}

void writeTable(TString fileName, TString caption, vector<TString> headers, vector<vector<TString>> rows)
{
    ofstream outfile(fileName + ".tex");

    outfile << "\\begin{table}[]" << endl;
    outfile << "	\\caption*{" << caption << "}" << endl;
    outfile << "    \\resizebox{\\columnwidth}{!}{" << endl;

    outfile << "	    \\begin{tabular}{";
    for(auto header : headers) outfile << "l";
    outfile << "}" << endl;
    
    outfile << "		    \\toprule" << endl;

    outfile << "            ";
    for (int i = 0 ; i < headers.size() ; i++) {
        outfile << " " << headers[i] << " ";
        if (i < headers.size() - 1) outfile << "&";
        else outfile << " \\\\" << endl;
    }
    
    outfile << "		    \\midrule" << endl;

    for (int i = 0 ; i < rows.size() ; i++) {
        outfile << "        ";
        for (int j = 0; j < rows[i].size() ; j++) {
            rows[i][j].ReplaceAll("_","\\_");
            outfile << " " << rows[i][j] << " ";
            if (j < rows[i].size() - 1) outfile << "&";
            else if (rows[i][j] != "\\midrule") outfile << " \\\\" << endl;
        }
    }

    outfile << "		    \\toprule  " << endl;
    outfile << "	    \\end{tabular}" << endl;
    outfile << "    }" << endl;
    outfile << "\\end{table}" << endl;
    outfile << endl;
    outfile << endl;

    cout << "Table written in " << fileName + ".tex" << endl;
}

double wrapMax(double x, double max)
{
    return fmod(max + fmod(x, max), max);
}

double wrapMinMax(double x, double min, double max)
{
    return min + wrapMax(x - min, max - min);
}

float getTransverseMass(float lepton_pt, float nu_pt, float lepton_phi, float nu_phi) {
    return sqrt(2*lepton_pt*nu_pt*(1 - TMath::Cos(lepton_phi - nu_phi)));
}

pair<Double_t,Double_t> integrateGraph(TGraph * g, vector<double> y_err = {}) {
    vector<double> x(g->GetX(),g->GetX()+g->GetN());
    vector<double> y(g->GetY(),g->GetY()+g->GetN());

    vector<double> sorted_x, sorted_y, sorted_y_err;

    for (int i = 0 ; i < g->GetN() ; i++) {
        sorted_x.push_back(x[i]);
    }

    sort(sorted_x.begin(), sorted_x.end());

    for (int i = 0 ; i < sorted_x.size() ; i++) {
        for (int j = 0 ; j < sorted_x.size() ; j++) {
            if (sorted_x.at(i) == x[j]) {
                sorted_y.push_back(y[j]);
                if (y_err.size() != 0) sorted_y_err.push_back(y_err[j]);
                break;
            }
        }
    }

    Double_t integral = 0;
    Double_t integral_err = 0;
    for (int i = 0 ; i < sorted_x.size()-1 ; i++) {
        vector<double> ys = {sorted_y.at(i),sorted_y.at(i+1)};
        double yterm = 0.5 * (sorted_y.at(i+1) + sorted_y.at(i));
        double xterm = sorted_x.at(i+1) - sorted_x.at(i);
        // double yerr = (y_err.size() != 0) ? sqrt(0.5*(pow(sorted_y_err.at(i),2) + pow(sorted_y_err.at(i+1),2))) : 0;
        // double yerr = (y_err.size() != 0) ? 0.5*(pow(sorted_y_err.at(i+1),2) + pow(sorted_y_err.at(i),2))*xterm : 0;
        double yerr = (y_err.size() != 0) ? pow(0.5*xterm,2)*(pow(sorted_y_err.at(i+1),2) + pow(sorted_y_err.at(i),2)) : 0;
        double z = yterm * xterm;
        // double zerr = z * yerr / yterm;
        integral += z;
        // integral_err += sqrt(pow(integral_err,2) + pow(zerr,2));
        integral_err += yerr;
    }
    integral_err = sqrt(integral_err);

    return make_pair(integral,integral_err);
}

pair<double,double> GetGaussianMeanErr(vector<double> x_vector, vector<double> y_vector, vector<double> yErr_vector, TString fileName = "", TString varLabel = "", int color = kBlack) {
    auto params = GetMeanErr(y_vector);
    int best_binning = 5;
    double best_goodness = -1;
    for (int binning = 5 ; binning <= y_vector.size() ; binning++) {
        TH1D * hist = GetHistFromVector(y_vector,binning);
        int binmax = hist->GetMaximumBin(); double maxValue = hist->GetXaxis()->GetBinCenter(binmax);
        TF1 * gaus_fit = new TF1("","gaus",hist->GetXaxis()->GetXmin(),hist->GetXaxis()->GetXmax());
        gaus_fit->SetParameter(0,hist->GetBinContent(binmax)); gaus_fit->SetParameter(1,maxValue); gaus_fit->SetLineColor(kRed);
        TFitResultPtr theFit = hist->Fit(gaus_fit,"MNSRQ0");
        double goodness = theFit->Chi2() / theFit->Ndf();
        if (best_goodness == -1 || best_goodness > goodness) {best_binning = binning; best_goodness = goodness;}
    }
    TH1D * hist = GetHistFromVector(y_vector,best_binning); hist->SetLineColor(color);
    int binmax = hist->GetMaximumBin(); double maxValue = hist->GetXaxis()->GetBinCenter(binmax);
    TF1 * gaus_fit = new TF1("","gaus",hist->GetXaxis()->GetXmin(),hist->GetXaxis()->GetXmax());
    gaus_fit->SetParameter(0,hist->GetBinContent(binmax)); gaus_fit->SetParameter(1,maxValue); gaus_fit->SetLineColor(kRed);
    TFitResultPtr theFit = hist->Fit(gaus_fit,"MNSRQ0");
    double mean_result = gaus_fit->GetParameter(1);
    double meanErr_result = gaus_fit->GetParError(1);
    TLine * line = new TLine(mean_result, 0, mean_result, hist->GetMaximum()); line->SetLineColor(kRed); line->SetLineStyle(9); line->SetLineWidth(2);
    if (fileName != "") plotHist(hist, varLabel, fileName + "_GaussFit", false, "", false, {}, {}, false, {"mean = " + ResultWithFigures(mean_result,meanErr_result), "#frac{#chi^{2}}{ndf} = " + to_string_with_precision(best_goodness,1)}, {0.2,0.2}, {0.85,0.79}, {kBlack,kBlack}, {gaus_fit}, 0.04, "hist", {line});
    return make_pair(mean_result,meanErr_result);
}

float GetRatioError(float num_val, float denom_val, float num_err, float denom_err) {
    return sqrt( pow(num_err/denom_val, 2) + pow(num_val * denom_err / pow(denom_val,2),2) );
}

TString convertFitFormula(TString fitFormula, TF1 * fitFunction, TString varName = "x", vector<int> index_inverted_par = {}) {
    TString converted = "";
    for (int i_str = 0 ; i_str < fitFormula.Length() ; i_str++) {
        TString curr_str = fitFormula(i_str);
        if (curr_str == "[") {
            i_str++;
            int parIndex = ((TString)fitFormula(i_str)).Atoi();
            if (!contains(parIndex,index_inverted_par)) {
                converted += ResultWithFigures(fitFunction->GetParameter(parIndex),fitFunction->GetParError(parIndex));
            }
            else {
                if (((TString)converted(converted.Length()-1)) == "*") converted(converted.Length()-1) = '/'; 
                else if (((TString)converted(converted.Length()-2)) == "*") converted(converted.Length()-2) = '/'; 
                converted += ResultWithFigures(1./fitFunction->GetParameter(parIndex),pow(1./fitFunction->GetParameter(parIndex),2) * fitFunction->GetParError(parIndex));
            }
            i_str++;
        } else if (curr_str == "x" && ((TString)fitFormula(i_str+1)) != "p") {
            converted += varName;
        } else {
            converted += fitFormula(i_str);
        }
    }
    return converted;
}

vector<TString> getRatioFiles(TString txtFolderName, TString txtKeyName, TString pmt_str, TString part_str, int columnToUse) {
    vector<TString> ratio_files;
    vector<TString> list_of_files = getFilesList(txtFolderName, txtKeyName);
    for (TString file : list_of_files) {
        if (file.Contains("muSlopes")) continue;
        if (file.Contains("Errors")) continue;
        if (!file.Contains(GetColumnSuffix(columnToUse))) continue;
        if (!file.Contains(pmt_str)) continue;
        if (!file.Contains(part_str)) continue;
        ratio_files.push_back(txtFolderName + "/" + file);
    }
    return ratio_files;
}

double getAverageRatioFromFile(TString file_path) {
    ifstream file(file_path);
    if (!file.is_open()) {coutError(file_path.Data()); return -999;}

    string str;
    int line = 0;
    double avg_val = -999;
    while (getline(file, str))
    {
        line++;
        istringstream iss(str);
        vector<string> subs;

        do {
            string s;
            iss >> s;
            if (s.size() > 0) subs.push_back(s);
        } while (iss);

        avg_val = stod(subs.at(subs.size()-1));
    }

    return avg_val;
}

void printGraph(TGraph * graph) {
    for (int i_point = 0 ; i_point < graph->GetN() ; i_point++) {
        Double_t x,y;
        graph->GetPoint(i_point, x, y);
        cout << x << ", " << y << endl;
    }
}

TGraph * shiftGraph(TGraph * graph, double x_shift, double margin = 10) {
    TGraph * shifted_graph = new TGraph();
    for (int i_point = 0 ; i_point < graph->GetN() ; i_point++) {
        double x_point, y_point; graph->GetPoint(i_point, x_point, y_point);
        shifted_graph->SetPoint(i_point, x_point + x_shift, y_point);
    }
    return shifted_graph;
}

bool badLB(int runNumber, int LB) {
    if (runNumber == 279259 && LB >= 440 && LB <= 600) return true;
    if (runNumber == 279345 && LB >= 200 && LB <= 800) return true;
    if (runNumber == 280753 && LB >= 600 && LB <= 800) return true;
    if (runNumber == 280853 && LB >= 180 && LB <= 210) return true;
    if (runNumber == 276689 && LB >= 370 && LB <= 385) return true;
    if (runNumber == 284484 && LB >= 450 && LB <= 480) return true;
    if (runNumber == 284484 && LB >= 60 && LB <= 130) return true;
    if (runNumber == 298595 && LB >= 210 && LB <= 250) return true;
    if (runNumber == 310247 && LB >= 490 && LB <= 500) return true;
    if (runNumber == 298862 && LB >= 210 && LB <= 215) return true;
    if (runNumber == 360244 && LB >= 190 && LB <= 210) return true;
    if (runNumber == 358516 && LB == 760) return true;
    if (runNumber == 355331 && LB >= 1380 && LB <= 1390) return true;
    if (runNumber == 355331 && (LB == 1709)) return true;
    if (runNumber == 355261 && (LB == 269)) return true;
    if (runNumber == 354494 && (LB == 1799)) return true;
    if (runNumber == 354396 && (LB == 269)) return true;
    if (runNumber == 354359 && (LB == 322 || LB == 323)) return true;
    if (runNumber == 354174 && (LB == 145 || LB == 150 || LB == 151 || LB == 152 || LB == 153)) return true;
    if (runNumber == 349944 && LB > 444 && LB < 448) return true;
    if (runNumber == 349944 && LB > 566 && LB < 570) return true;
    if (runNumber == 349944 && LB > 754 && LB < 757) return true;
    if (runNumber == 349944 && LB > 828 && LB < 832) return true;
    if (runNumber == 349944 && LB > 930 && LB < 934) return true;
    if (runNumber == 352274 && LB > 110 && LB < 120) return true;
    if (runNumber == 352274 && LB > 135 && LB < 150) return true;
    if (runNumber == 358656 && LB > 600 && LB < 650) return true;
    if (runNumber == 350803 && LB > 530 && LB < 570) return true;
    if (runNumber == 350803 && LB > 448 && LB < 451) return true;
    if (runNumber == 364098 && LB > 370 && LB < 390) return true;
    if (runNumber == 364098 && LB > 480 && LB < 505) return true;
    if (runNumber == 364098 && LB > 620 && LB < 660) return true;
    if (runNumber == 349451 && LB > 500 && LB < 550) return true;
    if (runNumber == 349451 && LB > 610 && LB < 640) return true;
    if (runNumber == 349451 && LB > 680 && LB < 710) return true;
    if (runNumber == 358096 && LB > 400 && LB < 410) return true;
    if (runNumber == 358096 && LB > 550 && LB < 580) return true;
    if (runNumber == 358233 && LB > 600 && LB < 610) return true;
    if (runNumber == 358233 && LB > 800 && LB < 820) return true;
    if (runNumber == 349051 && LB > 268 && LB < 272) return true;
    if (runNumber == 330875 && LB > 357 && LB < 375) return true;
    return false;
}


histParams CharacterizeHistogram(TH1D * hist) {

    double xmin = hist->GetXaxis()->GetXmin();
    double xmax = hist->GetXaxis()->GetXmax();
    double hist_maxContent = hist->GetMaximum();
    double hist_mean = hist->GetMean();
    TF1 * f_gaus = new TF1("fit","gaus", xmin, xmax);
    f_gaus->SetParameter(0, hist_maxContent);
    f_gaus->SetParameter(1,hist_mean);
    TFitResultPtr theFit = hist->Fit(f_gaus,"MNSQR");

    histParams params;
    params.f_gaus = f_gaus;
    params.fit_mean = f_gaus->GetParameter(1);
    params.goodness_of_fit = theFit->Chi2() / theFit->Ndf();
    params.skewness = hist->GetSkewness();

    return params;
}


#endif
