RUN=${1:-0}
fileTracksIndex=${2:-0}

if [ "${fileTracksIndex}" -eq "0" ]
then
	fileTracksPath="/afs/cern.ch/atlas/project/LumiWG/CoolScanNtuple/2017/2017SigmaVis/2017NtuplesProduction/PerLB/2corrections/"
elif [ "${fileTracksIndex}" -eq "1" ]
then
	fileTracksPath="/afs/cern.ch/atlas/project/LumiWG/CoolScanNtuple/2018/PerLB/NoMuCorr/"
elif [ "${fileTracksIndex}" -eq "2" ]
then
	fileTracksPath="/afs/.cern.ch/atlas/project/LumiWG/CoolScanNtuple/2017/2017SigmaVis/2018NtuplesProduction/PerLB/NoMuCorr/"
elif [ "${fileTracksIndex}" -eq "3" ]
then
	fileTracksPath="/afs/.cern.ch/atlas/project/LumiWG/CoolScanNtuple/2017/2017SigmaVis/2018NtuplesProduction/PerLB/3corrections/"
elif [ "${fileTracksIndex}" -eq "4" ]
then
	fileTracksPath="/afs/cern.ch/atlas/project/LumiWG/CoolScanNtuple/2018/PerLB/1correction/"
elif [ "${fileTracksIndex}" -eq "5" ]
then
	fileTracksPath="/afs/cern.ch/atlas/project/LumiWG/CoolScanNtuple/2018/LowMu/PerLB/NoMuCorr/"
elif [ "${fileTracksIndex}" -eq "6" ]
then
	fileTracksPath="/afs/.cern.ch/atlas/project/LumiWG/CoolScanNtuple/2018/specialrun_nomucorr_stabLB/PerLB/NoMuCorr/"
elif [ "${fileTracksIndex}" -eq "7" ]
then
	fileTracksPath="/afs/.cern.ch/atlas/project/LumiWG/CoolScanNtuple/2017/2017SigmaVis/2018NtuplesProduction/LowMuRuns/AllLBs/PerLB/"
elif [ "${fileTracksIndex}" -eq "8" ]
then
	fileTracksPath="/afs/.cern.ch/atlas/project/LumiWG/CoolScanNtuple/2017/2017SigmaVis/2018NtuplesProduction/LowMuRuns/AllLBs/PerLB/"
elif [ "${fileTracksIndex}" -eq "9" ]
then
	fileTracksPath="/eos/atlas/atlascerngroupdisk/data-prep/lumi_track_counting/TCLumiBenedettoInputs/TCLUMI2016_FALL20/v2/PerLB/"
elif [ "${fileTracksIndex}" -eq "10" ]
then
	fileTracksPath="/afs/cern.ch/atlas/project/LumiWG/CoolScanNtuple/2016/PerLB/"
elif [ "${fileTracksIndex}" -eq "11" ]
then
	fileTracksPath="/eos/atlas/atlascerngroupdisk/data-prep/lumi_track_counting/TCLumiBenedettoInputs/TCLUMI2016_FALL20/v2/PerLB/"
elif [ "${fileTracksIndex}" -eq "12" ]
then
	fileTracksPath="/eos/atlas/atlascerngroupdisk/data-prep/lumi_track_counting/TCLumiBenedettoInputs/TCLUMI2015_SUMMER21/v1/PerLB/"
elif [ "${fileTracksIndex}" -eq "13" ]
then
	fileTracksPath="/eos/user/s/stile/2015Benedetto/"
fi


fileTracksName=\"$(ls ${fileTracksPath}*${RUN}* | head -1)\"

echo "${fileTracksName}"



