
class LumiTool {
    public:
        LumiTool(bool doDivideByGain, int _doLaser, int _doLasGap, int _doScintillatorIndex, TString _outFile_folder);
        void computeLuminosity(Run * run, vector<int> cells_to_compute, vector<TString> cells_to_print, int times_to_prune, vector<int> parts_to_compute);
        map<TString,vector<float>> GetAveragedLumi(Run * run, map<TString, float> * avLumiMap, map<TString, float> * avLumiMap_err, vector<int> cells_to_compute, int LB, int times_to_prune, vector<int> parts_to_compute, int index, string printlb, int RunNumber);
        double getLasGapValue(int doLasGap, double current, int part, int mod, int index);
        double getScintillatorCorrValue(int runNum, int anchorRun, int pmt, int part);

        string suffix = "";
        TString outFile_folder = "BFiles";
        bool divideByGain;
        int doLaser;
        int doLasGap;
        int doScintillatorIndex;
        vector< vector< float> > lbCurr [4]; 
        vector< vector< float> > lbCurr_err [4]; 
        vector<vector<vector< float>>> lbCurr_byLB [4];
        vector<float> lbs;

        double lasgap[4][64][4];
        double lasgap_quad[4][64][4];
        double lasgap_intercept[4][64][4];
        double lasgap_line1[4][64][4];
        double lasgap_line2[4][64][4];
        double lasgap_minThresh[4][64][4];
        double lasgap_pieceThresh[4][64][4];

        map<TString,double> map_scintillatorCorrValue;

        int LB_possible_offset[4][64][48];

        double usedLasGapCorr[4][64][48]; // For filling the tree in GetAveragedLumi

        map<int,TH1D*> lasGapHist[4];
        map<int,array<TH1D*,64>> lasGapHist_perChannel[4];

        bool perChannelFileCreated_flag[4][48];

};

LumiTool::LumiTool(bool doDivideByGain, int _doLaser, int _doLasGap, int _doScintillatorIndex, TString _outFile_folder = "BFiles") {

    divideByGain = doDivideByGain;
    doLaser = _doLaser;
    doLasGap = _doLasGap;
    doScintillatorIndex = _doScintillatorIndex;
    outFile_folder = _outFile_folder;

    for (int part = 0 ; part < 4 ; part ++) {
        for (int mod = 0 ; mod < 64 ; mod ++) {
            for (int pmt = 0 ; pmt < 4 ; pmt ++) {
                lasgap[part][mod][pmt] = 0;
                lasgap_quad[part][mod][pmt] = 0;
                lasgap_intercept[part][mod][pmt] = 1.0;
                lasgap_line1[part][mod][pmt] = 0.0;
                lasgap_line2[part][mod][pmt] = 0.0;
                lasgap_minThresh[part][mod][pmt] = 0.0;
                lasgap_pieceThresh[part][mod][pmt] = 0.0;
            }
            for (int pmt = 0 ; pmt < 48 ; pmt ++) {
                usedLasGapCorr[part][mod][pmt] = 1.0;
                LB_possible_offset[part][mod][pmt] = 0.0;
                perChannelFileCreated_flag[part][pmt] = false;
            }
        }
    }

    //Read Laser Gap Corrections
    if (doLasGap > 0) {
        TString inputFile = Global::GetInGapInputFile(doLasGap);
        bool perChannelCorrections = Global::PerChannelInGap(doLasGap);
        cout << "Reading LasGap Histograms from " << inputFile << endl;
        TFile * file = new TFile(inputFile);
        vector<int> part_vector = {PART::EBA, PART::EBC};
        vector<int> cell_vector = Global::GetInGapCellsToCompute(doLasGap);
        if (!perChannelCorrections) { // MEAN corrections
            for (int part : part_vector) {
                for (int cell : cell_vector) {
                    TString pmt_str = Utils::GetPMTString(cell,part);
                    TString part_str = Utils::GetPartString(part);
                    lasGapHist[part][cell] = (TH1D*)file->Get("h_mean_" + pmt_str + "_" + part_str);
                }
            }
        } else if (perChannelCorrections) { // PER CHANNEL corrections
            for (int part : part_vector) {
                for (int cell : cell_vector) {
                    TString pmt_str = Utils::GetPMTStringExtra(cell,part);
                    TString part_str = Utils::GetPartString(part);
                    for (int mod = 0 ; mod < 64 ; mod++) {
                        TString histKeyName = pmt_str + "_" + part_str + Utils::ToModString(mod);
                        if (!file->GetListOfKeys()->Contains(histKeyName)) continue;
                        TH1D * hist_perChannel = (TH1D*)file->Get(histKeyName);
                        if (doLasGap == 45) {
                            double cut = 5;
                            int bin = hist_perChannel->GetXaxis()->FindBin(1.2);
                            if (cell == PMT::A12L && part == PART::EBA) cut = 1.030;
                            if (cell == PMT::A12R && part == PART::EBA) cut = 1.025;
                            if (cell == PMT::A12L && part == PART::EBC) cut = 1.022;
                            if (cell == PMT::A12R && part == PART::EBC) cut = 1.025;
                            if (cell == PMT::A13L && part == PART::EBA) cut = 1.030;
                            if (cell == PMT::A13R && part == PART::EBA) cut = 1.025;
                            if (cell == PMT::A13L && part == PART::EBC) cut = 1.022;
                            if (cell == PMT::A13R && part == PART::EBC) cut = 1.025;
                            
                            if (hist_perChannel->GetBinContent(bin) > cut) continue;;
                        }
                        lasGapHist_perChannel[part][cell][mod] = hist_perChannel;
                    }
                }
            }
        }
    }

}

double LumiTool::getLasGapValue(int doLasGap, double current, int part, int mod, int index) {
    // return DUMMY::CORR so that the channel is skipped
    // return 1 so that the correction isn't applied
    if (Global::PerChannelInGap(doLasGap)) {
        if (lasGapHist_perChannel[part].count(index) > 0) {
            if (lasGapHist_perChannel[part][index].at(mod)) {
                int bin = lasGapHist_perChannel[part][index].at(mod)->GetXaxis()->FindBin(current/1000.);
                if (bin <= 0) bin = 1;
                if (bin > lasGapHist_perChannel[part][index].at(mod)->GetNbinsX()) bin = lasGapHist_perChannel[part][index].at(mod)->GetNbinsX();
                return lasGapHist_perChannel[part][index].at(mod)->GetBinContent(bin);
            } else {
                return DUMMY::CORR;
            }
        } else {
            return 1;
        }
    } else {
        if (lasGapHist[part].count(index) > 0) {
            int bin = lasGapHist[part][index]->GetXaxis()->FindBin(current/1000.);
            if (bin <= 0) bin = 1;
            if (bin > lasGapHist[part][index]->GetNbinsX()) bin = lasGapHist[part][index]->GetNbinsX();
            return lasGapHist[part][index]->GetBinContent(bin);
        } else {
            return 1;
        }
    }
}

double LumiTool::getScintillatorCorrValue(int runNum, int anchorRun, int pmt, int part) {
    if (part != PART::EBA && part != PART::EBC) return 1;
    if (pmt != PMT::E3 && pmt != PMT::E4) return 1;

    TString pmt_str = Utils::GetPMTStringExtra(pmt,part);

    double correction_value = 1;

    if (map_scintillatorCorrValue.count(pmt_str) == 0) {
        TF1 * scintillatorCorrFit;
        vector<double> fit_params;
        if (Global::useTwoParScintillatorCorr()) {
            cout << "Opening scintillator fit: " << "share/ScintillatorCorrection_IlyaFunction_shiftL_" + pmt_str + ".dat" << endl;
            readFile("share/ScintillatorCorrection_IlyaFunction_shiftL_" + pmt_str + ".dat",1,{0},(vector<vector<double>*>){&fit_params});
            scintillatorCorrFit = new TF1("scintillatorCorrFit","[0]*exp(-(94800 + x) * [1])",0,fit_params.at(2));
            scintillatorCorrFit->SetParameter(0,fit_params.at(0));
            scintillatorCorrFit->SetParameter(1,fit_params.at(1));
        } else {
            cout << "Opening scintillator fit: " << "share/ScintillatorCorrection_IlyaFunction_onePar_" + pmt_str + ".dat" << endl;
            readFile("share/ScintillatorCorrection_IlyaFunction_onePar_" + pmt_str + ".dat",1,{0},(vector<vector<double>*>){&fit_params});
            scintillatorCorrFit = new TF1("scintillatorCorrFit","exp(-x*[0])",0,fit_params.at(1));
            scintillatorCorrFit->SetParameter(0,fit_params.at(0));
        }
        double intlumi_run = getIntegratedLumiAtRun(runNum, 4, 83);
        double intlumi_anchor = getIntegratedLumiAtRun(anchorRun, 4, 83);
        double corr_run = scintillatorCorrFit->Eval(intlumi_run);
        double corr_anchor = scintillatorCorrFit->Eval(intlumi_anchor);
        correction_value = corr_anchor / corr_run;
        map_scintillatorCorrValue[pmt_str] = correction_value;
        cout << "Int Lumi at " << runNum << " = " << intlumi_run << endl;
        cout << "Int Lumi at " << anchorRun << " = " << intlumi_anchor << endl;
        cout << "Scintillator correction for " << pmt_str << " = " << corr_anchor << " / " << corr_run << " = " << correction_value << endl;
    } else {
        correction_value = map_scintillatorCorrValue[pmt_str];
    }

    return correction_value;
}