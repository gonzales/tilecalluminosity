//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Nov  5 11:39:31 2018 by ROOT version 5.34/38
// from TTree IntegratorCalibration/IntegratorCalibration
// found on file: /eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171026_1830.root
//////////////////////////////////////////////////////////

#ifndef IntegratorCalibration_h
#define IntegratorCalibration_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class IntegratorCalibration {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Float_t         PMTgain[64][48];
   Float_t         PMTped[64][48];
   Float_t         PMTrms[64][48];
   Int_t           PMTgainNumber[64][48];
   Int_t           PMTpattern[64][48];
   UInt_t          RunNumber;

   // List of branches
   TBranch        *b_PMTgain;   //!
   TBranch        *b_PMTped;   //!
   TBranch        *b_PMTrms;   //!
   TBranch        *b_PMTgainNumber;   //!
   TBranch        *b_PMTpattern;   //!
   TBranch        *b_run_number;   //!

   IntegratorCalibration(TString path);
   virtual ~IntegratorCalibration();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef IntegratorCalibration_cxx
IntegratorCalibration::IntegratorCalibration(TString path) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
    TTree * tree;
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(path);
    if (!f || !f->IsOpen()) {
        f = new TFile(path);
    }
    f->GetObject("IntegratorCalibration",tree);

   Init(tree);
}

IntegratorCalibration::~IntegratorCalibration()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t IntegratorCalibration::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t IntegratorCalibration::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void IntegratorCalibration::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("PMTgain", PMTgain, &b_PMTgain);
   fChain->SetBranchAddress("PMTped", PMTped, &b_PMTped);
   fChain->SetBranchAddress("PMTrms", PMTrms, &b_PMTrms);
   fChain->SetBranchAddress("PMTgainNumber", PMTgainNumber, &b_PMTgainNumber);
   fChain->SetBranchAddress("PMTpattern", PMTpattern, &b_PMTpattern);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_run_number);
   Notify();
}

Bool_t IntegratorCalibration::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void IntegratorCalibration::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t IntegratorCalibration::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef IntegratorCalibration_cxx
