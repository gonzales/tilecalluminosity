//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Jan 30 12:01:11 2020 by ROOT version 6.14/04
// from TTree EpicTree_EBA/EpicTree_EBA
// found on file: ../Luminosities/run_331020_onlyLaser_currents_expoPed_ECells_EXTRA.root
//////////////////////////////////////////////////////////

#ifndef EpicTree_h
#define EpicTree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class EpicTree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Double_t        abovePedestal;
   Double_t        anodeCurrent;
   Double_t        lumi;
   Int_t           pmt;
   Int_t           mod;
   Double_t        gain;
   Int_t           gg;
   Int_t           LB;
   Double_t        time;
   Double_t        RunDuration;
   Double_t        activity;
   Double_t        pedestal;
   Double_t        uncorr_pedestal;
   Double_t        corr_pedestal;
   Double_t        SAValue;

   // List of branches
   TBranch        *b_abovePedestal;   //!
   TBranch        *b_anodeCurrent;   //!
   TBranch        *b_lumi;   //!
   TBranch        *b_pmt;   //!
   TBranch        *b_mod;   //!
   TBranch        *b_gain;   //!
   TBranch        *b_gg;   //!
   TBranch        *b_LB;   //!
   TBranch        *b_time;   //!
   TBranch        *b_RunDuration;   //!
   TBranch        *b_activity;   //!
   TBranch        *b_pedestal;   //!
   TBranch        *b_uncorr_pedestal;   //!
   TBranch        *b_corr_pedestal;   //!
   TBranch        *b_SAValue;   //!

   EpicTree(TString path, TString part);
   virtual ~EpicTree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef EpicTree_cxx
EpicTree::EpicTree(TString path, TString part) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   TTree* tree = 0;
   TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(path);
   if (!f || !f->IsOpen()) {
      f = new TFile(path);
      if (!f->IsOpen()) coutError("The EXTRA file does not exist!");
   }
   f->GetObject("EpicTree_" + part,tree);

   Init(tree);
}

EpicTree::~EpicTree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t EpicTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t EpicTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void EpicTree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("abovePedestal", &abovePedestal, &b_abovePedestal);
   fChain->SetBranchAddress("anodeCurrent", &anodeCurrent, &b_anodeCurrent);
   fChain->SetBranchAddress("lumi", &lumi, &b_lumi);
   fChain->SetBranchAddress("pmt", &pmt, &b_pmt);
   fChain->SetBranchAddress("mod", &mod, &b_mod);
   fChain->SetBranchAddress("gain", &gain, &b_gain);
   fChain->SetBranchAddress("gg", &gg, &b_gg);
   fChain->SetBranchAddress("LB", &LB, &b_LB);
   fChain->SetBranchAddress("time", &time, &b_time);
   fChain->SetBranchAddress("RunDuration", &RunDuration, &b_RunDuration);
   fChain->SetBranchAddress("activity", &activity, &b_activity);
   fChain->SetBranchAddress("pedestal", &pedestal, &b_pedestal);
   fChain->SetBranchAddress("uncorr_pedestal", &uncorr_pedestal, &b_uncorr_pedestal);
   fChain->SetBranchAddress("corr_pedestal", &corr_pedestal, &b_corr_pedestal);
   fChain->SetBranchAddress("SAValue", &SAValue, &b_SAValue);
   Notify();
}

Bool_t EpicTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void EpicTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t EpicTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef EpicTree_cxx
