
namespace PlotUtils {
    void plotGraph(TGraph * graph, int mod, int pmt, float fit0, float fit1, int part, Run * run, int pedStart, int pedEnd);
    void plotGraph_Exponential(TGraph* pedgraph, int mod, int pmt, float fit0, float fit1, float fit2, int part, Run * run, float timeStart, float timeEnd, TGraph* pedgraph_for_fit = 0);
    void plotGraph(TGraphErrors *pedgraph, TF1 *pedfit, TF1 *pedfit2, float refPoint, int part, int mod, int pmt, int run);
    void plotGraph_339197(TGraph* gr, int part, int mod, int pmt, double par0, double par1, double par2, double par3, double refPoint, double t0, double t1, double t2, double t3, int run);
    void plotGraph_354124(TGraph* gr, int part, int mod, int pmt, double par0, double par1, double par2, double par3, double t0, double t1, double t2, double t3, int gG, int run);
    void plotGraph_SecondExponentialFit(TGraph* gr, int mod, int pmt, TF1* fit, double goodness_of_fit, int part, double pedFromFit, double pedFromMean, int run, Double_t par0, Double_t par1, Double_t par2);
    void plotGraph_INTERPOLATION(TGraph* gr, int part, int mod, int pmt, double par0, double par1, double par2, double par3, double t0, double t1, double t2, double t3, Run * run);
    void plotGraph_INTERPOLATION_TO_LINEAR_FIT(TGraph* gr, int part, int mod, int pmt, double par0, double par1, double par2, double par3, double par4, double par5, double t0, double t1, double t2, double t3, Run * run);
    void plotGraph_addExpos(TGraph* pedgraph, int mod, int pmt, float fit0, float fit1, float fit2, float fit3, float fit4, float fit5, int part, Run * run, float timeStart, float timeEnd);
    void plotSecondPolyGraph(TGraph* pedgraph, int mod, int pmt, float fit0, float fit1, float fit2, int part, int run, float timeStart, float timeEnd);
    void plotVDMExpoGraph(TGraph* pedgraph, int mod, int pmt, float fit0, float fit1, float fit2, float fit3, float refPoint, int part, int run, float timeStart, float timeEnd);


}