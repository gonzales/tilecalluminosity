#include "src/IntegratorCalibration.C"
#include "src/IntegratorData.C"

class PFile {
    public:
        PFile(TString path); 

        IntegratorCalibration * calibTree;
        IntegratorData * intTree;

};

PFile::PFile(TString path){
    calibTree = new IntegratorCalibration(path);
    intTree = new IntegratorData(path);
}