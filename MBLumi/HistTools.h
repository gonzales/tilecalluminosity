#include "/afs/cern.ch/user/g/gonzales/public/generalMacros/functions.h"

struct Histogram1D {
    TH1D * hist;
    TString name;
    TString xLabel;
    bool logScale;
};

struct Histogram2D {
    TH2D * hist;
    TString name;
    TString xLabel;
    TString yLabel;
};

struct Graph {
    vector<double> x;
    vector<double> y;
    TString xLabel;
    TString yLabel;
}

class HistTools {

    public:
        map<TString,Histogram1D> histograms1D;
        map<TString,Histogram2D> histograms2D;
        map<TString,Graph> graphs;
        TString output = "";

        HistTools(TString output);

        virtual void setOutput(TString output);

        virtual void addHistogram(TString name, Int_t nBins, Double_t min, Double_t max, TString xLabel, bool logScale);
        virtual void addHistogram(TString name, Int_t nBinsx, Double_t minx, Double_t maxx, Int_t nBinsy, Double_t miny, Double_t maxy, TString xLabel, TString yLabel);
        virtual void addGraph(TString name, TString xLabel, TString yLabel);

        virtual void Fill(TString name, Double_t value, Double_t weight = 1);
        virtual void Fill(TString name, Double_t valuex, Double_t valuey, Double_t weight = 1);
        virtual void FillGraph(TString name, Double_t valuex, Double_t valuey);

        virtual void SetBinContent(TString name, Double_t x, Double_t value);
        virtual void SetBinContent(TString name, Double_t x, Double_t y, Double_t value);

        virtual Double_t getEntries(TString name);
        virtual TH1D* GetHistogram(TString name);
        virtual TH2D* Get2DHistogram(TString name);

        virtual void plotHistograms();
};

HistTools::HistTools(TString o) {
    output = o + "/";
}