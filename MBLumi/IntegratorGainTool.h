
class IntegratorGainTool {
    public:
        IntegratorGainTool(int runNum, TString pathToGainFiles);
        ~IntegratorGainTool();

        virtual float GetGain(int part, int mod, int pmt, int gg);
        virtual float GetGainError(int part, int mod, int pmt, int gg);
        
        float gains[4][64][48][6];
        float gains_err[4][64][48][6];
};

IntegratorGainTool::IntegratorGainTool(int runNum, TString pathToGainFiles) {

    memset(gains,0,sizeof(gains));

    TString inputFile = pathToGainFiles + TString::Format("/gains_%d.txt", runNum);
    //Read gains file
    fstream gainsFile; gainsFile.open(inputFile);
    if (!gainsFile.is_open()) {
        coutError("IntegratorGainTool - File not found " + inputFile);
        return;
    }
    string file_line;
    double gain, gain_err, chi2, pedestal, DAC, pedestal_error, RMS, RMS_err;
    int pmt, gain_group;
    char partmod_chars[10];
    while(getline(gainsFile, file_line)) {

        sscanf(file_line.c_str(),"%9s\t%i\t%i\t%lf\t%lf\n", partmod_chars, &pmt, &gain_group, &gain, &gain_err, &chi2, &pedestal, &DAC, &pedestal_error, &RMS, &RMS_err); // gain error(gain) chi2 pedestal DAC error(pedestal) RMS error(RMS)

        string partmod_str = partmod_chars;
        int mod = stoi(partmod_str.substr(3,5));

        gains[Utils::GetPartIndex(partmod_str.substr(0,3))][mod - 1][pmt][gain_group] = gain;
        gains_err[Utils::GetPartIndex(partmod_str.substr(0,3))][mod - 1][pmt][gain_group] = gain_err;

    }
    gainsFile.close();

}