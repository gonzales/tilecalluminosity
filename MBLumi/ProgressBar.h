#ifndef ProgressBar_h
#define ProgressBar_h

class ProgressBar {

    public:

        int barWidth;
        int prevProgress;

        ProgressBar(int barWidth);

        virtual void print(float progress, TString message);

};


ProgressBar::ProgressBar(int bw) {
    barWidth = bw;
    prevProgress = -1;
}

void ProgressBar::print(float progress, TString message = "PROGRESS") {
    if (prevProgress != int(progress*100)) {
        cout << message << " " << "[";
        int pos = barWidth * progress;
        for (int i = 0 ; i < barWidth ; i++) {
            if (i < pos) cout << "=";
            else if (i == pos) cout << ">";
            else cout << " ";
        }
        cout << "] " << int(progress * 100) + 1 << " %\r";
        cout.flush();
        prevProgress = int(progress*100);
    }
}


#endif