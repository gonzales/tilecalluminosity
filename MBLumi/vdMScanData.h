//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Mar 16 16:54:24 2020 by ROOT version 6.14/04
// from TTree vdMScanData/vdMScanData
// found on file: scan1806301208.root
//////////////////////////////////////////////////////////

#ifndef vdMScanData_h
#define vdMScanData_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class vdMScanData {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   ULong64_t       StartTime;
   ULong64_t       EndTime;
   UInt_t          ScanRun;
   UInt_t          ScanLB;
   Float_t         StepProgress;
   Float_t         ScanningIP;
   Float_t         AcquisitionFlag;
   Float_t         MovingBeam;
   Float_t         NominalSeparation;
   Float_t         ScanInPlane;
   Int_t           ScanStep;
   UInt_t          NominalSeparationPlane;
   Float_t         B1DeltaXSet;
   Float_t         B1DeltaYSet;
   Float_t         B2DeltaXSet;
   Float_t         B2DeltaYSet;
   Float_t         B1PositionH;
   Float_t         B1PositionV;
   Float_t         B1AngleH;
   Float_t         B1AngleV;
   Float_t         B2PositionH;
   Float_t         B2PositionV;
   Float_t         B2AngleH;
   Float_t         B2AngleV;
   Int_t           B1Bunches;
   Int_t           B1BCIDs[140];   //[B1Bunches]
   Int_t           B2Bunches;
   Int_t           B2BCIDs[140];   //[B2Bunches]
   Int_t           LuminousBunches;
   Int_t           LuminousBCIDs[124];   //[LuminousBunches]
   Float_t         BPTX_B1Intensity;
   Float_t         BPTX_B2Intensity;
   Float_t         BPTX_B1IntensityAll;
   Float_t         BPTX_B2IntensityAll;
   Float_t         BCT_B1Intensity;
   Float_t         BCT_B2Intensity;
   Float_t         BCT_B1IntensityAll;
   Float_t         BCT_B2IntensityAll;
   Float_t         DCCT_B1IntensityAll;
   Float_t         DCCT_B2IntensityAll;
   Float_t         DCCT24_B1IntensityAll;
   Float_t         DCCT24_B2IntensityAll;
   UInt_t          BCT_Valid;
   Float_t         BCT_B1BunchIntensity[3564];
   Float_t         BCT_B2BunchIntensity[3564];
   UInt_t          BPTX_Valid;
   Float_t         BPTX_B1BunchIntensity[3564];
   Float_t         BPTX_B2BunchIntensity[3564];
   UInt_t          lucBiPMTA1_Channel;
   UInt_t          lucBiPMTA1_Valid;
   Float_t         lucBiPMTA1_AverageRawInstLum;
   Float_t         lucBiPMTA1_BunchRawInstLum[3564];
   UInt_t          lucBiPMTA7_Channel;
   UInt_t          lucBiPMTA7_Valid;
   Float_t         lucBiPMTA7_AverageRawInstLum;
   Float_t         lucBiPMTA7_BunchRawInstLum[3564];
   UInt_t          lucBiPMTA9_Channel;
   UInt_t          lucBiPMTA9_Valid;
   Float_t         lucBiPMTA9_AverageRawInstLum;
   Float_t         lucBiPMTA9_BunchRawInstLum[3564];
   UInt_t          lucBiPMTA13_Channel;
   UInt_t          lucBiPMTA13_Valid;
   Float_t         lucBiPMTA13_AverageRawInstLum;
   Float_t         lucBiPMTA13_BunchRawInstLum[3564];
   UInt_t          lucBiPMTC1_Channel;
   UInt_t          lucBiPMTC1_Valid;
   Float_t         lucBiPMTC1_AverageRawInstLum;
   Float_t         lucBiPMTC1_BunchRawInstLum[3564];
   UInt_t          lucBiPMTC5_Channel;
   UInt_t          lucBiPMTC5_Valid;
   Float_t         lucBiPMTC5_AverageRawInstLum;
   Float_t         lucBiPMTC5_BunchRawInstLum[3564];
   UInt_t          lucBiPMTC9_Channel;
   UInt_t          lucBiPMTC9_Valid;
   Float_t         lucBiPMTC9_AverageRawInstLum;
   Float_t         lucBiPMTC9_BunchRawInstLum[3564];
   UInt_t          lucBiPMTC13_Channel;
   UInt_t          lucBiPMTC13_Valid;
   Float_t         lucBiPMTC13_AverageRawInstLum;
   Float_t         lucBiPMTC13_BunchRawInstLum[3564];
   UInt_t          lucBi2PMTA4_Channel;
   UInt_t          lucBi2PMTA4_Valid;
   Float_t         lucBi2PMTA4_AverageRawInstLum;
   Float_t         lucBi2PMTA4_BunchRawInstLum[3564];
   UInt_t          lucBi2PMTA8_Channel;
   UInt_t          lucBi2PMTA8_Valid;
   Float_t         lucBi2PMTA8_AverageRawInstLum;
   Float_t         lucBi2PMTA8_BunchRawInstLum[3564];
   UInt_t          lucBi2PMTA12_Channel;
   UInt_t          lucBi2PMTA12_Valid;
   Float_t         lucBi2PMTA12_AverageRawInstLum;
   Float_t         lucBi2PMTA12_BunchRawInstLum[3564];
   UInt_t          lucBi2PMTA16_Channel;
   UInt_t          lucBi2PMTA16_Valid;
   Float_t         lucBi2PMTA16_AverageRawInstLum;
   Float_t         lucBi2PMTA16_BunchRawInstLum[3564];
   UInt_t          lucBiHitOR_Channel;
   UInt_t          lucBiHitOR_Valid;
   Float_t         lucBiHitOR_AverageRawInstLum;
   Float_t         lucBiHitOR_BunchRawInstLum[3564];
   UInt_t          lucBi2PMTC8_Channel;
   UInt_t          lucBi2PMTC8_Valid;
   Float_t         lucBi2PMTC8_AverageRawInstLum;
   Float_t         lucBi2PMTC8_BunchRawInstLum[3564];
   UInt_t          lucBi2PMTC12_Channel;
   UInt_t          lucBi2PMTC12_Valid;
   Float_t         lucBi2PMTC12_AverageRawInstLum;
   Float_t         lucBi2PMTC12_BunchRawInstLum[3564];
   UInt_t          lucBi2PMTC16_Channel;
   UInt_t          lucBi2PMTC16_Valid;
   Float_t         lucBi2PMTC16_AverageRawInstLum;
   Float_t         lucBi2PMTC16_BunchRawInstLum[3564];
   UInt_t          lucBi2PMTC4_Channel;
   UInt_t          lucBi2PMTC4_Valid;
   Float_t         lucBi2PMTC4_AverageRawInstLum;
   Float_t         lucBi2PMTC4_BunchRawInstLum[3564];
   UInt_t          lucBi2HitOR_Channel;
   UInt_t          lucBi2HitOR_Valid;
   Float_t         lucBi2HitOR_AverageRawInstLum;
   Float_t         lucBi2HitOR_BunchRawInstLum[3564];
   UInt_t          lucBiHitOR_LumiChannel;
   Float_t         lucBiHitOR_LBAvInstLumPhys;
   Float_t         lucBiHitOR_LBAvEvtsPerBXPhys;
   Float_t         lucBiHitOR_LBAvRawInstLumPhys;
   Float_t         lucBiHitOR_LBAvInstLumAll;
   Float_t         lucBiHitOR_LBAvEvtsPerBXAll;
   Float_t         lucBiHitOR_LBAvRawInstLumAll;
   Float_t         lucBiHitOR_LBAvOLCInstLum;
   Float_t         lucBiHitOR_LBAvOLCEvtsPerBX;
   Float_t         lucBiHitOR_LBAvOLCRawInstLum;
   UInt_t          lucBiHitOR_NOrbPhys;
   UInt_t          lucBiHitOR_NOrbAll;
   UInt_t          lucBiHitOR_NOrbOLC;
   UInt_t          lucBiHitOR_DetectorState;
   UInt_t          lucBiHitOR_LumiValid;
   UInt_t          lucBi2HitOR_LumiChannel;
   Float_t         lucBi2HitOR_LBAvInstLumPhys;
   Float_t         lucBi2HitOR_LBAvEvtsPerBXPhys;
   Float_t         lucBi2HitOR_LBAvRawInstLumPhys;
   Float_t         lucBi2HitOR_LBAvInstLumAll;
   Float_t         lucBi2HitOR_LBAvEvtsPerBXAll;
   Float_t         lucBi2HitOR_LBAvRawInstLumAll;
   Float_t         lucBi2HitOR_LBAvOLCInstLum;
   Float_t         lucBi2HitOR_LBAvOLCEvtsPerBX;
   Float_t         lucBi2HitOR_LBAvOLCRawInstLum;
   UInt_t          lucBi2HitOR_NOrbPhys;
   UInt_t          lucBi2HitOR_NOrbAll;
   UInt_t          lucBi2HitOR_NOrbOLC;
   UInt_t          lucBi2HitOR_DetectorState;
   UInt_t          lucBi2HitOR_LumiValid;
   UInt_t          lucBiPMTA1_LumiChannel;
   Float_t         lucBiPMTA1_LBAvInstLumPhys;
   Float_t         lucBiPMTA1_LBAvEvtsPerBXPhys;
   Float_t         lucBiPMTA1_LBAvRawInstLumPhys;
   Float_t         lucBiPMTA1_LBAvInstLumAll;
   Float_t         lucBiPMTA1_LBAvEvtsPerBXAll;
   Float_t         lucBiPMTA1_LBAvRawInstLumAll;
   Float_t         lucBiPMTA1_LBAvOLCInstLum;
   Float_t         lucBiPMTA1_LBAvOLCEvtsPerBX;
   Float_t         lucBiPMTA1_LBAvOLCRawInstLum;
   UInt_t          lucBiPMTA1_NOrbPhys;
   UInt_t          lucBiPMTA1_NOrbAll;
   UInt_t          lucBiPMTA1_NOrbOLC;
   UInt_t          lucBiPMTA1_DetectorState;
   UInt_t          lucBiPMTA1_LumiValid;
   UInt_t          lucBiPMTA7_LumiChannel;
   Float_t         lucBiPMTA7_LBAvInstLumPhys;
   Float_t         lucBiPMTA7_LBAvEvtsPerBXPhys;
   Float_t         lucBiPMTA7_LBAvRawInstLumPhys;
   Float_t         lucBiPMTA7_LBAvInstLumAll;
   Float_t         lucBiPMTA7_LBAvEvtsPerBXAll;
   Float_t         lucBiPMTA7_LBAvRawInstLumAll;
   Float_t         lucBiPMTA7_LBAvOLCInstLum;
   Float_t         lucBiPMTA7_LBAvOLCEvtsPerBX;
   Float_t         lucBiPMTA7_LBAvOLCRawInstLum;
   UInt_t          lucBiPMTA7_NOrbPhys;
   UInt_t          lucBiPMTA7_NOrbAll;
   UInt_t          lucBiPMTA7_NOrbOLC;
   UInt_t          lucBiPMTA7_DetectorState;
   UInt_t          lucBiPMTA7_LumiValid;
   UInt_t          lucBiPMTA9_LumiChannel;
   Float_t         lucBiPMTA9_LBAvInstLumPhys;
   Float_t         lucBiPMTA9_LBAvEvtsPerBXPhys;
   Float_t         lucBiPMTA9_LBAvRawInstLumPhys;
   Float_t         lucBiPMTA9_LBAvInstLumAll;
   Float_t         lucBiPMTA9_LBAvEvtsPerBXAll;
   Float_t         lucBiPMTA9_LBAvRawInstLumAll;
   Float_t         lucBiPMTA9_LBAvOLCInstLum;
   Float_t         lucBiPMTA9_LBAvOLCEvtsPerBX;
   Float_t         lucBiPMTA9_LBAvOLCRawInstLum;
   UInt_t          lucBiPMTA9_NOrbPhys;
   UInt_t          lucBiPMTA9_NOrbAll;
   UInt_t          lucBiPMTA9_NOrbOLC;
   UInt_t          lucBiPMTA9_DetectorState;
   UInt_t          lucBiPMTA9_LumiValid;
   UInt_t          lucBiPMTA13_LumiChannel;
   Float_t         lucBiPMTA13_LBAvInstLumPhys;
   Float_t         lucBiPMTA13_LBAvEvtsPerBXPhys;
   Float_t         lucBiPMTA13_LBAvRawInstLumPhys;
   Float_t         lucBiPMTA13_LBAvInstLumAll;
   Float_t         lucBiPMTA13_LBAvEvtsPerBXAll;
   Float_t         lucBiPMTA13_LBAvRawInstLumAll;
   Float_t         lucBiPMTA13_LBAvOLCInstLum;
   Float_t         lucBiPMTA13_LBAvOLCEvtsPerBX;
   Float_t         lucBiPMTA13_LBAvOLCRawInstLum;
   UInt_t          lucBiPMTA13_NOrbPhys;
   UInt_t          lucBiPMTA13_NOrbAll;
   UInt_t          lucBiPMTA13_NOrbOLC;
   UInt_t          lucBiPMTA13_DetectorState;
   UInt_t          lucBiPMTA13_LumiValid;
   UInt_t          lucBiPMTC1_LumiChannel;
   Float_t         lucBiPMTC1_LBAvInstLumPhys;
   Float_t         lucBiPMTC1_LBAvEvtsPerBXPhys;
   Float_t         lucBiPMTC1_LBAvRawInstLumPhys;
   Float_t         lucBiPMTC1_LBAvInstLumAll;
   Float_t         lucBiPMTC1_LBAvEvtsPerBXAll;
   Float_t         lucBiPMTC1_LBAvRawInstLumAll;
   Float_t         lucBiPMTC1_LBAvOLCInstLum;
   Float_t         lucBiPMTC1_LBAvOLCEvtsPerBX;
   Float_t         lucBiPMTC1_LBAvOLCRawInstLum;
   UInt_t          lucBiPMTC1_NOrbPhys;
   UInt_t          lucBiPMTC1_NOrbAll;
   UInt_t          lucBiPMTC1_NOrbOLC;
   UInt_t          lucBiPMTC1_DetectorState;
   UInt_t          lucBiPMTC1_LumiValid;
   UInt_t          lucBiPMTC5_LumiChannel;
   Float_t         lucBiPMTC5_LBAvInstLumPhys;
   Float_t         lucBiPMTC5_LBAvEvtsPerBXPhys;
   Float_t         lucBiPMTC5_LBAvRawInstLumPhys;
   Float_t         lucBiPMTC5_LBAvInstLumAll;
   Float_t         lucBiPMTC5_LBAvEvtsPerBXAll;
   Float_t         lucBiPMTC5_LBAvRawInstLumAll;
   Float_t         lucBiPMTC5_LBAvOLCInstLum;
   Float_t         lucBiPMTC5_LBAvOLCEvtsPerBX;
   Float_t         lucBiPMTC5_LBAvOLCRawInstLum;
   UInt_t          lucBiPMTC5_NOrbPhys;
   UInt_t          lucBiPMTC5_NOrbAll;
   UInt_t          lucBiPMTC5_NOrbOLC;
   UInt_t          lucBiPMTC5_DetectorState;
   UInt_t          lucBiPMTC5_LumiValid;
   UInt_t          lucBiPMTC9_LumiChannel;
   Float_t         lucBiPMTC9_LBAvInstLumPhys;
   Float_t         lucBiPMTC9_LBAvEvtsPerBXPhys;
   Float_t         lucBiPMTC9_LBAvRawInstLumPhys;
   Float_t         lucBiPMTC9_LBAvInstLumAll;
   Float_t         lucBiPMTC9_LBAvEvtsPerBXAll;
   Float_t         lucBiPMTC9_LBAvRawInstLumAll;
   Float_t         lucBiPMTC9_LBAvOLCInstLum;
   Float_t         lucBiPMTC9_LBAvOLCEvtsPerBX;
   Float_t         lucBiPMTC9_LBAvOLCRawInstLum;
   UInt_t          lucBiPMTC9_NOrbPhys;
   UInt_t          lucBiPMTC9_NOrbAll;
   UInt_t          lucBiPMTC9_NOrbOLC;
   UInt_t          lucBiPMTC9_DetectorState;
   UInt_t          lucBiPMTC9_LumiValid;
   UInt_t          lucBiPMTC13_LumiChannel;
   Float_t         lucBiPMTC13_LBAvInstLumPhys;
   Float_t         lucBiPMTC13_LBAvEvtsPerBXPhys;
   Float_t         lucBiPMTC13_LBAvRawInstLumPhys;
   Float_t         lucBiPMTC13_LBAvInstLumAll;
   Float_t         lucBiPMTC13_LBAvEvtsPerBXAll;
   Float_t         lucBiPMTC13_LBAvRawInstLumAll;
   Float_t         lucBiPMTC13_LBAvOLCInstLum;
   Float_t         lucBiPMTC13_LBAvOLCEvtsPerBX;
   Float_t         lucBiPMTC13_LBAvOLCRawInstLum;
   UInt_t          lucBiPMTC13_NOrbPhys;
   UInt_t          lucBiPMTC13_NOrbAll;
   UInt_t          lucBiPMTC13_NOrbOLC;
   UInt_t          lucBiPMTC13_DetectorState;
   UInt_t          lucBiPMTC13_LumiValid;
   UInt_t          lucBi2PMTA4_LumiChannel;
   Float_t         lucBi2PMTA4_LBAvInstLumPhys;
   Float_t         lucBi2PMTA4_LBAvEvtsPerBXPhys;
   Float_t         lucBi2PMTA4_LBAvRawInstLumPhys;
   Float_t         lucBi2PMTA4_LBAvInstLumAll;
   Float_t         lucBi2PMTA4_LBAvEvtsPerBXAll;
   Float_t         lucBi2PMTA4_LBAvRawInstLumAll;
   Float_t         lucBi2PMTA4_LBAvOLCInstLum;
   Float_t         lucBi2PMTA4_LBAvOLCEvtsPerBX;
   Float_t         lucBi2PMTA4_LBAvOLCRawInstLum;
   UInt_t          lucBi2PMTA4_NOrbPhys;
   UInt_t          lucBi2PMTA4_NOrbAll;
   UInt_t          lucBi2PMTA4_NOrbOLC;
   UInt_t          lucBi2PMTA4_DetectorState;
   UInt_t          lucBi2PMTA4_LumiValid;
   UInt_t          lucBi2PMTA8_LumiChannel;
   Float_t         lucBi2PMTA8_LBAvInstLumPhys;
   Float_t         lucBi2PMTA8_LBAvEvtsPerBXPhys;
   Float_t         lucBi2PMTA8_LBAvRawInstLumPhys;
   Float_t         lucBi2PMTA8_LBAvInstLumAll;
   Float_t         lucBi2PMTA8_LBAvEvtsPerBXAll;
   Float_t         lucBi2PMTA8_LBAvRawInstLumAll;
   Float_t         lucBi2PMTA8_LBAvOLCInstLum;
   Float_t         lucBi2PMTA8_LBAvOLCEvtsPerBX;
   Float_t         lucBi2PMTA8_LBAvOLCRawInstLum;
   UInt_t          lucBi2PMTA8_NOrbPhys;
   UInt_t          lucBi2PMTA8_NOrbAll;
   UInt_t          lucBi2PMTA8_NOrbOLC;
   UInt_t          lucBi2PMTA8_DetectorState;
   UInt_t          lucBi2PMTA8_LumiValid;
   UInt_t          lucBi2PMTA12_LumiChannel;
   Float_t         lucBi2PMTA12_LBAvInstLumPhys;
   Float_t         lucBi2PMTA12_LBAvEvtsPerBXPhys;
   Float_t         lucBi2PMTA12_LBAvRawInstLumPhys;
   Float_t         lucBi2PMTA12_LBAvInstLumAll;
   Float_t         lucBi2PMTA12_LBAvEvtsPerBXAll;
   Float_t         lucBi2PMTA12_LBAvRawInstLumAll;
   Float_t         lucBi2PMTA12_LBAvOLCInstLum;
   Float_t         lucBi2PMTA12_LBAvOLCEvtsPerBX;
   Float_t         lucBi2PMTA12_LBAvOLCRawInstLum;
   UInt_t          lucBi2PMTA12_NOrbPhys;
   UInt_t          lucBi2PMTA12_NOrbAll;
   UInt_t          lucBi2PMTA12_NOrbOLC;
   UInt_t          lucBi2PMTA12_DetectorState;
   UInt_t          lucBi2PMTA12_LumiValid;
   UInt_t          lucBi2PMTA16_LumiChannel;
   Float_t         lucBi2PMTA16_LBAvInstLumPhys;
   Float_t         lucBi2PMTA16_LBAvEvtsPerBXPhys;
   Float_t         lucBi2PMTA16_LBAvRawInstLumPhys;
   Float_t         lucBi2PMTA16_LBAvInstLumAll;
   Float_t         lucBi2PMTA16_LBAvEvtsPerBXAll;
   Float_t         lucBi2PMTA16_LBAvRawInstLumAll;
   Float_t         lucBi2PMTA16_LBAvOLCInstLum;
   Float_t         lucBi2PMTA16_LBAvOLCEvtsPerBX;
   Float_t         lucBi2PMTA16_LBAvOLCRawInstLum;
   UInt_t          lucBi2PMTA16_NOrbPhys;
   UInt_t          lucBi2PMTA16_NOrbAll;
   UInt_t          lucBi2PMTA16_NOrbOLC;
   UInt_t          lucBi2PMTA16_DetectorState;
   UInt_t          lucBi2PMTA16_LumiValid;
   UInt_t          lucBi2PMTC4_LumiChannel;
   Float_t         lucBi2PMTC4_LBAvInstLumPhys;
   Float_t         lucBi2PMTC4_LBAvEvtsPerBXPhys;
   Float_t         lucBi2PMTC4_LBAvRawInstLumPhys;
   Float_t         lucBi2PMTC4_LBAvInstLumAll;
   Float_t         lucBi2PMTC4_LBAvEvtsPerBXAll;
   Float_t         lucBi2PMTC4_LBAvRawInstLumAll;
   Float_t         lucBi2PMTC4_LBAvOLCInstLum;
   Float_t         lucBi2PMTC4_LBAvOLCEvtsPerBX;
   Float_t         lucBi2PMTC4_LBAvOLCRawInstLum;
   UInt_t          lucBi2PMTC4_NOrbPhys;
   UInt_t          lucBi2PMTC4_NOrbAll;
   UInt_t          lucBi2PMTC4_NOrbOLC;
   UInt_t          lucBi2PMTC4_DetectorState;
   UInt_t          lucBi2PMTC4_LumiValid;
   UInt_t          lucBi2PMTC8_LumiChannel;
   Float_t         lucBi2PMTC8_LBAvInstLumPhys;
   Float_t         lucBi2PMTC8_LBAvEvtsPerBXPhys;
   Float_t         lucBi2PMTC8_LBAvRawInstLumPhys;
   Float_t         lucBi2PMTC8_LBAvInstLumAll;
   Float_t         lucBi2PMTC8_LBAvEvtsPerBXAll;
   Float_t         lucBi2PMTC8_LBAvRawInstLumAll;
   Float_t         lucBi2PMTC8_LBAvOLCInstLum;
   Float_t         lucBi2PMTC8_LBAvOLCEvtsPerBX;
   Float_t         lucBi2PMTC8_LBAvOLCRawInstLum;
   UInt_t          lucBi2PMTC8_NOrbPhys;
   UInt_t          lucBi2PMTC8_NOrbAll;
   UInt_t          lucBi2PMTC8_NOrbOLC;
   UInt_t          lucBi2PMTC8_DetectorState;
   UInt_t          lucBi2PMTC8_LumiValid;
   UInt_t          lucBi2PMTC12_LumiChannel;
   Float_t         lucBi2PMTC12_LBAvInstLumPhys;
   Float_t         lucBi2PMTC12_LBAvEvtsPerBXPhys;
   Float_t         lucBi2PMTC12_LBAvRawInstLumPhys;
   Float_t         lucBi2PMTC12_LBAvInstLumAll;
   Float_t         lucBi2PMTC12_LBAvEvtsPerBXAll;
   Float_t         lucBi2PMTC12_LBAvRawInstLumAll;
   Float_t         lucBi2PMTC12_LBAvOLCInstLum;
   Float_t         lucBi2PMTC12_LBAvOLCEvtsPerBX;
   Float_t         lucBi2PMTC12_LBAvOLCRawInstLum;
   UInt_t          lucBi2PMTC12_NOrbPhys;
   UInt_t          lucBi2PMTC12_NOrbAll;
   UInt_t          lucBi2PMTC12_NOrbOLC;
   UInt_t          lucBi2PMTC12_DetectorState;
   UInt_t          lucBi2PMTC12_LumiValid;
   UInt_t          lucBi2PMTC16_LumiChannel;
   Float_t         lucBi2PMTC16_LBAvInstLumPhys;
   Float_t         lucBi2PMTC16_LBAvEvtsPerBXPhys;
   Float_t         lucBi2PMTC16_LBAvRawInstLumPhys;
   Float_t         lucBi2PMTC16_LBAvInstLumAll;
   Float_t         lucBi2PMTC16_LBAvEvtsPerBXAll;
   Float_t         lucBi2PMTC16_LBAvRawInstLumAll;
   Float_t         lucBi2PMTC16_LBAvOLCInstLum;
   Float_t         lucBi2PMTC16_LBAvOLCEvtsPerBX;
   Float_t         lucBi2PMTC16_LBAvOLCRawInstLum;
   UInt_t          lucBi2PMTC16_NOrbPhys;
   UInt_t          lucBi2PMTC16_NOrbAll;
   UInt_t          lucBi2PMTC16_NOrbOLC;
   UInt_t          lucBi2PMTC16_DetectorState;
   UInt_t          lucBi2PMTC16_LumiValid;
   Int_t           lucBiPMTA1_CalValid;
   Float_t         lucBiPMTA1_LBAvInstLumi;
   Float_t         lucBiPMTA1_LBAvEvtsPerBX;
   Float_t         lucBiPMTA1_BunchInstLumi[3564];
   Int_t           lucBiPMTA7_CalValid;
   Float_t         lucBiPMTA7_LBAvInstLumi;
   Float_t         lucBiPMTA7_LBAvEvtsPerBX;
   Float_t         lucBiPMTA7_BunchInstLumi[3564];
   Int_t           lucBiPMTA9_CalValid;
   Float_t         lucBiPMTA9_LBAvInstLumi;
   Float_t         lucBiPMTA9_LBAvEvtsPerBX;
   Float_t         lucBiPMTA9_BunchInstLumi[3564];
   Int_t           lucBiPMTA13_CalValid;
   Float_t         lucBiPMTA13_LBAvInstLumi;
   Float_t         lucBiPMTA13_LBAvEvtsPerBX;
   Float_t         lucBiPMTA13_BunchInstLumi[3564];
   Int_t           lucBiPMTC1_CalValid;
   Float_t         lucBiPMTC1_LBAvInstLumi;
   Float_t         lucBiPMTC1_LBAvEvtsPerBX;
   Float_t         lucBiPMTC1_BunchInstLumi[3564];
   Int_t           lucBiPMTC5_CalValid;
   Float_t         lucBiPMTC5_LBAvInstLumi;
   Float_t         lucBiPMTC5_LBAvEvtsPerBX;
   Float_t         lucBiPMTC5_BunchInstLumi[3564];
   Int_t           lucBiPMTC9_CalValid;
   Float_t         lucBiPMTC9_LBAvInstLumi;
   Float_t         lucBiPMTC9_LBAvEvtsPerBX;
   Float_t         lucBiPMTC9_BunchInstLumi[3564];
   Int_t           lucBiPMTC13_CalValid;
   Float_t         lucBiPMTC13_LBAvInstLumi;
   Float_t         lucBiPMTC13_LBAvEvtsPerBX;
   Float_t         lucBiPMTC13_BunchInstLumi[3564];
   Int_t           lucBi2PMTA4_CalValid;
   Float_t         lucBi2PMTA4_LBAvInstLumi;
   Float_t         lucBi2PMTA4_LBAvEvtsPerBX;
   Float_t         lucBi2PMTA4_BunchInstLumi[3564];
   Int_t           lucBi2PMTA8_CalValid;
   Float_t         lucBi2PMTA8_LBAvInstLumi;
   Float_t         lucBi2PMTA8_LBAvEvtsPerBX;
   Float_t         lucBi2PMTA8_BunchInstLumi[3564];
   Int_t           lucBi2PMTA12_CalValid;
   Float_t         lucBi2PMTA12_LBAvInstLumi;
   Float_t         lucBi2PMTA12_LBAvEvtsPerBX;
   Float_t         lucBi2PMTA12_BunchInstLumi[3564];
   Int_t           lucBi2PMTA16_CalValid;
   Float_t         lucBi2PMTA16_LBAvInstLumi;
   Float_t         lucBi2PMTA16_LBAvEvtsPerBX;
   Float_t         lucBi2PMTA16_BunchInstLumi[3564];
   Int_t           lucBiHitOR_CalValid;
   Float_t         lucBiHitOR_LBAvInstLumi;
   Float_t         lucBiHitOR_LBAvEvtsPerBX;
   Float_t         lucBiHitOR_BunchInstLumi[3564];
   Int_t           lucBi2PMTC8_CalValid;
   Float_t         lucBi2PMTC8_LBAvInstLumi;
   Float_t         lucBi2PMTC8_LBAvEvtsPerBX;
   Float_t         lucBi2PMTC8_BunchInstLumi[3564];
   Int_t           lucBi2PMTC12_CalValid;
   Float_t         lucBi2PMTC12_LBAvInstLumi;
   Float_t         lucBi2PMTC12_LBAvEvtsPerBX;
   Float_t         lucBi2PMTC12_BunchInstLumi[3564];
   Int_t           lucBi2PMTC16_CalValid;
   Float_t         lucBi2PMTC16_LBAvInstLumi;
   Float_t         lucBi2PMTC16_LBAvEvtsPerBX;
   Float_t         lucBi2PMTC16_BunchInstLumi[3564];
   Int_t           lucBi2PMTC4_CalValid;
   Float_t         lucBi2PMTC4_LBAvInstLumi;
   Float_t         lucBi2PMTC4_LBAvEvtsPerBX;
   Float_t         lucBi2PMTC4_BunchInstLumi[3564];
   Int_t           lucBi2HitOR_CalValid;
   Float_t         lucBi2HitOR_LBAvInstLumi;
   Float_t         lucBi2HitOR_LBAvEvtsPerBX;
   Float_t         lucBi2HitOR_BunchInstLumi[3564];

   // List of branches
   TBranch        *b_StartTime;   //!
   TBranch        *b_EndTime;   //!
   TBranch        *b_ScanRun;   //!
   TBranch        *b_ScanLB;   //!
   TBranch        *b_StepProgress;   //!
   TBranch        *b_ScanningIP;   //!
   TBranch        *b_AcquisitionFlag;   //!
   TBranch        *b_MovingBeam;   //!
   TBranch        *b_NominalSeparation;   //!
   TBranch        *b_ScanInPlane;   //!
   TBranch        *b_ScanStep;   //!
   TBranch        *b_NominalSeparationPlane;   //!
   TBranch        *b_B1DeltaXSet;   //!
   TBranch        *b_B1DeltaYSet;   //!
   TBranch        *b_B2DeltaXSet;   //!
   TBranch        *b_B2DeltaYSet;   //!
   TBranch        *b_B1PositionH;   //!
   TBranch        *b_B1PositionV;   //!
   TBranch        *b_B1AngleH;   //!
   TBranch        *b_B1AngleV;   //!
   TBranch        *b_B2PositionH;   //!
   TBranch        *b_B2PositionV;   //!
   TBranch        *b_B2AngleH;   //!
   TBranch        *b_B2AngleV;   //!
   TBranch        *b_B1Bunches;   //!
   TBranch        *b_B1BCIDs;   //!
   TBranch        *b_B2Bunches;   //!
   TBranch        *b_B2BCIDs;   //!
   TBranch        *b_LuminousBunches;   //!
   TBranch        *b_LuminousBCIDs;   //!
   TBranch        *b_BPTX_B1Intensity;   //!
   TBranch        *b_BPTX_B2Intensity;   //!
   TBranch        *b_BPTX_B1IntensityAll;   //!
   TBranch        *b_BPTX_B2IntensityAll;   //!
   TBranch        *b_BCT_B1Intensity;   //!
   TBranch        *b_BCT_B2Intensity;   //!
   TBranch        *b_BCT_B1IntensityAll;   //!
   TBranch        *b_BCT_B2IntensityAll;   //!
   TBranch        *b_DCCT_B1IntensityAll;   //!
   TBranch        *b_DCCT_B2IntensityAll;   //!
   TBranch        *b_DCCT24_B1IntensityAll;   //!
   TBranch        *b_DCCT24_B2IntensityAll;   //!
   TBranch        *b_BCT_Valid;   //!
   TBranch        *b_BCT_B1BunchIntensity;   //!
   TBranch        *b_BCT_B2BunchIntensity;   //!
   TBranch        *b_BPTX_Valid;   //!
   TBranch        *b_BPTX_B1BunchIntensity;   //!
   TBranch        *b_BPTX_B2BunchIntensity;   //!
   TBranch        *b_lucBiPMTA1_Channel;   //!
   TBranch        *b_lucBiPMTA1_Valid;   //!
   TBranch        *b_lucBiPMTA1_AverageRawInstLum;   //!
   TBranch        *b_lucBiPMTA1_BunchRawInstLum;   //!
   TBranch        *b_lucBiPMTA7_Channel;   //!
   TBranch        *b_lucBiPMTA7_Valid;   //!
   TBranch        *b_lucBiPMTA7_AverageRawInstLum;   //!
   TBranch        *b_lucBiPMTA7_BunchRawInstLum;   //!
   TBranch        *b_lucBiPMTA9_Channel;   //!
   TBranch        *b_lucBiPMTA9_Valid;   //!
   TBranch        *b_lucBiPMTA9_AverageRawInstLum;   //!
   TBranch        *b_lucBiPMTA9_BunchRawInstLum;   //!
   TBranch        *b_lucBiPMTA13_Channel;   //!
   TBranch        *b_lucBiPMTA13_Valid;   //!
   TBranch        *b_lucBiPMTA13_AverageRawInstLum;   //!
   TBranch        *b_lucBiPMTA13_BunchRawInstLum;   //!
   TBranch        *b_lucBiPMTC1_Channel;   //!
   TBranch        *b_lucBiPMTC1_Valid;   //!
   TBranch        *b_lucBiPMTC1_AverageRawInstLum;   //!
   TBranch        *b_lucBiPMTC1_BunchRawInstLum;   //!
   TBranch        *b_lucBiPMTC5_Channel;   //!
   TBranch        *b_lucBiPMTC5_Valid;   //!
   TBranch        *b_lucBiPMTC5_AverageRawInstLum;   //!
   TBranch        *b_lucBiPMTC5_BunchRawInstLum;   //!
   TBranch        *b_lucBiPMTC9_Channel;   //!
   TBranch        *b_lucBiPMTC9_Valid;   //!
   TBranch        *b_lucBiPMTC9_AverageRawInstLum;   //!
   TBranch        *b_lucBiPMTC9_BunchRawInstLum;   //!
   TBranch        *b_lucBiPMTC13_Channel;   //!
   TBranch        *b_lucBiPMTC13_Valid;   //!
   TBranch        *b_lucBiPMTC13_AverageRawInstLum;   //!
   TBranch        *b_lucBiPMTC13_BunchRawInstLum;   //!
   TBranch        *b_lucBi2PMTA4_Channel;   //!
   TBranch        *b_lucBi2PMTA4_Valid;   //!
   TBranch        *b_lucBi2PMTA4_AverageRawInstLum;   //!
   TBranch        *b_lucBi2PMTA4_BunchRawInstLum;   //!
   TBranch        *b_lucBi2PMTA8_Channel;   //!
   TBranch        *b_lucBi2PMTA8_Valid;   //!
   TBranch        *b_lucBi2PMTA8_AverageRawInstLum;   //!
   TBranch        *b_lucBi2PMTA8_BunchRawInstLum;   //!
   TBranch        *b_lucBi2PMTA12_Channel;   //!
   TBranch        *b_lucBi2PMTA12_Valid;   //!
   TBranch        *b_lucBi2PMTA12_AverageRawInstLum;   //!
   TBranch        *b_lucBi2PMTA12_BunchRawInstLum;   //!
   TBranch        *b_lucBi2PMTA16_Channel;   //!
   TBranch        *b_lucBi2PMTA16_Valid;   //!
   TBranch        *b_lucBi2PMTA16_AverageRawInstLum;   //!
   TBranch        *b_lucBi2PMTA16_BunchRawInstLum;   //!
   TBranch        *b_lucBiHitOR_Channel;   //!
   TBranch        *b_lucBiHitOR_Valid;   //!
   TBranch        *b_lucBiHitOR_AverageRawInstLum;   //!
   TBranch        *b_lucBiHitOR_BunchRawInstLum;   //!
   TBranch        *b_lucBi2PMTC8_Channel;   //!
   TBranch        *b_lucBi2PMTC8_Valid;   //!
   TBranch        *b_lucBi2PMTC8_AverageRawInstLum;   //!
   TBranch        *b_lucBi2PMTC8_BunchRawInstLum;   //!
   TBranch        *b_lucBi2PMTC12_Channel;   //!
   TBranch        *b_lucBi2PMTC12_Valid;   //!
   TBranch        *b_lucBi2PMTC12_AverageRawInstLum;   //!
   TBranch        *b_lucBi2PMTC12_BunchRawInstLum;   //!
   TBranch        *b_lucBi2PMTC16_Channel;   //!
   TBranch        *b_lucBi2PMTC16_Valid;   //!
   TBranch        *b_lucBi2PMTC16_AverageRawInstLum;   //!
   TBranch        *b_lucBi2PMTC16_BunchRawInstLum;   //!
   TBranch        *b_lucBi2PMTC4_Channel;   //!
   TBranch        *b_lucBi2PMTC4_Valid;   //!
   TBranch        *b_lucBi2PMTC4_AverageRawInstLum;   //!
   TBranch        *b_lucBi2PMTC4_BunchRawInstLum;   //!
   TBranch        *b_lucBi2HitOR_Channel;   //!
   TBranch        *b_lucBi2HitOR_Valid;   //!
   TBranch        *b_lucBi2HitOR_AverageRawInstLum;   //!
   TBranch        *b_lucBi2HitOR_BunchRawInstLum;   //!
   TBranch        *b_lucBiHitOR_LumiChannel;   //!
   TBranch        *b_lucBiHitOR_LBAvInstLumPhys;   //!
   TBranch        *b_lucBiHitOR_LBAvEvtsPerBXPhys;   //!
   TBranch        *b_lucBiHitOR_LBAvRawInstLumPhys;   //!
   TBranch        *b_lucBiHitOR_LBAvInstLumAll;   //!
   TBranch        *b_lucBiHitOR_LBAvEvtsPerBXAll;   //!
   TBranch        *b_lucBiHitOR_LBAvRawInstLumAll;   //!
   TBranch        *b_lucBiHitOR_LBAvOLCInstLum;   //!
   TBranch        *b_lucBiHitOR_LBAvOLCEvtsPerBX;   //!
   TBranch        *b_lucBiHitOR_LBAvOLCRawInstLum;   //!
   TBranch        *b_lucBiHitOR_NOrbPhys;   //!
   TBranch        *b_lucBiHitOR_NOrbAll;   //!
   TBranch        *b_lucBiHitOR_NOrbOLC;   //!
   TBranch        *b_lucBiHitOR_DetectorState;   //!
   TBranch        *b_lucBiHitOR_LumiValid;   //!
   TBranch        *b_lucBi2HitOR_LumiChannel;   //!
   TBranch        *b_lucBi2HitOR_LBAvInstLumPhys;   //!
   TBranch        *b_lucBi2HitOR_LBAvEvtsPerBXPhys;   //!
   TBranch        *b_lucBi2HitOR_LBAvRawInstLumPhys;   //!
   TBranch        *b_lucBi2HitOR_LBAvInstLumAll;   //!
   TBranch        *b_lucBi2HitOR_LBAvEvtsPerBXAll;   //!
   TBranch        *b_lucBi2HitOR_LBAvRawInstLumAll;   //!
   TBranch        *b_lucBi2HitOR_LBAvOLCInstLum;   //!
   TBranch        *b_lucBi2HitOR_LBAvOLCEvtsPerBX;   //!
   TBranch        *b_lucBi2HitOR_LBAvOLCRawInstLum;   //!
   TBranch        *b_lucBi2HitOR_NOrbPhys;   //!
   TBranch        *b_lucBi2HitOR_NOrbAll;   //!
   TBranch        *b_lucBi2HitOR_NOrbOLC;   //!
   TBranch        *b_lucBi2HitOR_DetectorState;   //!
   TBranch        *b_lucBi2HitOR_LumiValid;   //!
   TBranch        *b_lucBiPMTA1_LumiChannel;   //!
   TBranch        *b_lucBiPMTA1_LBAvInstLumPhys;   //!
   TBranch        *b_lucBiPMTA1_LBAvEvtsPerBXPhys;   //!
   TBranch        *b_lucBiPMTA1_LBAvRawInstLumPhys;   //!
   TBranch        *b_lucBiPMTA1_LBAvInstLumAll;   //!
   TBranch        *b_lucBiPMTA1_LBAvEvtsPerBXAll;   //!
   TBranch        *b_lucBiPMTA1_LBAvRawInstLumAll;   //!
   TBranch        *b_lucBiPMTA1_LBAvOLCInstLum;   //!
   TBranch        *b_lucBiPMTA1_LBAvOLCEvtsPerBX;   //!
   TBranch        *b_lucBiPMTA1_LBAvOLCRawInstLum;   //!
   TBranch        *b_lucBiPMTA1_NOrbPhys;   //!
   TBranch        *b_lucBiPMTA1_NOrbAll;   //!
   TBranch        *b_lucBiPMTA1_NOrbOLC;   //!
   TBranch        *b_lucBiPMTA1_DetectorState;   //!
   TBranch        *b_lucBiPMTA1_LumiValid;   //!
   TBranch        *b_lucBiPMTA7_LumiChannel;   //!
   TBranch        *b_lucBiPMTA7_LBAvInstLumPhys;   //!
   TBranch        *b_lucBiPMTA7_LBAvEvtsPerBXPhys;   //!
   TBranch        *b_lucBiPMTA7_LBAvRawInstLumPhys;   //!
   TBranch        *b_lucBiPMTA7_LBAvInstLumAll;   //!
   TBranch        *b_lucBiPMTA7_LBAvEvtsPerBXAll;   //!
   TBranch        *b_lucBiPMTA7_LBAvRawInstLumAll;   //!
   TBranch        *b_lucBiPMTA7_LBAvOLCInstLum;   //!
   TBranch        *b_lucBiPMTA7_LBAvOLCEvtsPerBX;   //!
   TBranch        *b_lucBiPMTA7_LBAvOLCRawInstLum;   //!
   TBranch        *b_lucBiPMTA7_NOrbPhys;   //!
   TBranch        *b_lucBiPMTA7_NOrbAll;   //!
   TBranch        *b_lucBiPMTA7_NOrbOLC;   //!
   TBranch        *b_lucBiPMTA7_DetectorState;   //!
   TBranch        *b_lucBiPMTA7_LumiValid;   //!
   TBranch        *b_lucBiPMTA9_LumiChannel;   //!
   TBranch        *b_lucBiPMTA9_LBAvInstLumPhys;   //!
   TBranch        *b_lucBiPMTA9_LBAvEvtsPerBXPhys;   //!
   TBranch        *b_lucBiPMTA9_LBAvRawInstLumPhys;   //!
   TBranch        *b_lucBiPMTA9_LBAvInstLumAll;   //!
   TBranch        *b_lucBiPMTA9_LBAvEvtsPerBXAll;   //!
   TBranch        *b_lucBiPMTA9_LBAvRawInstLumAll;   //!
   TBranch        *b_lucBiPMTA9_LBAvOLCInstLum;   //!
   TBranch        *b_lucBiPMTA9_LBAvOLCEvtsPerBX;   //!
   TBranch        *b_lucBiPMTA9_LBAvOLCRawInstLum;   //!
   TBranch        *b_lucBiPMTA9_NOrbPhys;   //!
   TBranch        *b_lucBiPMTA9_NOrbAll;   //!
   TBranch        *b_lucBiPMTA9_NOrbOLC;   //!
   TBranch        *b_lucBiPMTA9_DetectorState;   //!
   TBranch        *b_lucBiPMTA9_LumiValid;   //!
   TBranch        *b_lucBiPMTA13_LumiChannel;   //!
   TBranch        *b_lucBiPMTA13_LBAvInstLumPhys;   //!
   TBranch        *b_lucBiPMTA13_LBAvEvtsPerBXPhys;   //!
   TBranch        *b_lucBiPMTA13_LBAvRawInstLumPhys;   //!
   TBranch        *b_lucBiPMTA13_LBAvInstLumAll;   //!
   TBranch        *b_lucBiPMTA13_LBAvEvtsPerBXAll;   //!
   TBranch        *b_lucBiPMTA13_LBAvRawInstLumAll;   //!
   TBranch        *b_lucBiPMTA13_LBAvOLCInstLum;   //!
   TBranch        *b_lucBiPMTA13_LBAvOLCEvtsPerBX;   //!
   TBranch        *b_lucBiPMTA13_LBAvOLCRawInstLum;   //!
   TBranch        *b_lucBiPMTA13_NOrbPhys;   //!
   TBranch        *b_lucBiPMTA13_NOrbAll;   //!
   TBranch        *b_lucBiPMTA13_NOrbOLC;   //!
   TBranch        *b_lucBiPMTA13_DetectorState;   //!
   TBranch        *b_lucBiPMTA13_LumiValid;   //!
   TBranch        *b_lucBiPMTC1_LumiChannel;   //!
   TBranch        *b_lucBiPMTC1_LBAvInstLumPhys;   //!
   TBranch        *b_lucBiPMTC1_LBAvEvtsPerBXPhys;   //!
   TBranch        *b_lucBiPMTC1_LBAvRawInstLumPhys;   //!
   TBranch        *b_lucBiPMTC1_LBAvInstLumAll;   //!
   TBranch        *b_lucBiPMTC1_LBAvEvtsPerBXAll;   //!
   TBranch        *b_lucBiPMTC1_LBAvRawInstLumAll;   //!
   TBranch        *b_lucBiPMTC1_LBAvOLCInstLum;   //!
   TBranch        *b_lucBiPMTC1_LBAvOLCEvtsPerBX;   //!
   TBranch        *b_lucBiPMTC1_LBAvOLCRawInstLum;   //!
   TBranch        *b_lucBiPMTC1_NOrbPhys;   //!
   TBranch        *b_lucBiPMTC1_NOrbAll;   //!
   TBranch        *b_lucBiPMTC1_NOrbOLC;   //!
   TBranch        *b_lucBiPMTC1_DetectorState;   //!
   TBranch        *b_lucBiPMTC1_LumiValid;   //!
   TBranch        *b_lucBiPMTC5_LumiChannel;   //!
   TBranch        *b_lucBiPMTC5_LBAvInstLumPhys;   //!
   TBranch        *b_lucBiPMTC5_LBAvEvtsPerBXPhys;   //!
   TBranch        *b_lucBiPMTC5_LBAvRawInstLumPhys;   //!
   TBranch        *b_lucBiPMTC5_LBAvInstLumAll;   //!
   TBranch        *b_lucBiPMTC5_LBAvEvtsPerBXAll;   //!
   TBranch        *b_lucBiPMTC5_LBAvRawInstLumAll;   //!
   TBranch        *b_lucBiPMTC5_LBAvOLCInstLum;   //!
   TBranch        *b_lucBiPMTC5_LBAvOLCEvtsPerBX;   //!
   TBranch        *b_lucBiPMTC5_LBAvOLCRawInstLum;   //!
   TBranch        *b_lucBiPMTC5_NOrbPhys;   //!
   TBranch        *b_lucBiPMTC5_NOrbAll;   //!
   TBranch        *b_lucBiPMTC5_NOrbOLC;   //!
   TBranch        *b_lucBiPMTC5_DetectorState;   //!
   TBranch        *b_lucBiPMTC5_LumiValid;   //!
   TBranch        *b_lucBiPMTC9_LumiChannel;   //!
   TBranch        *b_lucBiPMTC9_LBAvInstLumPhys;   //!
   TBranch        *b_lucBiPMTC9_LBAvEvtsPerBXPhys;   //!
   TBranch        *b_lucBiPMTC9_LBAvRawInstLumPhys;   //!
   TBranch        *b_lucBiPMTC9_LBAvInstLumAll;   //!
   TBranch        *b_lucBiPMTC9_LBAvEvtsPerBXAll;   //!
   TBranch        *b_lucBiPMTC9_LBAvRawInstLumAll;   //!
   TBranch        *b_lucBiPMTC9_LBAvOLCInstLum;   //!
   TBranch        *b_lucBiPMTC9_LBAvOLCEvtsPerBX;   //!
   TBranch        *b_lucBiPMTC9_LBAvOLCRawInstLum;   //!
   TBranch        *b_lucBiPMTC9_NOrbPhys;   //!
   TBranch        *b_lucBiPMTC9_NOrbAll;   //!
   TBranch        *b_lucBiPMTC9_NOrbOLC;   //!
   TBranch        *b_lucBiPMTC9_DetectorState;   //!
   TBranch        *b_lucBiPMTC9_LumiValid;   //!
   TBranch        *b_lucBiPMTC13_LumiChannel;   //!
   TBranch        *b_lucBiPMTC13_LBAvInstLumPhys;   //!
   TBranch        *b_lucBiPMTC13_LBAvEvtsPerBXPhys;   //!
   TBranch        *b_lucBiPMTC13_LBAvRawInstLumPhys;   //!
   TBranch        *b_lucBiPMTC13_LBAvInstLumAll;   //!
   TBranch        *b_lucBiPMTC13_LBAvEvtsPerBXAll;   //!
   TBranch        *b_lucBiPMTC13_LBAvRawInstLumAll;   //!
   TBranch        *b_lucBiPMTC13_LBAvOLCInstLum;   //!
   TBranch        *b_lucBiPMTC13_LBAvOLCEvtsPerBX;   //!
   TBranch        *b_lucBiPMTC13_LBAvOLCRawInstLum;   //!
   TBranch        *b_lucBiPMTC13_NOrbPhys;   //!
   TBranch        *b_lucBiPMTC13_NOrbAll;   //!
   TBranch        *b_lucBiPMTC13_NOrbOLC;   //!
   TBranch        *b_lucBiPMTC13_DetectorState;   //!
   TBranch        *b_lucBiPMTC13_LumiValid;   //!
   TBranch        *b_lucBi2PMTA4_LumiChannel;   //!
   TBranch        *b_lucBi2PMTA4_LBAvInstLumPhys;   //!
   TBranch        *b_lucBi2PMTA4_LBAvEvtsPerBXPhys;   //!
   TBranch        *b_lucBi2PMTA4_LBAvRawInstLumPhys;   //!
   TBranch        *b_lucBi2PMTA4_LBAvInstLumAll;   //!
   TBranch        *b_lucBi2PMTA4_LBAvEvtsPerBXAll;   //!
   TBranch        *b_lucBi2PMTA4_LBAvRawInstLumAll;   //!
   TBranch        *b_lucBi2PMTA4_LBAvOLCInstLum;   //!
   TBranch        *b_lucBi2PMTA4_LBAvOLCEvtsPerBX;   //!
   TBranch        *b_lucBi2PMTA4_LBAvOLCRawInstLum;   //!
   TBranch        *b_lucBi2PMTA4_NOrbPhys;   //!
   TBranch        *b_lucBi2PMTA4_NOrbAll;   //!
   TBranch        *b_lucBi2PMTA4_NOrbOLC;   //!
   TBranch        *b_lucBi2PMTA4_DetectorState;   //!
   TBranch        *b_lucBi2PMTA4_LumiValid;   //!
   TBranch        *b_lucBi2PMTA8_LumiChannel;   //!
   TBranch        *b_lucBi2PMTA8_LBAvInstLumPhys;   //!
   TBranch        *b_lucBi2PMTA8_LBAvEvtsPerBXPhys;   //!
   TBranch        *b_lucBi2PMTA8_LBAvRawInstLumPhys;   //!
   TBranch        *b_lucBi2PMTA8_LBAvInstLumAll;   //!
   TBranch        *b_lucBi2PMTA8_LBAvEvtsPerBXAll;   //!
   TBranch        *b_lucBi2PMTA8_LBAvRawInstLumAll;   //!
   TBranch        *b_lucBi2PMTA8_LBAvOLCInstLum;   //!
   TBranch        *b_lucBi2PMTA8_LBAvOLCEvtsPerBX;   //!
   TBranch        *b_lucBi2PMTA8_LBAvOLCRawInstLum;   //!
   TBranch        *b_lucBi2PMTA8_NOrbPhys;   //!
   TBranch        *b_lucBi2PMTA8_NOrbAll;   //!
   TBranch        *b_lucBi2PMTA8_NOrbOLC;   //!
   TBranch        *b_lucBi2PMTA8_DetectorState;   //!
   TBranch        *b_lucBi2PMTA8_LumiValid;   //!
   TBranch        *b_lucBi2PMTA12_LumiChannel;   //!
   TBranch        *b_lucBi2PMTA12_LBAvInstLumPhys;   //!
   TBranch        *b_lucBi2PMTA12_LBAvEvtsPerBXPhys;   //!
   TBranch        *b_lucBi2PMTA12_LBAvRawInstLumPhys;   //!
   TBranch        *b_lucBi2PMTA12_LBAvInstLumAll;   //!
   TBranch        *b_lucBi2PMTA12_LBAvEvtsPerBXAll;   //!
   TBranch        *b_lucBi2PMTA12_LBAvRawInstLumAll;   //!
   TBranch        *b_lucBi2PMTA12_LBAvOLCInstLum;   //!
   TBranch        *b_lucBi2PMTA12_LBAvOLCEvtsPerBX;   //!
   TBranch        *b_lucBi2PMTA12_LBAvOLCRawInstLum;   //!
   TBranch        *b_lucBi2PMTA12_NOrbPhys;   //!
   TBranch        *b_lucBi2PMTA12_NOrbAll;   //!
   TBranch        *b_lucBi2PMTA12_NOrbOLC;   //!
   TBranch        *b_lucBi2PMTA12_DetectorState;   //!
   TBranch        *b_lucBi2PMTA12_LumiValid;   //!
   TBranch        *b_lucBi2PMTA16_LumiChannel;   //!
   TBranch        *b_lucBi2PMTA16_LBAvInstLumPhys;   //!
   TBranch        *b_lucBi2PMTA16_LBAvEvtsPerBXPhys;   //!
   TBranch        *b_lucBi2PMTA16_LBAvRawInstLumPhys;   //!
   TBranch        *b_lucBi2PMTA16_LBAvInstLumAll;   //!
   TBranch        *b_lucBi2PMTA16_LBAvEvtsPerBXAll;   //!
   TBranch        *b_lucBi2PMTA16_LBAvRawInstLumAll;   //!
   TBranch        *b_lucBi2PMTA16_LBAvOLCInstLum;   //!
   TBranch        *b_lucBi2PMTA16_LBAvOLCEvtsPerBX;   //!
   TBranch        *b_lucBi2PMTA16_LBAvOLCRawInstLum;   //!
   TBranch        *b_lucBi2PMTA16_NOrbPhys;   //!
   TBranch        *b_lucBi2PMTA16_NOrbAll;   //!
   TBranch        *b_lucBi2PMTA16_NOrbOLC;   //!
   TBranch        *b_lucBi2PMTA16_DetectorState;   //!
   TBranch        *b_lucBi2PMTA16_LumiValid;   //!
   TBranch        *b_lucBi2PMTC4_LumiChannel;   //!
   TBranch        *b_lucBi2PMTC4_LBAvInstLumPhys;   //!
   TBranch        *b_lucBi2PMTC4_LBAvEvtsPerBXPhys;   //!
   TBranch        *b_lucBi2PMTC4_LBAvRawInstLumPhys;   //!
   TBranch        *b_lucBi2PMTC4_LBAvInstLumAll;   //!
   TBranch        *b_lucBi2PMTC4_LBAvEvtsPerBXAll;   //!
   TBranch        *b_lucBi2PMTC4_LBAvRawInstLumAll;   //!
   TBranch        *b_lucBi2PMTC4_LBAvOLCInstLum;   //!
   TBranch        *b_lucBi2PMTC4_LBAvOLCEvtsPerBX;   //!
   TBranch        *b_lucBi2PMTC4_LBAvOLCRawInstLum;   //!
   TBranch        *b_lucBi2PMTC4_NOrbPhys;   //!
   TBranch        *b_lucBi2PMTC4_NOrbAll;   //!
   TBranch        *b_lucBi2PMTC4_NOrbOLC;   //!
   TBranch        *b_lucBi2PMTC4_DetectorState;   //!
   TBranch        *b_lucBi2PMTC4_LumiValid;   //!
   TBranch        *b_lucBi2PMTC8_LumiChannel;   //!
   TBranch        *b_lucBi2PMTC8_LBAvInstLumPhys;   //!
   TBranch        *b_lucBi2PMTC8_LBAvEvtsPerBXPhys;   //!
   TBranch        *b_lucBi2PMTC8_LBAvRawInstLumPhys;   //!
   TBranch        *b_lucBi2PMTC8_LBAvInstLumAll;   //!
   TBranch        *b_lucBi2PMTC8_LBAvEvtsPerBXAll;   //!
   TBranch        *b_lucBi2PMTC8_LBAvRawInstLumAll;   //!
   TBranch        *b_lucBi2PMTC8_LBAvOLCInstLum;   //!
   TBranch        *b_lucBi2PMTC8_LBAvOLCEvtsPerBX;   //!
   TBranch        *b_lucBi2PMTC8_LBAvOLCRawInstLum;   //!
   TBranch        *b_lucBi2PMTC8_NOrbPhys;   //!
   TBranch        *b_lucBi2PMTC8_NOrbAll;   //!
   TBranch        *b_lucBi2PMTC8_NOrbOLC;   //!
   TBranch        *b_lucBi2PMTC8_DetectorState;   //!
   TBranch        *b_lucBi2PMTC8_LumiValid;   //!
   TBranch        *b_lucBi2PMTC12_LumiChannel;   //!
   TBranch        *b_lucBi2PMTC12_LBAvInstLumPhys;   //!
   TBranch        *b_lucBi2PMTC12_LBAvEvtsPerBXPhys;   //!
   TBranch        *b_lucBi2PMTC12_LBAvRawInstLumPhys;   //!
   TBranch        *b_lucBi2PMTC12_LBAvInstLumAll;   //!
   TBranch        *b_lucBi2PMTC12_LBAvEvtsPerBXAll;   //!
   TBranch        *b_lucBi2PMTC12_LBAvRawInstLumAll;   //!
   TBranch        *b_lucBi2PMTC12_LBAvOLCInstLum;   //!
   TBranch        *b_lucBi2PMTC12_LBAvOLCEvtsPerBX;   //!
   TBranch        *b_lucBi2PMTC12_LBAvOLCRawInstLum;   //!
   TBranch        *b_lucBi2PMTC12_NOrbPhys;   //!
   TBranch        *b_lucBi2PMTC12_NOrbAll;   //!
   TBranch        *b_lucBi2PMTC12_NOrbOLC;   //!
   TBranch        *b_lucBi2PMTC12_DetectorState;   //!
   TBranch        *b_lucBi2PMTC12_LumiValid;   //!
   TBranch        *b_lucBi2PMTC16_LumiChannel;   //!
   TBranch        *b_lucBi2PMTC16_LBAvInstLumPhys;   //!
   TBranch        *b_lucBi2PMTC16_LBAvEvtsPerBXPhys;   //!
   TBranch        *b_lucBi2PMTC16_LBAvRawInstLumPhys;   //!
   TBranch        *b_lucBi2PMTC16_LBAvInstLumAll;   //!
   TBranch        *b_lucBi2PMTC16_LBAvEvtsPerBXAll;   //!
   TBranch        *b_lucBi2PMTC16_LBAvRawInstLumAll;   //!
   TBranch        *b_lucBi2PMTC16_LBAvOLCInstLum;   //!
   TBranch        *b_lucBi2PMTC16_LBAvOLCEvtsPerBX;   //!
   TBranch        *b_lucBi2PMTC16_LBAvOLCRawInstLum;   //!
   TBranch        *b_lucBi2PMTC16_NOrbPhys;   //!
   TBranch        *b_lucBi2PMTC16_NOrbAll;   //!
   TBranch        *b_lucBi2PMTC16_NOrbOLC;   //!
   TBranch        *b_lucBi2PMTC16_DetectorState;   //!
   TBranch        *b_lucBi2PMTC16_LumiValid;   //!
   TBranch        *b_lucBiPMTA1_CalValid;   //!
   TBranch        *b_lucBiPMTA1_LBAvInstLumi;   //!
   TBranch        *b_lucBiPMTA1_LBAvEvtsPerBX;   //!
   TBranch        *b_lucBiPMTA1_BunchInstLumi;   //!
   TBranch        *b_lucBiPMTA7_CalValid;   //!
   TBranch        *b_lucBiPMTA7_LBAvInstLumi;   //!
   TBranch        *b_lucBiPMTA7_LBAvEvtsPerBX;   //!
   TBranch        *b_lucBiPMTA7_BunchInstLumi;   //!
   TBranch        *b_lucBiPMTA9_CalValid;   //!
   TBranch        *b_lucBiPMTA9_LBAvInstLumi;   //!
   TBranch        *b_lucBiPMTA9_LBAvEvtsPerBX;   //!
   TBranch        *b_lucBiPMTA9_BunchInstLumi;   //!
   TBranch        *b_lucBiPMTA13_CalValid;   //!
   TBranch        *b_lucBiPMTA13_LBAvInstLumi;   //!
   TBranch        *b_lucBiPMTA13_LBAvEvtsPerBX;   //!
   TBranch        *b_lucBiPMTA13_BunchInstLumi;   //!
   TBranch        *b_lucBiPMTC1_CalValid;   //!
   TBranch        *b_lucBiPMTC1_LBAvInstLumi;   //!
   TBranch        *b_lucBiPMTC1_LBAvEvtsPerBX;   //!
   TBranch        *b_lucBiPMTC1_BunchInstLumi;   //!
   TBranch        *b_lucBiPMTC5_CalValid;   //!
   TBranch        *b_lucBiPMTC5_LBAvInstLumi;   //!
   TBranch        *b_lucBiPMTC5_LBAvEvtsPerBX;   //!
   TBranch        *b_lucBiPMTC5_BunchInstLumi;   //!
   TBranch        *b_lucBiPMTC9_CalValid;   //!
   TBranch        *b_lucBiPMTC9_LBAvInstLumi;   //!
   TBranch        *b_lucBiPMTC9_LBAvEvtsPerBX;   //!
   TBranch        *b_lucBiPMTC9_BunchInstLumi;   //!
   TBranch        *b_lucBiPMTC13_CalValid;   //!
   TBranch        *b_lucBiPMTC13_LBAvInstLumi;   //!
   TBranch        *b_lucBiPMTC13_LBAvEvtsPerBX;   //!
   TBranch        *b_lucBiPMTC13_BunchInstLumi;   //!
   TBranch        *b_lucBi2PMTA4_CalValid;   //!
   TBranch        *b_lucBi2PMTA4_LBAvInstLumi;   //!
   TBranch        *b_lucBi2PMTA4_LBAvEvtsPerBX;   //!
   TBranch        *b_lucBi2PMTA4_BunchInstLumi;   //!
   TBranch        *b_lucBi2PMTA8_CalValid;   //!
   TBranch        *b_lucBi2PMTA8_LBAvInstLumi;   //!
   TBranch        *b_lucBi2PMTA8_LBAvEvtsPerBX;   //!
   TBranch        *b_lucBi2PMTA8_BunchInstLumi;   //!
   TBranch        *b_lucBi2PMTA12_CalValid;   //!
   TBranch        *b_lucBi2PMTA12_LBAvInstLumi;   //!
   TBranch        *b_lucBi2PMTA12_LBAvEvtsPerBX;   //!
   TBranch        *b_lucBi2PMTA12_BunchInstLumi;   //!
   TBranch        *b_lucBi2PMTA16_CalValid;   //!
   TBranch        *b_lucBi2PMTA16_LBAvInstLumi;   //!
   TBranch        *b_lucBi2PMTA16_LBAvEvtsPerBX;   //!
   TBranch        *b_lucBi2PMTA16_BunchInstLumi;   //!
   TBranch        *b_lucBiHitOR_CalValid;   //!
   TBranch        *b_lucBiHitOR_LBAvInstLumi;   //!
   TBranch        *b_lucBiHitOR_LBAvEvtsPerBX;   //!
   TBranch        *b_lucBiHitOR_BunchInstLumi;   //!
   TBranch        *b_lucBi2PMTC8_CalValid;   //!
   TBranch        *b_lucBi2PMTC8_LBAvInstLumi;   //!
   TBranch        *b_lucBi2PMTC8_LBAvEvtsPerBX;   //!
   TBranch        *b_lucBi2PMTC8_BunchInstLumi;   //!
   TBranch        *b_lucBi2PMTC12_CalValid;   //!
   TBranch        *b_lucBi2PMTC12_LBAvInstLumi;   //!
   TBranch        *b_lucBi2PMTC12_LBAvEvtsPerBX;   //!
   TBranch        *b_lucBi2PMTC12_BunchInstLumi;   //!
   TBranch        *b_lucBi2PMTC16_CalValid;   //!
   TBranch        *b_lucBi2PMTC16_LBAvInstLumi;   //!
   TBranch        *b_lucBi2PMTC16_LBAvEvtsPerBX;   //!
   TBranch        *b_lucBi2PMTC16_BunchInstLumi;   //!
   TBranch        *b_lucBi2PMTC4_CalValid;   //!
   TBranch        *b_lucBi2PMTC4_LBAvInstLumi;   //!
   TBranch        *b_lucBi2PMTC4_LBAvEvtsPerBX;   //!
   TBranch        *b_lucBi2PMTC4_BunchInstLumi;   //!
   TBranch        *b_lucBi2HitOR_CalValid;   //!
   TBranch        *b_lucBi2HitOR_LBAvInstLumi;   //!
   TBranch        *b_lucBi2HitOR_LBAvEvtsPerBX;   //!
   TBranch        *b_lucBi2HitOR_BunchInstLumi;   //!

   vdMScanData(TString path);
   virtual ~vdMScanData();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef vdMScanData_cxx
vdMScanData::vdMScanData(TString path) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.

   TTree * tree;

   TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(path);
   if (!f || !f->IsOpen()) {
      f = new TFile(path);
   }
   f->GetObject("vdMScanData",tree);

   Init(tree);
}

vdMScanData::~vdMScanData()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t vdMScanData::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t vdMScanData::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void vdMScanData::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("StartTime", &StartTime, &b_StartTime);
   fChain->SetBranchAddress("EndTime", &EndTime, &b_EndTime);
   fChain->SetBranchAddress("ScanRun", &ScanRun, &b_ScanRun);
   fChain->SetBranchAddress("ScanLB", &ScanLB, &b_ScanLB);
   fChain->SetBranchAddress("StepProgress", &StepProgress, &b_StepProgress);
   fChain->SetBranchAddress("ScanningIP", &ScanningIP, &b_ScanningIP);
   fChain->SetBranchAddress("AcquisitionFlag", &AcquisitionFlag, &b_AcquisitionFlag);
   fChain->SetBranchAddress("MovingBeam", &MovingBeam, &b_MovingBeam);
   fChain->SetBranchAddress("NominalSeparation", &NominalSeparation, &b_NominalSeparation);
   fChain->SetBranchAddress("ScanInPlane", &ScanInPlane, &b_ScanInPlane);
   fChain->SetBranchAddress("ScanStep", &ScanStep, &b_ScanStep);
   fChain->SetBranchAddress("NominalSeparationPlane", &NominalSeparationPlane, &b_NominalSeparationPlane);
   fChain->SetBranchAddress("B1DeltaXSet", &B1DeltaXSet, &b_B1DeltaXSet);
   fChain->SetBranchAddress("B1DeltaYSet", &B1DeltaYSet, &b_B1DeltaYSet);
   fChain->SetBranchAddress("B2DeltaXSet", &B2DeltaXSet, &b_B2DeltaXSet);
   fChain->SetBranchAddress("B2DeltaYSet", &B2DeltaYSet, &b_B2DeltaYSet);
   fChain->SetBranchAddress("B1PositionH", &B1PositionH, &b_B1PositionH);
   fChain->SetBranchAddress("B1PositionV", &B1PositionV, &b_B1PositionV);
   fChain->SetBranchAddress("B1AngleH", &B1AngleH, &b_B1AngleH);
   fChain->SetBranchAddress("B1AngleV", &B1AngleV, &b_B1AngleV);
   fChain->SetBranchAddress("B2PositionH", &B2PositionH, &b_B2PositionH);
   fChain->SetBranchAddress("B2PositionV", &B2PositionV, &b_B2PositionV);
   fChain->SetBranchAddress("B2AngleH", &B2AngleH, &b_B2AngleH);
   fChain->SetBranchAddress("B2AngleV", &B2AngleV, &b_B2AngleV);
   fChain->SetBranchAddress("B1Bunches", &B1Bunches, &b_B1Bunches);
   fChain->SetBranchAddress("B1BCIDs", B1BCIDs, &b_B1BCIDs);
   fChain->SetBranchAddress("B2Bunches", &B2Bunches, &b_B2Bunches);
   fChain->SetBranchAddress("B2BCIDs", B2BCIDs, &b_B2BCIDs);
   fChain->SetBranchAddress("LuminousBunches", &LuminousBunches, &b_LuminousBunches);
   fChain->SetBranchAddress("LuminousBCIDs", LuminousBCIDs, &b_LuminousBCIDs);
   fChain->SetBranchAddress("BPTX_B1Intensity", &BPTX_B1Intensity, &b_BPTX_B1Intensity);
   fChain->SetBranchAddress("BPTX_B2Intensity", &BPTX_B2Intensity, &b_BPTX_B2Intensity);
   fChain->SetBranchAddress("BPTX_B1IntensityAll", &BPTX_B1IntensityAll, &b_BPTX_B1IntensityAll);
   fChain->SetBranchAddress("BPTX_B2IntensityAll", &BPTX_B2IntensityAll, &b_BPTX_B2IntensityAll);
   fChain->SetBranchAddress("BCT_B1Intensity", &BCT_B1Intensity, &b_BCT_B1Intensity);
   fChain->SetBranchAddress("BCT_B2Intensity", &BCT_B2Intensity, &b_BCT_B2Intensity);
   fChain->SetBranchAddress("BCT_B1IntensityAll", &BCT_B1IntensityAll, &b_BCT_B1IntensityAll);
   fChain->SetBranchAddress("BCT_B2IntensityAll", &BCT_B2IntensityAll, &b_BCT_B2IntensityAll);
   fChain->SetBranchAddress("DCCT_B1IntensityAll", &DCCT_B1IntensityAll, &b_DCCT_B1IntensityAll);
   fChain->SetBranchAddress("DCCT_B2IntensityAll", &DCCT_B2IntensityAll, &b_DCCT_B2IntensityAll);
   fChain->SetBranchAddress("DCCT24_B1IntensityAll", &DCCT24_B1IntensityAll, &b_DCCT24_B1IntensityAll);
   fChain->SetBranchAddress("DCCT24_B2IntensityAll", &DCCT24_B2IntensityAll, &b_DCCT24_B2IntensityAll);
   fChain->SetBranchAddress("BCT_Valid", &BCT_Valid, &b_BCT_Valid);
   fChain->SetBranchAddress("BCT_B1BunchIntensity", BCT_B1BunchIntensity, &b_BCT_B1BunchIntensity);
   fChain->SetBranchAddress("BCT_B2BunchIntensity", BCT_B2BunchIntensity, &b_BCT_B2BunchIntensity);
   fChain->SetBranchAddress("BPTX_Valid", &BPTX_Valid, &b_BPTX_Valid);
   fChain->SetBranchAddress("BPTX_B1BunchIntensity", BPTX_B1BunchIntensity, &b_BPTX_B1BunchIntensity);
   fChain->SetBranchAddress("BPTX_B2BunchIntensity", BPTX_B2BunchIntensity, &b_BPTX_B2BunchIntensity);
   fChain->SetBranchAddress("lucBiPMTA1_Channel", &lucBiPMTA1_Channel, &b_lucBiPMTA1_Channel);
   fChain->SetBranchAddress("lucBiPMTA1_Valid", &lucBiPMTA1_Valid, &b_lucBiPMTA1_Valid);
   fChain->SetBranchAddress("lucBiPMTA1_AverageRawInstLum", &lucBiPMTA1_AverageRawInstLum, &b_lucBiPMTA1_AverageRawInstLum);
   fChain->SetBranchAddress("lucBiPMTA1_BunchRawInstLum", lucBiPMTA1_BunchRawInstLum, &b_lucBiPMTA1_BunchRawInstLum);
   fChain->SetBranchAddress("lucBiPMTA7_Channel", &lucBiPMTA7_Channel, &b_lucBiPMTA7_Channel);
   fChain->SetBranchAddress("lucBiPMTA7_Valid", &lucBiPMTA7_Valid, &b_lucBiPMTA7_Valid);
   fChain->SetBranchAddress("lucBiPMTA7_AverageRawInstLum", &lucBiPMTA7_AverageRawInstLum, &b_lucBiPMTA7_AverageRawInstLum);
   fChain->SetBranchAddress("lucBiPMTA7_BunchRawInstLum", lucBiPMTA7_BunchRawInstLum, &b_lucBiPMTA7_BunchRawInstLum);
   fChain->SetBranchAddress("lucBiPMTA9_Channel", &lucBiPMTA9_Channel, &b_lucBiPMTA9_Channel);
   fChain->SetBranchAddress("lucBiPMTA9_Valid", &lucBiPMTA9_Valid, &b_lucBiPMTA9_Valid);
   fChain->SetBranchAddress("lucBiPMTA9_AverageRawInstLum", &lucBiPMTA9_AverageRawInstLum, &b_lucBiPMTA9_AverageRawInstLum);
   fChain->SetBranchAddress("lucBiPMTA9_BunchRawInstLum", lucBiPMTA9_BunchRawInstLum, &b_lucBiPMTA9_BunchRawInstLum);
   fChain->SetBranchAddress("lucBiPMTA13_Channel", &lucBiPMTA13_Channel, &b_lucBiPMTA13_Channel);
   fChain->SetBranchAddress("lucBiPMTA13_Valid", &lucBiPMTA13_Valid, &b_lucBiPMTA13_Valid);
   fChain->SetBranchAddress("lucBiPMTA13_AverageRawInstLum", &lucBiPMTA13_AverageRawInstLum, &b_lucBiPMTA13_AverageRawInstLum);
   fChain->SetBranchAddress("lucBiPMTA13_BunchRawInstLum", lucBiPMTA13_BunchRawInstLum, &b_lucBiPMTA13_BunchRawInstLum);
   fChain->SetBranchAddress("lucBiPMTC1_Channel", &lucBiPMTC1_Channel, &b_lucBiPMTC1_Channel);
   fChain->SetBranchAddress("lucBiPMTC1_Valid", &lucBiPMTC1_Valid, &b_lucBiPMTC1_Valid);
   fChain->SetBranchAddress("lucBiPMTC1_AverageRawInstLum", &lucBiPMTC1_AverageRawInstLum, &b_lucBiPMTC1_AverageRawInstLum);
   fChain->SetBranchAddress("lucBiPMTC1_BunchRawInstLum", lucBiPMTC1_BunchRawInstLum, &b_lucBiPMTC1_BunchRawInstLum);
   fChain->SetBranchAddress("lucBiPMTC5_Channel", &lucBiPMTC5_Channel, &b_lucBiPMTC5_Channel);
   fChain->SetBranchAddress("lucBiPMTC5_Valid", &lucBiPMTC5_Valid, &b_lucBiPMTC5_Valid);
   fChain->SetBranchAddress("lucBiPMTC5_AverageRawInstLum", &lucBiPMTC5_AverageRawInstLum, &b_lucBiPMTC5_AverageRawInstLum);
   fChain->SetBranchAddress("lucBiPMTC5_BunchRawInstLum", lucBiPMTC5_BunchRawInstLum, &b_lucBiPMTC5_BunchRawInstLum);
   fChain->SetBranchAddress("lucBiPMTC9_Channel", &lucBiPMTC9_Channel, &b_lucBiPMTC9_Channel);
   fChain->SetBranchAddress("lucBiPMTC9_Valid", &lucBiPMTC9_Valid, &b_lucBiPMTC9_Valid);
   fChain->SetBranchAddress("lucBiPMTC9_AverageRawInstLum", &lucBiPMTC9_AverageRawInstLum, &b_lucBiPMTC9_AverageRawInstLum);
   fChain->SetBranchAddress("lucBiPMTC9_BunchRawInstLum", lucBiPMTC9_BunchRawInstLum, &b_lucBiPMTC9_BunchRawInstLum);
   fChain->SetBranchAddress("lucBiPMTC13_Channel", &lucBiPMTC13_Channel, &b_lucBiPMTC13_Channel);
   fChain->SetBranchAddress("lucBiPMTC13_Valid", &lucBiPMTC13_Valid, &b_lucBiPMTC13_Valid);
   fChain->SetBranchAddress("lucBiPMTC13_AverageRawInstLum", &lucBiPMTC13_AverageRawInstLum, &b_lucBiPMTC13_AverageRawInstLum);
   fChain->SetBranchAddress("lucBiPMTC13_BunchRawInstLum", lucBiPMTC13_BunchRawInstLum, &b_lucBiPMTC13_BunchRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTA4_Channel", &lucBi2PMTA4_Channel, &b_lucBi2PMTA4_Channel);
   fChain->SetBranchAddress("lucBi2PMTA4_Valid", &lucBi2PMTA4_Valid, &b_lucBi2PMTA4_Valid);
   fChain->SetBranchAddress("lucBi2PMTA4_AverageRawInstLum", &lucBi2PMTA4_AverageRawInstLum, &b_lucBi2PMTA4_AverageRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTA4_BunchRawInstLum", lucBi2PMTA4_BunchRawInstLum, &b_lucBi2PMTA4_BunchRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTA8_Channel", &lucBi2PMTA8_Channel, &b_lucBi2PMTA8_Channel);
   fChain->SetBranchAddress("lucBi2PMTA8_Valid", &lucBi2PMTA8_Valid, &b_lucBi2PMTA8_Valid);
   fChain->SetBranchAddress("lucBi2PMTA8_AverageRawInstLum", &lucBi2PMTA8_AverageRawInstLum, &b_lucBi2PMTA8_AverageRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTA8_BunchRawInstLum", lucBi2PMTA8_BunchRawInstLum, &b_lucBi2PMTA8_BunchRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTA12_Channel", &lucBi2PMTA12_Channel, &b_lucBi2PMTA12_Channel);
   fChain->SetBranchAddress("lucBi2PMTA12_Valid", &lucBi2PMTA12_Valid, &b_lucBi2PMTA12_Valid);
   fChain->SetBranchAddress("lucBi2PMTA12_AverageRawInstLum", &lucBi2PMTA12_AverageRawInstLum, &b_lucBi2PMTA12_AverageRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTA12_BunchRawInstLum", lucBi2PMTA12_BunchRawInstLum, &b_lucBi2PMTA12_BunchRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTA16_Channel", &lucBi2PMTA16_Channel, &b_lucBi2PMTA16_Channel);
   fChain->SetBranchAddress("lucBi2PMTA16_Valid", &lucBi2PMTA16_Valid, &b_lucBi2PMTA16_Valid);
   fChain->SetBranchAddress("lucBi2PMTA16_AverageRawInstLum", &lucBi2PMTA16_AverageRawInstLum, &b_lucBi2PMTA16_AverageRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTA16_BunchRawInstLum", lucBi2PMTA16_BunchRawInstLum, &b_lucBi2PMTA16_BunchRawInstLum);
   fChain->SetBranchAddress("lucBiHitOR_Channel", &lucBiHitOR_Channel, &b_lucBiHitOR_Channel);
   fChain->SetBranchAddress("lucBiHitOR_Valid", &lucBiHitOR_Valid, &b_lucBiHitOR_Valid);
   fChain->SetBranchAddress("lucBiHitOR_AverageRawInstLum", &lucBiHitOR_AverageRawInstLum, &b_lucBiHitOR_AverageRawInstLum);
   fChain->SetBranchAddress("lucBiHitOR_BunchRawInstLum", lucBiHitOR_BunchRawInstLum, &b_lucBiHitOR_BunchRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTC8_Channel", &lucBi2PMTC8_Channel, &b_lucBi2PMTC8_Channel);
   fChain->SetBranchAddress("lucBi2PMTC8_Valid", &lucBi2PMTC8_Valid, &b_lucBi2PMTC8_Valid);
   fChain->SetBranchAddress("lucBi2PMTC8_AverageRawInstLum", &lucBi2PMTC8_AverageRawInstLum, &b_lucBi2PMTC8_AverageRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTC8_BunchRawInstLum", lucBi2PMTC8_BunchRawInstLum, &b_lucBi2PMTC8_BunchRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTC12_Channel", &lucBi2PMTC12_Channel, &b_lucBi2PMTC12_Channel);
   fChain->SetBranchAddress("lucBi2PMTC12_Valid", &lucBi2PMTC12_Valid, &b_lucBi2PMTC12_Valid);
   fChain->SetBranchAddress("lucBi2PMTC12_AverageRawInstLum", &lucBi2PMTC12_AverageRawInstLum, &b_lucBi2PMTC12_AverageRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTC12_BunchRawInstLum", lucBi2PMTC12_BunchRawInstLum, &b_lucBi2PMTC12_BunchRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTC16_Channel", &lucBi2PMTC16_Channel, &b_lucBi2PMTC16_Channel);
   fChain->SetBranchAddress("lucBi2PMTC16_Valid", &lucBi2PMTC16_Valid, &b_lucBi2PMTC16_Valid);
   fChain->SetBranchAddress("lucBi2PMTC16_AverageRawInstLum", &lucBi2PMTC16_AverageRawInstLum, &b_lucBi2PMTC16_AverageRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTC16_BunchRawInstLum", lucBi2PMTC16_BunchRawInstLum, &b_lucBi2PMTC16_BunchRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTC4_Channel", &lucBi2PMTC4_Channel, &b_lucBi2PMTC4_Channel);
   fChain->SetBranchAddress("lucBi2PMTC4_Valid", &lucBi2PMTC4_Valid, &b_lucBi2PMTC4_Valid);
   fChain->SetBranchAddress("lucBi2PMTC4_AverageRawInstLum", &lucBi2PMTC4_AverageRawInstLum, &b_lucBi2PMTC4_AverageRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTC4_BunchRawInstLum", lucBi2PMTC4_BunchRawInstLum, &b_lucBi2PMTC4_BunchRawInstLum);
   fChain->SetBranchAddress("lucBi2HitOR_Channel", &lucBi2HitOR_Channel, &b_lucBi2HitOR_Channel);
   fChain->SetBranchAddress("lucBi2HitOR_Valid", &lucBi2HitOR_Valid, &b_lucBi2HitOR_Valid);
   fChain->SetBranchAddress("lucBi2HitOR_AverageRawInstLum", &lucBi2HitOR_AverageRawInstLum, &b_lucBi2HitOR_AverageRawInstLum);
   fChain->SetBranchAddress("lucBi2HitOR_BunchRawInstLum", lucBi2HitOR_BunchRawInstLum, &b_lucBi2HitOR_BunchRawInstLum);
   fChain->SetBranchAddress("lucBiHitOR_LumiChannel", &lucBiHitOR_LumiChannel, &b_lucBiHitOR_LumiChannel);
   fChain->SetBranchAddress("lucBiHitOR_LBAvInstLumPhys", &lucBiHitOR_LBAvInstLumPhys, &b_lucBiHitOR_LBAvInstLumPhys);
   fChain->SetBranchAddress("lucBiHitOR_LBAvEvtsPerBXPhys", &lucBiHitOR_LBAvEvtsPerBXPhys, &b_lucBiHitOR_LBAvEvtsPerBXPhys);
   fChain->SetBranchAddress("lucBiHitOR_LBAvRawInstLumPhys", &lucBiHitOR_LBAvRawInstLumPhys, &b_lucBiHitOR_LBAvRawInstLumPhys);
   fChain->SetBranchAddress("lucBiHitOR_LBAvInstLumAll", &lucBiHitOR_LBAvInstLumAll, &b_lucBiHitOR_LBAvInstLumAll);
   fChain->SetBranchAddress("lucBiHitOR_LBAvEvtsPerBXAll", &lucBiHitOR_LBAvEvtsPerBXAll, &b_lucBiHitOR_LBAvEvtsPerBXAll);
   fChain->SetBranchAddress("lucBiHitOR_LBAvRawInstLumAll", &lucBiHitOR_LBAvRawInstLumAll, &b_lucBiHitOR_LBAvRawInstLumAll);
   fChain->SetBranchAddress("lucBiHitOR_LBAvOLCInstLum", &lucBiHitOR_LBAvOLCInstLum, &b_lucBiHitOR_LBAvOLCInstLum);
   fChain->SetBranchAddress("lucBiHitOR_LBAvOLCEvtsPerBX", &lucBiHitOR_LBAvOLCEvtsPerBX, &b_lucBiHitOR_LBAvOLCEvtsPerBX);
   fChain->SetBranchAddress("lucBiHitOR_LBAvOLCRawInstLum", &lucBiHitOR_LBAvOLCRawInstLum, &b_lucBiHitOR_LBAvOLCRawInstLum);
   fChain->SetBranchAddress("lucBiHitOR_NOrbPhys", &lucBiHitOR_NOrbPhys, &b_lucBiHitOR_NOrbPhys);
   fChain->SetBranchAddress("lucBiHitOR_NOrbAll", &lucBiHitOR_NOrbAll, &b_lucBiHitOR_NOrbAll);
   fChain->SetBranchAddress("lucBiHitOR_NOrbOLC", &lucBiHitOR_NOrbOLC, &b_lucBiHitOR_NOrbOLC);
   fChain->SetBranchAddress("lucBiHitOR_DetectorState", &lucBiHitOR_DetectorState, &b_lucBiHitOR_DetectorState);
   fChain->SetBranchAddress("lucBiHitOR_LumiValid", &lucBiHitOR_LumiValid, &b_lucBiHitOR_LumiValid);
   fChain->SetBranchAddress("lucBi2HitOR_LumiChannel", &lucBi2HitOR_LumiChannel, &b_lucBi2HitOR_LumiChannel);
   fChain->SetBranchAddress("lucBi2HitOR_LBAvInstLumPhys", &lucBi2HitOR_LBAvInstLumPhys, &b_lucBi2HitOR_LBAvInstLumPhys);
   fChain->SetBranchAddress("lucBi2HitOR_LBAvEvtsPerBXPhys", &lucBi2HitOR_LBAvEvtsPerBXPhys, &b_lucBi2HitOR_LBAvEvtsPerBXPhys);
   fChain->SetBranchAddress("lucBi2HitOR_LBAvRawInstLumPhys", &lucBi2HitOR_LBAvRawInstLumPhys, &b_lucBi2HitOR_LBAvRawInstLumPhys);
   fChain->SetBranchAddress("lucBi2HitOR_LBAvInstLumAll", &lucBi2HitOR_LBAvInstLumAll, &b_lucBi2HitOR_LBAvInstLumAll);
   fChain->SetBranchAddress("lucBi2HitOR_LBAvEvtsPerBXAll", &lucBi2HitOR_LBAvEvtsPerBXAll, &b_lucBi2HitOR_LBAvEvtsPerBXAll);
   fChain->SetBranchAddress("lucBi2HitOR_LBAvRawInstLumAll", &lucBi2HitOR_LBAvRawInstLumAll, &b_lucBi2HitOR_LBAvRawInstLumAll);
   fChain->SetBranchAddress("lucBi2HitOR_LBAvOLCInstLum", &lucBi2HitOR_LBAvOLCInstLum, &b_lucBi2HitOR_LBAvOLCInstLum);
   fChain->SetBranchAddress("lucBi2HitOR_LBAvOLCEvtsPerBX", &lucBi2HitOR_LBAvOLCEvtsPerBX, &b_lucBi2HitOR_LBAvOLCEvtsPerBX);
   fChain->SetBranchAddress("lucBi2HitOR_LBAvOLCRawInstLum", &lucBi2HitOR_LBAvOLCRawInstLum, &b_lucBi2HitOR_LBAvOLCRawInstLum);
   fChain->SetBranchAddress("lucBi2HitOR_NOrbPhys", &lucBi2HitOR_NOrbPhys, &b_lucBi2HitOR_NOrbPhys);
   fChain->SetBranchAddress("lucBi2HitOR_NOrbAll", &lucBi2HitOR_NOrbAll, &b_lucBi2HitOR_NOrbAll);
   fChain->SetBranchAddress("lucBi2HitOR_NOrbOLC", &lucBi2HitOR_NOrbOLC, &b_lucBi2HitOR_NOrbOLC);
   fChain->SetBranchAddress("lucBi2HitOR_DetectorState", &lucBi2HitOR_DetectorState, &b_lucBi2HitOR_DetectorState);
   fChain->SetBranchAddress("lucBi2HitOR_LumiValid", &lucBi2HitOR_LumiValid, &b_lucBi2HitOR_LumiValid);
   fChain->SetBranchAddress("lucBiPMTA1_LumiChannel", &lucBiPMTA1_LumiChannel, &b_lucBiPMTA1_LumiChannel);
   fChain->SetBranchAddress("lucBiPMTA1_LBAvInstLumPhys", &lucBiPMTA1_LBAvInstLumPhys, &b_lucBiPMTA1_LBAvInstLumPhys);
   fChain->SetBranchAddress("lucBiPMTA1_LBAvEvtsPerBXPhys", &lucBiPMTA1_LBAvEvtsPerBXPhys, &b_lucBiPMTA1_LBAvEvtsPerBXPhys);
   fChain->SetBranchAddress("lucBiPMTA1_LBAvRawInstLumPhys", &lucBiPMTA1_LBAvRawInstLumPhys, &b_lucBiPMTA1_LBAvRawInstLumPhys);
   fChain->SetBranchAddress("lucBiPMTA1_LBAvInstLumAll", &lucBiPMTA1_LBAvInstLumAll, &b_lucBiPMTA1_LBAvInstLumAll);
   fChain->SetBranchAddress("lucBiPMTA1_LBAvEvtsPerBXAll", &lucBiPMTA1_LBAvEvtsPerBXAll, &b_lucBiPMTA1_LBAvEvtsPerBXAll);
   fChain->SetBranchAddress("lucBiPMTA1_LBAvRawInstLumAll", &lucBiPMTA1_LBAvRawInstLumAll, &b_lucBiPMTA1_LBAvRawInstLumAll);
   fChain->SetBranchAddress("lucBiPMTA1_LBAvOLCInstLum", &lucBiPMTA1_LBAvOLCInstLum, &b_lucBiPMTA1_LBAvOLCInstLum);
   fChain->SetBranchAddress("lucBiPMTA1_LBAvOLCEvtsPerBX", &lucBiPMTA1_LBAvOLCEvtsPerBX, &b_lucBiPMTA1_LBAvOLCEvtsPerBX);
   fChain->SetBranchAddress("lucBiPMTA1_LBAvOLCRawInstLum", &lucBiPMTA1_LBAvOLCRawInstLum, &b_lucBiPMTA1_LBAvOLCRawInstLum);
   fChain->SetBranchAddress("lucBiPMTA1_NOrbPhys", &lucBiPMTA1_NOrbPhys, &b_lucBiPMTA1_NOrbPhys);
   fChain->SetBranchAddress("lucBiPMTA1_NOrbAll", &lucBiPMTA1_NOrbAll, &b_lucBiPMTA1_NOrbAll);
   fChain->SetBranchAddress("lucBiPMTA1_NOrbOLC", &lucBiPMTA1_NOrbOLC, &b_lucBiPMTA1_NOrbOLC);
   fChain->SetBranchAddress("lucBiPMTA1_DetectorState", &lucBiPMTA1_DetectorState, &b_lucBiPMTA1_DetectorState);
   fChain->SetBranchAddress("lucBiPMTA1_LumiValid", &lucBiPMTA1_LumiValid, &b_lucBiPMTA1_LumiValid);
   fChain->SetBranchAddress("lucBiPMTA7_LumiChannel", &lucBiPMTA7_LumiChannel, &b_lucBiPMTA7_LumiChannel);
   fChain->SetBranchAddress("lucBiPMTA7_LBAvInstLumPhys", &lucBiPMTA7_LBAvInstLumPhys, &b_lucBiPMTA7_LBAvInstLumPhys);
   fChain->SetBranchAddress("lucBiPMTA7_LBAvEvtsPerBXPhys", &lucBiPMTA7_LBAvEvtsPerBXPhys, &b_lucBiPMTA7_LBAvEvtsPerBXPhys);
   fChain->SetBranchAddress("lucBiPMTA7_LBAvRawInstLumPhys", &lucBiPMTA7_LBAvRawInstLumPhys, &b_lucBiPMTA7_LBAvRawInstLumPhys);
   fChain->SetBranchAddress("lucBiPMTA7_LBAvInstLumAll", &lucBiPMTA7_LBAvInstLumAll, &b_lucBiPMTA7_LBAvInstLumAll);
   fChain->SetBranchAddress("lucBiPMTA7_LBAvEvtsPerBXAll", &lucBiPMTA7_LBAvEvtsPerBXAll, &b_lucBiPMTA7_LBAvEvtsPerBXAll);
   fChain->SetBranchAddress("lucBiPMTA7_LBAvRawInstLumAll", &lucBiPMTA7_LBAvRawInstLumAll, &b_lucBiPMTA7_LBAvRawInstLumAll);
   fChain->SetBranchAddress("lucBiPMTA7_LBAvOLCInstLum", &lucBiPMTA7_LBAvOLCInstLum, &b_lucBiPMTA7_LBAvOLCInstLum);
   fChain->SetBranchAddress("lucBiPMTA7_LBAvOLCEvtsPerBX", &lucBiPMTA7_LBAvOLCEvtsPerBX, &b_lucBiPMTA7_LBAvOLCEvtsPerBX);
   fChain->SetBranchAddress("lucBiPMTA7_LBAvOLCRawInstLum", &lucBiPMTA7_LBAvOLCRawInstLum, &b_lucBiPMTA7_LBAvOLCRawInstLum);
   fChain->SetBranchAddress("lucBiPMTA7_NOrbPhys", &lucBiPMTA7_NOrbPhys, &b_lucBiPMTA7_NOrbPhys);
   fChain->SetBranchAddress("lucBiPMTA7_NOrbAll", &lucBiPMTA7_NOrbAll, &b_lucBiPMTA7_NOrbAll);
   fChain->SetBranchAddress("lucBiPMTA7_NOrbOLC", &lucBiPMTA7_NOrbOLC, &b_lucBiPMTA7_NOrbOLC);
   fChain->SetBranchAddress("lucBiPMTA7_DetectorState", &lucBiPMTA7_DetectorState, &b_lucBiPMTA7_DetectorState);
   fChain->SetBranchAddress("lucBiPMTA7_LumiValid", &lucBiPMTA7_LumiValid, &b_lucBiPMTA7_LumiValid);
   fChain->SetBranchAddress("lucBiPMTA9_LumiChannel", &lucBiPMTA9_LumiChannel, &b_lucBiPMTA9_LumiChannel);
   fChain->SetBranchAddress("lucBiPMTA9_LBAvInstLumPhys", &lucBiPMTA9_LBAvInstLumPhys, &b_lucBiPMTA9_LBAvInstLumPhys);
   fChain->SetBranchAddress("lucBiPMTA9_LBAvEvtsPerBXPhys", &lucBiPMTA9_LBAvEvtsPerBXPhys, &b_lucBiPMTA9_LBAvEvtsPerBXPhys);
   fChain->SetBranchAddress("lucBiPMTA9_LBAvRawInstLumPhys", &lucBiPMTA9_LBAvRawInstLumPhys, &b_lucBiPMTA9_LBAvRawInstLumPhys);
   fChain->SetBranchAddress("lucBiPMTA9_LBAvInstLumAll", &lucBiPMTA9_LBAvInstLumAll, &b_lucBiPMTA9_LBAvInstLumAll);
   fChain->SetBranchAddress("lucBiPMTA9_LBAvEvtsPerBXAll", &lucBiPMTA9_LBAvEvtsPerBXAll, &b_lucBiPMTA9_LBAvEvtsPerBXAll);
   fChain->SetBranchAddress("lucBiPMTA9_LBAvRawInstLumAll", &lucBiPMTA9_LBAvRawInstLumAll, &b_lucBiPMTA9_LBAvRawInstLumAll);
   fChain->SetBranchAddress("lucBiPMTA9_LBAvOLCInstLum", &lucBiPMTA9_LBAvOLCInstLum, &b_lucBiPMTA9_LBAvOLCInstLum);
   fChain->SetBranchAddress("lucBiPMTA9_LBAvOLCEvtsPerBX", &lucBiPMTA9_LBAvOLCEvtsPerBX, &b_lucBiPMTA9_LBAvOLCEvtsPerBX);
   fChain->SetBranchAddress("lucBiPMTA9_LBAvOLCRawInstLum", &lucBiPMTA9_LBAvOLCRawInstLum, &b_lucBiPMTA9_LBAvOLCRawInstLum);
   fChain->SetBranchAddress("lucBiPMTA9_NOrbPhys", &lucBiPMTA9_NOrbPhys, &b_lucBiPMTA9_NOrbPhys);
   fChain->SetBranchAddress("lucBiPMTA9_NOrbAll", &lucBiPMTA9_NOrbAll, &b_lucBiPMTA9_NOrbAll);
   fChain->SetBranchAddress("lucBiPMTA9_NOrbOLC", &lucBiPMTA9_NOrbOLC, &b_lucBiPMTA9_NOrbOLC);
   fChain->SetBranchAddress("lucBiPMTA9_DetectorState", &lucBiPMTA9_DetectorState, &b_lucBiPMTA9_DetectorState);
   fChain->SetBranchAddress("lucBiPMTA9_LumiValid", &lucBiPMTA9_LumiValid, &b_lucBiPMTA9_LumiValid);
   fChain->SetBranchAddress("lucBiPMTA13_LumiChannel", &lucBiPMTA13_LumiChannel, &b_lucBiPMTA13_LumiChannel);
   fChain->SetBranchAddress("lucBiPMTA13_LBAvInstLumPhys", &lucBiPMTA13_LBAvInstLumPhys, &b_lucBiPMTA13_LBAvInstLumPhys);
   fChain->SetBranchAddress("lucBiPMTA13_LBAvEvtsPerBXPhys", &lucBiPMTA13_LBAvEvtsPerBXPhys, &b_lucBiPMTA13_LBAvEvtsPerBXPhys);
   fChain->SetBranchAddress("lucBiPMTA13_LBAvRawInstLumPhys", &lucBiPMTA13_LBAvRawInstLumPhys, &b_lucBiPMTA13_LBAvRawInstLumPhys);
   fChain->SetBranchAddress("lucBiPMTA13_LBAvInstLumAll", &lucBiPMTA13_LBAvInstLumAll, &b_lucBiPMTA13_LBAvInstLumAll);
   fChain->SetBranchAddress("lucBiPMTA13_LBAvEvtsPerBXAll", &lucBiPMTA13_LBAvEvtsPerBXAll, &b_lucBiPMTA13_LBAvEvtsPerBXAll);
   fChain->SetBranchAddress("lucBiPMTA13_LBAvRawInstLumAll", &lucBiPMTA13_LBAvRawInstLumAll, &b_lucBiPMTA13_LBAvRawInstLumAll);
   fChain->SetBranchAddress("lucBiPMTA13_LBAvOLCInstLum", &lucBiPMTA13_LBAvOLCInstLum, &b_lucBiPMTA13_LBAvOLCInstLum);
   fChain->SetBranchAddress("lucBiPMTA13_LBAvOLCEvtsPerBX", &lucBiPMTA13_LBAvOLCEvtsPerBX, &b_lucBiPMTA13_LBAvOLCEvtsPerBX);
   fChain->SetBranchAddress("lucBiPMTA13_LBAvOLCRawInstLum", &lucBiPMTA13_LBAvOLCRawInstLum, &b_lucBiPMTA13_LBAvOLCRawInstLum);
   fChain->SetBranchAddress("lucBiPMTA13_NOrbPhys", &lucBiPMTA13_NOrbPhys, &b_lucBiPMTA13_NOrbPhys);
   fChain->SetBranchAddress("lucBiPMTA13_NOrbAll", &lucBiPMTA13_NOrbAll, &b_lucBiPMTA13_NOrbAll);
   fChain->SetBranchAddress("lucBiPMTA13_NOrbOLC", &lucBiPMTA13_NOrbOLC, &b_lucBiPMTA13_NOrbOLC);
   fChain->SetBranchAddress("lucBiPMTA13_DetectorState", &lucBiPMTA13_DetectorState, &b_lucBiPMTA13_DetectorState);
   fChain->SetBranchAddress("lucBiPMTA13_LumiValid", &lucBiPMTA13_LumiValid, &b_lucBiPMTA13_LumiValid);
   fChain->SetBranchAddress("lucBiPMTC1_LumiChannel", &lucBiPMTC1_LumiChannel, &b_lucBiPMTC1_LumiChannel);
   fChain->SetBranchAddress("lucBiPMTC1_LBAvInstLumPhys", &lucBiPMTC1_LBAvInstLumPhys, &b_lucBiPMTC1_LBAvInstLumPhys);
   fChain->SetBranchAddress("lucBiPMTC1_LBAvEvtsPerBXPhys", &lucBiPMTC1_LBAvEvtsPerBXPhys, &b_lucBiPMTC1_LBAvEvtsPerBXPhys);
   fChain->SetBranchAddress("lucBiPMTC1_LBAvRawInstLumPhys", &lucBiPMTC1_LBAvRawInstLumPhys, &b_lucBiPMTC1_LBAvRawInstLumPhys);
   fChain->SetBranchAddress("lucBiPMTC1_LBAvInstLumAll", &lucBiPMTC1_LBAvInstLumAll, &b_lucBiPMTC1_LBAvInstLumAll);
   fChain->SetBranchAddress("lucBiPMTC1_LBAvEvtsPerBXAll", &lucBiPMTC1_LBAvEvtsPerBXAll, &b_lucBiPMTC1_LBAvEvtsPerBXAll);
   fChain->SetBranchAddress("lucBiPMTC1_LBAvRawInstLumAll", &lucBiPMTC1_LBAvRawInstLumAll, &b_lucBiPMTC1_LBAvRawInstLumAll);
   fChain->SetBranchAddress("lucBiPMTC1_LBAvOLCInstLum", &lucBiPMTC1_LBAvOLCInstLum, &b_lucBiPMTC1_LBAvOLCInstLum);
   fChain->SetBranchAddress("lucBiPMTC1_LBAvOLCEvtsPerBX", &lucBiPMTC1_LBAvOLCEvtsPerBX, &b_lucBiPMTC1_LBAvOLCEvtsPerBX);
   fChain->SetBranchAddress("lucBiPMTC1_LBAvOLCRawInstLum", &lucBiPMTC1_LBAvOLCRawInstLum, &b_lucBiPMTC1_LBAvOLCRawInstLum);
   fChain->SetBranchAddress("lucBiPMTC1_NOrbPhys", &lucBiPMTC1_NOrbPhys, &b_lucBiPMTC1_NOrbPhys);
   fChain->SetBranchAddress("lucBiPMTC1_NOrbAll", &lucBiPMTC1_NOrbAll, &b_lucBiPMTC1_NOrbAll);
   fChain->SetBranchAddress("lucBiPMTC1_NOrbOLC", &lucBiPMTC1_NOrbOLC, &b_lucBiPMTC1_NOrbOLC);
   fChain->SetBranchAddress("lucBiPMTC1_DetectorState", &lucBiPMTC1_DetectorState, &b_lucBiPMTC1_DetectorState);
   fChain->SetBranchAddress("lucBiPMTC1_LumiValid", &lucBiPMTC1_LumiValid, &b_lucBiPMTC1_LumiValid);
   fChain->SetBranchAddress("lucBiPMTC5_LumiChannel", &lucBiPMTC5_LumiChannel, &b_lucBiPMTC5_LumiChannel);
   fChain->SetBranchAddress("lucBiPMTC5_LBAvInstLumPhys", &lucBiPMTC5_LBAvInstLumPhys, &b_lucBiPMTC5_LBAvInstLumPhys);
   fChain->SetBranchAddress("lucBiPMTC5_LBAvEvtsPerBXPhys", &lucBiPMTC5_LBAvEvtsPerBXPhys, &b_lucBiPMTC5_LBAvEvtsPerBXPhys);
   fChain->SetBranchAddress("lucBiPMTC5_LBAvRawInstLumPhys", &lucBiPMTC5_LBAvRawInstLumPhys, &b_lucBiPMTC5_LBAvRawInstLumPhys);
   fChain->SetBranchAddress("lucBiPMTC5_LBAvInstLumAll", &lucBiPMTC5_LBAvInstLumAll, &b_lucBiPMTC5_LBAvInstLumAll);
   fChain->SetBranchAddress("lucBiPMTC5_LBAvEvtsPerBXAll", &lucBiPMTC5_LBAvEvtsPerBXAll, &b_lucBiPMTC5_LBAvEvtsPerBXAll);
   fChain->SetBranchAddress("lucBiPMTC5_LBAvRawInstLumAll", &lucBiPMTC5_LBAvRawInstLumAll, &b_lucBiPMTC5_LBAvRawInstLumAll);
   fChain->SetBranchAddress("lucBiPMTC5_LBAvOLCInstLum", &lucBiPMTC5_LBAvOLCInstLum, &b_lucBiPMTC5_LBAvOLCInstLum);
   fChain->SetBranchAddress("lucBiPMTC5_LBAvOLCEvtsPerBX", &lucBiPMTC5_LBAvOLCEvtsPerBX, &b_lucBiPMTC5_LBAvOLCEvtsPerBX);
   fChain->SetBranchAddress("lucBiPMTC5_LBAvOLCRawInstLum", &lucBiPMTC5_LBAvOLCRawInstLum, &b_lucBiPMTC5_LBAvOLCRawInstLum);
   fChain->SetBranchAddress("lucBiPMTC5_NOrbPhys", &lucBiPMTC5_NOrbPhys, &b_lucBiPMTC5_NOrbPhys);
   fChain->SetBranchAddress("lucBiPMTC5_NOrbAll", &lucBiPMTC5_NOrbAll, &b_lucBiPMTC5_NOrbAll);
   fChain->SetBranchAddress("lucBiPMTC5_NOrbOLC", &lucBiPMTC5_NOrbOLC, &b_lucBiPMTC5_NOrbOLC);
   fChain->SetBranchAddress("lucBiPMTC5_DetectorState", &lucBiPMTC5_DetectorState, &b_lucBiPMTC5_DetectorState);
   fChain->SetBranchAddress("lucBiPMTC5_LumiValid", &lucBiPMTC5_LumiValid, &b_lucBiPMTC5_LumiValid);
   fChain->SetBranchAddress("lucBiPMTC9_LumiChannel", &lucBiPMTC9_LumiChannel, &b_lucBiPMTC9_LumiChannel);
   fChain->SetBranchAddress("lucBiPMTC9_LBAvInstLumPhys", &lucBiPMTC9_LBAvInstLumPhys, &b_lucBiPMTC9_LBAvInstLumPhys);
   fChain->SetBranchAddress("lucBiPMTC9_LBAvEvtsPerBXPhys", &lucBiPMTC9_LBAvEvtsPerBXPhys, &b_lucBiPMTC9_LBAvEvtsPerBXPhys);
   fChain->SetBranchAddress("lucBiPMTC9_LBAvRawInstLumPhys", &lucBiPMTC9_LBAvRawInstLumPhys, &b_lucBiPMTC9_LBAvRawInstLumPhys);
   fChain->SetBranchAddress("lucBiPMTC9_LBAvInstLumAll", &lucBiPMTC9_LBAvInstLumAll, &b_lucBiPMTC9_LBAvInstLumAll);
   fChain->SetBranchAddress("lucBiPMTC9_LBAvEvtsPerBXAll", &lucBiPMTC9_LBAvEvtsPerBXAll, &b_lucBiPMTC9_LBAvEvtsPerBXAll);
   fChain->SetBranchAddress("lucBiPMTC9_LBAvRawInstLumAll", &lucBiPMTC9_LBAvRawInstLumAll, &b_lucBiPMTC9_LBAvRawInstLumAll);
   fChain->SetBranchAddress("lucBiPMTC9_LBAvOLCInstLum", &lucBiPMTC9_LBAvOLCInstLum, &b_lucBiPMTC9_LBAvOLCInstLum);
   fChain->SetBranchAddress("lucBiPMTC9_LBAvOLCEvtsPerBX", &lucBiPMTC9_LBAvOLCEvtsPerBX, &b_lucBiPMTC9_LBAvOLCEvtsPerBX);
   fChain->SetBranchAddress("lucBiPMTC9_LBAvOLCRawInstLum", &lucBiPMTC9_LBAvOLCRawInstLum, &b_lucBiPMTC9_LBAvOLCRawInstLum);
   fChain->SetBranchAddress("lucBiPMTC9_NOrbPhys", &lucBiPMTC9_NOrbPhys, &b_lucBiPMTC9_NOrbPhys);
   fChain->SetBranchAddress("lucBiPMTC9_NOrbAll", &lucBiPMTC9_NOrbAll, &b_lucBiPMTC9_NOrbAll);
   fChain->SetBranchAddress("lucBiPMTC9_NOrbOLC", &lucBiPMTC9_NOrbOLC, &b_lucBiPMTC9_NOrbOLC);
   fChain->SetBranchAddress("lucBiPMTC9_DetectorState", &lucBiPMTC9_DetectorState, &b_lucBiPMTC9_DetectorState);
   fChain->SetBranchAddress("lucBiPMTC9_LumiValid", &lucBiPMTC9_LumiValid, &b_lucBiPMTC9_LumiValid);
   fChain->SetBranchAddress("lucBiPMTC13_LumiChannel", &lucBiPMTC13_LumiChannel, &b_lucBiPMTC13_LumiChannel);
   fChain->SetBranchAddress("lucBiPMTC13_LBAvInstLumPhys", &lucBiPMTC13_LBAvInstLumPhys, &b_lucBiPMTC13_LBAvInstLumPhys);
   fChain->SetBranchAddress("lucBiPMTC13_LBAvEvtsPerBXPhys", &lucBiPMTC13_LBAvEvtsPerBXPhys, &b_lucBiPMTC13_LBAvEvtsPerBXPhys);
   fChain->SetBranchAddress("lucBiPMTC13_LBAvRawInstLumPhys", &lucBiPMTC13_LBAvRawInstLumPhys, &b_lucBiPMTC13_LBAvRawInstLumPhys);
   fChain->SetBranchAddress("lucBiPMTC13_LBAvInstLumAll", &lucBiPMTC13_LBAvInstLumAll, &b_lucBiPMTC13_LBAvInstLumAll);
   fChain->SetBranchAddress("lucBiPMTC13_LBAvEvtsPerBXAll", &lucBiPMTC13_LBAvEvtsPerBXAll, &b_lucBiPMTC13_LBAvEvtsPerBXAll);
   fChain->SetBranchAddress("lucBiPMTC13_LBAvRawInstLumAll", &lucBiPMTC13_LBAvRawInstLumAll, &b_lucBiPMTC13_LBAvRawInstLumAll);
   fChain->SetBranchAddress("lucBiPMTC13_LBAvOLCInstLum", &lucBiPMTC13_LBAvOLCInstLum, &b_lucBiPMTC13_LBAvOLCInstLum);
   fChain->SetBranchAddress("lucBiPMTC13_LBAvOLCEvtsPerBX", &lucBiPMTC13_LBAvOLCEvtsPerBX, &b_lucBiPMTC13_LBAvOLCEvtsPerBX);
   fChain->SetBranchAddress("lucBiPMTC13_LBAvOLCRawInstLum", &lucBiPMTC13_LBAvOLCRawInstLum, &b_lucBiPMTC13_LBAvOLCRawInstLum);
   fChain->SetBranchAddress("lucBiPMTC13_NOrbPhys", &lucBiPMTC13_NOrbPhys, &b_lucBiPMTC13_NOrbPhys);
   fChain->SetBranchAddress("lucBiPMTC13_NOrbAll", &lucBiPMTC13_NOrbAll, &b_lucBiPMTC13_NOrbAll);
   fChain->SetBranchAddress("lucBiPMTC13_NOrbOLC", &lucBiPMTC13_NOrbOLC, &b_lucBiPMTC13_NOrbOLC);
   fChain->SetBranchAddress("lucBiPMTC13_DetectorState", &lucBiPMTC13_DetectorState, &b_lucBiPMTC13_DetectorState);
   fChain->SetBranchAddress("lucBiPMTC13_LumiValid", &lucBiPMTC13_LumiValid, &b_lucBiPMTC13_LumiValid);
   fChain->SetBranchAddress("lucBi2PMTA4_LumiChannel", &lucBi2PMTA4_LumiChannel, &b_lucBi2PMTA4_LumiChannel);
   fChain->SetBranchAddress("lucBi2PMTA4_LBAvInstLumPhys", &lucBi2PMTA4_LBAvInstLumPhys, &b_lucBi2PMTA4_LBAvInstLumPhys);
   fChain->SetBranchAddress("lucBi2PMTA4_LBAvEvtsPerBXPhys", &lucBi2PMTA4_LBAvEvtsPerBXPhys, &b_lucBi2PMTA4_LBAvEvtsPerBXPhys);
   fChain->SetBranchAddress("lucBi2PMTA4_LBAvRawInstLumPhys", &lucBi2PMTA4_LBAvRawInstLumPhys, &b_lucBi2PMTA4_LBAvRawInstLumPhys);
   fChain->SetBranchAddress("lucBi2PMTA4_LBAvInstLumAll", &lucBi2PMTA4_LBAvInstLumAll, &b_lucBi2PMTA4_LBAvInstLumAll);
   fChain->SetBranchAddress("lucBi2PMTA4_LBAvEvtsPerBXAll", &lucBi2PMTA4_LBAvEvtsPerBXAll, &b_lucBi2PMTA4_LBAvEvtsPerBXAll);
   fChain->SetBranchAddress("lucBi2PMTA4_LBAvRawInstLumAll", &lucBi2PMTA4_LBAvRawInstLumAll, &b_lucBi2PMTA4_LBAvRawInstLumAll);
   fChain->SetBranchAddress("lucBi2PMTA4_LBAvOLCInstLum", &lucBi2PMTA4_LBAvOLCInstLum, &b_lucBi2PMTA4_LBAvOLCInstLum);
   fChain->SetBranchAddress("lucBi2PMTA4_LBAvOLCEvtsPerBX", &lucBi2PMTA4_LBAvOLCEvtsPerBX, &b_lucBi2PMTA4_LBAvOLCEvtsPerBX);
   fChain->SetBranchAddress("lucBi2PMTA4_LBAvOLCRawInstLum", &lucBi2PMTA4_LBAvOLCRawInstLum, &b_lucBi2PMTA4_LBAvOLCRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTA4_NOrbPhys", &lucBi2PMTA4_NOrbPhys, &b_lucBi2PMTA4_NOrbPhys);
   fChain->SetBranchAddress("lucBi2PMTA4_NOrbAll", &lucBi2PMTA4_NOrbAll, &b_lucBi2PMTA4_NOrbAll);
   fChain->SetBranchAddress("lucBi2PMTA4_NOrbOLC", &lucBi2PMTA4_NOrbOLC, &b_lucBi2PMTA4_NOrbOLC);
   fChain->SetBranchAddress("lucBi2PMTA4_DetectorState", &lucBi2PMTA4_DetectorState, &b_lucBi2PMTA4_DetectorState);
   fChain->SetBranchAddress("lucBi2PMTA4_LumiValid", &lucBi2PMTA4_LumiValid, &b_lucBi2PMTA4_LumiValid);
   fChain->SetBranchAddress("lucBi2PMTA8_LumiChannel", &lucBi2PMTA8_LumiChannel, &b_lucBi2PMTA8_LumiChannel);
   fChain->SetBranchAddress("lucBi2PMTA8_LBAvInstLumPhys", &lucBi2PMTA8_LBAvInstLumPhys, &b_lucBi2PMTA8_LBAvInstLumPhys);
   fChain->SetBranchAddress("lucBi2PMTA8_LBAvEvtsPerBXPhys", &lucBi2PMTA8_LBAvEvtsPerBXPhys, &b_lucBi2PMTA8_LBAvEvtsPerBXPhys);
   fChain->SetBranchAddress("lucBi2PMTA8_LBAvRawInstLumPhys", &lucBi2PMTA8_LBAvRawInstLumPhys, &b_lucBi2PMTA8_LBAvRawInstLumPhys);
   fChain->SetBranchAddress("lucBi2PMTA8_LBAvInstLumAll", &lucBi2PMTA8_LBAvInstLumAll, &b_lucBi2PMTA8_LBAvInstLumAll);
   fChain->SetBranchAddress("lucBi2PMTA8_LBAvEvtsPerBXAll", &lucBi2PMTA8_LBAvEvtsPerBXAll, &b_lucBi2PMTA8_LBAvEvtsPerBXAll);
   fChain->SetBranchAddress("lucBi2PMTA8_LBAvRawInstLumAll", &lucBi2PMTA8_LBAvRawInstLumAll, &b_lucBi2PMTA8_LBAvRawInstLumAll);
   fChain->SetBranchAddress("lucBi2PMTA8_LBAvOLCInstLum", &lucBi2PMTA8_LBAvOLCInstLum, &b_lucBi2PMTA8_LBAvOLCInstLum);
   fChain->SetBranchAddress("lucBi2PMTA8_LBAvOLCEvtsPerBX", &lucBi2PMTA8_LBAvOLCEvtsPerBX, &b_lucBi2PMTA8_LBAvOLCEvtsPerBX);
   fChain->SetBranchAddress("lucBi2PMTA8_LBAvOLCRawInstLum", &lucBi2PMTA8_LBAvOLCRawInstLum, &b_lucBi2PMTA8_LBAvOLCRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTA8_NOrbPhys", &lucBi2PMTA8_NOrbPhys, &b_lucBi2PMTA8_NOrbPhys);
   fChain->SetBranchAddress("lucBi2PMTA8_NOrbAll", &lucBi2PMTA8_NOrbAll, &b_lucBi2PMTA8_NOrbAll);
   fChain->SetBranchAddress("lucBi2PMTA8_NOrbOLC", &lucBi2PMTA8_NOrbOLC, &b_lucBi2PMTA8_NOrbOLC);
   fChain->SetBranchAddress("lucBi2PMTA8_DetectorState", &lucBi2PMTA8_DetectorState, &b_lucBi2PMTA8_DetectorState);
   fChain->SetBranchAddress("lucBi2PMTA8_LumiValid", &lucBi2PMTA8_LumiValid, &b_lucBi2PMTA8_LumiValid);
   fChain->SetBranchAddress("lucBi2PMTA12_LumiChannel", &lucBi2PMTA12_LumiChannel, &b_lucBi2PMTA12_LumiChannel);
   fChain->SetBranchAddress("lucBi2PMTA12_LBAvInstLumPhys", &lucBi2PMTA12_LBAvInstLumPhys, &b_lucBi2PMTA12_LBAvInstLumPhys);
   fChain->SetBranchAddress("lucBi2PMTA12_LBAvEvtsPerBXPhys", &lucBi2PMTA12_LBAvEvtsPerBXPhys, &b_lucBi2PMTA12_LBAvEvtsPerBXPhys);
   fChain->SetBranchAddress("lucBi2PMTA12_LBAvRawInstLumPhys", &lucBi2PMTA12_LBAvRawInstLumPhys, &b_lucBi2PMTA12_LBAvRawInstLumPhys);
   fChain->SetBranchAddress("lucBi2PMTA12_LBAvInstLumAll", &lucBi2PMTA12_LBAvInstLumAll, &b_lucBi2PMTA12_LBAvInstLumAll);
   fChain->SetBranchAddress("lucBi2PMTA12_LBAvEvtsPerBXAll", &lucBi2PMTA12_LBAvEvtsPerBXAll, &b_lucBi2PMTA12_LBAvEvtsPerBXAll);
   fChain->SetBranchAddress("lucBi2PMTA12_LBAvRawInstLumAll", &lucBi2PMTA12_LBAvRawInstLumAll, &b_lucBi2PMTA12_LBAvRawInstLumAll);
   fChain->SetBranchAddress("lucBi2PMTA12_LBAvOLCInstLum", &lucBi2PMTA12_LBAvOLCInstLum, &b_lucBi2PMTA12_LBAvOLCInstLum);
   fChain->SetBranchAddress("lucBi2PMTA12_LBAvOLCEvtsPerBX", &lucBi2PMTA12_LBAvOLCEvtsPerBX, &b_lucBi2PMTA12_LBAvOLCEvtsPerBX);
   fChain->SetBranchAddress("lucBi2PMTA12_LBAvOLCRawInstLum", &lucBi2PMTA12_LBAvOLCRawInstLum, &b_lucBi2PMTA12_LBAvOLCRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTA12_NOrbPhys", &lucBi2PMTA12_NOrbPhys, &b_lucBi2PMTA12_NOrbPhys);
   fChain->SetBranchAddress("lucBi2PMTA12_NOrbAll", &lucBi2PMTA12_NOrbAll, &b_lucBi2PMTA12_NOrbAll);
   fChain->SetBranchAddress("lucBi2PMTA12_NOrbOLC", &lucBi2PMTA12_NOrbOLC, &b_lucBi2PMTA12_NOrbOLC);
   fChain->SetBranchAddress("lucBi2PMTA12_DetectorState", &lucBi2PMTA12_DetectorState, &b_lucBi2PMTA12_DetectorState);
   fChain->SetBranchAddress("lucBi2PMTA12_LumiValid", &lucBi2PMTA12_LumiValid, &b_lucBi2PMTA12_LumiValid);
   fChain->SetBranchAddress("lucBi2PMTA16_LumiChannel", &lucBi2PMTA16_LumiChannel, &b_lucBi2PMTA16_LumiChannel);
   fChain->SetBranchAddress("lucBi2PMTA16_LBAvInstLumPhys", &lucBi2PMTA16_LBAvInstLumPhys, &b_lucBi2PMTA16_LBAvInstLumPhys);
   fChain->SetBranchAddress("lucBi2PMTA16_LBAvEvtsPerBXPhys", &lucBi2PMTA16_LBAvEvtsPerBXPhys, &b_lucBi2PMTA16_LBAvEvtsPerBXPhys);
   fChain->SetBranchAddress("lucBi2PMTA16_LBAvRawInstLumPhys", &lucBi2PMTA16_LBAvRawInstLumPhys, &b_lucBi2PMTA16_LBAvRawInstLumPhys);
   fChain->SetBranchAddress("lucBi2PMTA16_LBAvInstLumAll", &lucBi2PMTA16_LBAvInstLumAll, &b_lucBi2PMTA16_LBAvInstLumAll);
   fChain->SetBranchAddress("lucBi2PMTA16_LBAvEvtsPerBXAll", &lucBi2PMTA16_LBAvEvtsPerBXAll, &b_lucBi2PMTA16_LBAvEvtsPerBXAll);
   fChain->SetBranchAddress("lucBi2PMTA16_LBAvRawInstLumAll", &lucBi2PMTA16_LBAvRawInstLumAll, &b_lucBi2PMTA16_LBAvRawInstLumAll);
   fChain->SetBranchAddress("lucBi2PMTA16_LBAvOLCInstLum", &lucBi2PMTA16_LBAvOLCInstLum, &b_lucBi2PMTA16_LBAvOLCInstLum);
   fChain->SetBranchAddress("lucBi2PMTA16_LBAvOLCEvtsPerBX", &lucBi2PMTA16_LBAvOLCEvtsPerBX, &b_lucBi2PMTA16_LBAvOLCEvtsPerBX);
   fChain->SetBranchAddress("lucBi2PMTA16_LBAvOLCRawInstLum", &lucBi2PMTA16_LBAvOLCRawInstLum, &b_lucBi2PMTA16_LBAvOLCRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTA16_NOrbPhys", &lucBi2PMTA16_NOrbPhys, &b_lucBi2PMTA16_NOrbPhys);
   fChain->SetBranchAddress("lucBi2PMTA16_NOrbAll", &lucBi2PMTA16_NOrbAll, &b_lucBi2PMTA16_NOrbAll);
   fChain->SetBranchAddress("lucBi2PMTA16_NOrbOLC", &lucBi2PMTA16_NOrbOLC, &b_lucBi2PMTA16_NOrbOLC);
   fChain->SetBranchAddress("lucBi2PMTA16_DetectorState", &lucBi2PMTA16_DetectorState, &b_lucBi2PMTA16_DetectorState);
   fChain->SetBranchAddress("lucBi2PMTA16_LumiValid", &lucBi2PMTA16_LumiValid, &b_lucBi2PMTA16_LumiValid);
   fChain->SetBranchAddress("lucBi2PMTC4_LumiChannel", &lucBi2PMTC4_LumiChannel, &b_lucBi2PMTC4_LumiChannel);
   fChain->SetBranchAddress("lucBi2PMTC4_LBAvInstLumPhys", &lucBi2PMTC4_LBAvInstLumPhys, &b_lucBi2PMTC4_LBAvInstLumPhys);
   fChain->SetBranchAddress("lucBi2PMTC4_LBAvEvtsPerBXPhys", &lucBi2PMTC4_LBAvEvtsPerBXPhys, &b_lucBi2PMTC4_LBAvEvtsPerBXPhys);
   fChain->SetBranchAddress("lucBi2PMTC4_LBAvRawInstLumPhys", &lucBi2PMTC4_LBAvRawInstLumPhys, &b_lucBi2PMTC4_LBAvRawInstLumPhys);
   fChain->SetBranchAddress("lucBi2PMTC4_LBAvInstLumAll", &lucBi2PMTC4_LBAvInstLumAll, &b_lucBi2PMTC4_LBAvInstLumAll);
   fChain->SetBranchAddress("lucBi2PMTC4_LBAvEvtsPerBXAll", &lucBi2PMTC4_LBAvEvtsPerBXAll, &b_lucBi2PMTC4_LBAvEvtsPerBXAll);
   fChain->SetBranchAddress("lucBi2PMTC4_LBAvRawInstLumAll", &lucBi2PMTC4_LBAvRawInstLumAll, &b_lucBi2PMTC4_LBAvRawInstLumAll);
   fChain->SetBranchAddress("lucBi2PMTC4_LBAvOLCInstLum", &lucBi2PMTC4_LBAvOLCInstLum, &b_lucBi2PMTC4_LBAvOLCInstLum);
   fChain->SetBranchAddress("lucBi2PMTC4_LBAvOLCEvtsPerBX", &lucBi2PMTC4_LBAvOLCEvtsPerBX, &b_lucBi2PMTC4_LBAvOLCEvtsPerBX);
   fChain->SetBranchAddress("lucBi2PMTC4_LBAvOLCRawInstLum", &lucBi2PMTC4_LBAvOLCRawInstLum, &b_lucBi2PMTC4_LBAvOLCRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTC4_NOrbPhys", &lucBi2PMTC4_NOrbPhys, &b_lucBi2PMTC4_NOrbPhys);
   fChain->SetBranchAddress("lucBi2PMTC4_NOrbAll", &lucBi2PMTC4_NOrbAll, &b_lucBi2PMTC4_NOrbAll);
   fChain->SetBranchAddress("lucBi2PMTC4_NOrbOLC", &lucBi2PMTC4_NOrbOLC, &b_lucBi2PMTC4_NOrbOLC);
   fChain->SetBranchAddress("lucBi2PMTC4_DetectorState", &lucBi2PMTC4_DetectorState, &b_lucBi2PMTC4_DetectorState);
   fChain->SetBranchAddress("lucBi2PMTC4_LumiValid", &lucBi2PMTC4_LumiValid, &b_lucBi2PMTC4_LumiValid);
   fChain->SetBranchAddress("lucBi2PMTC8_LumiChannel", &lucBi2PMTC8_LumiChannel, &b_lucBi2PMTC8_LumiChannel);
   fChain->SetBranchAddress("lucBi2PMTC8_LBAvInstLumPhys", &lucBi2PMTC8_LBAvInstLumPhys, &b_lucBi2PMTC8_LBAvInstLumPhys);
   fChain->SetBranchAddress("lucBi2PMTC8_LBAvEvtsPerBXPhys", &lucBi2PMTC8_LBAvEvtsPerBXPhys, &b_lucBi2PMTC8_LBAvEvtsPerBXPhys);
   fChain->SetBranchAddress("lucBi2PMTC8_LBAvRawInstLumPhys", &lucBi2PMTC8_LBAvRawInstLumPhys, &b_lucBi2PMTC8_LBAvRawInstLumPhys);
   fChain->SetBranchAddress("lucBi2PMTC8_LBAvInstLumAll", &lucBi2PMTC8_LBAvInstLumAll, &b_lucBi2PMTC8_LBAvInstLumAll);
   fChain->SetBranchAddress("lucBi2PMTC8_LBAvEvtsPerBXAll", &lucBi2PMTC8_LBAvEvtsPerBXAll, &b_lucBi2PMTC8_LBAvEvtsPerBXAll);
   fChain->SetBranchAddress("lucBi2PMTC8_LBAvRawInstLumAll", &lucBi2PMTC8_LBAvRawInstLumAll, &b_lucBi2PMTC8_LBAvRawInstLumAll);
   fChain->SetBranchAddress("lucBi2PMTC8_LBAvOLCInstLum", &lucBi2PMTC8_LBAvOLCInstLum, &b_lucBi2PMTC8_LBAvOLCInstLum);
   fChain->SetBranchAddress("lucBi2PMTC8_LBAvOLCEvtsPerBX", &lucBi2PMTC8_LBAvOLCEvtsPerBX, &b_lucBi2PMTC8_LBAvOLCEvtsPerBX);
   fChain->SetBranchAddress("lucBi2PMTC8_LBAvOLCRawInstLum", &lucBi2PMTC8_LBAvOLCRawInstLum, &b_lucBi2PMTC8_LBAvOLCRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTC8_NOrbPhys", &lucBi2PMTC8_NOrbPhys, &b_lucBi2PMTC8_NOrbPhys);
   fChain->SetBranchAddress("lucBi2PMTC8_NOrbAll", &lucBi2PMTC8_NOrbAll, &b_lucBi2PMTC8_NOrbAll);
   fChain->SetBranchAddress("lucBi2PMTC8_NOrbOLC", &lucBi2PMTC8_NOrbOLC, &b_lucBi2PMTC8_NOrbOLC);
   fChain->SetBranchAddress("lucBi2PMTC8_DetectorState", &lucBi2PMTC8_DetectorState, &b_lucBi2PMTC8_DetectorState);
   fChain->SetBranchAddress("lucBi2PMTC8_LumiValid", &lucBi2PMTC8_LumiValid, &b_lucBi2PMTC8_LumiValid);
   fChain->SetBranchAddress("lucBi2PMTC12_LumiChannel", &lucBi2PMTC12_LumiChannel, &b_lucBi2PMTC12_LumiChannel);
   fChain->SetBranchAddress("lucBi2PMTC12_LBAvInstLumPhys", &lucBi2PMTC12_LBAvInstLumPhys, &b_lucBi2PMTC12_LBAvInstLumPhys);
   fChain->SetBranchAddress("lucBi2PMTC12_LBAvEvtsPerBXPhys", &lucBi2PMTC12_LBAvEvtsPerBXPhys, &b_lucBi2PMTC12_LBAvEvtsPerBXPhys);
   fChain->SetBranchAddress("lucBi2PMTC12_LBAvRawInstLumPhys", &lucBi2PMTC12_LBAvRawInstLumPhys, &b_lucBi2PMTC12_LBAvRawInstLumPhys);
   fChain->SetBranchAddress("lucBi2PMTC12_LBAvInstLumAll", &lucBi2PMTC12_LBAvInstLumAll, &b_lucBi2PMTC12_LBAvInstLumAll);
   fChain->SetBranchAddress("lucBi2PMTC12_LBAvEvtsPerBXAll", &lucBi2PMTC12_LBAvEvtsPerBXAll, &b_lucBi2PMTC12_LBAvEvtsPerBXAll);
   fChain->SetBranchAddress("lucBi2PMTC12_LBAvRawInstLumAll", &lucBi2PMTC12_LBAvRawInstLumAll, &b_lucBi2PMTC12_LBAvRawInstLumAll);
   fChain->SetBranchAddress("lucBi2PMTC12_LBAvOLCInstLum", &lucBi2PMTC12_LBAvOLCInstLum, &b_lucBi2PMTC12_LBAvOLCInstLum);
   fChain->SetBranchAddress("lucBi2PMTC12_LBAvOLCEvtsPerBX", &lucBi2PMTC12_LBAvOLCEvtsPerBX, &b_lucBi2PMTC12_LBAvOLCEvtsPerBX);
   fChain->SetBranchAddress("lucBi2PMTC12_LBAvOLCRawInstLum", &lucBi2PMTC12_LBAvOLCRawInstLum, &b_lucBi2PMTC12_LBAvOLCRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTC12_NOrbPhys", &lucBi2PMTC12_NOrbPhys, &b_lucBi2PMTC12_NOrbPhys);
   fChain->SetBranchAddress("lucBi2PMTC12_NOrbAll", &lucBi2PMTC12_NOrbAll, &b_lucBi2PMTC12_NOrbAll);
   fChain->SetBranchAddress("lucBi2PMTC12_NOrbOLC", &lucBi2PMTC12_NOrbOLC, &b_lucBi2PMTC12_NOrbOLC);
   fChain->SetBranchAddress("lucBi2PMTC12_DetectorState", &lucBi2PMTC12_DetectorState, &b_lucBi2PMTC12_DetectorState);
   fChain->SetBranchAddress("lucBi2PMTC12_LumiValid", &lucBi2PMTC12_LumiValid, &b_lucBi2PMTC12_LumiValid);
   fChain->SetBranchAddress("lucBi2PMTC16_LumiChannel", &lucBi2PMTC16_LumiChannel, &b_lucBi2PMTC16_LumiChannel);
   fChain->SetBranchAddress("lucBi2PMTC16_LBAvInstLumPhys", &lucBi2PMTC16_LBAvInstLumPhys, &b_lucBi2PMTC16_LBAvInstLumPhys);
   fChain->SetBranchAddress("lucBi2PMTC16_LBAvEvtsPerBXPhys", &lucBi2PMTC16_LBAvEvtsPerBXPhys, &b_lucBi2PMTC16_LBAvEvtsPerBXPhys);
   fChain->SetBranchAddress("lucBi2PMTC16_LBAvRawInstLumPhys", &lucBi2PMTC16_LBAvRawInstLumPhys, &b_lucBi2PMTC16_LBAvRawInstLumPhys);
   fChain->SetBranchAddress("lucBi2PMTC16_LBAvInstLumAll", &lucBi2PMTC16_LBAvInstLumAll, &b_lucBi2PMTC16_LBAvInstLumAll);
   fChain->SetBranchAddress("lucBi2PMTC16_LBAvEvtsPerBXAll", &lucBi2PMTC16_LBAvEvtsPerBXAll, &b_lucBi2PMTC16_LBAvEvtsPerBXAll);
   fChain->SetBranchAddress("lucBi2PMTC16_LBAvRawInstLumAll", &lucBi2PMTC16_LBAvRawInstLumAll, &b_lucBi2PMTC16_LBAvRawInstLumAll);
   fChain->SetBranchAddress("lucBi2PMTC16_LBAvOLCInstLum", &lucBi2PMTC16_LBAvOLCInstLum, &b_lucBi2PMTC16_LBAvOLCInstLum);
   fChain->SetBranchAddress("lucBi2PMTC16_LBAvOLCEvtsPerBX", &lucBi2PMTC16_LBAvOLCEvtsPerBX, &b_lucBi2PMTC16_LBAvOLCEvtsPerBX);
   fChain->SetBranchAddress("lucBi2PMTC16_LBAvOLCRawInstLum", &lucBi2PMTC16_LBAvOLCRawInstLum, &b_lucBi2PMTC16_LBAvOLCRawInstLum);
   fChain->SetBranchAddress("lucBi2PMTC16_NOrbPhys", &lucBi2PMTC16_NOrbPhys, &b_lucBi2PMTC16_NOrbPhys);
   fChain->SetBranchAddress("lucBi2PMTC16_NOrbAll", &lucBi2PMTC16_NOrbAll, &b_lucBi2PMTC16_NOrbAll);
   fChain->SetBranchAddress("lucBi2PMTC16_NOrbOLC", &lucBi2PMTC16_NOrbOLC, &b_lucBi2PMTC16_NOrbOLC);
   fChain->SetBranchAddress("lucBi2PMTC16_DetectorState", &lucBi2PMTC16_DetectorState, &b_lucBi2PMTC16_DetectorState);
   fChain->SetBranchAddress("lucBi2PMTC16_LumiValid", &lucBi2PMTC16_LumiValid, &b_lucBi2PMTC16_LumiValid);
   fChain->SetBranchAddress("lucBiPMTA1_CalValid", &lucBiPMTA1_CalValid, &b_lucBiPMTA1_CalValid);
   fChain->SetBranchAddress("lucBiPMTA1_LBAvInstLumi", &lucBiPMTA1_LBAvInstLumi, &b_lucBiPMTA1_LBAvInstLumi);
   fChain->SetBranchAddress("lucBiPMTA1_LBAvEvtsPerBX", &lucBiPMTA1_LBAvEvtsPerBX, &b_lucBiPMTA1_LBAvEvtsPerBX);
   fChain->SetBranchAddress("lucBiPMTA1_BunchInstLumi", lucBiPMTA1_BunchInstLumi, &b_lucBiPMTA1_BunchInstLumi);
   fChain->SetBranchAddress("lucBiPMTA7_CalValid", &lucBiPMTA7_CalValid, &b_lucBiPMTA7_CalValid);
   fChain->SetBranchAddress("lucBiPMTA7_LBAvInstLumi", &lucBiPMTA7_LBAvInstLumi, &b_lucBiPMTA7_LBAvInstLumi);
   fChain->SetBranchAddress("lucBiPMTA7_LBAvEvtsPerBX", &lucBiPMTA7_LBAvEvtsPerBX, &b_lucBiPMTA7_LBAvEvtsPerBX);
   fChain->SetBranchAddress("lucBiPMTA7_BunchInstLumi", lucBiPMTA7_BunchInstLumi, &b_lucBiPMTA7_BunchInstLumi);
   fChain->SetBranchAddress("lucBiPMTA9_CalValid", &lucBiPMTA9_CalValid, &b_lucBiPMTA9_CalValid);
   fChain->SetBranchAddress("lucBiPMTA9_LBAvInstLumi", &lucBiPMTA9_LBAvInstLumi, &b_lucBiPMTA9_LBAvInstLumi);
   fChain->SetBranchAddress("lucBiPMTA9_LBAvEvtsPerBX", &lucBiPMTA9_LBAvEvtsPerBX, &b_lucBiPMTA9_LBAvEvtsPerBX);
   fChain->SetBranchAddress("lucBiPMTA9_BunchInstLumi", lucBiPMTA9_BunchInstLumi, &b_lucBiPMTA9_BunchInstLumi);
   fChain->SetBranchAddress("lucBiPMTA13_CalValid", &lucBiPMTA13_CalValid, &b_lucBiPMTA13_CalValid);
   fChain->SetBranchAddress("lucBiPMTA13_LBAvInstLumi", &lucBiPMTA13_LBAvInstLumi, &b_lucBiPMTA13_LBAvInstLumi);
   fChain->SetBranchAddress("lucBiPMTA13_LBAvEvtsPerBX", &lucBiPMTA13_LBAvEvtsPerBX, &b_lucBiPMTA13_LBAvEvtsPerBX);
   fChain->SetBranchAddress("lucBiPMTA13_BunchInstLumi", lucBiPMTA13_BunchInstLumi, &b_lucBiPMTA13_BunchInstLumi);
   fChain->SetBranchAddress("lucBiPMTC1_CalValid", &lucBiPMTC1_CalValid, &b_lucBiPMTC1_CalValid);
   fChain->SetBranchAddress("lucBiPMTC1_LBAvInstLumi", &lucBiPMTC1_LBAvInstLumi, &b_lucBiPMTC1_LBAvInstLumi);
   fChain->SetBranchAddress("lucBiPMTC1_LBAvEvtsPerBX", &lucBiPMTC1_LBAvEvtsPerBX, &b_lucBiPMTC1_LBAvEvtsPerBX);
   fChain->SetBranchAddress("lucBiPMTC1_BunchInstLumi", lucBiPMTC1_BunchInstLumi, &b_lucBiPMTC1_BunchInstLumi);
   fChain->SetBranchAddress("lucBiPMTC5_CalValid", &lucBiPMTC5_CalValid, &b_lucBiPMTC5_CalValid);
   fChain->SetBranchAddress("lucBiPMTC5_LBAvInstLumi", &lucBiPMTC5_LBAvInstLumi, &b_lucBiPMTC5_LBAvInstLumi);
   fChain->SetBranchAddress("lucBiPMTC5_LBAvEvtsPerBX", &lucBiPMTC5_LBAvEvtsPerBX, &b_lucBiPMTC5_LBAvEvtsPerBX);
   fChain->SetBranchAddress("lucBiPMTC5_BunchInstLumi", lucBiPMTC5_BunchInstLumi, &b_lucBiPMTC5_BunchInstLumi);
   fChain->SetBranchAddress("lucBiPMTC9_CalValid", &lucBiPMTC9_CalValid, &b_lucBiPMTC9_CalValid);
   fChain->SetBranchAddress("lucBiPMTC9_LBAvInstLumi", &lucBiPMTC9_LBAvInstLumi, &b_lucBiPMTC9_LBAvInstLumi);
   fChain->SetBranchAddress("lucBiPMTC9_LBAvEvtsPerBX", &lucBiPMTC9_LBAvEvtsPerBX, &b_lucBiPMTC9_LBAvEvtsPerBX);
   fChain->SetBranchAddress("lucBiPMTC9_BunchInstLumi", lucBiPMTC9_BunchInstLumi, &b_lucBiPMTC9_BunchInstLumi);
   fChain->SetBranchAddress("lucBiPMTC13_CalValid", &lucBiPMTC13_CalValid, &b_lucBiPMTC13_CalValid);
   fChain->SetBranchAddress("lucBiPMTC13_LBAvInstLumi", &lucBiPMTC13_LBAvInstLumi, &b_lucBiPMTC13_LBAvInstLumi);
   fChain->SetBranchAddress("lucBiPMTC13_LBAvEvtsPerBX", &lucBiPMTC13_LBAvEvtsPerBX, &b_lucBiPMTC13_LBAvEvtsPerBX);
   fChain->SetBranchAddress("lucBiPMTC13_BunchInstLumi", lucBiPMTC13_BunchInstLumi, &b_lucBiPMTC13_BunchInstLumi);
   fChain->SetBranchAddress("lucBi2PMTA4_CalValid", &lucBi2PMTA4_CalValid, &b_lucBi2PMTA4_CalValid);
   fChain->SetBranchAddress("lucBi2PMTA4_LBAvInstLumi", &lucBi2PMTA4_LBAvInstLumi, &b_lucBi2PMTA4_LBAvInstLumi);
   fChain->SetBranchAddress("lucBi2PMTA4_LBAvEvtsPerBX", &lucBi2PMTA4_LBAvEvtsPerBX, &b_lucBi2PMTA4_LBAvEvtsPerBX);
   fChain->SetBranchAddress("lucBi2PMTA4_BunchInstLumi", lucBi2PMTA4_BunchInstLumi, &b_lucBi2PMTA4_BunchInstLumi);
   fChain->SetBranchAddress("lucBi2PMTA8_CalValid", &lucBi2PMTA8_CalValid, &b_lucBi2PMTA8_CalValid);
   fChain->SetBranchAddress("lucBi2PMTA8_LBAvInstLumi", &lucBi2PMTA8_LBAvInstLumi, &b_lucBi2PMTA8_LBAvInstLumi);
   fChain->SetBranchAddress("lucBi2PMTA8_LBAvEvtsPerBX", &lucBi2PMTA8_LBAvEvtsPerBX, &b_lucBi2PMTA8_LBAvEvtsPerBX);
   fChain->SetBranchAddress("lucBi2PMTA8_BunchInstLumi", lucBi2PMTA8_BunchInstLumi, &b_lucBi2PMTA8_BunchInstLumi);
   fChain->SetBranchAddress("lucBi2PMTA12_CalValid", &lucBi2PMTA12_CalValid, &b_lucBi2PMTA12_CalValid);
   fChain->SetBranchAddress("lucBi2PMTA12_LBAvInstLumi", &lucBi2PMTA12_LBAvInstLumi, &b_lucBi2PMTA12_LBAvInstLumi);
   fChain->SetBranchAddress("lucBi2PMTA12_LBAvEvtsPerBX", &lucBi2PMTA12_LBAvEvtsPerBX, &b_lucBi2PMTA12_LBAvEvtsPerBX);
   fChain->SetBranchAddress("lucBi2PMTA12_BunchInstLumi", lucBi2PMTA12_BunchInstLumi, &b_lucBi2PMTA12_BunchInstLumi);
   fChain->SetBranchAddress("lucBi2PMTA16_CalValid", &lucBi2PMTA16_CalValid, &b_lucBi2PMTA16_CalValid);
   fChain->SetBranchAddress("lucBi2PMTA16_LBAvInstLumi", &lucBi2PMTA16_LBAvInstLumi, &b_lucBi2PMTA16_LBAvInstLumi);
   fChain->SetBranchAddress("lucBi2PMTA16_LBAvEvtsPerBX", &lucBi2PMTA16_LBAvEvtsPerBX, &b_lucBi2PMTA16_LBAvEvtsPerBX);
   fChain->SetBranchAddress("lucBi2PMTA16_BunchInstLumi", lucBi2PMTA16_BunchInstLumi, &b_lucBi2PMTA16_BunchInstLumi);
   fChain->SetBranchAddress("lucBiHitOR_CalValid", &lucBiHitOR_CalValid, &b_lucBiHitOR_CalValid);
   fChain->SetBranchAddress("lucBiHitOR_LBAvInstLumi", &lucBiHitOR_LBAvInstLumi, &b_lucBiHitOR_LBAvInstLumi);
   fChain->SetBranchAddress("lucBiHitOR_LBAvEvtsPerBX", &lucBiHitOR_LBAvEvtsPerBX, &b_lucBiHitOR_LBAvEvtsPerBX);
   fChain->SetBranchAddress("lucBiHitOR_BunchInstLumi", lucBiHitOR_BunchInstLumi, &b_lucBiHitOR_BunchInstLumi);
   fChain->SetBranchAddress("lucBi2PMTC8_CalValid", &lucBi2PMTC8_CalValid, &b_lucBi2PMTC8_CalValid);
   fChain->SetBranchAddress("lucBi2PMTC8_LBAvInstLumi", &lucBi2PMTC8_LBAvInstLumi, &b_lucBi2PMTC8_LBAvInstLumi);
   fChain->SetBranchAddress("lucBi2PMTC8_LBAvEvtsPerBX", &lucBi2PMTC8_LBAvEvtsPerBX, &b_lucBi2PMTC8_LBAvEvtsPerBX);
   fChain->SetBranchAddress("lucBi2PMTC8_BunchInstLumi", lucBi2PMTC8_BunchInstLumi, &b_lucBi2PMTC8_BunchInstLumi);
   fChain->SetBranchAddress("lucBi2PMTC12_CalValid", &lucBi2PMTC12_CalValid, &b_lucBi2PMTC12_CalValid);
   fChain->SetBranchAddress("lucBi2PMTC12_LBAvInstLumi", &lucBi2PMTC12_LBAvInstLumi, &b_lucBi2PMTC12_LBAvInstLumi);
   fChain->SetBranchAddress("lucBi2PMTC12_LBAvEvtsPerBX", &lucBi2PMTC12_LBAvEvtsPerBX, &b_lucBi2PMTC12_LBAvEvtsPerBX);
   fChain->SetBranchAddress("lucBi2PMTC12_BunchInstLumi", lucBi2PMTC12_BunchInstLumi, &b_lucBi2PMTC12_BunchInstLumi);
   fChain->SetBranchAddress("lucBi2PMTC16_CalValid", &lucBi2PMTC16_CalValid, &b_lucBi2PMTC16_CalValid);
   fChain->SetBranchAddress("lucBi2PMTC16_LBAvInstLumi", &lucBi2PMTC16_LBAvInstLumi, &b_lucBi2PMTC16_LBAvInstLumi);
   fChain->SetBranchAddress("lucBi2PMTC16_LBAvEvtsPerBX", &lucBi2PMTC16_LBAvEvtsPerBX, &b_lucBi2PMTC16_LBAvEvtsPerBX);
   fChain->SetBranchAddress("lucBi2PMTC16_BunchInstLumi", lucBi2PMTC16_BunchInstLumi, &b_lucBi2PMTC16_BunchInstLumi);
   fChain->SetBranchAddress("lucBi2PMTC4_CalValid", &lucBi2PMTC4_CalValid, &b_lucBi2PMTC4_CalValid);
   fChain->SetBranchAddress("lucBi2PMTC4_LBAvInstLumi", &lucBi2PMTC4_LBAvInstLumi, &b_lucBi2PMTC4_LBAvInstLumi);
   fChain->SetBranchAddress("lucBi2PMTC4_LBAvEvtsPerBX", &lucBi2PMTC4_LBAvEvtsPerBX, &b_lucBi2PMTC4_LBAvEvtsPerBX);
   fChain->SetBranchAddress("lucBi2PMTC4_BunchInstLumi", lucBi2PMTC4_BunchInstLumi, &b_lucBi2PMTC4_BunchInstLumi);
   fChain->SetBranchAddress("lucBi2HitOR_CalValid", &lucBi2HitOR_CalValid, &b_lucBi2HitOR_CalValid);
   fChain->SetBranchAddress("lucBi2HitOR_LBAvInstLumi", &lucBi2HitOR_LBAvInstLumi, &b_lucBi2HitOR_LBAvInstLumi);
   fChain->SetBranchAddress("lucBi2HitOR_LBAvEvtsPerBX", &lucBi2HitOR_LBAvEvtsPerBX, &b_lucBi2HitOR_LBAvEvtsPerBX);
   fChain->SetBranchAddress("lucBi2HitOR_BunchInstLumi", lucBi2HitOR_BunchInstLumi, &b_lucBi2HitOR_BunchInstLumi);
   Notify();
}

Bool_t vdMScanData::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void vdMScanData::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t vdMScanData::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef vdMScanData_cxx
