
namespace Utils {
    pair<double, double> GetMeanErr(vector<float> v);
    pair<float,float> GetMinMax(vector<float> v);
    void remove(vector<float> &v, vector<int> toRemove);
    double GetErrorFromAMean(vector<float> v);
    TString GetPartString(int part);
    int GetPartIndex(TString part);
    TString GetPMTString(int pmt, int part);
    TString GetPMTStringExtra(int pmt, int part, bool warning);
    TString GetFitArray(int idx);
    int GetPMTFromString(TString s);
    int GetPartFromString(TString s);
    bool HasLeftRight(int pmt, int part);
    bool IsRightPMT(int pmt, int part);
    vector<int> Prune(vector<float> &v, int LB, int runNum, double threshold);
    int DeltaNFunction(vector<float> &v, int index);
    vector<float> pruningFunction(vector<float> &v);
    int GetLaserIndex(int pmt, int part);
    TString PedestalTypeAsString(int pedestalType);
    TString ToModString(int mod);
    double GetGroupGainTransimpedance(int group, int part, int pmt, int mod);
    bool IsMBTSInner(int part, int pmt, int mod);
    bool IsMBTSOuter(int part, int pmt, int mod);
    bool IsE4Prime(int part, int pmt, int mod);
}