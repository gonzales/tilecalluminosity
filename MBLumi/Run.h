
class Run {
    public:
        Run(int runNum, string runSuffix, string convConstFilename, string laserFileName, string anchorLaserFileName, int cells_type, bool doSACorrection, TString overridePedestal, bool useTime, bool noLBavg);
        ~Run();
        virtual void ComputePedestal(int doPedestal, int part, int doPedestalPlots);
        void PrunePedestal(int part);
        void ComputeSignalFreq(int part, int startLB, int endLB);
        void SavePedestal(int part, int toSave);
        void LoadPedestal(int part, int toLoad, TString ped_suffix = "", bool toExtraPed = false, TString file_suffix = "");
        float GetPedestal(int part, int mod, int pmt, float time);
        double GetGain(int part, int mod, int pmt);
        int GetGainGroup(int part, int mod, int pmt);
        int GetNumberOfBunches();
        vector<int> getPedLimits(int verbose);
        vector<double> getTimeLimits();
        vector<double> getRatioLimits();
        int GetLBUpperLimit();
        void getPedLimitsAutomatically(int verbose);
        vector<int> getLBBoundaries();
        void EpicPruning(int part, vector<int> cells_to_compute);
        void SetRefPFile(int refRun, int deltaIndex = 0);
        void SetUseGroupGain(int use);

        virtual string GetXToPrint(int LB, long long time);
        virtual void ComputeCurrents(int part, vector<int> cells_to_compute) = 0;

        int runNum;
        TString suffix = "";
        int numberForMean = 20;
        bool skipLBBeforeStart = false;
        bool useRefGain = false;
        int useGroupGain = -1;
        int pedestalFileIndex_offset = 0;
        PFile * pFiles [4];
        PFile * refPFile_EBA;
        PFile * refPFile_EBC;
        PFile * refPFile_LBA;
        PFile * refPFile_LBC;
        float fit0[4][64][48];
        float fit1[4][64][48];
        float fit2[4][64][48];
        float fit3[4][64][48];
        float refPoint[4][64][48];
        float fit5[4][64][48];
        float extraPedInfo[4][64][48];
        int nEntries;
        int pedStart, pedEnd;
        long double tinit;
        long long utinit;
        long long utend;
        int LBStart, LBEnd;
        int auto_LBStart = 0, auto_LBEnd = -1;
        vector<map<int,vector<float>>> detectedBad[4];
        map<int,vector<float>> epicBad[4];
        vector<float> LBs[4];
        vector<long long> times_perCurrent[4][48]; // in seconds without tinit subtraction
        vector<float> avCurrents[4][64][48];
        vector<float> avCurrents_err[4][64][48];
        vector<long long> times[4]; // in seconds with tinit subtraction
        int pedestalType;
        bool twoFiles;
        int LBOffset = 0;
        int LBBound = 0;
        bool usePLB;
        int debug_LB;
        TString lumiFilename;
        TFile * lumiFile;
        map<TString,vector<float>> debug_AboveADC_map_1;
        map<TString,vector<float>> debug_AboveADC_map_2;
        map<TString,vector<float>> debug_AboveADC_map_3;
        TTree* epicTrees[4];
        bool doSACorrection = false;
        vector<double> timeLBBoundaries [4];
        map<int,TF1*> SA_fits[4];
        map<int,TF1*> SA_decays[4];
        int cells_type = -1;

        double laser[4][64][48];
        double laser_anchor[4][64][48];
        double conv_const[4][64][48];

        TString overridePedestal = "";

        bool useTime = false;
        bool noLBavg = false;

};

Run::Run(int run, string runSuffix, string convConstFilename, string laserFileName, string anchorLaserFileName, int _cells_type = -1, bool _doSACorrection = false, TString _overridePedestal = "", bool _useTime = false, bool _noLBavg = false) {
    runNum = run;
    suffix = runSuffix;
    useTime = _useTime;
    noLBavg = _noLBavg;
    int verbose = (_cells_type >= -1) ? 1 : 0;
    if (verbose > 0) cout << "Initializing " << run << endl;
    vector<TString> pFilesNames = Global::getPFilesPath(runNum);
    if (pFilesNames.size() == 0) return;

    // pedestal type
    // if (runNum == 354494 && cells_type > 1) pedestalType = PEDESTALS::FIT_ADDEXPO;
    // pedestalType = Global::getPedestalType(runNum);
    // if (runNum == 331020 && (pedestalType == PEDESTALS::FIT_SECONDEXPO || pedestalType == PEDESTALS::FIT_THIRDEXPO)) {
    //     pedestalFileIndex_offset = 4;
    //     pedStart = getPedLimits(verbose).at(2);
    //     pedEnd = getPedLimits(verbose).at(3);
    //     numberForMean = 5;
    // }
    
    pedestalType = Global::getPedestalType(runNum);
    overridePedestal = _overridePedestal;
    if (overridePedestal != "") {
        pedestalType = Global::getOverridenPedestalType(overridePedestal);
    }
    //

    if (verbose > 0) {
        cout << "---- PFiles are:" << endl;
        pFiles[PART::EBA] = new PFile(pFilesNames.at(PART::EBA)); cout << pFilesNames.at(PART::EBA) << endl;
        pFiles[PART::LBA] = new PFile(pFilesNames.at(PART::LBA)); cout << pFilesNames.at(PART::LBA) << endl;
        pFiles[PART::LBC] = new PFile(pFilesNames.at(PART::LBC)); cout << pFilesNames.at(PART::LBC) << endl;
        pFiles[PART::EBC] = new PFile(pFilesNames.at(PART::EBC)); cout << pFilesNames.at(PART::EBC) << endl;
        cout << "---------------------------------------------" << endl;

        pFiles[PART::EBA]->intTree->fChain->GetEntry(0);
        pFiles[PART::LBA]->intTree->fChain->GetEntry(0);
        pFiles[PART::LBC]->intTree->fChain->GetEntry(0);
        pFiles[PART::EBC]->intTree->fChain->GetEntry(0);
        long double tinitEBA = pFiles[PART::EBA]->intTree->timesec[0] + pFiles[PART::EBA]->intTree->timeusec[0]/1000000;
        long double tinitLBA = pFiles[PART::LBA]->intTree->timesec[0] + pFiles[PART::LBA]->intTree->timeusec[0]/1000000;
        long double tinitLBC = pFiles[PART::LBC]->intTree->timesec[0] + pFiles[PART::LBC]->intTree->timeusec[0]/1000000;
        long double tinitEBC = pFiles[PART::EBC]->intTree->timesec[0] + pFiles[PART::EBC]->intTree->timeusec[0]/1000000;
        tinit = min(min(tinitEBA,tinitLBA),min(tinitLBC,tinitEBC));

        long long utinitEBA = pFiles[PART::EBA]->intTree->timesec[0]*1000000. + pFiles[PART::EBA]->intTree->timeusec[0];
        long long utinitLBA = pFiles[PART::LBA]->intTree->timesec[0]*1000000. + pFiles[PART::LBA]->intTree->timeusec[0];
        long long utinitLBC = pFiles[PART::LBC]->intTree->timesec[0]*1000000. + pFiles[PART::LBC]->intTree->timeusec[0];
        long long utinitEBC = pFiles[PART::EBC]->intTree->timesec[0]*1000000. + pFiles[PART::EBC]->intTree->timeusec[0];
        utinit = min(min(utinitEBA,utinitLBA),min(utinitLBC,utinitEBC));

        vector<int> pedLimits = getPedLimits(verbose);
        pedStart = -1;
        pedEnd = -1;
        if (_cells_type >= -1) {
            if (pedLimits.size() != 0) {
                pedStart = pedLimits.at(0);
                pedEnd = pedLimits.at(1);
            }
        }

        if (runNum == 331020 && (pedestalType == PEDESTALS::FIT_SECONDEXPO || pedestalType == PEDESTALS::FIT_THIRDEXPO)) {
            if (pedLimits.size() < 3) {
                coutError("Not four values defined in 331020 ped Limits");
            } else {
                pedestalFileIndex_offset = 4;
                pedStart = pedLimits.at(2);
                pedEnd = pedLimits.at(3);
                numberForMean = 5;
            }
        }

        int nEntries_EBA = pFiles[PART::EBA]->intTree->fChain->GetEntries();
        int nEntries_EBC = pFiles[PART::EBC]->intTree->fChain->GetEntries();
        int nEntries_LBA = pFiles[PART::LBA]->intTree->fChain->GetEntries();
        int nEntries_LBC = pFiles[PART::LBC]->intTree->fChain->GetEntries();

        cout << "nEntries_EBA = " << nEntries_EBA << endl;
        cout << "nEntries_EBC = " << nEntries_EBC << endl;
        cout << "nEntries_LBA = " << nEntries_LBA << endl;
        cout << "nEntries_LBC = " << nEntries_LBC << endl;

        int nEntries_E = max(nEntries_EBA, nEntries_EBC);
        int nEntries_L = max(nEntries_LBA, nEntries_LBC);
        nEntries = max(nEntries_E, nEntries_L);

        pFiles[PART::EBA]->intTree->fChain->GetEntry(0);
        LBStart = pFiles[PART::EBA]->intTree->LB;
        pFiles[PART::EBA]->intTree->fChain->GetEntry(nEntries_EBA - 1);
        LBEnd = pFiles[PART::EBA]->intTree->LB;
        utend = pFiles[PART::EBA]->intTree->timesec[0]*1000000. + pFiles[PART::EBA]->intTree->timeusec[0];

        debug_LB = Global::GetDebugLB(run);
        if (debug_LB == -1) debug_LB = pedEnd + (LBEnd - pedEnd)/2;

        if (verbose > 0) {
            cout << "Run tinit = " << tinit << endl;
            cout << "Run utinit = " << utinit << endl;
            cout << "Run utend = " << utend << endl;
        }

        // Check gain reference 
        if (suffix.Contains("refGain354124") && runNum != 354124) SetRefPFile(354124);
        if (suffix.Contains("refGain339197") && runNum != 339197) SetRefPFile(339197);
        // if (suffix.Contains("refGain331020High")) SetRefPFile(331020,0);
        if (suffix.Contains("refGain331020Low")) SetRefPFile(331020,4);

        cout << "Initializing CalibTrees" << endl;

        pFiles[PART::EBA]->calibTree->GetEntry(0);
        pFiles[PART::LBA]->calibTree->GetEntry(0);
        pFiles[PART::LBC]->calibTree->GetEntry(0);
        pFiles[PART::EBC]->calibTree->GetEntry(0);

        cells_type = _cells_type;
        if (convConstFilename == "" && laserFileName == "" && anchorLaserFileName == "" && cells_type == -1) return;

        memset(fit0,0,sizeof(fit0));
        memset(extraPedInfo,0,sizeof(extraPedInfo));
        if (pedestalType == PEDESTALS::FIT_LINEAR || pedestalType == PEDESTALS::FIT_SECONDEXPO || pedestalType == PEDESTALS::FIT_THIRDEXPO) {
            memset(fit1,0,sizeof(fit1));
        }
        if (pedestalType == PEDESTALS::FIT_EXPO || PEDESTALS::VDM || PEDESTALS::FIT_SECONDPOLY ) {
            memset(fit1,0,sizeof(fit1));
            memset(fit2,0,sizeof(fit2));
        }
        if (pedestalType == PEDESTALS::FIT_339197 || pedestalType == PEDESTALS::INTERPOLATION || pedestalType == PEDESTALS::INTERPOLATION_TO_LINEAR_FIT || pedestalType == PEDESTALS::VDM_EXPO || pedestalType == PEDESTALS::VDM_EXPO_wINTERP || pedestalType == PEDESTALS::VDM2017_EXPO_w2INTERP  ) {
            memset(fit1,0,sizeof(fit1));
            memset(fit2,0,sizeof(fit2));
            memset(fit3,0,sizeof(fit3));
            memset(refPoint,0,sizeof(refPoint));
        }
        if (pedestalType == PEDESTALS::FIT_354124) {
            memset(fit1,0,sizeof(fit1));
            memset(fit2,0,sizeof(fit2));
            memset(fit3,0,sizeof(fit3));
        }
        if (pedestalType == PEDESTALS::FIT_ADDEXPO || pedestalType == PEDESTALS::VDM2017_EXPO_w3INTERP) {
            memset(fit1,0,sizeof(fit1));
            memset(fit2,0,sizeof(fit2));
            memset(fit3,0,sizeof(fit3));
            memset(refPoint,0,sizeof(refPoint));
            memset(fit5,0,sizeof(fit5));
        }

        if (cells_type > -1) {
            lumiFilename = TString::Format("Luminosities/run_%d%s_%s", runNum, suffix.Data(), getCellsName(cells_type).Data());
            lumiFile = new TFile(lumiFilename + ".root","RECREATE");
        } else {
            lumiFile = new TFile("TMP.root","RECREATE");
        }

        // cout << "Initializing CalibTrees" << endl;

        // pFiles[PART::EBA]->calibTree->GetEntry(0);
        // pFiles[PART::LBA]->calibTree->GetEntry(0);
        // pFiles[PART::LBC]->calibTree->GetEntry(0);
        // pFiles[PART::EBC]->calibTree->GetEntry(0);
    }
    twoFiles = pFilesNames.size() > 4;

    for (int part = 0 ; part < 4 ; part ++) {
        for (int mod = 0 ; mod < 64 ; mod ++) {
            for (int pmt = 0 ; pmt < 48 ; pmt ++) {
                // laser[part][mod][pmt] = 1;
                // laser_anchor[part][mod][pmt] = 1;
                conv_const[part][mod][pmt] = (convConstFilename == "share/") ? 1 : -1;
            }
        }
    }
    //Read Conv Constants file
    if (convConstFilename != "") {
        fstream calibfile; calibfile.open(convConstFilename);
        if (!calibfile.is_open()) {coutError("No calibration constants will be used since I can't see the file.");}
        string cline;
        double c_const, c_error;
        int c_mod, c_pmt;
        char c_part[10];
        while(getline(calibfile, cline)) {

            sscanf (cline.c_str(),"%9s\t%i\t%i\t%lf\t%lf\n", c_part, &c_mod, &c_pmt, &c_const, &c_error);

            conv_const[Utils::GetPartIndex(c_part)][c_mod][c_pmt] = c_const;
            if (((TString)convConstFilename).Contains("LumiScale")) {
                conv_const[Utils::GetPartIndex(c_part)][c_mod][c_pmt] = 1.003 * (PARAMS::LHC_FREQ * 1e8 * 1e11 * 1e11 * 1e-30) / (GetNumberOfBunches() * c_const) ; // Convert to cm2, normalize laser intensities, 10^30 is implied, correction R=1.003
            }

        }
        if (((TString)convConstFilename).Contains("LumiScale_")) {
            coutError("Setting mod57 E4 A to zero");
            conv_const[PART::EBA][56][PMT::E4] = 0;
        }
        calibfile.close();

        if (GetLBUpperLimit() >= 0) {
            coutError("This run has an LB upperlimit!");
        }
    }

    // Read Laser Corrections
    LaserUtils::getLaserConstants(laserFileName, laser, verbose);

    // Read Anchor Laser Corrections
    LaserUtils::getLaserConstants(anchorLaserFileName, laser_anchor, verbose);

    // ShortActivation correction
    doSACorrection = _doSACorrection;
    if (doSACorrection) {
        cout << "==============================================" << endl;
        cout << "Opening Short Activation Fit Files" << endl;
        cout << "==============================================" << endl;

        vector<double> SA_pmt_EBA, SA_par0_EBA, SA_par1_EBA, SA_par2_EBA, SA_par3_EBA;
        readFile("ActivationFit/fits_EBA.dat", 0, {0,1,2,3,4}, {&SA_pmt_EBA,&SA_par0_EBA,&SA_par1_EBA,&SA_par2_EBA,&SA_par3_EBA});
        for (int i_SA = 0 ; i_SA < SA_pmt_EBA.size() ; i_SA++) {
            int pmt = SA_pmt_EBA.at(i_SA);
            if (SA_par2_EBA.at(i_SA) == -9999 && SA_par3_EBA.at(i_SA) == -9999) {
                SA_fits[PART::EBA][pmt] = new TF1(Utils::GetPMTStringExtra(pmt,PART::EBA),"[0]*(1-exp([1]*x))"); SA_fits[PART::EBA][pmt]->SetParameter(0,SA_par0_EBA.at(i_SA)); SA_fits[PART::EBA][pmt]->SetParameter(1,SA_par1_EBA.at(i_SA));
                SA_decays[PART::EBA][pmt] = new TF1(Utils::GetPMTStringExtra(pmt,PART::EBA),"exp([0]*x)"); SA_decays[PART::EBA][pmt]->SetParameter(0,SA_par1_EBA.at(i_SA));
            }
            else {
                SA_fits[PART::EBA][pmt] = new TF1(Utils::GetPMTStringExtra(pmt,PART::EBA),"[0]*(1-exp([1]*x)) + [2]*(1-exp([3]*x))"); SA_fits[PART::EBA][pmt]->SetParameter(0,SA_par0_EBA.at(i_SA)); SA_fits[PART::EBA][pmt]->SetParameter(1,SA_par1_EBA.at(i_SA)); SA_fits[PART::EBA][pmt]->SetParameter(2,SA_par2_EBA.at(i_SA)); SA_fits[PART::EBA][pmt]->SetParameter(3,SA_par3_EBA.at(i_SA)); 
                SA_decays[PART::EBA][pmt] = new TF1(Utils::GetPMTStringExtra(pmt,PART::EBA),"(1./2.)*exp([0]*x) + (1./2.)*exp([1]*x)"); SA_decays[PART::EBA][pmt]->SetParameter(0,SA_par1_EBA.at(i_SA)); SA_decays[PART::EBA][pmt]->SetParameter(1,SA_par3_EBA.at(i_SA));
            }
        }

        vector<double> SA_pmt_EBC, SA_par0_EBC, SA_par1_EBC, SA_par2_EBC, SA_par3_EBC;
        readFile("ActivationFit/fits_EBC.dat", 0, {0,1,2,3,4}, {&SA_pmt_EBC,&SA_par0_EBC,&SA_par1_EBC,&SA_par2_EBC,&SA_par3_EBC});
        for (int i_SA = 0 ; i_SA < SA_pmt_EBC.size() ; i_SA++) {
            int pmt = SA_pmt_EBC.at(i_SA);
            if (SA_par2_EBC.at(i_SA) == -9999 && SA_par3_EBC.at(i_SA) == -9999) {
                SA_fits[PART::EBC][pmt] = new TF1(Utils::GetPMTStringExtra(pmt,PART::EBC),"[0]*(1-exp([1]*x))"); SA_fits[PART::EBC][pmt]->SetParameter(0,SA_par0_EBC.at(i_SA)); SA_fits[PART::EBC][pmt]->SetParameter(1,SA_par1_EBC.at(i_SA)); 
                SA_decays[PART::EBC][pmt] = new TF1(Utils::GetPMTStringExtra(pmt,PART::EBC),"exp([0]*x)"); SA_decays[PART::EBC][pmt]->SetParameter(0,SA_par1_EBC.at(i_SA));
            }
            else {
                SA_fits[PART::EBC][pmt] = new TF1(Utils::GetPMTStringExtra(pmt,PART::EBC),"[0]*(1-exp([1]*x)) + [2]*(1-exp([3]*x))"); SA_fits[PART::EBC][pmt]->SetParameter(0,SA_par0_EBC.at(i_SA)); SA_fits[PART::EBC][pmt]->SetParameter(1,SA_par1_EBC.at(i_SA)); SA_fits[PART::EBC][pmt]->SetParameter(2,SA_par2_EBC.at(i_SA)); SA_fits[PART::EBC][pmt]->SetParameter(3,SA_par3_EBC.at(i_SA)); 
                SA_decays[PART::EBC][pmt] = new TF1(Utils::GetPMTStringExtra(pmt,PART::EBC),"(1./2.)*exp([0]*x) + (1./2.)*exp([1]*x)"); SA_decays[PART::EBC][pmt]->SetParameter(0,SA_par1_EBC.at(i_SA)); SA_decays[PART::EBC][pmt]->SetParameter(1,SA_par3_EBC.at(i_SA));
            }
        }

        // for (int LBBound : getLBBoundaries()) {
        timeLBBoundaries[PART::EBA].push_back(-1);
        timeLBBoundaries[PART::EBC].push_back(-1);
        timeLBBoundaries[PART::LBA].push_back(-1);
        timeLBBoundaries[PART::LBC].push_back(-1);
        // }
    }

}

void Run::SetRefPFile(int refRun, int deltaIndex = 0) {
    cout << "Changing reference gain classification to " << refRun << endl;
    useRefGain = true;

    refPFile_EBA = new PFile(Global::getPFilesPath(refRun).at(PART::EBA + deltaIndex));
    refPFile_EBC = new PFile(Global::getPFilesPath(refRun).at(PART::EBC + deltaIndex));
    refPFile_LBA = new PFile(Global::getPFilesPath(refRun).at(PART::LBA + deltaIndex));
    refPFile_LBC = new PFile(Global::getPFilesPath(refRun).at(PART::LBC + deltaIndex));
    refPFile_EBA->calibTree->GetEntry(0);
    refPFile_EBC->calibTree->GetEntry(0);
    refPFile_LBA->calibTree->GetEntry(0);
    refPFile_LBC->calibTree->GetEntry(0);
}

string Run::GetXToPrint(int LB, long long time) {
    return (useTime) ? to_string(time) : to_string(LB);
}

vector<double> Run::getTimeLimits() {
        vector<double> timeLimits;

        if (runNum == 339500) {timeLimits.push_back(0);timeLimits.push_back(1295);timeLimits.push_back(16548);timeLimits.push_back(16900); timeLimits.push_back(1306);}
        if (runNum == 335056) {timeLimits.push_back(3000);timeLimits.push_back(7780);timeLimits.push_back(9421);timeLimits.push_back(9540); timeLimits.push_back(7804);}
        if (runNum == 334580) {timeLimits.push_back(3000);timeLimits.push_back(16920);timeLimits.push_back(19676);timeLimits.push_back(19800); timeLimits.push_back(16930);}
        if (runNum == 339562) {timeLimits.push_back(3000);timeLimits.push_back(6650);timeLimits.push_back(17932);timeLimits.push_back(18170); timeLimits.push_back(6664);}
        if (runNum == 339535) {timeLimits.push_back(3000);timeLimits.push_back(6840);timeLimits.push_back(23793);timeLimits.push_back(24020); timeLimits.push_back(6850);}
        if (runNum == 334842) {timeLimits.push_back(3000);timeLimits.push_back(15360);timeLimits.push_back(35420);timeLimits.push_back(35660); timeLimits.push_back(15365);}
        if (runNum == 325789) {timeLimits.push_back(0);timeLimits.push_back(6360);timeLimits.push_back(7723);timeLimits.push_back(11000); timeLimits.push_back(6373);}
        // if (runNum == 329778) {timeLimits.push_back(0);timeLimits.push_back(2430);timeLimits.push_back(3362);timeLimits.push_back(7350); timeLimits.push_back(2440);}
        if (runNum == 329778) {timeLimits.push_back(0);timeLimits.push_back(2000);timeLimits.push_back(4080);timeLimits.push_back(7350); timeLimits.push_back(2640);}
        if (runNum == 332720) {timeLimits.push_back(0);timeLimits.push_back(11200);timeLimits.push_back(13351);timeLimits.push_back(13600); timeLimits.push_back(11262);}
        if (runNum == 333853) {timeLimits.push_back(165*60);timeLimits.push_back(17440);timeLimits.push_back(25961);timeLimits.push_back(26850); timeLimits.push_back(17452);}
        if (runNum == 335082) {timeLimits.push_back(0);timeLimits.push_back(1600);timeLimits.push_back(3125);timeLimits.push_back(3700); timeLimits.push_back(1613);}
        if (runNum == 339590) {timeLimits.push_back(0);timeLimits.push_back(4630);timeLimits.push_back(8491);timeLimits.push_back(8830); timeLimits.push_back(4634);}
        if (runNum == 338967) {timeLimits.push_back(0);timeLimits.push_back(4000);timeLimits.push_back(13714);timeLimits.push_back(14190); timeLimits.push_back(4007);}
        // if (runNum == 331019) {timeLimits.push_back(3000);timeLimits.push_back(6500);timeLimits.push_back(7473);timeLimits.push_back(84450); timeLimits.push_back(6524);}
        if (runNum == 331019) {timeLimits.push_back(3000);timeLimits.push_back(6250);timeLimits.push_back(7500);timeLimits.push_back(84450); timeLimits.push_back(6500);}
        // if (runNum == 327761) {timeLimits.push_back(3000);timeLimits.push_back(8120);timeLimits.push_back(8971);timeLimits.push_back(9850); timeLimits.push_back(8128);}
        if (runNum == 327761) {timeLimits.push_back(3000);timeLimits.push_back(8120);timeLimits.push_back(8971);timeLimits.push_back(9850); timeLimits.push_back(8126);}
        if (runNum == 326468) {timeLimits.push_back(0);timeLimits.push_back(7350);timeLimits.push_back(9879);timeLimits.push_back(10600); timeLimits.push_back(7431);}
        if (runNum == 334455) {timeLimits.push_back(3000);timeLimits.push_back(19550);timeLimits.push_back(21042);timeLimits.push_back(21380); timeLimits.push_back(19580);}
        if (runNum == 339346) {timeLimits.push_back(3000);timeLimits.push_back(13900);timeLimits.push_back(17609);timeLimits.push_back(18060); timeLimits.push_back(13926);}

        return timeLimits;

}

vector<int> Run::getPedLimits(int verbose = 1) { 
        vector<int> pedLimits; 

        if (runNum == 276952) {pedLimits.push_back(0); pedLimits.push_back(50);}
        if (runNum == 276954) {pedLimits.push_back(0); pedLimits.push_back(150);}
        if (runNum == 278880) {pedLimits.push_back(0); pedLimits.push_back(50);}
        if (runNum == 278912) {pedLimits.push_back(0); pedLimits.push_back(50);}
        if (runNum == 278968) {pedLimits.push_back(0); pedLimits.push_back(10);}
        if (runNum == 279169) {pedLimits.push_back(0); pedLimits.push_back(200);}
        if (runNum == 279259) {pedLimits.push_back(0); pedLimits.push_back(300);}
        if (runNum == 279279) {pedLimits.push_back(0); pedLimits.push_back(200);}
        if (runNum == 279284) {pedLimits.push_back(0); pedLimits.push_back(300);}
        if (runNum == 279345) {pedLimits.push_back(0); pedLimits.push_back(50);}
        if (runNum == 279598) {pedLimits.push_back(0); pedLimits.push_back(300);}
        if (runNum == 279685) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 279764) {pedLimits.push_back(0); pedLimits.push_back(10);}
        if (runNum == 279813) {pedLimits.push_back(0); pedLimits.push_back(200);}
        if (runNum == 279867) {pedLimits.push_back(0); pedLimits.push_back(150);}
        if (runNum == 279932) {pedLimits.push_back(0); pedLimits.push_back(200);}
        if (runNum == 280231) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 280319) {pedLimits.push_back(0); pedLimits.push_back(30);}
        if (runNum == 280368) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 280423) {pedLimits.push_back(0); pedLimits.push_back(200);}
        if (runNum == 280464) {pedLimits.push_back(0); pedLimits.push_back(300);}
        if (runNum == 280614) {pedLimits.push_back(0); pedLimits.push_back(300);}
        if (runNum == 280673) {pedLimits.push_back(0); pedLimits.push_back(50);}
        if (runNum == 280753) {pedLimits.push_back(0); pedLimits.push_back(300);}
        if (runNum == 280853) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 280862) {pedLimits.push_back(0); pedLimits.push_back(20);}
        if (runNum == 280950) {pedLimits.push_back(0); pedLimits.push_back(20);}
        if (runNum == 280977) {pedLimits.push_back(0); pedLimits.push_back(200);}
        if (runNum == 281070) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 281074) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 281317) {pedLimits.push_back(0); pedLimits.push_back(50);}
        if (runNum == 281411) {pedLimits.push_back(0); pedLimits.push_back(30);}
        if (runNum == 282625) {pedLimits.push_back(0); pedLimits.push_back(10);}


        if (runNum == 276262) {pedLimits.push_back(0); pedLimits.push_back(50);}
        if (runNum == 276329) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 276336) {pedLimits.push_back(56); pedLimits.push_back(60);}
        if (runNum == 276416) {pedLimits.push_back(0); pedLimits.push_back(300);}
        if (runNum == 276511) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 276689) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 276778) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 276790) {pedLimits.push_back(0); pedLimits.push_back(200);}

        if (runNum == 301918) {pedLimits.push_back(245); pedLimits.push_back(260);}
        if (runNum == 298687) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 298690) {pedLimits.push_back(0); pedLimits.push_back(80);}
        if (runNum == 299055) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 299243) {pedLimits.push_back(122); pedLimits.push_back(130);}
        

        if (runNum == 284213) {pedLimits.push_back(0); pedLimits.push_back(20);}
        if (runNum == 284484) {pedLimits.push_back(507); pedLimits.push_back(520);}
        if (runNum == 284154) {pedLimits.push_back(0); pedLimits.push_back(60);}
        if (runNum == 283780) {pedLimits.push_back(0); pedLimits.push_back(200);}
        if (runNum == 282631) {pedLimits.push_back(0); pedLimits.push_back(60);}
        if (runNum == 281070) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 282625) {pedLimits.push_back(0); pedLimits.push_back(10);}
        if (runNum == 281411) {pedLimits.push_back(0); pedLimits.push_back(40);}
        if (runNum == 280673) {pedLimits.push_back(0); pedLimits.push_back(60);}
        if (runNum == 279598) {pedLimits.push_back(0); pedLimits.push_back(400);}
        if (runNum == 279345) {pedLimits.push_back(0); pedLimits.push_back(60);}
        if (runNum == 280614) {pedLimits.push_back(0); pedLimits.push_back(300);}
        if (runNum == 280464) {pedLimits.push_back(0); pedLimits.push_back(300);}
        if (runNum == 279867) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 280950) {pedLimits.push_back(0); pedLimits.push_back(50);}
        if (runNum == 280853) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 281317) {pedLimits.push_back(0); pedLimits.push_back(50);}
        if (runNum == 282784) {pedLimits.push_back(0); pedLimits.push_back(50);}
        if (runNum == 282992) {pedLimits.push_back(0); pedLimits.push_back(50);}
        if (runNum == 283074) {pedLimits.push_back(0); pedLimits.push_back(50);}
        if (runNum == 283155) {pedLimits.push_back(0); pedLimits.push_back(50);}
        if (runNum == 283270) {pedLimits.push_back(80); pedLimits.push_back(90);}
        if (runNum == 283429) {pedLimits.push_back(0); pedLimits.push_back(60);}
        if (runNum == 283608) {pedLimits.push_back(193); pedLimits.push_back(200);}
        if (runNum == 284006) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 284427) {pedLimits.push_back(0); pedLimits.push_back(60);}

        if (runNum == 280753) {pedLimits.push_back(0); pedLimits.push_back(200);}
        if (runNum == 280319) {pedLimits.push_back(0); pedLimits.push_back(60);}
        if (runNum == 310473) {pedLimits.push_back(20); pedLimits.push_back(80);}
        if (runNum == 310468) {pedLimits.push_back(40); pedLimits.push_back(160);}
        if (runNum == 310405) {pedLimits.push_back(100); pedLimits.push_back(500);}
        if (runNum == 310370) {pedLimits.push_back(20); pedLimits.push_back(70);}
        if (runNum == 310341) {pedLimits.push_back(30); pedLimits.push_back(80);}
        if (runNum == 298595) {pedLimits.push_back(0); pedLimits.push_back(50);}
        if (runNum == 310691) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 307656) {pedLimits.push_back(0); pedLimits.push_back(140);}
        if (runNum == 304431) {pedLimits.push_back(0); pedLimits.push_back(250);}
        if (runNum == 304178) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 304128) {pedLimits.push_back(0); pedLimits.push_back(90);}
        if (runNum == 302872) {pedLimits.push_back(0); pedLimits.push_back(120);}
        if (runNum == 302393) {pedLimits.push_back(0); pedLimits.push_back(50);}
        if (runNum == 302391) {pedLimits.push_back(0); pedLimits.push_back(80);}
        if (runNum == 301973) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 301932) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 300800) {pedLimits.push_back(0); pedLimits.push_back(80);}
        if (runNum == 300687) {pedLimits.push_back(0); pedLimits.push_back(60);}
        if (runNum == 298773) {pedLimits.push_back(10); pedLimits.push_back(120);}
        if (runNum == 308047) {pedLimits.push_back(10); pedLimits.push_back(70);}
        if (runNum == 307935) {pedLimits.push_back(10); pedLimits.push_back(60);}
        if (runNum == 302831) {pedLimits.push_back(120); pedLimits.push_back(130);}
        if (runNum == 298633) {pedLimits.push_back(0); pedLimits.push_back(120);}
        if (runNum == 298609) {pedLimits.push_back(0); pedLimits.push_back(160);}
        if (runNum == 298595) {pedLimits.push_back(0); pedLimits.push_back(70);}
        if (runNum == 311481) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 311402) {pedLimits.push_back(0); pedLimits.push_back(60);}
        if (runNum == 311365) {pedLimits.push_back(0); pedLimits.push_back(120);}
        if (runNum == 311287) {pedLimits.push_back(20); pedLimits.push_back(60);}
        if (runNum == 311170) {pedLimits.push_back(20); pedLimits.push_back(80);}
        if (runNum == 310969) {pedLimits.push_back(200); pedLimits.push_back(300);}
        if (runNum == 310249) {pedLimits.push_back(120); pedLimits.push_back(160);}
        if (runNum == 310247) {pedLimits.push_back(300); pedLimits.push_back(400);}

        if (runNum == 299147) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 299184) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 299241) {pedLimits.push_back(0); pedLimits.push_back(100);}
        if (runNum == 299340) {pedLimits.push_back(0); pedLimits.push_back(350);}
        if (runNum == 330025) {pedLimits.push_back(0); pedLimits.push_back(80);}
        if (runNum == 331129) {pedLimits.push_back(0); pedLimits.push_back(50);}
        if (runNum == 330203) {pedLimits.push_back(0); pedLimits.push_back(50);}
        if (runNum == 300279) {pedLimits.push_back(0); pedLimits.push_back(250);}

        if (runNum == 300415) {pedLimits.push_back(0); pedLimits.push_back(105);}
        if (runNum == 300287) {pedLimits.push_back(0); pedLimits.push_back(105); pedLimits.push_back(525); pedLimits.push_back(535);}
        if (runNum == 299390) {pedLimits.push_back(0); pedLimits.push_back(220); pedLimits.push_back(708); pedLimits.push_back(730);}
        if (runNum == 299584) {pedLimits.push_back(0); pedLimits.push_back(80); pedLimits.push_back(2304); pedLimits.push_back(2310);}

        if (runNum == 356427) {pedLimits.push_back(0); pedLimits.push_back(100); pedLimits.push_back(460); pedLimits.push_back(800);}

        if (runNum == 338183) {pedLimits.push_back(0); pedLimits.push_back(150);}

        //------------------------------ 2018 Activation
        if (runNum == 351325) {pedLimits.push_back(0); pedLimits.push_back(80); pedLimits.push_back(452); pedLimits.push_back(480); pedLimits.push_back(95);}
        if (runNum == 351455) {pedLimits.push_back(0); pedLimits.push_back(60); pedLimits.push_back(639); pedLimits.push_back(650); pedLimits.push_back(68);}
        if (runNum == 351698) {pedLimits.push_back(0); pedLimits.push_back(50); pedLimits.push_back(340); pedLimits.push_back(360); pedLimits.push_back(59);}
        if (runNum == 352494) {pedLimits.push_back(0); pedLimits.push_back(170); pedLimits.push_back(344); pedLimits.push_back(360); pedLimits.push_back(185); pedLimits.push_back(343);}
        if (runNum == 352514) {pedLimits.push_back(0); pedLimits.push_back(140); pedLimits.push_back(422); pedLimits.push_back(440); pedLimits.push_back(147); pedLimits.push_back(421);}
        if (runNum == 355563) {pedLimits.push_back(0); pedLimits.push_back(100); pedLimits.push_back(442); pedLimits.push_back(477); pedLimits.push_back(109);}
        if (runNum == 356077) {pedLimits.push_back(0); pedLimits.push_back(110); pedLimits.push_back(256); pedLimits.push_back(288); pedLimits.push_back(122); pedLimits.push_back(255);}
        if (runNum == 356366) {pedLimits.push_back(0); pedLimits.push_back(180); pedLimits.push_back(309); pedLimits.push_back(320); pedLimits.push_back(190);}
        if (runNum == 358096) {pedLimits.push_back(0); pedLimits.push_back(130); pedLimits.push_back(816); pedLimits.push_back(830); pedLimits.push_back(144);}
        if (runNum == 358175) {pedLimits.push_back(0); pedLimits.push_back(50); pedLimits.push_back(535); pedLimits.push_back(550); pedLimits.push_back(75);}
        if (runNum == 359623) {pedLimits.push_back(0); pedLimits.push_back(90); pedLimits.push_back(130); pedLimits.push_back(150); pedLimits.push_back(99);}
        if (runNum == 362388) {pedLimits.push_back(0); pedLimits.push_back(130); pedLimits.push_back(538); pedLimits.push_back(570); pedLimits.push_back(136); pedLimits.push_back(537);}
        if (runNum == 362619) {pedLimits.push_back(0); pedLimits.push_back(40); pedLimits.push_back(478); pedLimits.push_back(500); pedLimits.push_back(52); pedLimits.push_back(477);}
        if (runNum == 359766) {pedLimits.push_back(0); pedLimits.push_back(60); pedLimits.push_back(478); pedLimits.push_back(500); pedLimits.push_back(71); pedLimits.push_back(477);}
        if (runNum == 360129) {pedLimits.push_back(0); pedLimits.push_back(50); pedLimits.push_back(666); pedLimits.push_back(690); pedLimits.push_back(62); pedLimits.push_back(665);}
        if (runNum == 360373) {pedLimits.push_back(0); pedLimits.push_back(230); pedLimits.push_back(822); pedLimits.push_back(840); pedLimits.push_back(247); pedLimits.push_back(820);}
        if (runNum == 359010) {pedLimits.push_back(0); pedLimits.push_back(310); pedLimits.push_back(1094); pedLimits.push_back(1120); pedLimits.push_back(320); pedLimits.push_back(1093);}
        if (runNum == 358233) {pedLimits.push_back(0); pedLimits.push_back(150); pedLimits.push_back(926); pedLimits.push_back(940); pedLimits.push_back(159);}
        if (runNum == 356259) {pedLimits.push_back(0); pedLimits.push_back(40); pedLimits.push_back(653); pedLimits.push_back(670); pedLimits.push_back(45);}

        if (runNum == 360348) {pedLimits.push_back(40); pedLimits.push_back(65); pedLimits.push_back(87); pedLimits.push_back(100); pedLimits.push_back(75);}
        if (runNum == 350431) {pedLimits.push_back(0);pedLimits.push_back(100);pedLimits.push_back(124);pedLimits.push_back(140); pedLimits.push_back(108);}
        if (runNum == 359677) {pedLimits.push_back(0); pedLimits.push_back(70); pedLimits.push_back(100); pedLimits.push_back(120); pedLimits.push_back(82); }
        if (runNum == 357355) {pedLimits.push_back(0); pedLimits.push_back(265); pedLimits.push_back(424); pedLimits.push_back(440); pedLimits.push_back(278); }
        if (runNum == 364160) {pedLimits.push_back(0); pedLimits.push_back(180); pedLimits.push_back(340); pedLimits.push_back(360); pedLimits.push_back(192); }
        if (runNum == 363096) {pedLimits.push_back(0); pedLimits.push_back(90); pedLimits.push_back(183); pedLimits.push_back(210); pedLimits.push_back(97); pedLimits.push_back(181);}
        if (runNum == 359398) {pedLimits.push_back(0); pedLimits.push_back(80); pedLimits.push_back(218); pedLimits.push_back(240); pedLimits.push_back(93); }
        if (runNum == 359279) {pedLimits.push_back(0); pedLimits.push_back(105); pedLimits.push_back(303); pedLimits.push_back(330); pedLimits.push_back(114); }
        if (runNum == 350749) {pedLimits.push_back(0); pedLimits.push_back(70); pedLimits.push_back(299); pedLimits.push_back(310); pedLimits.push_back(79); }
        if (runNum == 351359) {pedLimits.push_back(0); pedLimits.push_back(140); pedLimits.push_back(170); pedLimits.push_back(190); pedLimits.push_back(149); }
        if (runNum == 357293) {pedLimits.push_back(0); pedLimits.push_back(105); pedLimits.push_back(213); pedLimits.push_back(230); pedLimits.push_back(116); pedLimits.push_back(211);}
        if (runNum == 355651) {pedLimits.push_back(0); pedLimits.push_back(120); pedLimits.push_back(456); pedLimits.push_back(470); pedLimits.push_back(138); }
        if (runNum == 354315) {pedLimits.push_back(0); pedLimits.push_back(100); pedLimits.push_back(164); pedLimits.push_back(200); pedLimits.push_back(122); }
        if (runNum == 359586) {pedLimits.push_back(0); pedLimits.push_back(40); pedLimits.push_back(71); pedLimits.push_back(100); pedLimits.push_back(49); }
        if (runNum == 359441) {pedLimits.push_back(0); pedLimits.push_back(45); pedLimits.push_back(223); pedLimits.push_back(240); pedLimits.push_back(53); }
        if (runNum == 355599) {pedLimits.push_back(0); pedLimits.push_back(30); pedLimits.push_back(162); pedLimits.push_back(200); pedLimits.push_back(40); }
        if (runNum == 350751) {pedLimits.push_back(150); pedLimits.push_back(400); pedLimits.push_back(691); pedLimits.push_back(710); pedLimits.push_back(460); }
        if (runNum == 351160) {pedLimits.push_back(0); pedLimits.push_back(140); pedLimits.push_back(223); pedLimits.push_back(240); pedLimits.push_back(150); pedLimits.push_back(222);}
        if (runNum == 351296) {pedLimits.push_back(0); pedLimits.push_back(90); pedLimits.push_back(307); pedLimits.push_back(330); pedLimits.push_back(100); }
        if (runNum == 351832) {pedLimits.push_back(0); pedLimits.push_back(290); pedLimits.push_back(371); pedLimits.push_back(400); pedLimits.push_back(303); }
        if (runNum == 355848) {pedLimits.push_back(0); pedLimits.push_back(210); pedLimits.push_back(439); pedLimits.push_back(450); pedLimits.push_back(220); }
        if (runNum == 356095) {pedLimits.push_back(0); pedLimits.push_back(150); pedLimits.push_back(240); pedLimits.push_back(260); pedLimits.push_back(163); }
        if (runNum == 357750) {pedLimits.push_back(0); pedLimits.push_back(60); pedLimits.push_back(125); pedLimits.push_back(140); pedLimits.push_back(76); }
        if (runNum == 358325) {pedLimits.push_back(0); pedLimits.push_back(50); pedLimits.push_back(83); pedLimits.push_back(100); pedLimits.push_back(66); }
        if (runNum == 363262) {pedLimits.push_back(0); pedLimits.push_back(45); pedLimits.push_back(291); pedLimits.push_back(310); pedLimits.push_back(52); }
        //------------------------------



        if (runNum == 338480) {pedLimits.push_back(0);pedLimits.push_back(50);pedLimits.push_back(683);pedLimits.push_back(695); pedLimits.push_back(59);}
        if (runNum == 339396) {pedLimits.push_back(0);pedLimits.push_back(50);pedLimits.push_back(394);pedLimits.push_back(410); pedLimits.push_back(61);}
        if (runNum == 340072) {pedLimits.push_back(0);pedLimits.push_back(345);pedLimits.push_back(2050);pedLimits.push_back(2060); pedLimits.push_back(354);}
        if (runNum == 331710) {pedLimits.push_back(0);pedLimits.push_back(130);pedLimits.push_back(870);pedLimits.push_back(890); pedLimits.push_back(137);}
        if (runNum == 329780) {pedLimits.push_back(0);pedLimits.push_back(185);pedLimits.push_back(842);pedLimits.push_back(870); pedLimits.push_back(196);}
        if (runNum == 334890) {pedLimits.push_back(0);pedLimits.push_back(70);pedLimits.push_back(380);pedLimits.push_back(400); pedLimits.push_back(80);}
        if (runNum == 334878) {pedLimits.push_back(0);pedLimits.push_back(60);pedLimits.push_back(487);pedLimits.push_back(510); pedLimits.push_back(71);}
        if (runNum == 334849) {pedLimits.push_back(0);pedLimits.push_back(80);pedLimits.push_back(379);pedLimits.push_back(400); pedLimits.push_back(92);}
        if (runNum == 335282) {pedLimits.push_back(0);pedLimits.push_back(180);pedLimits.push_back(416);pedLimits.push_back(440); pedLimits.push_back(194);}
        if (runNum == 336497) {pedLimits.push_back(0);pedLimits.push_back(180);pedLimits.push_back(416);pedLimits.push_back(440); pedLimits.push_back(194);}
        if (runNum == 336944) {pedLimits.push_back(0);pedLimits.push_back(190);pedLimits.push_back(415);pedLimits.push_back(440); pedLimits.push_back(196);}
        if (runNum == 337404) {pedLimits.push_back(0);pedLimits.push_back(95);pedLimits.push_back(319);pedLimits.push_back(400); pedLimits.push_back(101);}
        if (runNum == 337371) {pedLimits.push_back(0);pedLimits.push_back(110);pedLimits.push_back(538);pedLimits.push_back(560); pedLimits.push_back(112);}
        if (runNum == 337335) {pedLimits.push_back(0);pedLimits.push_back(75);pedLimits.push_back(430);pedLimits.push_back(440); pedLimits.push_back(84);}
        if (runNum == 337005) {pedLimits.push_back(0);pedLimits.push_back(75);pedLimits.push_back(270);pedLimits.push_back(300); pedLimits.push_back(77);}
        if (runNum == 336998) {pedLimits.push_back(0);pedLimits.push_back(60);pedLimits.push_back(309);pedLimits.push_back(330); pedLimits.push_back(70);}
        if (runNum == 338259) {pedLimits.push_back(0);pedLimits.push_back(80);pedLimits.push_back(567);pedLimits.push_back(585); pedLimits.push_back(87);}
        if (runNum == 337491) {pedLimits.push_back(0);pedLimits.push_back(45);pedLimits.push_back(495);pedLimits.push_back(520); pedLimits.push_back(56);}
        if (runNum == 338675) {pedLimits.push_back(0);pedLimits.push_back(14);pedLimits.push_back(95);pedLimits.push_back(120); pedLimits.push_back(19);}
        if (runNum == 326870) {pedLimits.push_back(0);pedLimits.push_back(30);pedLimits.push_back(252);pedLimits.push_back(280); pedLimits.push_back(36);}
        if (runNum == 326446) {pedLimits.push_back(0);pedLimits.push_back(130);pedLimits.push_back(380);pedLimits.push_back(400); pedLimits.push_back(142);}
        if (runNum == 325790) {pedLimits.push_back(100);pedLimits.push_back(140);pedLimits.push_back(511);pedLimits.push_back(520); pedLimits.push_back(148);}
        if (runNum == 324910) {pedLimits.push_back(0);pedLimits.push_back(40);pedLimits.push_back(717);pedLimits.push_back(730); pedLimits.push_back(46);}

        if (runNum == 335056) {pedLimits.push_back(50);pedLimits.push_back(130);pedLimits.push_back(167);pedLimits.push_back(175); pedLimits.push_back(139);}
        if (runNum == 334580) {pedLimits.push_back(50);pedLimits.push_back(280);pedLimits.push_back(355);pedLimits.push_back(370); pedLimits.push_back(303);}
        if (runNum == 339562) {pedLimits.push_back(50);pedLimits.push_back(100);pedLimits.push_back(323);pedLimits.push_back(340); pedLimits.push_back(114);}
        if (runNum == 339535) {pedLimits.push_back(50);pedLimits.push_back(100);pedLimits.push_back(433);pedLimits.push_back(450); pedLimits.push_back(119);}
        if (runNum == 334842) {pedLimits.push_back(50);pedLimits.push_back(220);pedLimits.push_back(607);pedLimits.push_back(620); pedLimits.push_back(262);}
        if (runNum == 339500) {pedLimits.push_back(0);pedLimits.push_back(25);pedLimits.push_back(324);pedLimits.push_back(340); pedLimits.push_back(29);}

        if (runNum == 325789) {pedLimits.push_back(0);pedLimits.push_back(100);pedLimits.push_back(138);pedLimits.push_back(180); pedLimits.push_back(112);}
        if (runNum == 329778) {pedLimits.push_back(0);pedLimits.push_back(35);pedLimits.push_back(67);pedLimits.push_back(120); pedLimits.push_back(44);}
        if (runNum == 332720) {pedLimits.push_back(0);pedLimits.push_back(180);pedLimits.push_back(230);pedLimits.push_back(240); pedLimits.push_back(194);}
        if (runNum == 333853) {pedLimits.push_back(165);pedLimits.push_back(290);pedLimits.push_back(461);pedLimits.push_back(490); pedLimits.push_back(304);}
        if (runNum == 335082) {pedLimits.push_back(0);pedLimits.push_back(25);pedLimits.push_back(60);pedLimits.push_back(80); pedLimits.push_back(31);}
        if (runNum == 339590) {pedLimits.push_back(0);pedLimits.push_back(70);pedLimits.push_back(153);pedLimits.push_back(160); pedLimits.push_back(81);}
        if (runNum == 338967) {pedLimits.push_back(0);pedLimits.push_back(60);pedLimits.push_back(273);pedLimits.push_back(285); pedLimits.push_back(76);}

        if (runNum == 331019) {pedLimits.push_back(50);pedLimits.push_back(110);pedLimits.push_back(134);pedLimits.push_back(150); pedLimits.push_back(113);}
        if (runNum == 327761) {pedLimits.push_back(50);pedLimits.push_back(130);pedLimits.push_back(162);pedLimits.push_back(180); pedLimits.push_back(141);}
        if (runNum == 326468) {pedLimits.push_back(0);pedLimits.push_back(130);pedLimits.push_back(184);pedLimits.push_back(200); pedLimits.push_back(139);}
        if (runNum == 334455) {pedLimits.push_back(50);pedLimits.push_back(320);pedLimits.push_back(365);pedLimits.push_back(375); pedLimits.push_back(337);}
        if (runNum == 339346) {pedLimits.push_back(50);pedLimits.push_back(230);pedLimits.push_back(327);pedLimits.push_back(340); pedLimits.push_back(242);}

        if (runNum == 339758) {pedLimits.push_back(0);pedLimits.push_back(120);pedLimits.push_back(220);pedLimits.push_back(390);}
        if (runNum == 336497) {pedLimits.push_back(0);pedLimits.push_back(160);pedLimits.push_back(417);pedLimits.push_back(430);}
        if (runNum == 333994) {pedLimits.push_back(0);pedLimits.push_back(190);pedLimits.push_back(247);pedLimits.push_back(260);}
        if (runNum == 330874) {pedLimits.push_back(0);pedLimits.push_back(70);pedLimits.push_back(110);pedLimits.push_back(180);}

        if (runNum == 351364) {pedLimits.push_back(0);pedLimits.push_back(60);pedLimits.push_back(896);pedLimits.push_back(905);}
        if (runNum == 349637) {pedLimits.push_back(0);pedLimits.push_back(70);pedLimits.push_back(957);pedLimits.push_back(980);}
        if (runNum == 356124) {pedLimits.push_back(0);pedLimits.push_back(200);pedLimits.push_back(1084);pedLimits.push_back(1100);}
        if (runNum == 357962) {pedLimits.push_back(0);pedLimits.push_back(45);pedLimits.push_back(715);pedLimits.push_back(720);}
        if (runNum == 359593) {pedLimits.push_back(0);pedLimits.push_back(50);pedLimits.push_back(820);pedLimits.push_back(830);}
        if (runNum == 362619) {pedLimits.push_back(0);pedLimits.push_back(45);pedLimits.push_back(478);pedLimits.push_back(490);}
        if (runNum == 364076) {pedLimits.push_back(0);pedLimits.push_back(210);pedLimits.push_back(878);pedLimits.push_back(885);}

        if (runNum == 341123) {pedLimits.push_back(783);pedLimits.push_back(800);}

        if (runNum == 357283) {pedLimits.push_back(316);pedLimits.push_back(330);}

        if (runNum == 330328) {pedLimits.push_back(0); pedLimits.push_back(90);}
        if (runNum == 329964) {pedLimits.push_back(0); pedLimits.push_back(80);}

        if (runNum == 339849) {pedLimits.push_back(0);pedLimits.push_back(100);}
        if (runNum == 336852) {pedLimits.push_back(0);pedLimits.push_back(90);}
        if (runNum == 340453) {pedLimits.push_back(0);pedLimits.push_back(50);}
        if (runNum == 340072) {pedLimits.push_back(0);pedLimits.push_back(300);}
        if (runNum == 341184) {pedLimits.push_back(0);pedLimits.push_back(30);}

        if (runNum == 348511) {pedLimits.push_back(0);pedLimits.push_back(60);}

        // if (runNum == 358175) {pedLimits.push_back(0);pedLimits.push_back(50);}
        if (runNum == 359286) {pedLimits.push_back(0);pedLimits.push_back(120);}
        if (runNum == 358516) {pedLimits.push_back(0);pedLimits.push_back(160);}
        if (runNum == 355544) {pedLimits.push_back(10);pedLimits.push_back(80);}
        if (runNum == 348534) {pedLimits.push_back(350);pedLimits.push_back(550);}
        if (runNum == 350880) {pedLimits.push_back(0);pedLimits.push_back(50);}
        if (runNum == 351223) {pedLimits.push_back(0);pedLimits.push_back(230);}
        if (runNum == 350220) {pedLimits.push_back(0);pedLimits.push_back(70);}
        if (runNum == 349842) {pedLimits.push_back(350);pedLimits.push_back(400);}
        if (runNum == 362204) {pedLimits.push_back(0);pedLimits.push_back(100);}
        if (runNum == 363096) {pedLimits.push_back(10);pedLimits.push_back(80);}
        if (runNum == 363910) {pedLimits.push_back(0);pedLimits.push_back(40);}
        if (runNum == 355468) {pedLimits.push_back(0);pedLimits.push_back(420);}
        if (runNum == 355416) {pedLimits.push_back(0);pedLimits.push_back(70);}
        if (runNum == 355389) {pedLimits.push_back(0);pedLimits.push_back(130);}
        if (runNum == 355331) {pedLimits.push_back(0);pedLimits.push_back(40);}
        if (runNum == 354311) {pedLimits.push_back(20);pedLimits.push_back(60);}

        if (runNum == 355261) {pedLimits.push_back(0);pedLimits.push_back(20);pedLimits.push_back(960);pedLimits.push_back(1000);}

        if (runNum == 363830) {pedLimits.push_back(0); pedLimits.push_back(40);};
        if (runNum == 359124) {pedLimits.push_back(0); pedLimits.push_back(140);};
        if (runNum == 361689) {pedLimits.push_back(0); pedLimits.push_back(80);};
        if (runNum == 364292) {pedLimits.push_back(0); pedLimits.push_back(90);};
        if (runNum == 362776) {pedLimits.push_back(0); pedLimits.push_back(160);};
        if (runNum == 364098) {pedLimits.push_back(0); pedLimits.push_back(120);};
        if (runNum == 364485) {pedLimits.push_back(0); pedLimits.push_back(690);};
        
        if (runNum == 355224) {pedLimits.push_back(0); pedLimits.push_back(90);};
        if (runNum == 355181) {pedLimits.push_back(0); pedLimits.push_back(90);};
        if (runNum == 354396) {pedLimits.push_back(460); pedLimits.push_back(500);};
        if (runNum == 354176) {pedLimits.push_back(160); pedLimits.push_back(200);};

        if (runNum == 355754) {pedLimits.push_back(0);pedLimits.push_back(60);};

        if (runNum == 348618) {pedLimits.push_back(0);pedLimits.push_back(60);};
        if (runNum == 348836) {pedLimits.push_back(0);pedLimits.push_back(70);};
        if (runNum == 348885) {pedLimits.push_back(0);pedLimits.push_back(180);};
        if (runNum == 349526) {pedLimits.push_back(0);pedLimits.push_back(60);};
        if (runNum == 351671) {pedLimits.push_back(0);pedLimits.push_back(60);};
        if (runNum == 356259) {pedLimits.push_back(0);pedLimits.push_back(40);};
        if (runNum == 357500) {pedLimits.push_back(0);pedLimits.push_back(90);};
        if (runNum == 359735) {pedLimits.push_back(0);pedLimits.push_back(70);};
        if (runNum == 362661) {pedLimits.push_back(0);pedLimits.push_back(120);pedLimits.push_back(765);pedLimits.push_back(800);};
        if (runNum == 364214) {pedLimits.push_back(0);pedLimits.push_back(40);};
        if (runNum == 364292) {pedLimits.push_back(0);pedLimits.push_back(90);};


        if (runNum == 354494) {pedLimits.push_back(0);pedLimits.push_back(120);pedLimits.push_back(2255);pedLimits.push_back(2295);}
        if (runNum == 354476) {pedLimits.push_back(0);pedLimits.push_back(190);pedLimits.push_back(930);pedLimits.push_back(969);}
        if (runNum == 355273) {pedLimits.push_back(0);pedLimits.push_back(80);pedLimits.push_back(530);pedLimits.push_back(570);}
        if (runNum == 354359) {pedLimits.push_back(0);pedLimits.push_back(240);} //pedLimits.push_back(250);}
        // if (runNum == 354315) {pedLimits.push_back(0);pedLimits.push_back(100);}
        if (runNum == 354309) {pedLimits.push_back(0);pedLimits.push_back(100);}
        if (runNum == 339197) {pedLimits.push_back(0);pedLimits.push_back(200);}
        if (runNum == 339205) {pedLimits.push_back(0);pedLimits.push_back(90);}
        if (runNum == 336506) {pedLimits.push_back(0);pedLimits.push_back(100);}
        if (runNum == 330875) {pedLimits.push_back(0);pedLimits.push_back(50);pedLimits.push_back(1055);pedLimits.push_back(1100);}
        if (runNum == 331085) {pedLimits.push_back(0);pedLimits.push_back(150);}
        if (runNum == 335302) {pedLimits.push_back(-1);pedLimits.push_back(-1);}
        // if (runNum == 354124) {pedLimits.push_back(150);pedLimits.push_back(500);}
        if (runNum == 354124) {pedLimits.push_back(0);pedLimits.push_back(500);}
        if (runNum == 354174) {pedLimits.push_back(0);pedLimits.push_back(90);}
        if (runNum == 329542) {pedLimits.push_back(0);pedLimits.push_back(50);}
        if (runNum == 331020) {pedLimits.push_back(1);pedLimits.push_back(340);pedLimits.push_back(578);pedLimits.push_back(620);}
        // if (runNum == 331020) {pedLimits.push_back(1);pedLimits.push_back(300);}
        if (runNum == 341294) {pedLimits.push_back(250);pedLimits.push_back(270);}
        // if (runNum == 341294) {pedLimits.push_back(0);pedLimits.push_back(70);pedLimits.push_back(250);pedLimits.push_back(270);}
        if (runNum == 341312) {pedLimits.push_back(760);pedLimits.push_back(780);}
        // if (runNum == 341312) {pedLimits.push_back(0);pedLimits.push_back(60);pedLimits.push_back(760);pedLimits.push_back(780);}
        if (runNum == 341419) {pedLimits.push_back(1142);pedLimits.push_back(1161);}
        // if (runNum == 341419) {pedLimits.push_back(0);pedLimits.push_back(80);pedLimits.push_back(1142);pedLimits.push_back(1161);}
        if (runNum == 341534) {pedLimits.push_back(1816);pedLimits.push_back(1836);}
        // if (runNum == 341534) {pedLimits.push_back(0);pedLimits.push_back(40);pedLimits.push_back(1816);pedLimits.push_back(1836);}
        if (runNum == 341615) {pedLimits.push_back(1165);pedLimits.push_back(1185);}
        // if (runNum == 341615) {pedLimits.push_back(1165);pedLimits.push_back(1185);pedLimits.push_back(1165);pedLimits.push_back(1185);}
        // if (runNum == 341649) {pedLimits.push_back(627);pedLimits.push_back(629);}
        if (runNum == 341649) {pedLimits.push_back(0);pedLimits.push_back(75);pedLimits.push_back(627);pedLimits.push_back(629);}
        if (runNum == 286364) {pedLimits.push_back(0);pedLimits.push_back(100);}
        if (runNum == 286474) {pedLimits.push_back(0);pedLimits.push_back(100);}
        if (runNum == 336471) {pedLimits.push_back(0);pedLimits.push_back(190);}
        if (runNum == 338022) {pedLimits.push_back(0);pedLimits.push_back(190);}
        if (runNum == 325020) {pedLimits.push_back(0);pedLimits.push_back(20);}
        // if (runNum == 326446) {pedLimits.push_back(0);pedLimits.push_back(130);}
        if (runNum == 326870) {pedLimits.push_back(0);pedLimits.push_back(30);}
        if (runNum == 326945) {pedLimits.push_back(0);pedLimits.push_back(85);}
        if (runNum == 327860) {pedLimits.push_back(0);pedLimits.push_back(50);}
        if (runNum == 328099) {pedLimits.push_back(0);pedLimits.push_back(80);}
        if (runNum == 330294) {pedLimits.push_back(0);pedLimits.push_back(30);}
        if (runNum == 333192) {pedLimits.push_back(0);pedLimits.push_back(140);}
        if (runNum == 334993) {pedLimits.push_back(0);pedLimits.push_back(40);}
        if (runNum == 336505) {pedLimits.push_back(0);pedLimits.push_back(90);}
        if (runNum == 336506) {pedLimits.push_back(0);pedLimits.push_back(110);}
        if (runNum == 337005) {pedLimits.push_back(0);pedLimits.push_back(70);}
        if (runNum == 337107) {pedLimits.push_back(0);pedLimits.push_back(80);}
        if (runNum == 337451) {pedLimits.push_back(0);pedLimits.push_back(190);}
        if (runNum == 324910) {pedLimits.push_back(0);pedLimits.push_back(40);}
        if (runNum == 327057) {pedLimits.push_back(0);pedLimits.push_back(40);}
        if (runNum == 340634) {pedLimits.push_back(0);pedLimits.push_back(12);pedLimits.push_back(630);pedLimits.push_back(640);}
        if (runNum == 340644) {pedLimits.push_back(0);pedLimits.push_back(20);}
        if (runNum == 340683) {pedLimits.push_back(0);pedLimits.push_back(23);pedLimits.push_back(93);pedLimits.push_back(100);}
        if (runNum == 340697) {pedLimits.push_back(0);pedLimits.push_back(30);}
        if (runNum == 340718) {pedLimits.push_back(0);pedLimits.push_back(40);}
        if (runNum == 340814) {pedLimits.push_back(0);pedLimits.push_back(20);}
        if (runNum == 340850) {pedLimits.push_back(0);pedLimits.push_back(60);}
        if (runNum == 340910) {pedLimits.push_back(0);pedLimits.push_back(30);}
        if (runNum == 340925) {pedLimits.push_back(200);pedLimits.push_back(208);}
        if (runNum == 340973) {pedLimits.push_back(0);pedLimits.push_back(180);}
        if (runNum == 341027) {pedLimits.push_back(0);pedLimits.push_back(15);}
        if (runNum == 341123) {pedLimits.push_back(0);pedLimits.push_back(60);}
        if (runNum == 341184) {pedLimits.push_back(0);pedLimits.push_back(40);}
        if (runNum == 339205) {pedLimits.push_back(0);pedLimits.push_back(80);}
        if (runNum == 361635) {pedLimits.push_back(0);pedLimits.push_back(70);}
        if (runNum == 348495) {pedLimits.push_back(0);pedLimits.push_back(50);}
        if (runNum == 358115) {pedLimits.push_back(0);pedLimits.push_back(50);}
        if (runNum == 357713) {pedLimits.push_back(0);pedLimits.push_back(60);}
        // if (runNum == 352494) {pedLimits.push_back(0);pedLimits.push_back(170);}
        if (runNum == 355599) {pedLimits.push_back(0);pedLimits.push_back(35);}
        // if (runNum == 351698) {pedLimits.push_back(0);pedLimits.push_back(50);}
        if (runNum == 351628) {pedLimits.push_back(0);pedLimits.push_back(50);}
        if (runNum == 350923) {pedLimits.push_back(0);pedLimits.push_back(40);}
        // if (runNum == 355563) {pedLimits.push_back(0);pedLimits.push_back(100);}
        if (runNum == 357293) {pedLimits.push_back(0);pedLimits.push_back(100);}

        if (runNum == 348251) {pedLimits.push_back(0);pedLimits.push_back(80);};
        if (runNum == 348354) {pedLimits.push_back(0);pedLimits.push_back(200);};

        if (pedLimits.size() == 0) {
            if (auto_LBEnd == -1) getPedLimitsAutomatically(verbose);
            if (auto_LBEnd != -1) {pedLimits.push_back(auto_LBStart); pedLimits.push_back(auto_LBEnd);}
        }

        return pedLimits;
}

void Run::getPedLimitsAutomatically(int verbose = 1) {

    if (verbose > 0) cout << "====================== FINDING PED LIMITS ==============================" << endl; 

    vector<float> LB_LCD, LB_LCD_duration, Lumi_LCD;
    vector<vector<float>*> toFill = {&LB_LCD, &LB_LCD_duration, &Lumi_LCD};
    int file_index = (runNum >= 348618) ? 4 : 3; 
    readIntegratedLumiFile(file_index, runNum, 2, toFill);
    if (LB_LCD.size() > 0) {
        auto_LBEnd = LB_LCD.at(0) - 10;
        if (verbose > 0) cout << "Using the range 0 - " << auto_LBEnd << endl;
        return;
    }

    if (verbose > 0) cout << "Wasn't able to get PED LIMITS by reding the LCD Luminosity" << endl;

    if (verbose > 0) cout << "Trying to find a jump in ADCs" << endl;
    IntegratorData * data = pFiles[PART::EBA]->intTree;

    vector<double> ADCs_E3_mod0_EBA = getVectorFromTree(data->fChain, "rawADCCurrent[0][0]");
    if (ADCs_E3_mod0_EBA.size() > 0) {
        auto params_ADCs_E3_mod0_EBA = GetMinMax(ADCs_E3_mod0_EBA);
        double range_ADCs = params_ADCs_E3_mod0_EBA.second - params_ADCs_E3_mod0_EBA.first;

        // data->GetEntry(0);
        double prevADCs = -1;
        for (int i_ped = 0 ; i_ped < nEntries ; i_ped++) {
            Long64_t ientry = data->LoadTree(i_ped);
            if (ientry < 0) break;
            data->fChain->GetEntry(i_ped); 
            int LB = data->LB;

            if (i_ped == 0) {
                prevADCs = data->rawADCCurrent[0][0];
            } else {
                double currADCs = data->rawADCCurrent[0][0];
                double step = currADCs - prevADCs;
                if (step / range_ADCs > 0.55) {
                    auto_LBEnd = int(0.9*(LB-1));
                    if (verbose > 0) cout << "Using the range 0 - " << auto_LBEnd << endl;
                    return;
                }
                // cout << LB << " " << step << " ----- " << range_ADCs << " --> " << step / range_ADCs << endl;
                prevADCs = currADCs;
            }
        }
    }

    // vector<double> v_ADCs;
    // for (int i_ped = 0 ; i_ped < nEntries ; i_ped++) {
    //     Long64_t ientry = data->LoadTree(i_ped);
    //     if (ientry < 0) break;
    //     data->fChain->GetEntry(i_ped); 

    //     int LB = data->LB;
    //     double ADCs = data->rawADCCurrent[0][0];

    //     if (QualityCriteria::isBadCurrentMeasurement(ADCs)) {throw std::invalid_argument( "Trying to find the ped limits. The channel 0 0 seems to be bad" );}

    //     if (LB > 20) {
    //         auto params = GetMeanErr(v_ADCs);
    //         if (fabs(ADCs - params.first) > 1.96 * params.second) {
    //             auto_LBEnd = int(0.9*(LB-1));
    //             if (verbose > 0) cout << "Using the range 0 - " << auto_LBEnd << endl;
    //             break;
    //         }
    //     }

    //     v_ADCs.push_back(ADCs);

    // }


    if (verbose > 0) cout << "Will use the CALIB_PED" << endl;
    overridePedestal = "CALIB_PED";
    pedestalType = Global::getOverridenPedestalType(overridePedestal);

}

int Run::GetNumberOfBunches() {
    if (runNum == 354494) return 124;

    coutError("Run::GetNumberOfBunches " + to_string(runNum) + " -- not found");
    return 0;
}

vector<double> Run::getRatioLimits() {
    vector<double> limits;

    if (runNum == 276689) {limits = {250,450};}
    if (runNum == 299584) {limits = {600,2200};}
    if (runNum == 350144) {limits = {200,800};}
    if (runNum == 364030) {limits = {200,900};}
    if (runNum == 358096) {limits = {300,650};}
    if (runNum == 349451) {limits = {300,800};}
    if (runNum == 351671) {limits = {200,700};}
    if (runNum == 364098) {limits = {300,700};}
    if (runNum == 350803) {limits = {200,700};}
    if (runNum == 350184) {limits = {400,1000};}
    if (runNum == 358656) {limits = {300,640};}
    if (runNum == 350121) {limits = {200,800};}
    if (runNum == 359010) {limits = {400,1000};}
    if (runNum == 352056) {limits = {200,700};}
    if (runNum == 363738) {limits = {200,700};}
    if (runNum == 360402) {limits = {200,500};}
    if (runNum == 348495) {limits = {150,400};}
    if (runNum == 348511) {limits = {300,550};}
    if (runNum == 355651) {limits = {200,430};}
    if (runNum == 357539) {limits = {400,950};}
    if (runNum == 359717) {limits = {100,450};}
    if (runNum == 355754) {limits = {400,450};}
    if (runNum == 359286) {limits = {250,600};}
    if (runNum == 358175) {limits = {300,400};}
    if (runNum == 359124) {limits = {250,900};}
    if (runNum == 358115) {limits = {100,800};}
    if (runNum == 360293) {limits = {100,750};}
    if (runNum == 354359) {limits = {250,350};}
    if (runNum == 354315) {limits = {120,170};}
    if (runNum == 351359) {limits = {140,180};}
    if (runNum == 352274) {limits = {150,180};}
    if (runNum == 359677) {limits = {80,100};}
    if (runNum == 357679) {limits = {200,1000};}
    if (runNum == 358325) {limits = {65,85};}
    if (runNum == 359586) {limits = {50,70};}
    if (runNum == 357620) {limits = {125,230};}
    if (runNum == 364076) {limits = {300,800};}
    if (runNum == 352137) {limits = {150,600};}
    if (runNum == 358233) {limits = {700,900};}
    if (runNum == 364214) {limits = {100,390};}
    if (runNum == 349592) {limits = {120,170};}
    if (runNum == 359286) {limits = {250,600};}
    if (runNum == 354309) {limits = {230,300};}
    if (runNum == 358516) {limits = {300,1000};}
    if (runNum == 355544) {limits = {200,600};}
    if (runNum == 358031) {limits = {200,800};}
    if (runNum == 363664) {limits = {650,820};}
    if (runNum == 355053) {limits = {220,320};}
    if (runNum == 357293) {limits = {120,200};}
    if (runNum == 358300) {limits = {200,400};}
    if (runNum == 358395) {limits = {200,1200};}
    if (runNum == 355563) {limits = {120,400};}
    if (runNum == 350923) {limits = {100,900};}
    if (runNum == 359735) {limits = {160,240};}
    if (runNum == 354359) {limits = {300,350};}
    if (runNum == 351628) {limits = {80,250};}
    if (runNum == 361695) {limits = {130,160};}
    if (runNum == 351698) {limits = {70,320};}
    if (runNum == 355599) {limits = {60,160};}
    if (runNum == 357713) {limits = {200,600};}
    if (runNum == 361689) {limits = {170,200};}
    if (runNum == 354944) {limits = {200,1000};}
    if (runNum == 348534) {limits = {600,650};}
    if (runNum == 361635) {limits = {300,450};}
    if (runNum == 363947) {limits = {165,195};}
    if (runNum == 363910) {limits = {100,600};}
    if (runNum == 361844) {limits = {270,320};}
    if (runNum == 349842) {limits = {0,250};}
    if (runNum == 350220) {limits = {110,180};}
    if (runNum == 354494) {limits = {1600,2100};}

    if (limits.size() == 0) {
        pFiles[PART::LBA]->intTree->fChain->GetEntry(0);
        double limitStart = pFiles[PART::LBA]->intTree->LB;
        pFiles[PART::LBA]->intTree->fChain->GetEntry(pFiles[PART::LBA]->intTree->fChain->GetEntries() - 1);
        double limitEnd = pFiles[PART::LBA]->intTree->LB;
        limits = {limitStart, limitEnd};
        // double startLimit = pedEnd + (LBEnd - pedEnd)/2.;
        // double endLimit = LBEnd;
        // limits = {0, endLimit};
    }
    return limits;
}

vector<int> Run::getLBBoundaries() {
    if (runNum == 340072) return {354,16}; // Start collisions at 354, and do step after 16 LB (16 LBs is the amount to have lumi reduced 10%) <-- 1600 LBs in collisions divided by (1000 lumi range * 0.1)
    // if (runNum == 329964) return {87,119,177,227,359};
    if (runNum == 329964) return {87,3};
    if (runNum == 330328) return {99,172,227,250,300};
    // if (runNum == 341294) return {79,149};
    if (runNum == 341294) return {79,3};
    if (runNum == 331020) return {351,432};//,463}; //432};
    if (runNum == 331085) return {189,10};
    // if (runNum == 331085) return {189,328,459,591,710,925};
    if (runNum == 334890) return {80};
    if (runNum == 334878) return {71};
    if (runNum == 334849) return {92};
    if (runNum == 335282) return {194};
    if (runNum == 336497) return {194};
    if (runNum == 336944) return {196};
    if (runNum == 337404) return {101};
    if (runNum == 337371) return {112};
    if (runNum == 337335) return {84};
    if (runNum == 337005) return {77};
    if (runNum == 336998) return {70};
    if (runNum == 338259) return {87};
    if (runNum == 337491) return {56};
    if (runNum == 338675) return {19};
    if (runNum == 326870) return {36};
    if (runNum == 326446) return {142};
    if (runNum == 325790) return {148};
    if (runNum == 324910) return {46};
    if (runNum == 335056) return {139};
    if (runNum == 334580) return {303};
    if (runNum == 339562) return {114};
    if (runNum == 339535) return {119};
    if (runNum == 334842) return {262};
    if (runNum == 339500) return {29};
    if (runNum == 325789) return {112};
    if (runNum == 329778) return {44};
    if (runNum == 332720) return {194};
    if (runNum == 333853) return {304};
    if (runNum == 335082) return {31};
    if (runNum == 339590) return {81};
    if (runNum == 338967) return {76};
    if (runNum == 331019) return {113};
    if (runNum == 327761) return {141};
    if (runNum == 326468) return {139};
    if (runNum == 334455) return {337};
    if (runNum == 339346) return {242};


    coutError("The LBBoundaries are not defined for this run!");

    return {getPedLimits().at(1)};
}

void Run::SetUseGroupGain(int use) {
    useGroupGain = use;
}

int Run::GetLBUpperLimit() {
    // if (runNum == 303421) return 1100;
    // if (runNum == 311481) return 400;
    // if (runNum == 311365) return 220;
    // if (runNum == 298773) return 220;
    // if (runNum == 299584) return 490;
    // if (runNum == 300279) return 360;
    // if (runNum == 300687) return 400;
    // if (runNum == 300800) return 250;
    // if (runNum == 301932) return 990;
    // if (runNum == 301973) return 1500;

    // if (runNum == 302391) return 300; 
    // if (runNum == 302393) return 500;
    // if (runNum == 302872) return 990;
    // if (runNum == 304128) return 1050;
    // if (runNum == 304178) return 800;
    // if (runNum == 304431) return 560;
    // if (runNum == 307656) return 300;
    // if (runNum == 310691) return 340;
    return -1;
}


Run::~Run()
{
    // delete[] pFiles;
//    delete *laser;
//    delete laser_anchor;
//    delete conv_const;
//    delete fit0;
//    delete fit1;
//    delete fit2;
//    delete fit3;
//    delete refPoint;
//    delete fit5;
//    delete extraPedInfo;
}