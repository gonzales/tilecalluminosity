
class RunLB : public Run {
    public:
        RunLB(int runNum, string runSuffix, string convConstFilename, string laserFileName, string anchorLaserFileName, int cell_type = -1, bool doSACorrection = 0, TString overridePedestal = "", bool useTime = false, bool noLBavg = false) : Run(runNum, runSuffix, convConstFilename, laserFileName, anchorLaserFileName, cell_type, doSACorrection, overridePedestal, useTime, noLBavg) {
            usePLB = false;
        }
        // string GetXToPrint(int LB, long long time);
        void ComputeCurrents(int part, vector<int> cells_to_compute);
};


// string RunLB::GetXToPrint(int LB, long long time) {
//     return to_string(LB);
// }
