
class RunPLB : public Run {
    public:
        string GetXToPrint(int LB, long long time);
        void ComputeCurrents(int part, vector<int> cells_to_compute);
        int IsStable(long long utime);
        pair<double,double> GetTimeBounds(int pLB_nPLB);
        pair<double,double> GetTimeBoundsFromTime(long long utime);

        double tfinal=0, LBstart=0, LBend=99999;
        vector<int> v_index, v_run, v_lb, v_plb;
        vector<double> v_ti, v_tf, v_delt;
        int minLBs[4];

        RunPLB(int runNum, string runSuffix, string convConstFilename, string laserFileName, string anchorLaserFileName, int cell_type, TString overridePedestal = "", bool useTime = false, bool noLBavg = false) : Run(runNum, runSuffix, convConstFilename, laserFileName, anchorLaserFileName, cell_type, 0, overridePedestal, useTime, noLBavg) {
            usePLB = true;
            int index=0, run=0, LB=0;
            double ti=0, tf=0;
            int lbshift=0;
            int nSetPLB=0;
            string inname;
            if(runNum==339197){inname="share/R339197_LBlist_periodsAll_cleaned.txt"; tinit=1509035506.95871; tfinal=1509073166.06669; LBstart=-1; LBend=99999;}
            if(runNum==339205){inname="share/LB_run339205.txt"; tinit=1509073227.04526; tfinal=1509099430.40082; LBstart=-1; LBend=99999;}
            else if(runNum==336506){inname="share/R336506_LBlist_periodsABA_cleaned.txt"; tinit=1506214709.07567; tfinal=1506248146.94204; LBstart=122; LBend=488;}
            else if (runNum==330875){inname="share/Jul17vdM_LBboundaries_all.txt"; tinit=1501214334.171953917; tfinal=1501276231.52285; LBstart=67; LBend=99999;}
            else if (runNum==299390){inname="share/vdM299390_PLBboundaries_all.txt"; tinit=1463573982.98534; tfinal=1463616898.67676; LBstart=-1; LBend=99999;}
            else if (runNum==300287){inname="share/vdM300287_PLBboundaries_all.txt"; tinit=1464328118.12950; tfinal=1464358539.45385; LBstart=-1; LBend=99999;}
            string line;
            fstream infile(inname);
            if (infile) {
                while (getline(infile, line)) {
                    sscanf (line.c_str(),"%i\t%lf\t%lf\t%i\t%i\n", &index, &ti, &tf, &run, &LB);
                    bool switchLB = v_lb.size() > 1 ? (LB < v_lb.at(v_lb.size()-1&&run==0)) : false;
                    if (runNum == 330875 && switchLB) { lbshift = 500 * nSetPLB; nSetPLB++;}

                    v_index.push_back(index);
                    if (run == runNum) {v_lb.push_back(LB); v_plb.push_back(-1);}
                    else if (run == 0) {v_lb.push_back(-1); v_plb.push_back(LB+lbshift);}
                    else {v_lb.push_back(-1); v_plb.push_back(-1);}
                    v_run.push_back(run);
                    v_ti.push_back(ti);
                    v_tf.push_back(tf);
                }
            } else {
                cout << "WARNING: File " << inname << " wasn't found " << endl;
            }

            minLBs[0] = 99999999; minLBs[1] = 99999999; minLBs[2] = 99999999; minLBs[3] = 99999999;


        }
};
        

string RunPLB::GetXToPrint(int LB, long long time) {
    return to_string(LB);
}

int RunPLB::IsStable(long long utime) {
    for (int i_index = 0 ; i_index < v_ti.size() ; i_index++) {
        if (v_ti.at(i_index) <= utime/1000000. && utime/1000000. <= v_tf.at(i_index)) return i_index;
    }
    return -1;
}

pair<double,double> RunPLB::GetTimeBounds(int pLB_nPLB) {
    for (int i_index = 0 ; i_index < v_ti.size() ; i_index++) {
        if (v_lb.at(i_index) == pLB_nPLB && v_run.at(i_index) != 0) return make_pair(v_ti.at(i_index), v_tf.at(i_index));
        if (v_plb.at(i_index) == fabs(pLB_nPLB) && v_run.at(i_index) == 0) return make_pair(v_ti.at(i_index), v_tf.at(i_index));
    } 
    return make_pair(-1,-1);
}


pair<double,double> RunPLB::GetTimeBoundsFromTime(long long utime) {
    int index = IsStable(utime);
    if (index == -1) return make_pair(-1,-1);
    return make_pair(v_ti.at(index), v_tf.at(index));
}