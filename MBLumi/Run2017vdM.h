
class Run2017vdM : public RunLB {
    public:

        void ComputeCurrents(int part, vector<int> cells_to_compute);
        string GetXToPrint(int LB, long long time);
        double GetRow(long long time);

        vector<double> rows;
        vector<long long> startTimes;
        vector<long long> endTimes;

        int hist_plotted = 0;

        Run2017vdM(int runNum, string runSuffix, string convConstFilename, string laserFileName, string anchorLaserFileName, int cell_type) : RunLB(runNum, runSuffix, convConstFilename, laserFileName, anchorLaserFileName, cell_type) {
            TString LBboundaries_file;
            if (runNum == 330875)      LBboundaries_file = "share/Jul17vdM_LBboundaries_all_annotated.txt";
            else if (runNum == 340634) LBboundaries_file = "share/Nov17vdM_LBboundaries_all_annotated.txt";
            readPLBFile(LBboundaries_file, 6, &rows, &startTimes, &endTimes);
        }


};

string Run2017vdM::GetXToPrint(int LB, long long time) {
    return to_string(LB);
    // return to_string(time);
}

double Run2017vdM::GetRow(long long time) {
    for (int i = 0 ; i < rows.size() ; i++) {
        if (startTimes.at(i) <= time && endTimes.at(i) > time) return rows.at(i);
    }
    return 0;
}



