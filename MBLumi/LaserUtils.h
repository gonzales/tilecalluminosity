
namespace LaserUtils {
    void getLaserConstants(string laserFileName, double laser[4][64][48], int verbose);
    void saveLaserConstants(string laserFileName, double laser[4][64][48]);
    map<TString,vector<int>> getRemoveMods(int year);
    void FillLaserGraphs(int doLaser, vector<int> cells_to_compute, vector<int> parts_to_compute, int anchor_run, vector<TString> * labels, map<TString, array<TGraph*,64>> * map_cell_graphVsRunNumber, map<TString, array<TGraph*,64>> * map_cell_graphVsDate, map<TString, array<TGraph*,64>> * map_cell_graphVsIntLumi, map<TString, array<TGraph*,64>> * map_cell_graphVsLabelRunNumber, int fromRun = -1, int toRun = -1, vector<int> useRuns = {}, vector<int> ignoreRuns = {});
}