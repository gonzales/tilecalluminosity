

namespace QualityCriteria {
    
    bool isBadCurrentMeasurement(Float_t rawADCCurrent);
    bool isSaturated(Float_t rawADCCurrent);
    bool isValidForCurrentAverage(pair<double, double> params_curr, double gain);
    bool isValidForCurrentAverage(vector<float> tempRawADCs, double gain);
    bool isGoodCurr(float current, double convConst, int part, int mod, int pmt);
    bool isNotHardCodedAsBad(int runNum, int part, int mod, int pmt, TString suffix, int LB);
    bool doPlot(int part, int pmt, int cells_type);

}

