#ifndef Global_h
#define Global_h

namespace Global {

        double t_min_firPed_339197 = 0;
        double t_max_firPed_339197 = 13250;
        double t_min_secPed_339197 = 21000;
        double t_max_secPed_339197 = 22250;

        double t_max_firPed_354124 = 29000;
        double t_min_secPed_354124 = 38325;
        double t_max_secPed_354124 = 40000;

        set<float> E3_339197={2.1, 3.1, 7.1, 10.1, 11.1, 14.1, 15.1, 19.1, 22.1, 23.1, 26.1, 27.1, 31.1, 35.1, 38.1, 39.1, 42.1, 43.1, 46.1, 47.1, 50.1, 51.1, 54.1, 55.1, 58.1, 59.1, 62.1, 63.1, 6.2, 18.2, 29.2, 30.2, 34.2, 0.3, 1.3, 4.3, 5.3, 8.3, 9.3, 12.3, 13.3, 16.3, 17.3, 20.3, 21.3, 24.3, 25.3, 28.3, 32.3, 33.3, 36.3, 37.3, 40.3, 41.3, 44.3, 45.3, 48.3, 49.3, 52.3, 53.3, 56.3, 57.3, 60.3, 61.3};
        set<float> E4_339197={2.1, 3.1, 6.1, 7.1, 10.1, 11.1, 14.1, 15.1, 18.1, 19.1, 22.1, 23.1, 26.1, 27.1, 30.1, 31.1, 34.1, 35.1, 38.1, 39.1, 42.1, 43.1, 46.1, 47.1, 50.1, 51.1, 54.1, 55.1, 58.1, 59.1, 62.1, 63.1, 29.2, 37.2, 0.3, 1.3, 4.3, 5.3, 8.3, 9.3, 12.3, 13.3, 16.3, 17.3, 20.3, 21.3, 24.3, 25.3, 28.3, 32.3, 33.3, 36.3, 40.3, 41.3, 44.3, 45.3, 48.3, 49.3, 52.3, 53.3, 56.3, 57.3, 60.3, 61.3};

        vector<TString> getPFilesPath(int runNum);
        vector<TString> getPFilesAutomatically(int runNum);

        int getRunYearByNumber(int year);

        TDatime getRunTDate(int runNum);
        TString getRunDate(int runNum);
        TString getRunDateByFile(int runNum, TString fileName);
        TString getRunDateByShortFile(int runNum);
        TString getLaserList(int year);
        map<int,pair<TDatime,TDatime>> getLaserRunsMap(int year);
        int getLaserRun(int runNum, int beforem1_after1 = -1);
        int getFillNumber(int runNum);

        vector<TString> GetBadChannels(int runNum);
        
        vector<int> getLBRangeForCalibConst(int runNum);
        // int getNBunch(int runNum);
        bool usePLB(int runNum);
        int getPedestalType(int runNum);
        int getOverridenPedestalType(TString overridenPedestal);
        string GetConvConstantFilename(int convConstantIndex, int doLasGap = 0, int doSACorrection = 0, int anchor_index = 0, int useGroupGain = -1);
        int GetDebugLB(int runNum);
        bool makePerChannelLumiFile(int runNum);
        vector<int> GetPartsToCompute(int cells_type);
        vector<int> GetCellsToCompute(int cells_type);
        vector<TString> GetCellsToPrint(int cells_type);
        string GetCellsSuffix(int cells_type);
        string GetLaserSuffix(int doLaser);
        string GetInGapLaserSuffix(int doLasGap);
        TString GetInGapInputFile(int doLasGap);
        vector<int> GetInGapCellsToCompute(int doLasGap);
        bool PerChannelInGap(int doLasGap);
        string GetScintillatorCorrSuffix(int doScintillatorIndex);
        string GetSASuffix(int doSACorrection);
        string GetRunSuffix(int doLaser, int doLasGap, int doSACorrection, int pruning, int convConstantIndex, int doScintillatorIndex, bool useTime, bool noLBavg);
        pair<string,string> GetLaserDirectory(int doLaser);
        pair<string,string> GetLaserFiles(int runNum, int anchorRun, int doLaser);
        string CreateInterpolatedLaserFile(int runNum, int doLaser);
        TString GetLegendFromSuffix(TString suffix);
        bool useTwoParScintillatorCorr();
        bool useLaserInterpolation(int doLaser);
        
}

vector<TString> Global::getPFilesPath(int runNum) {
        vector<TString> pFilesPaths;

        if (runNum == 278968) {
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p0_20150909_2323.root");
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p1_20150909_2323.root");
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p2_20150909_2323.root");
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p3_20150909_2323.root");
        }                
        if (runNum == 279685) {
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p0_20150917_1829.root");
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p1_20150917_1829.root");
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p2_20150917_1829.root");
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p3_20150917_1829.root");
        }                
        if (runNum == 279764) {
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p0_20150918_1727.root");
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p1_20150918_1727.root");
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p2_20150918_1727.root");
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p3_20150918_1727.root");
        }                
        if (runNum == 279928) {
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p0_20150920_1352.root");
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p1_20150920_1352.root");
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p2_20150920_1352.root");
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p3_20150920_1352.root");
        }                
        if (runNum == 280273) {
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p0_20150924_2348.root");
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p1_20150924_2348.root");
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p2_20150924_2348.root");
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p3_20150924_2348.root");
        }                
        if (runNum == 280368) {
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p0_20150926_0903.root");
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p1_20150926_0903.root");
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p2_20150926_0903.root");
                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p3_20150926_0903.root");
        }                

        if (runNum == 276336) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p0_20150817_0738.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p1_20150817_0738.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p2_20150817_0738.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2015/mbias/TileMb_p3_20150817_0738.root");}

        if (runNum == 297730) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2016/mbias/TileMb_p0_20160428_2051_INCOMPLETE.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2016/mbias/TileMb_p1_20160428_2051_INCOMPLETE.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2016/mbias/TileMb_p2_20160428_2051_INCOMPLETE.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2016/mbias/TileMb_p3_20160428_2051_INCOMPLETE.root");}

        if (runNum == 324320) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170523_1332.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170523_1332.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170523_1332.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170523_1332.root");}
        
        if (runNum == 330328) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170722_1159.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170722_1159.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170722_1159.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170722_1159.root");}

        if (runNum == 326446) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170612_1655.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170612_1654.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170612_1655.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170612_1655.root");}

        if (runNum == 355754) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20180716_1455.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20180716_1455.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20180716_1455.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20180716_1455.root");}


        if (runNum == 348618) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20180422_2100.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20180422_2100.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20180422_2100.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20180422_2100.root");}


        if (runNum == 349526) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20180504_2022.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20180504_2022.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20180504_2022.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20180504_2022.root");}


        if (runNum == 351671) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20180601_2027.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20180601_2027.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20180601_2027.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20180601_2027.root");}


        if (runNum == 354863) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20180702_1414.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20180702_1414.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20180702_1414.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20180702_1414.root");}


        if (runNum == 356259) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20180722_1946.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20180722_1946.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20180722_1946.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20180722_1946.root");}


        if (runNum == 364292) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20181023_1456.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20181023_1456.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20181023_1456.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20181023_1456.root");}


        if (runNum == 364214) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20181022_2231.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20181022_2231.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20181022_2231.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20181022_2231.root");}


        if (runNum == 362661) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20181004_0129.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20181004_0129.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20181004_0129.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20181004_0129.root");}


        if (runNum == 359735) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20180902_2217.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20180902_2217.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20180902_2217.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20180902_2217.root");}


        if (runNum == 357500) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20180804_1733.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20180804_1733.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20180804_1733.root");
                        pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20180804_1733.root");}


        if (runNum == 354494) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20180630_0924.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20180630_0924.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20180630_0924.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20180630_0924.root");}

        if (runNum == 354476) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20180629_1551.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20180629_1551.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20180629_1551.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20180629_1551.root");}

        if (runNum == 355273) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20180708_2002.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20180708_2002.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20180708_2002.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20180708_2002.root");}

        if (runNum == 354359) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20180628_1309.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20180628_1309.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20180628_1309.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20180628_1309.root");}

        if (runNum == 354315) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20180628_0624.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20180628_0624.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20180628_0624.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20180628_0624.root");}

        if (runNum == 354309) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20180627_2058.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20180627_2058.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20180627_2058.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20180627_2058.root");}

        if (runNum == 339197) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171026_1830.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171026_1830.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171026_1830.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171026_1830.root");}

        if (runNum == 339205) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171027_0459.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171027_0459.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171027_0459.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171027_0459.root");}

        if (runNum == 336506) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170924_0257.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170924_0257.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170924_0257.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170924_0257.root");}

        if (runNum == 330875) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170728_0449.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170728_0449.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170728_0449.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170728_0449.root");}

        if (runNum == 331085) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170729_2033.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170729_2033.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170729_2033.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170729_2033.root");}

        if (runNum == 335302) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170913_0652.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170913_0652.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170913_0652.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170913_0652.root");}

        if (runNum == 354124) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20180626_0035.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20180626_0035.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20180626_0035.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20180626_0035.root");}

        if (runNum == 354174) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20180626_1937.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20180626_1937.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20180626_1937.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20180626_1937.root");}

        if (runNum == 329542) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170712_0403.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170712_0403.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170712_0403.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170712_0403.root");}

        if (runNum == 331020) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170729_0230.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170729_0230.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170729_0230.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170729_0230.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170729_0936.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170729_0936.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170729_0936.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170729_0936.root");}

        if (runNum == 341294) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171121_1644.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171121_1644.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171121_1644.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171121_1644.root");}

        if (runNum == 341312) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171121_2129.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171121_2129.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171121_2129.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171121_2129.root");}

        if (runNum == 341419) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171122_2111.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171122_2111.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171122_2111.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171122_2111.root");}

        if (runNum == 341534) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171123_1944.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171123_1944.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171123_1944.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171123_1944.root");}

        if (runNum == 341615) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171125_0403.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171125_0402.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171125_0402.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171125_0403.root");}

        if (runNum == 341649) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171126_0130.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171126_0130.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171126_0130.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171126_0130.root");}

        if (runNum == 286364) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20151121_0012.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20151121_0012.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20151121_0012.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20151121_0012.root");}

        if (runNum == 286474) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20151122_2253.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20151122_2253.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20151122_2253.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20151122_2253.root");}

        if (runNum == 336471) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170922_1626.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170922_1626.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170922_1626.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170922_1626.root");}

        if (runNum == 338022) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171012_1013.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171012_1013.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171012_1013.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171012_1013.root");}

        if (runNum == 325020) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170530_1137.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170530_1137.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170530_1137.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170530_1137.root");}

        if (runNum == 326446) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170612_1655.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170612_1655.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170612_1655.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170612_1655.root");}

        if (runNum == 326870) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170616_1418.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170616_1418.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170616_1418.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170616_1418.root");}

        if (runNum == 326945) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170617_0535.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170617_0535.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170617_0535.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170617_0535.root");}

        if (runNum == 327860) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170626_0122.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170626_0122.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170626_0122.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170626_0122.root");}

        if (runNum == 328099) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170627_1533.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170627_1533.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170627_1533.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170627_1533.root");}

        if (runNum == 330294) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170721_1824.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170721_1824.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170721_1823.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170721_1824.root");}

        if (runNum == 333192) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170816_0841.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170816_0841.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170816_0841.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170816_0841.root");}

        if (runNum == 334993) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170907_1244.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170907_1244.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170907_1244.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170907_1245.root");}

        if (runNum == 336505) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170923_1914.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170923_1914.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170923_1914.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170923_1914.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170923_2208.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170923_2208.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170923_2208.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170923_2208.root");}

        if (runNum == 337005) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171002_0617.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171002_0617.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171002_0617.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171002_0618.root");}

        if (runNum == 337107) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171003_1232.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171003_1233.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171003_1233.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171003_1233.root");}

        if (runNum == 337451) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171008_0003.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171008_0002.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171008_0002.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171008_0003.root");}

        if (runNum == 324910) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170528_2138.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170528_2138.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170528_2138.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170528_2138.root");}

        if (runNum == 327057) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20170617_2011.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20170617_2011.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20170617_2011.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20170617_2011.root");}

        if (runNum == 340644) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171112_0753.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171112_0753.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171112_0753.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171112_0753.root");}

        if (runNum == 340683) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171112_1545.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171112_1545.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171112_1545.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171112_1545.root");}

        if (runNum == 340697) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171112_1906.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171112_1906.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171112_1906.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171112_1906.root");}

        if (runNum == 340718) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171113_0754.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171113_0754.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171113_0754.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171113_0754.root");}

        if (runNum == 340814) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171114_1223.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171114_1223.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171114_1223.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171114_1223.root");}

        if (runNum == 340850) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171115_0422.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171115_0422.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171115_0422.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171115_0422.root");}

        if (runNum == 340910) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171115_1536.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171115_1536.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171115_1536.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171115_1536.root");}

        if (runNum == 340925) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171116_1144.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171116_1144.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171116_1144.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171116_1144.root");}

        if (runNum == 340973) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171116_1948.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171116_1948.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171116_1948.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171116_1948.root");}

        if (runNum == 341027) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171118_1315.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171118_1315.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171118_1315.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171118_1315.root");}

        if (runNum == 341123) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171119_2151.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171119_2151.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171119_2151.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171119_2151.root");}

        if (runNum == 341184) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171120_1217.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171120_1217.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171120_1217.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171120_1217.root");}
                                            
        if (runNum == 339205) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171027_0459.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p1_20171027_0459.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p2_20171027_0459.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p3_20171027_0459.root");
                                
        if (runNum == 360402) {pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p0_20180910_0424.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p1_20180910_0424.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p2_20180910_0424.root");
                                pFilesPaths.push_back("/eos/atlas/atlascerngroupdisk/det-tile/online/2018/mbias/TileMb_p3_20180910_0424.root");}}

        if (pFilesPaths.size() == 0) pFilesPaths = Global::getPFilesAutomatically(runNum);

        return pFilesPaths;
}

int Global::getRunYearByNumber(int runNum) {
        if (runNum >= 348251) return 2018;
        if (runNum <= 284484) return 2015;
        if (runNum <= 314245) return 2016;
        return 2017;
}

TDatime Global::getRunTDate(int runNum) {

        if (runNum == 298293) return TDatime(2016,5,4,0,0,0);
        if (runNum == 276336) return TDatime(2015,8,17,0,0,0);
        if (runNum == 278968) return TDatime(2015,9,9,0,0,0);
        if (runNum == 279685) return TDatime(2015,9,17,0,0,0);
        if (runNum == 279764) return TDatime(2015,9,18,0,0,0);
        if (runNum == 279928) return TDatime(2015,9,20,0,0,0);
        if (runNum == 279984) return TDatime(2015,9,21,0,0,0);
        if (runNum == 280273) return TDatime(2015,9,24,0,0,0);
        if (runNum == 280368) return TDatime(2015,9,26,0,0,0);

        int yearNumber = getRunYearByNumber(runNum);

        map<int,pair<TDatime,TDatime>> laser_run_map = getLaserRunsMap(yearNumber);
        if (laser_run_map.count(runNum) > 0) return laser_run_map[runNum].first;

        string year = to_string(yearNumber);
        string short_files_path = "/afs/cern.ch/user/t/tilmbias/work/public/" + year + "/";
        string command_get_shortFile = "ls -1 " + short_files_path + "short_*" + to_string(runNum) + "*.root";

        string shortPath = getCMDoutput(command_get_shortFile);
        if (shortPath == "") {
                return TDatime(0,0,0,0,0,0);
        }

        shortPath.erase(shortPath.find(short_files_path), short_files_path.length());

        TString date = shortPath.substr(shortPath.find("tag")+3,8);
        TString time = shortPath.substr(shortPath.find(date)+9,4);

        TDatime datime = TDatime(stoi(date(0,4)),stoi(date(4,2)),stoi(date(6,2)),stoi(time(0,2)),stoi(time(2,2)),0);

        return datime;

}

TString Global::getRunDate(int runNum) {

        TDatime datime = getRunTDate(runNum);

        TString str_date = datime.AsString();

        return str_date(4,6);

}

TString Global::getRunDateByFile(int runNum, TString fileName) {

        vector<string> v_runNumbers, v_dates, v_times;
        readFile(fileName, 1, {0,1,2}, {&v_runNumbers, &v_dates, &v_times});
        TString date = "";
        TString time = "";
        for (int i_run = 0 ; i_run < v_runNumbers.size() ; i_run++) {
                if (stoi(v_runNumbers.at(i_run)) == runNum) {
                        date = v_dates.at(i_run);
                        time = v_times.at(i_run);
                }
        }
        TString return_date = date + "_" + time(0,4);
        return_date.ReplaceAll("-","");
        return_date.ReplaceAll(":","");

        return return_date;
}

TString Global::getRunDateByShortFile(int runNum) {
        int yearNumber = getRunYearByNumber(runNum);
        string year = to_string(yearNumber);
        string short_files_path = "/afs/cern.ch/user/t/tilmbias/work/public/" + year + "/";
        string command_get_shortFile = "ls -1 " + short_files_path + "short_*" + to_string(runNum) + "*.root";

        string shortPath = getCMDoutput(command_get_shortFile);
        if (shortPath.length() == 0) {
                coutError("The date could not be found for " + to_string(runNum));
                return "";
        }

        shortPath.erase(shortPath.find(short_files_path), short_files_path.length());

        TString date = shortPath.substr(shortPath.find("tag")+3,8);
        TString hour = shortPath.substr(shortPath.find("tag")+12,3);

        return date + "_" + hour;
}

TString Global::getLaserList(int year) {
        if (year == 2018) return "share/LaserRuns_wDate.txt";
        if (year == 2017) return "share/All2017Runs_wDate.txt";
        if (year == 2016) return "share/Laser2016Runs_wDate.txt";
        if (year == 2015) return "share/Laser2015Runs_wDate.txt";
        coutError("Global::getLaserList -- List not provided for year " + to_string(year));
        return "";
}

map<int,pair<TDatime,TDatime>> Global::getLaserRunsMap(int year) {
        map<int,pair<TDatime,TDatime>> laser_run_map;
        vector<string> v_laserRun_numberStr, v_startDate, v_startTime, v_endDate, v_endTime;
        TString input_laser_run_list = getLaserList(year);
        readFile(input_laser_run_list, 1, {0,1,2,3,4}, {&v_laserRun_numberStr, &v_startDate, &v_startTime, &v_endDate, &v_endTime});
        for (int i_laserRun = 0 ; i_laserRun < v_laserRun_numberStr.size() ; i_laserRun++) {
                int laserRunNumber = stoi(v_laserRun_numberStr.at(i_laserRun));
                TString startDateStr = v_startDate.at(i_laserRun);
                TString startTimeStr = v_startTime.at(i_laserRun);
                TString endDateStr = v_endDate.at(i_laserRun);
                TString endTimeStr = v_endTime.at(i_laserRun);
                TDatime startTDate = TDatime(stoi(startDateStr(0,4)),stoi(startDateStr(5,2)),stoi(startDateStr(8,2)),stoi(startTimeStr(0,2)),stoi(startTimeStr(3,2)),stoi(startTimeStr(6,2)));
                TDatime endTDate = TDatime(stoi(endDateStr(0,4)),stoi(endDateStr(5,2)),stoi(endDateStr(8,2)),stoi(endTimeStr(0,2)),stoi(endTimeStr(3,2)),stoi(endTimeStr(6,2)));
                laser_run_map[laserRunNumber] = make_pair(startTDate, endTDate);
        }
        return laser_run_map;
}

int Global::getLaserRun(int runNum, int beforem1_after1 = -1) {
        map<int,pair<TDatime,TDatime>> laser_run_map = getLaserRunsMap(getRunYearByNumber(runNum));
        TDatime runTDatime = getRunTDate(runNum);
        int prevLaserRun = -1;
        for (auto kv : laser_run_map) {
                int laserRunNumber = kv.first;
                TDatime startTDate = kv.second.first;
                TDatime endTDate = kv.second.second;
                if (startTDate > runTDatime) {
                        if (beforem1_after1 == -1) return prevLaserRun;
                        else if (beforem1_after1 == 1) return laserRunNumber;
                }
                prevLaserRun = laserRunNumber;
        }
        return -1;
}

int Global::getFillNumber(int runNum) {
        if (runNum == 340072) return 6364;
        if (runNum == 354124) return 6847;
        if (runNum == 354174) return 6850;
        if (runNum == 354309) return 6854;
        if (runNum == 354494) return 6868;
        if (runNum == 330875) return 6016;
        if (runNum == 331020) return 6019;
        if (runNum == 299390) return 4945;
        if (runNum == 299584) return 4947;
        if (runNum == 300287) return 4954;
        if (runNum == 300415) return 4958;
        if (runNum == 331085) return 6024;
        return 0;
}

vector<TString> Global::GetBadChannels(int runNum) {

        vector<TString> bad_channels;
        cout << "---- Calling database for bad channel information ----" << endl;
        TString tmp_file = "bad_channels/bad_channels_" + to_string(runNum) + ".txt";

        if (fileExists(tmp_file)) {
                fstream badChannelFile; badChannelFile.open(tmp_file);
                string line;
                char part[10];
                int pmt;
                while(getline(badChannelFile, line)) {
                        sscanf(line.c_str(),"%9s\t%i", part, &pmt);
                        string part_str = part;
                        TString part_name = part_str.substr(0,3);
                        TString pmt_name = Utils::GetPMTStringExtra(pmt,Utils::GetPartIndex(part_name),false);
                        if (pmt_name == "PMT_NOT_FOUND") continue;
                        bad_channels.push_back(TString::Format("%s_%s", pmt_name.Data(), part_str.c_str()));
                }
        } else {
                cout << "File not found. You should run:" << endl;
                cout << "source /afs/cern.ch/user/t/tilebeam/offline/reco-latest/setup.sh" << endl;
                cout << "ReadBchFromCool.py --run=" << runNum << " --tag=UPD4 | grep \"=> BAD\" > " << tmp_file << endl;
        }
        cout << "-------------------------------------------------" << endl;
        return bad_channels;

}

vector<TString> Global::getPFilesAutomatically(int runNum) {

        vector<TString> pFilesPaths;

        int yearNumber = getRunYearByNumber(runNum);
        string year = to_string(yearNumber);
        // string short_files_path = "/afs/cern.ch/user/t/tilmbias/work/public/" + year + "/";
        // string command_get_shortFile = "ls -1 " + short_files_path + "short_*" + to_string(runNum) + "*.root";

        // string shortPath = getCMDoutput(command_get_shortFile);
        // if (shortPath.length() == 0) {
        //         coutError("The PFiles could not be found for " + to_string(runNum));
        //         return pFilesPaths;
        // }

        // shortPath.erase(shortPath.find(short_files_path), short_files_path.length());

        // string date = shortPath.substr(shortPath.find("tag")+3,8);
        // string hour = shortPath.substr(shortPath.find("tag")+12,3);

        TString date_hour = getRunDateByShortFile(runNum);
        if (date_hour == "") return pFilesPaths;

        string path = "/eos/atlas/atlascerngroupdisk/det-tile/online/" + year + "/mbias/";
        string command0 = "ls -1 " + path + "TileMb_p0_" + date_hour.Data() + "*.root";
        string command1 = "ls -1 " + path + "TileMb_p1_" + date_hour.Data() + "*.root";
        string command2 = "ls -1 " + path + "TileMb_p2_" + date_hour.Data() + "*.root";
        string command3 = "ls -1 " + path + "TileMb_p3_" + date_hour.Data() + "*.root";

        string p0path = getCMDoutput(command0);
        string p1path = getCMDoutput(command1);
        string p2path = getCMDoutput(command2);
        string p3path = getCMDoutput(command3);
        pFilesPaths.push_back(p0path); pFilesPaths.push_back(p1path); pFilesPaths.push_back(p2path); pFilesPaths.push_back(p3path);

        return pFilesPaths;
}

vector<int> Global::getLBRangeForCalibConst(int runNum) {
        vector<int> pedLimits;

        if (runNum == 360063) {pedLimits.push_back(450);pedLimits.push_back(500);}
        if (runNum == 360129) {pedLimits.push_back(550);pedLimits.push_back(600);}
        if (runNum == 360161) {pedLimits.push_back(400);pedLimits.push_back(450);}
        if (runNum == 360209) {pedLimits.push_back(250);pedLimits.push_back(300);}
        if (runNum == 360244) {pedLimits.push_back(250);pedLimits.push_back(300);}
        if (runNum == 360293) {pedLimits.push_back(700);pedLimits.push_back(750);}
        if (runNum == 360309) {pedLimits.push_back(750);pedLimits.push_back(800);}
        if (runNum == 360373) {pedLimits.push_back(700);pedLimits.push_back(750);}
        if (runNum == 360402) {pedLimits.push_back(450);pedLimits.push_back(500);}
        if (runNum == 361738) {pedLimits.push_back(600);pedLimits.push_back(650);}
        if (runNum == 361862) {pedLimits.push_back(800);pedLimits.push_back(850);}
        if (runNum == 362204) {pedLimits.push_back(750);pedLimits.push_back(800);}
        if (runNum == 362297) {pedLimits.push_back(500);pedLimits.push_back(550);}
        if (runNum == 362345) {pedLimits.push_back(500);pedLimits.push_back(550);}
        if (runNum == 362354) {pedLimits.push_back(400);pedLimits.push_back(450);}
        if (runNum == 362388) {pedLimits.push_back(400);pedLimits.push_back(450);}
        if (runNum == 362445) {pedLimits.push_back(400);pedLimits.push_back(450);}

       
        if (runNum == 355754) {pedLimits.push_back(400);pedLimits.push_back(450);}

        // if (runNum == 331085) {pedLimits.push_back(1000);pedLimits.push_back(1010);}
        if (runNum == 331085) {pedLimits.push_back(1000);pedLimits.push_back(1020);}
        if (runNum == 339205) {pedLimits.push_back(130);pedLimits.push_back(150);}
        if (runNum == 354174) {pedLimits.push_back(120);pedLimits.push_back(140);}
        if (runNum == 340644) {pedLimits.push_back(30);pedLimits.push_back(80);}
        if (runNum == 340683) {pedLimits.push_back(30);pedLimits.push_back(80);}

        if (runNum == 348618) {pedLimits.push_back(300);pedLimits.push_back(350);}
        if (runNum == 349526) {pedLimits.push_back(250);pedLimits.push_back(300);}
        if (runNum == 351671) {pedLimits.push_back(200);pedLimits.push_back(250);}
        if (runNum == 354863) {pedLimits.push_back(250);pedLimits.push_back(300);}
        if (runNum == 355273) {pedLimits.push_back(350);pedLimits.push_back(400);}
        if (runNum == 356259) {pedLimits.push_back(250);pedLimits.push_back(300);}
        if (runNum == 357500) {pedLimits.push_back(260);pedLimits.push_back(310);}
        if (runNum == 359735) {pedLimits.push_back(200);pedLimits.push_back(250);}
        if (runNum == 362661) {pedLimits.push_back(350);pedLimits.push_back(400);}
        if (runNum == 364214) {pedLimits.push_back(220);pedLimits.push_back(270);}
        if (runNum == 364292) {pedLimits.push_back(400);pedLimits.push_back(450);}


        return pedLimits;
}

// int Global::getNBunch(int runNum) {
//         if (runNum == 331085) return 2544;

//         return -1;
// }

bool Global::usePLB(int runNum) {
        // if (runNum == 339197 || runNum == 339205 || runNum == 336506 || runNum == 330875 || runNum == 340634) return true;
        if (runNum == 339197 || runNum == 336506 || runNum == 330875 || runNum == 340634 || runNum == 299390 || runNum == 300287) return true;
        return false;
}

int Global::getPedestalType(int runNum) {
        // if (runNum == 339197) return PEDESTALS::FIT_339197;
        if (runNum == 300287) return PEDESTALS::FIT_EXPO; 
        if (runNum == 299390) return PEDESTALS::FIT_ADDEXPO; // FIT_EXPO
        if (runNum == 354124) return PEDESTALS::FIT_354124;
        if (runNum == 341534) return PEDESTALS::MEAN;
        if (runNum == 341615) return PEDESTALS::MEAN;
        if (runNum == 341649) return PEDESTALS::MEAN;
        if (runNum == 339205) return PEDESTALS::MEAN;
        if (runNum == 341312) return PEDESTALS::MEAN;
        if (runNum == 341419) return PEDESTALS::MEAN;
        if (runNum == 336505) return PEDESTALS::MEAN;
        if (runNum == 341294) return PEDESTALS::MEAN;
        if (runNum == 356427) return PEDESTALS::FIT_ADDEXPO;
        if (runNum == 354476) return PEDESTALS::FIT_ADDEXPO; // <-------------------
        // if (runNum == 354476) return PEDESTALS::INTERPOLATION;
        // if (runNum == 354494) return PEDESTALS::FIT_EXPO;
        // if (runNum == 354494) return PEDESTALS::MEAN;
        // if (runNum == 354494) return PEDESTALS::INTERPOLATION;
        // if (runNum == 354494) return PEDESTALS::ZEROES;
        // if (runNum == 354494) return PEDESTALS::FIT_SECONDPOLY;
        // if (runNum == 354494) return PEDESTALS::VDM_EXPO;
        if (runNum == 354494) return PEDESTALS::VDM_EXPO_wINTERP; //<----------------
        // if (runNum == 354494) return PEDESTALS::FIT_LINEAR;
        // if (runNum == 354494) return PEDESTALS::VDM;
        // if (runNum == 354494) return PEDESTALS::INTERPOLATION;
        if (runNum == 364485) return PEDESTALS::FIT_LINEAR;
        // if (runNum == 364485) return PEDESTALS::FIT_EXPO;
        if (runNum == 355261) return PEDESTALS::MEAN;
        // if (runNum == 355261) return PEDESTALS::INTERPOLATION;
        // if (runNum == 340683) return PEDESTALS::INTERPOLATION_TO_LINEAR_FIT;
        if (runNum == 355273) return PEDESTALS::MEAN;
        // if (runNum == 355273) return PEDESTALS::ZEROES;
        // if (runNum == 355273) return PEDESTALS::INTERPOLATION;
        // if (runNum == 354311) return PEDESTALS::FIT_LINEAR;
        // if (runNum == 330875) return PEDESTALS::ZEROES;
        // if (runNum == 330875) return PEDESTALS::MEAN;
        // if (runNum == 330875) return PEDESTALS::FIT_LINEAR;
        // if (runNum == 330875) return PEDESTALS::MEAN;
        if (runNum == 330875) return PEDESTALS::VDM2017_EXPO_w3INTERP; // <--------------------------
        if (runNum == 362661) return PEDESTALS::FIT_LINEAR;
        if (runNum == 354359) return PEDESTALS::FIT_LINEAR;
        if (runNum == 340634) return PEDESTALS::VDM5TeV_EXPO_PLUS_LINE_FIT;
        // if (runNum == 340634) return PEDESTALS::MEAN;

        // if (runNum == 331020) return PEDESTALS::FIT_SECONDEXPO;
        if (runNum == 331020) return PEDESTALS::FIT_THIRDEXPO; 
        
        //Activation Study
        if (runNum == 331019) return PEDESTALS::FIT_LINEAR;
        if (runNum == 327761) return PEDESTALS::FIT_LINEAR;
        if (runNum == 335056) return PEDESTALS::FIT_LINEAR;
        if (runNum == 334455) return PEDESTALS::FIT_LINEAR;
        if (runNum == 334580) return PEDESTALS::FIT_LINEAR;
        if (runNum == 339346) return PEDESTALS::FIT_LINEAR;
        if (runNum == 339562) return PEDESTALS::FIT_LINEAR;
        if (runNum == 339535) return PEDESTALS::FIT_LINEAR;
        if (runNum == 334842) return PEDESTALS::FIT_LINEAR;
        if (runNum == 329778) return PEDESTALS::FIT_LINEAR;
        if (runNum == 335082) return PEDESTALS::FIT_LINEAR;
        if (runNum == 325789) return PEDESTALS::FIT_LINEAR;
        if (runNum == 332720) return PEDESTALS::FIT_LINEAR;
        if (runNum == 326468) return PEDESTALS::FIT_LINEAR;
        if (runNum == 339590) return PEDESTALS::FIT_LINEAR;
        if (runNum == 333853) return PEDESTALS::FIT_LINEAR;
        if (runNum == 338967) return PEDESTALS::FIT_LINEAR;
        if (runNum == 339500) return PEDESTALS::FIT_LINEAR;

        return PEDESTALS::MEAN;
}

int Global::getOverridenPedestalType(TString overridenPedestal) {
        cout << "Override pedestal to " << overridenPedestal << endl;
        if (overridenPedestal == "ZEROES") return PEDESTALS::ZEROES;
        if (overridenPedestal == "MEAN") return PEDESTALS::MEAN;
        if (overridenPedestal == "FIT_LINEAR") return PEDESTALS::FIT_LINEAR;
        if (overridenPedestal == "FIT_ADDEXPO") return PEDESTALS::FIT_ADDEXPO;
        if (overridenPedestal == "EXPO") return PEDESTALS::FIT_EXPO;
        if (overridenPedestal == "CALIB_PED") return PEDESTALS::CALIB_PED;
        coutError("Override pedestal not implemented for " + overridenPedestal);
        return PEDESTALS::MEAN;
}

string Global::GetConvConstantFilename(int convConstantIndex, int doLasGap = 0, int doSACorrection = 0, int anchor_index = 0, int useGroupGain = -1) {
        string convFile = "";
        if (convConstantIndex == 339205) convFile = "CalibConst_339205_250LB500.dat";
        else if (convConstantIndex == 3392052) convFile = "CalibConst_339205_130LB150.dat";
        else if (convConstantIndex == 349033) convFile = "CalibConst_349033_500LB600.dat";
        else if (convConstantIndex == 354174) convFile = "CalibConst_354174_120LB140.dat";
        else if (convConstantIndex == 354359) convFile = "CalibConst_354359_290LB350.dat";
        else if (convConstantIndex == 3543592) convFile = "CalibConst_354359_330LB350.dat";
        else if (convConstantIndex == 355273) convFile = "CalibConst_355273_450LB500.dat";
        else if (convConstantIndex == 354396) convFile = "CalibConst_354396_300LB350.dat";
        else if (convConstantIndex == 340072) convFile = "CalibConst_340072_1800LB1900.dat";
        else if (convConstantIndex == 3400721) convFile = "CalibConst_340072_600LB700.dat";
        else if (convConstantIndex == 3400722) convFile = "CalibConst_340072_800LB900.dat";
        else if (convConstantIndex == 3400723) convFile = "CalibConst_340072_1000LB1100.dat";
        else if (convConstantIndex == 3400724) convFile = "CalibConst_340072_1100LB1200.dat";
        else if (convConstantIndex == 3400725) convFile = "CalibConst_340072_1200LB1300.dat";
        else if (convConstantIndex == 3400726) convFile = "CalibConst_340072_1300LB1400.dat";
        else if (convConstantIndex == 3400727) convFile = "CalibConst_340072_1400LB1500.dat";
        else if (convConstantIndex == 3400728) convFile = "CalibConst_340072_1500LB1600.dat";
        else if (convConstantIndex == 3400729) convFile = "CalibConst_340072_1800LB1900.dat";
        else if (convConstantIndex == 363033) convFile = "CalibConst_363033_900LB1000.dat";
        else if (convConstantIndex == 3630331) convFile = "CalibConst_363033_400LB500.dat";
        else if (convConstantIndex == 3630332) convFile = "CalibConst_363033_500LB600.dat";
        else if (convConstantIndex == 3630333) convFile = "CalibConst_363033_600LB700.dat";
        else if (convConstantIndex == 3630334) convFile = "CalibConst_363033_700LB800.dat";
        else if (convConstantIndex == 3630335) convFile = "CalibConst_363033_800LB900.dat";
        else if (convConstantIndex == 362204) convFile = "CalibConst_362204_800LB900.dat";
        else if (convConstantIndex == 3622041) convFile = "CalibConst_362204_400LB500.dat";
        else if (convConstantIndex == 358516) convFile = "CalibConst_358516_900LB1000.dat";
        else if (convConstantIndex == 3585161) convFile = "CalibConst_358516_400LB500.dat";
        else if (convConstantIndex == 349944) convFile = "CalibConst_349944_900LB1000.dat";
        else if (convConstantIndex == 3499441) convFile = "CalibConst_349944_400LB500.dat";

        else if (convConstantIndex == 331085) convFile = "CalibConst_331085_800LB1000.dat";
        else if (convConstantIndex == 3310852) convFile = "CalibConst_331085_400LB500.dat";
        else if (convConstantIndex == 3310853) convFile = "CalibConst_331085_200LB300.dat";
        else if (convConstantIndex == 3310854) convFile = "CalibConst_331085_400LB600.dat";
        else if (convConstantIndex == 3310855) convFile = "CalibConst_331085_1000LB1200.dat";
        else if (convConstantIndex == 3310856) convFile = "CalibConst_331085_800LB1200.dat";
        else if (convConstantIndex == 3310857) convFile = "CalibConst_331085_600LB800.dat";
        
        else if (convConstantIndex == 340683) convFile = "CalibConst_340683_30LB90.dat";
        else if (convConstantIndex == 348618) convFile = "CalibConst_348618_300LB350.dat";
        else if (convConstantIndex == 364098) convFile = "CalibConst_364098_600LB700.dat";
        else if (convConstantIndex == 362776) convFile = "CalibConst_362776_600LB700.dat";
        else if (convConstantIndex == 364292) convFile = "CalibConst_364292_600LB800.dat";
        else if (convConstantIndex == 355754) convFile =  "CalibConst_355754_400LB450.dat";
        else if (convConstantIndex == 355331) convFile =  "CalibConst_355331_1400LB1600.dat";
        else if (convConstantIndex == 3553312) convFile =  "CalibConst_355331_200LB300.dat";
        else if (convConstantIndex == 3553313) convFile =  "CalibConst_355331_300LB400.dat";
        else if (convConstantIndex == 3553314) convFile =  "CalibConst_355331_400LB500.dat";
        else if (convConstantIndex == 3553315) convFile =  "CalibConst_355331_500LB600.dat";
        else if (convConstantIndex == 3553316) convFile =  "CalibConst_355331_600LB700.dat";
        else if (convConstantIndex == 3553317) convFile =  "CalibConst_355331_700LB800.dat";
        else if (convConstantIndex == 354494) return "CalibConst_354494_1600LB2100.dat";
        else if (convConstantIndex == 3544940) return "LumiScale_354494_0.dat";
        else if (convConstantIndex == 3544941) return "LumiScaleConstant_354494_0.dat";
        else if (convConstantIndex == 3544942) return "LumiScaleNoGausFit_354494_0.dat";
        else if (convConstantIndex == 3544943) return "LumiScaleConstant_354494_Scans_1278.dat";
        else if (convConstantIndex == 3544944) return "LumiScaleConstant_354494_Scans_1234781112.dat";
        else if (convConstantIndex == 3544945) return "LumiScaleConstant_354494_Scans_1234781112_useFitIntegral.dat";
        else if (convConstantIndex == 3544946) return "LumiScaleCombined_constantFit_354494_Scans_1234781112_useGausFit_FromFitIntegral.dat";
        else if (convConstantIndex == 3544947) return "LumiScaleCombined_averaging_354494_Scans_1234781112_useGausFit_FromFitIntegral.dat";
        else if (convConstantIndex == 3544948) return "LumiScaleCombineCalibration_constantFit_354494_Scans_1234781112_useGausFit_FromFitIntegral.dat";
        else if (convConstantIndex == 3544949) return "LumiScaleCombineCalibration_averaging_354494_Scans_1234781112_useGausFit_FromFitIntegral.dat";
        else if (convConstantIndex == 0) return "";
        else {
                vector<TString> possibleFiles = getFilesList("share","CalibConst_" + to_string(convConstantIndex));
                for (TString possibleFile : possibleFiles) {
                        if (convFile.size() == 0 || possibleFile.Length() < convFile.size()) convFile = possibleFile;
                }
        }

        // vector<TString> possibleFiles = getFilesList("share",to_string(convConstantIndex));
        // for (TString possibleFile : possibleFiles) {
        //         if (possibleFile.Contains("Ammara2018")) convFile = possibleFile;
        // }

        if (convFile == "" && convConstantIndex != 0) coutError("No conv file was found for " + to_string(convConstantIndex));

        if (anchor_index == ANCHOR_TYPE::LUCID) convFile.replace(convFile.end()-4,convFile.end(), "_anchLUCID.dat");
        if (doLasGap >= 1) convFile.replace(convFile.end()-4,convFile.end(), GetInGapLaserSuffix(doLasGap) + ".dat");
        if (doSACorrection == 1) convFile.replace(convFile.end()-4,convFile.end(), GetSASuffix(doSACorrection) + ".dat");
        if (useGroupGain != -1) convFile.replace(convFile.end()-4,convFile.end(), TString::Format("_GG%d.dat", useGroupGain));

        return convFile;

}

int Global::GetDebugLB(int runNum) {
        // if (runNum == 358516) return 760;
        if (runNum == 349944) return 755;
        if (runNum == 336505) return 150;
        if (runNum == 355331) return 1380;
        if (runNum == 358300) return 290;
        if (runNum == 361635) return 300;
        if (runNum == 339197) return 300;
        // if (runNum == 336505) return 444;
        if (runNum == 341294) return 210;
        if (runNum == 341615) return 400;
        if (runNum == 341649) return 380;
        if (runNum == 341534) return 800;
        if (runNum == 331020) return 500;
        if (runNum == 354174) return 120;
        if (runNum == 354124) return 560; 
        if (runNum == 354309) return 249;  
        if (runNum == 354315) return 140;  
        if (runNum == 354359) return 311;  
        if (runNum == 340644) return 60;  
        if (runNum == 340683) return 80;        
        if (runNum == 340925) return 100;  
        // if (runNum == 340973) return 100;  
        if (runNum == 355273) return 500;  
        if (runNum == 354494) return 1800;  
        if (runNum == 354476) return 250;  
        if (runNum == 340718) return 1000;  
        // if (runNum == 341312) return 150;  
        if (runNum == 364485) return 750;  
        if (runNum == 361689) return 180;  
        if (runNum == 354311) return 10;
        if (runNum == 354396) return 200;
        if (runNum == 354176) return 100;
        if (runNum == 357283) return 100;
        if (runNum == 341123) return 400;
        if (runNum == 330875) return 900;  // 600 - 900 - 1250
        if (runNum == 340634) return 600;  
        if (runNum == 340072) return 700;  
        return -1;
}

bool Global::makePerChannelLumiFile(int runNum) {

        // if (runNum > 347000 && runNum < 368000) return true;
        if (runNum == 327342 || runNum == 336852 || runNum == 340453 || runNum == 340072 || runNum == 341412 || runNum == 341419 || runNum == 341534 || runNum == 341615 || runNum == 341123 || runNum == 341184 || runNum == 331085) return true;
        if (runNum == 330875 || runNum == 340634) return true;
        if (runNum == 341312 || runNum == 341419 || runNum == 341534 || runNum == 341615 || runNum == 331020) return true;
        if (runNum == 351455 || runNum == 354174 || runNum == 354494 || runNum == 355261 || runNum == 361795 || runNum == 363400 || runNum == 354359) return true;
        if (runNum == 334842) return true;
        if (runNum == 348251 || runNum == 348354 || runNum == 348403 || runNum == 355331) return true;
        if (runNum == 348618 || runNum == 348836 || runNum == 348885 || runNum == 358395) return true;
        if (runNum == 332896 || runNum == 334264 || runNum == 330328 || runNum == 325713 || runNum == 324910) return true;
        if (runNum == 339849) return true;
        if (runNum == 340072) return true;
        if (runNum == 354174 || runNum == 354124 || runNum == 354309 || runNum == 354315 || runNum == 354359 || runNum == 355273 || runNum == 354476 || runNum == 354494 || runNum == 355261 || runNum == 354176 || runNum == 354396) return true;
        return true;
        // return false;
}


vector<int> Global::GetPartsToCompute(int cells_type) {
        vector<int> parts_to_compute = {PART::EBA,PART::EBC};
        if (cells_type == 3 || cells_type == 4 || cells_type == 6 || cells_type == 8 || cells_type == 9 || cells_type == 12) parts_to_compute = {PART::LBA,PART::LBC};
        if (cells_type == 17 || cells_type == 41 || cells_type == 42 || cells_type == 61 || cells_type == 62 || cells_type == 31 || cells_type == 32) parts_to_compute = {PART::LBA,PART::LBC};
        return parts_to_compute;
}

vector<int> Global::GetCellsToCompute(int cells_type) {
        vector<int> cells_to_compute;
        if (cells_type == 0 || cells_type == 1) cells_to_compute = {PMT::E3, PMT::E4};
        else if (cells_type == 2) cells_to_compute = {PMT::D5L, PMT::D5R, PMT::D6L, PMT::D6R};
        // else if (cells_type == 3) cells_to_compute = {PMT::D0, PMT::D1L, PMT::D1R, PMT::D2L, PMT::D2R};
        else if (cells_type == 3) cells_to_compute = {PMT::D1L, PMT::D1R, PMT::D2L, PMT::D2R};
        else if (cells_type == 4) cells_to_compute = {PMT::BC3L, PMT::BC3R, PMT::BC4L, PMT::BC4R};
        else if (cells_type == 5) cells_to_compute = {PMT::E1, PMT::E2};
        else if (cells_type == 6) cells_to_compute = {PMT::A2L, PMT::A2R, PMT::A9L, PMT::A9R};
        else if (cells_type == 7) cells_to_compute = {PMT::A12L, PMT::A12R, PMT::A15L, PMT::A15R};
        else if (cells_type == 8) cells_to_compute = {PMT::A1L, PMT::A1R, PMT::A2L, PMT::A2R};
        else if (cells_type == 9) cells_to_compute = {PMT::A9L, PMT::A9R, PMT::A10L, PMT::A10R};
        else if (cells_type == 10) cells_to_compute = {PMT::A12L, PMT::A12R, PMT::A13L, PMT::A13R};
        else if (cells_type == 11) cells_to_compute = {PMT::A15L, PMT::A15R, PMT::A16L, PMT::A16R};
        else if (cells_type == 12) cells_to_compute = {PMT::BC1L, PMT::BC1R, PMT::BC3L, PMT::BC3R};
        // else if (cells_type == 13) cells_to_compute = {PMT::BC8L, PMT::BC8R, PMT::BC9L, PMT::BC9R};
        else if (cells_type == 14) cells_to_compute = {PMT::BC11L, PMT::BC11R, PMT::BC12L, PMT::BC12R};
        // else if (cells_type == 15) cells_to_compute = {PMT::A12L, PMT::A12R, PMT::A13L, PMT::A13R};
        else if (cells_type == 15) cells_to_compute = {PMT::A14L, PMT::A14R};
        else if (cells_type == 1314) cells_to_compute = {PMT::A13L, PMT::A13R, PMT::A14L, PMT::A14R};
        else if (cells_type == 13) cells_to_compute = {PMT::A13L, PMT::A13R};
        else if (cells_type == 16) cells_to_compute = {PMT::D6L, PMT::D6R};
        else if (cells_type == 17) cells_to_compute = {PMT::BC1L, PMT::BC1R, PMT::BC2L, PMT::BC2R};
        else if (cells_type == 21) cells_to_compute = {PMT::D5L, PMT::D5R};
        else if (cells_type == 22) cells_to_compute = {PMT::D6L, PMT::D6R};
        else if (cells_type == 31) cells_to_compute = {PMT::D1L, PMT::D1R};
        else if (cells_type == 32) cells_to_compute = {PMT::D2L, PMT::D2R};
        else if (cells_type == 41) cells_to_compute = {PMT::BC3L, PMT::BC3R};
        else if (cells_type == 42) cells_to_compute = {PMT::BC4L, PMT::BC4R};
        else if (cells_type == 61) cells_to_compute = {PMT::A2L, PMT::A2R};
        else if (cells_type == 62) cells_to_compute = {PMT::A9L, PMT::A9R};
        else if (cells_type == 71) cells_to_compute = {PMT::A12L, PMT::A12R};
        else if (cells_type == 72) cells_to_compute = {PMT::A15L, PMT::A15R};
        else if (cells_type == 1234) cells_to_compute = {PMT::E1, PMT::E2, PMT::E3, PMT::E4};
        return cells_to_compute;
}

vector<TString> Global::GetCellsToPrint(int cells_type) {
        vector<TString> cells_to_print;
        if (cells_type == 0) cells_to_print = {"E3_A","E3_C","E4_A","E4_C"};
        else if (cells_type == 1) cells_to_print = {"E3_A_1","E3_A_2","E3_A_3","E3_C_1","E3_C_2","E3_C_3","E4_A_1","E4_A_2","E4_A_3","E4_C_1","E4_C_2","E4_C_3"};
        else if (cells_type == 2) cells_to_print = {"D5_A","D5_C","D6_A","D6_C"};
        // else if (cells_type == 3) cells_to_print = {"D0_A","D0_C","D1_A","D1_C","D2_A","D2_C"};
        else if (cells_type == 3) cells_to_print = {"D1_A","D1_C","D2_A","D2_C"};
        else if (cells_type == 4) cells_to_print = {"BC3_A","BC3_C","BC4_A","BC4_C"};
        else if (cells_type == 5) cells_to_print = {"E1_A","E1_C","E2_A","E2_C"};
        else if (cells_type == 6) cells_to_print = {"A2_A","A2_C","A9_A","A9_C"};
        else if (cells_type == 7) cells_to_print = {"A12_A","A12_C","A15_A","A15_C"};
        else if (cells_type == 8) cells_to_print = {"A1_A","A1_C","A2_A","A2_C"};
        else if (cells_type == 9) cells_to_print = {"A9_A","A9_C","A10_A","A10_C"};
        else if (cells_type == 10) cells_to_print = {"A12_A","A12_C","A13_A","A13_C"};
        else if (cells_type == 11) cells_to_print = {"A15_A","A15_C","A16_A","A16_C"};
        else if (cells_type == 12) cells_to_print = {"BC1_A","BC1_C","BC3_A","BC3_C"};
        // else if (cells_type == 13) cells_to_print = {"BC8_A","BC8_C","BC9_A","BC9_C"};
        else if (cells_type == 14) cells_to_print = {"BC11_A","BC11_C","BC12_A","BC12_C"};
        // else if (cells_type == 15) cells_to_print = {"A12L_A","A12R_A","A12L_C","A12R_C","A13L_A","A13R_A","A13L_C","A13R_C"};
        else if (cells_type == 15) cells_to_print = {"A14L_A","A14R_A","A14L_C","A14R_C"};
        else if (cells_type == 1314) cells_to_print = {"A13_A","A13_C","A14_A","A14_C"};
        else if (cells_type == 13) cells_to_print = {"A13_A","A13_C"};
        else if (cells_type == 16) cells_to_print = {"D6_A","D6_C"};
        else if (cells_type == 17) cells_to_print = {"BC1_A","BC1_C","BC2_A","BC2_C"};
        else if (cells_type == 1234) cells_to_print = {"E1_A","E1_C","E2_A","E2_C","E3_A","E3_C","E4_A","E4_C"};
        return cells_to_print;
}

string Global::GetCellsSuffix(int cells_type) {
        string suffix;
        if (cells_type == 0) suffix = "_ECells";
        else if (cells_type == 1) suffix = "_ECells_ByGainGroup";
        else if (cells_type == 2) suffix = "_DCells";
        else if (cells_type == 3) suffix = "_D0D1D2Cells";
        // else if (cells_type == suffix = "_D1D2Cells";
        else if (cells_type == 4) suffix = "_BCells";
        else if (cells_type == 5) suffix = "_e2Cells";
        else if (cells_type == 6) suffix = "_a2Cells";
        else if (cells_type == 7) suffix = "_ACells";
        else if (cells_type == 8) suffix = "_A1A2Cells";
        else if (cells_type == 9) suffix = "_A9A10Cells";
        else if (cells_type == 10) suffix = "_A12A13Cells";
        else if (cells_type == 11) suffix = "_A15A16Cells";
        else if (cells_type == 12) suffix = "_B1B3Cells";
        // else if (cells_type == 13) suffix = "_B8B9Cells";
        else if (cells_type == 1314) suffix = "_A13A14Cells";
        else if (cells_type == 13) suffix = "_A13Cells";
        else if (cells_type == 14) suffix = "_B11B12Cells";
        // else if (cells_type == 15) suffix = "_A12A13Cells_woCombining";
        else if (cells_type == 15) suffix = "_A15";
        else if (cells_type == 17) suffix = "_B1B2Cells";
        return suffix;
}

string Global::GetLaserSuffix(int doLaser) {
        string suffix = "";
        if (doLaser == 0) suffix = "_noLaser";
        else if (doLaser == 1) suffix = "_2018wIOVs";
        else if (doLaser == 2) suffix = "_wrongLaser";
        else if (doLaser == 3) suffix = "_onlyLaser";
        else if (doLaser == 4) suffix = "_RuteLaser";
        else if (doLaser == 5) suffix = "_RuteLaserCesium";
        else if (doLaser == 6) suffix = "_OGLaser";
        else if (doLaser == 7) suffix = "_2017NoIOVs";
        else if (doLaser == 8) suffix = "_2017wIOVs";
        else if (doLaser == 9) suffix = "_OGLaser16Jun2020";
        else if (doLaser == 10) suffix = "_2018woIOVsInterpolation";
        else if (doLaser == 11) suffix = "_2018woIOVsTimeInterpolation";
        else if (doLaser == 12) suffix = "_2016wIOVs";
        else if (doLaser == 13) suffix = "_2016woIOVs";
        else if (doLaser == 14) suffix = "_2016woIOVs_ORACLE";
        else if (doLaser == 15) suffix = "_2016woIOVs_ref299367";
        else if (doLaser == 16) suffix = "_2016woIOVs_ref299367_RESCALED";
        else if (doLaser == 17) suffix = "_2016woIOVs_ref299367_RESCALED_A13A14";
        else if (doLaser == 18) suffix = "_2016woIOVs_FromPawel_v1"; //Those are official references. Three references are inside.
        else if (doLaser == 19) suffix = "_2016woIOVs_FromPawel_v2"; //Those re also official references, but in this file there is only one reference from run 295683.
        else if (doLaser == 20) suffix = "_2016woIOVs_FromPawel_v3"; //Here I used new reference that I created using run 299371.
        else if (doLaser == 21) suffix = "_2016woIOVs_ReplicatePawel_v1"; 
        else if (doLaser == 22) suffix = "_2016woIOVs_FromAmmara"; 
        else if (doLaser == 23) suffix = "_2016woIOVs_Test"; // Using 2016 single IOV reference produced sqlite using all 2016 runs after the reference
        else if (doLaser == 24) suffix = "_2016woIOVs_Test2"; // Using 2016 single IOV reference produced sqlite using CT 2016 runs after the reference
        else if (doLaser == 25) suffix = "_2016woIOVs_FromPawel_v2_RESCALED"; // Using 2016 single IOV reference produced sqlite using CT 2016 runs after the reference and rescaling A/C sides (E3 and E4)
        else if (doLaser == 26) suffix = "_2016woIOVs_NoAppend"; // Using 2016 single IOV reference produced sqlite using CT 2016 runs after the reference and without append option
        else if (doLaser == 27) suffix = "_2016woIOVs_Correct"; // Using 2016 single IOV reference produced sqlite using All 2016 runs and with the list of runs correctly sorted
        else if (doLaser == 28) suffix = "_2016woIOVs_FromPawel_v2_RESCALED"; // Like 25 but with A13 and A14
        else if (doLaser == 29) suffix = "_2016woIOVs_FromPawel_v2_RESCALED_CSide"; // Using 2016 single IOV reference produced sqlite using CT 2016 runs after the reference and rescaling C side to A-side the runs affected by the issue (E3 and E4)
        else if (doLaser == 30) suffix = "_2016woIOVs_FromPawel_v2_RESCALED_CSide"; // Using 2016 single IOV reference produced sqlite using CT 2016 runs after the reference and rescaling C side to A-side the runs affected by the issue (A13 and A14)
        else if (doLaser == 31) suffix = "_2016woIOVs_ForStability"; // Using 2016 single IOV reference produced sqlite using CT 2016 runs after the reference and rescaling C side to A-side the runs affected by the issue (A13 and A14)
        else if (doLaser == 32) suffix = "_2016woIOVs_ForStability_RESCALED_CSide"; // Using 2016 single IOV reference produced sqlite using CT 2016 runs after the reference and rescaling C side to A-side the runs affected by the issue (A13 and A14)
        else if (doLaser == 33) suffix = "_2015woIOVs"; // Using 2016 single IOV reference produced sqlite using CT 2016 runs after the reference and rescaling C side to A-side the runs affected by the issue (A13 and A14)
        else if (doLaser == 34) suffix = "_ForShalini"; // Using /afs/cern.ch/user/k/klimek/public/Laser/Reference/Reference_FullRun2/ForAmmara/singleIOV/2015/tileSqlite_LasRef_2015_v2_249210.db

        return suffix;
}

pair<string,string> Global::GetLaserDirectory(int doLaser) {
        if (doLaser == 1)       return make_pair("LaserCorrections/Laser2018_wIOVs_16Jun2020","Laser_");
        else if (doLaser == 4)  return make_pair("LaserCorrections/RuteLaser","Laser_");
        else if (doLaser == 5)  return make_pair("LaserCorrections/RuteLaser","LaserCesium_");
        else if (doLaser == 6)  return make_pair("LaserCorrections/Laser2018_woIOVs","Laser_");
        else if (doLaser == 3)  return make_pair("LaserCorrections/Laser2017_wIOVs","Laser_");
        else if (doLaser == 7)  return make_pair("LaserCorrections/Laser2017_woIOVs","Laser_");
        else if (doLaser == 9)  return make_pair("LaserCorrections/Laser2018_woIOVs_16Jun2020","Laser_");
        else if (doLaser == 10) return make_pair("LaserCorrections/Laser2018_woIOVs_16Jun2020_IntLumiInterpolation","Laser_");
        else if (doLaser == 11) return make_pair("LaserCorrections/Laser2018_woIOVs_16Jun2020_TimeInterpolation","Laser_");
        else if (doLaser == 12) return make_pair("LaserCorrections/Laser2016_wIOVs","Laser_");
        else if (doLaser == 13) return make_pair("LaserCorrections/Laser2016_woIOVs","Laser_");
        else if (doLaser == 14) return make_pair("LaserCorrections/Laser2016_woIOVs_ORACLE","Laser_");
        else if (doLaser == 15) return make_pair("LaserCorrections/Laser2016_woIOVs_ref299367","Laser_");
        else if (doLaser == 16) return make_pair("LaserCorrections/Laser2016_woIOVs_ref299367_RESCALED","Laser_");
        else if (doLaser == 17) return make_pair("LaserCorrections/Laser2016_woIOVs_ref299367_RESCALED_A13A14","Laser_");
        else if (doLaser == 18) return make_pair("LaserCorrections/Laser2016_woIOVs_FromPawel_v1","Laser_");
        else if (doLaser == 19) return make_pair("LaserCorrections/Laser2016_woIOVs_FromPawel_v2","Laser_");
        else if (doLaser == 20) return make_pair("LaserCorrections/Laser2016_woIOVs_FromPawel_v3","Laser_");
        else if (doLaser == 21) return make_pair("LaserCorrections/Laser2016_woIOVs_ReplicatePawel_v1","Laser_");
        else if (doLaser == 22) return make_pair("LaserCorrections/Laser2016_woIOVs_FromAmmara","Laser_");
        else if (doLaser == 23) return make_pair("LaserCorrections/Laser2016_woIOVs_Test","Laser_");
        else if (doLaser == 24) return make_pair("LaserCorrections/Laser2016_woIOVs_Test2","Laser_");
        else if (doLaser == 25) return make_pair("LaserCorrections/Laser2016_woIOVs_FromPawel_v2_RESCALED","Laser_");
        else if (doLaser == 26) return make_pair("LaserCorrections/Laser2016_woIOVs_NoAppend","Laser_");
        else if (doLaser == 27) return make_pair("LaserCorrections/Laser2016_woIOVs_Correct","Laser_");
        else if (doLaser == 28) return make_pair("LaserCorrections/Laser2016_woIOVs_FromPawel_v2_RESCALED_A13A14","Laser_");
        else if (doLaser == 29) return make_pair("LaserCorrections/Laser2016_woIOVs_FromPawel_v2_RESCALED_CSide","Laser_");
        else if (doLaser == 30) return make_pair("LaserCorrections/Laser2016_woIOVs_FromPawel_v2_RESCALED_CSide_A13A14","Laser_");
        else if (doLaser == 31) return make_pair("LaserCorrections/Laser2016_woIOVs_Stability","Laser_");
        else if (doLaser == 32) return make_pair("LaserCorrections/Laser2016_woIOVs_Stability_RESCALED_CSide_D1D2","Laser_");
        else if (doLaser == 33) return make_pair("LaserCorrections/Laser2015_woIOVs","Laser_");
        else if (doLaser == 34) return make_pair("LaserCorrections/LaserForShalini","Laser_");
        else if (doLaser != 0)  return make_pair("LaserCorrections/Laser2017_wIOVs","Laser_");
        return make_pair("","");
}

string Global::GetInGapLaserSuffix(int doLasGap) {
        string suffix = "";
        if (doLasGap == 1) suffix = "_wLasGap";
        else if (doLasGap == 2) suffix = "_wLasGap_D6_R";
        else if (doLasGap == 3) suffix = "_wLasGap_D6_L";
        else if (doLasGap == 4) suffix = "_wLasGap_D6_R_fixedInt";
        else if (doLasGap == 5) suffix = "_wLasGap_D6_L_fixedInt";
        else if (doLasGap == 6) suffix = "_wLasGap_D6_R_wThresholds";
        else if (doLasGap == 7) suffix = "_wLasGap_D6_L_wThresholds";
        else if (doLasGap == 8) suffix = "_wLasGap_D6_R_wThresholds_removeNegSlope";
        else if (doLasGap == 9) suffix = "_wLasGap_D6_L_wThresholds_removeNegSlope";
        else if (doLasGap == 10) suffix = "_wLasGap_D6_L_wThresholds_removeNegSlope_RachelCrossChecks";
        else if (doLasGap == 11) suffix = "_wLasGap_D6_R_20200510_floatingIntercept";
        else if (doLasGap == 12) suffix = "_wLasGap_D6_L_20200510_floatingIntercept";
        else if (doLasGap == 13) suffix = "_wLasGap_D6_R_20200510_means_gt4uA_onlyForE3E4";
        else if (doLasGap == 14) suffix = "_wLasGap_D6_L_20200510_means_gt4uA_onlyForE3E4";
        else if (doLasGap == 15) suffix = "_wLasGap_D6_L_20200528_float_line";
        else if (doLasGap == 16) suffix = "_wLasGap_D6_L_20200617_keep_m555";
        else if (doLasGap == 17) suffix = "_wLasGap_D6_L_20200617_remove_m555";
        else if (doLasGap == 20) suffix = "_wLasGap_D6_L_vers_2_perChannel";
        // else if (doLasGap == 20) suffix = "_wLasGap_D6_L_20200629_v0";
        // else if (doLasGap == 21) suffix = "_wLasGap_D6_L_20200629_v1";
        // else if (doLasGap == 22) suffix = "_wLasGap_D6_L_20200629_v2";
        // else if (doLasGap == 23) suffix = "_wLasGap_D6_L_20200629_v3";
        // else if (doLasGap == 24) suffix = "_wLasGap_D6_L_20200629_v4";
        // else if (doLasGap == 25) suffix = "_wLasGap_D6_L_20200629_v5";
        // else if (doLasGap == 26) suffix = "_wLasGap_D6_L_20200629_v6";
        // else if (doLasGap == 27) suffix = "_wLasGap_D6_L_20200629_v7";
        // else if (doLasGap == 28) suffix = "_wLasGap_D6_L_20200629_v8";
        // else if (doLasGap == 29) suffix = "_wLasGap_D6_L_20200629_v9";
        else if (doLasGap == 30) suffix = "_wLasGap_D6_L_20200702_mean";
        else if (doLasGap == 31) suffix = "_wLasGap_D6_L_20200702_mean_tight";
        else if (doLasGap == 32) suffix = "_wLasGap_D6_L_20200702_mean_tight_E3A";
        else if (doLasGap == 33) suffix = "_wLasGap_D6_L_20200702_mean_tight_E3C";
        else if (doLasGap == 34) suffix = "_wLasGap_D6_L_20200702_mean_tight_ECellsMean";
        else if (doLasGap == 35) suffix = "_wLasGap_D6_L_20200901_noMissLB";
        else if (doLasGap == 36) suffix = "_wLasGap_D6_R_20200901_noMissLB";
        else if (doLasGap == 37) suffix = "_wLasGap_D6_L_vers_1";
        else if (doLasGap == 38) suffix = "_wLasGap_D6_R_vers_1";
        else if (doLasGap == 39) suffix = "_wLasGap_D6_L_vers_4";
        else if (doLasGap == 40) suffix = "_wLasGap_D6_R_vers_4";
        else if (doLasGap == 41) suffix = "_wLasGap_D6_L_vers_3";  // <----------- FINAL
        else if (doLasGap == 42) suffix = "_wLasGap_D6_R_vers_3";
        else if (doLasGap == 43) suffix = "_wLasGap_D6_L_ACells";
        else if (doLasGap == 44) suffix = "_wLasGap_D6_L_ACells_perChannel";
        else if (doLasGap == 45) suffix = "_wLasGap_D6_L_ACells_perChannelWithCleaning";
        else if (doLasGap == 46) suffix = "_wLasGap_D6_L_A13A14";
        else if (doLasGap == 47) suffix = "_wLasGap_D6_L_A13A14_perChannel";
        else if (doLasGap == 48) suffix = "_wLasGap_D6_L_A13A14_int1";
        else if (doLasGap == 49) suffix = "_wLasGap_D6_L_A13A14_int1_perChannel";
        return suffix;
}

TString Global::GetInGapInputFile(int doLasGap) {
        if (doLasGap == 41) return "share/fileForIntNote/vers_3/mean_corrections_2018_D6_L.root";
        if (doLasGap == 43) return "share/InGapCorrections/ACells/mean_corrections_2018_D6_L.root";
        if (doLasGap == 44) return "share/InGapCorrections/ACells/mean_corrections_2018_D6_L_bych.root";
        if (doLasGap == 45) return "share/InGapCorrections/ACells/mean_corrections_2018_D6_L_bych.root";
        if (doLasGap == 46) return "share/InGapCorrections/ACells/mean_corrections_2018_D6_L_cell_A13A14_20210127.root";
        if (doLasGap == 47) return "share/InGapCorrections/ACells/mean_corrections_2018_D6_L_A13A14_20210127.root";
        if (doLasGap == 48) return "share/InGapCorrections/ACells/mean_corrections_2018_D6_L_int1_meancell.root";
        if (doLasGap == 49) return "share/InGapCorrections/ACells/mean_corrections_2018_D6_L_int1_ch.root";
        coutError(Form("Global::GetInGapInputFile -- Input file not defined for doLasGap = %d",doLasGap));
        return "";
}

vector<int> Global::GetInGapCellsToCompute(int doLasGap) {
        vector<int> cell_vector;
        if (doLasGap == 41) cell_vector = {PMT::E3, PMT::E4};
        if (doLasGap == 42) cell_vector = {PMT::E3, PMT::E4};
        if (doLasGap == 43) cell_vector = {PMT::A12L, PMT::A12R, PMT::A13L, PMT::A13R};
        if (doLasGap == 44) cell_vector = {PMT::A12L, PMT::A12R, PMT::A13L, PMT::A13R};
        if (doLasGap == 45) cell_vector = {PMT::A12L, PMT::A12R, PMT::A13L, PMT::A13R};
        if (doLasGap == 46) cell_vector = {PMT::A13L, PMT::A13R, PMT::A14L, PMT::A14R};
        if (doLasGap == 47) cell_vector = {PMT::A13L, PMT::A13R, PMT::A14L, PMT::A14R};
        if (doLasGap == 48) cell_vector = {PMT::A13L, PMT::A13R, PMT::A14L, PMT::A14R};
        if (doLasGap == 49) cell_vector = {PMT::A13L, PMT::A13R, PMT::A14L, PMT::A14R};
        if (cell_vector.size() == 0) coutError(Form("Global::GetInGapCellsToCompute -- Input file not defined for doLasGap = %d",doLasGap));
        return cell_vector;
}

bool Global::PerChannelInGap(int doLasGap) {
        if (doLasGap == 44 || doLasGap == 45 || doLasGap == 47 || doLasGap == 49) return true;
        return false;
}

string Global::GetScintillatorCorrSuffix(int doScintillatorIndex) {
        string suffix = "";
        if (doScintillatorIndex != 0) {
                suffix = "_wScintCorr";
        }
        return suffix;
}

string Global::GetSASuffix(int doSACorrection) {
        string suffix = "";
        if (doSACorrection == 1) suffix = "_wSACorrection";
        return suffix;
}

string Global::GetRunSuffix(int doLaser, int doLasGap, int doSACorrection, int pruning, int convConstantIndex, int doScintillatorIndex, bool useTime, bool noLBavg) {
        string run_suffix = GetLaserSuffix(doLaser) + GetInGapLaserSuffix(doLasGap) + GetSASuffix(doSACorrection) + GetScintillatorCorrSuffix(doScintillatorIndex);
        if (pruning == 0) run_suffix += "_noPrunning";
        if (convConstantIndex == 3310852) run_suffix += "_calib400LB500";
        if (convConstantIndex == 3310853) run_suffix += "_calib200LB300";
        if (convConstantIndex == 3310854) run_suffix += "_calib400LB600";
        if (convConstantIndex == 3310855) run_suffix += "_calib1000LB1200";
        if (convConstantIndex == 3310856) run_suffix += "_calib800LB1200";
        if (convConstantIndex == 3310857) run_suffix += "_calib600LB800";
        if (convConstantIndex == 3553312) run_suffix += "_calib200LB300";
        if (convConstantIndex == 3553313) run_suffix += "_calib300LB400";
        if (convConstantIndex == 3553314) run_suffix += "_calib400LB500";
        if (convConstantIndex == 3553315) run_suffix += "_calib500LB600";
        if (convConstantIndex == 3553316) run_suffix += "_calib600LB700";
        if (convConstantIndex == 3553317) run_suffix += "_calib700LB800";
        if (convConstantIndex == 3544940) run_suffix += "_LumiScale";
        if (convConstantIndex == 3544941) run_suffix += "_LumiScaleConstant";
        if (convConstantIndex == 3544942) run_suffix += "_LumiScaleNoGausFit";
        if (convConstantIndex == 3544943) run_suffix += "_LumiScaleConstant_Scans_1278";
        if (convConstantIndex == 3544944) run_suffix += "_LumiScaleConstant_Scans_1234781112";
        if (convConstantIndex == 3544945) run_suffix += "_LumiScaleConstant_Scans_1234781112_useFitIntegral";
        if (convConstantIndex == 3544946) run_suffix += "_LumiScaleCombined_constantFit_354494_Scans_1234781112_useGausFit_FromFitIntegral";
        if (convConstantIndex == 3544947) run_suffix += "_LumiScaleCombined_averaging_354494_Scans_1234781112_useGausFit_FromFitIntegral";
        if (convConstantIndex == 3544948) run_suffix += "_LumiScaleCombineCalibration_constantFit_354494_Scans_1234781112_useGausFit_FromFitIntegral";
        if (convConstantIndex == 3544949) run_suffix += "_LumiScaleCombineCalibration_averaging_354494_Scans_1234781112_useGausFit_FromFitIntegral";
        if (convConstantIndex == 0) run_suffix += "_currents";

        if (useTime) run_suffix += "_time";
        if (noLBavg) run_suffix += "_noLBavg";

        return run_suffix;
}

pair<string,string> Global::GetLaserFiles(int runNum, int anchorRun, int doLaser) {

        string laserFileName = "";
        string anchorLaserFile = "";
        auto dir_and_prefix = GetLaserDirectory(doLaser);
        if (!(dir_and_prefix.first == "" && dir_and_prefix.second == "")) {
                laserFileName = dir_and_prefix.first + "/" + dir_and_prefix.second + to_string(runNum) + ".txt";
                anchorLaserFile = dir_and_prefix.first + "/" + dir_and_prefix.second + to_string(anchorRun) + ".txt";
        }

        if (!fileExists(laserFileName) && useLaserInterpolation(doLaser)) laserFileName = CreateInterpolatedLaserFile(runNum, doLaser);
        if (!fileExists(anchorLaserFile) && useLaserInterpolation(doLaser)) anchorLaserFile = CreateInterpolatedLaserFile(anchorRun, doLaser);

        return make_pair(laserFileName, anchorLaserFile);

}

string Global::CreateInterpolatedLaserFile(int runNum, int doLaser) {
        int laserRunBefore = getLaserRun(runNum,-1);
        int laserRunAfter = getLaserRun(runNum,1);
        if (laserRunBefore == -1 || laserRunAfter == -1) {coutError("Unable to find laser runs for " + to_string(runNum)); return "";}

        pair<string,string> laserFiles = GetLaserFiles(laserRunBefore, laserRunAfter, doLaser);
        double laser_before[4][64][48]; LaserUtils::getLaserConstants(laserFiles.first, laser_before, 0);
        double laser_after[4][64][48]; LaserUtils::getLaserConstants(laserFiles.second, laser_after, 0);

        double val_interpolation_before = (doLaser == 10) ?     getIntegratedLumiAtRun(laserRunBefore, 4, 83)   : 0;
        double val_interpolation_atRun = (doLaser == 10) ?      getIntegratedLumiAtRun(runNum, 4, 83)           : getRunTDate(runNum).Convert() - getRunTDate(laserRunBefore).Convert();
        double val_interpolation_after = (doLaser == 10) ?      getIntegratedLumiAtRun(laserRunAfter, 4, 83)    : getRunTDate(laserRunAfter).Convert() - getRunTDate(laserRunBefore).Convert();

        cout << "Preparing interpolated laser file for " << runNum << " using the laser runs: [" << laserRunBefore << ", " << laserRunAfter << "]" << endl;
        cout << "Integrated luminosities are " << val_interpolation_before << " " << val_interpolation_atRun << " " << val_interpolation_after << endl;

        double laser_atRun[4][64][48];

        auto dir_and_prefix = GetLaserDirectory(doLaser);
        string new_laser_file = dir_and_prefix.first + "/" + dir_and_prefix.second + to_string(runNum) + ".txt";

        for (int part = 0 ; part < 4 ; part ++) {
                for (int mod = 0 ; mod < 64 ; mod ++) {
                        for (int pmt = 0 ; pmt < 48 ; pmt ++) {
                                laser_atRun[part][mod][pmt] = Lerp(val_interpolation_before, laser_before[part][mod][pmt], val_interpolation_after, laser_after[part][mod][pmt], val_interpolation_atRun);
                        }
                }
        }

        LaserUtils::saveLaserConstants(new_laser_file, laser_atRun);

        return new_laser_file;
}

TString Global::GetLegendFromSuffix(TString suffix) {

        TString legend = "No correction";

        if (suffix.Contains("wSACorrection")) legend = "withSAcorr";
        if (suffix.Contains("wLasGap")) legend = "withLaserGap";

        return legend;

}

bool Global::useTwoParScintillatorCorr() {return true;}

bool Global::useLaserInterpolation(int doLaser) {return doLaser == 10 || doLaser == 11;}

#endif