//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Nov  5 11:39:37 2018 by ROOT version 5.34/38
// from TTree IntegratorData/IntegratorData
// found on file: /eos/atlas/atlascerngroupdisk/det-tile/online/2017/mbias/TileMb_p0_20171026_1830.root
//////////////////////////////////////////////////////////

#ifndef IntegratorData_h
#define IntegratorData_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class IntegratorData {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Int_t           LB;
   Int_t           BeamEnergy;
   UInt_t          RunNumber;
   UInt_t          BeamType;
   UInt_t          timesec[48];
   UInt_t          timeusec[48];
   UShort_t        rawADCCurrent[64][48];
   Char_t          RunType;

   // List of branches
   TBranch        *b_LB;   //!
   TBranch        *b_BeamEnergy;   //!
   TBranch        *b_run_number;   //!
   TBranch        *b_BeamType;   //!
   TBranch        *b_timesec;   //!
   TBranch        *b_timeusec;   //!
   TBranch        *b_rawADCCurrent;   //!
   TBranch        *b_RunType;   //!

   IntegratorData(TString path);
   virtual ~IntegratorData();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef IntegratorData_cxx
IntegratorData::IntegratorData(TString path) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
    TTree * tree;
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(path);
    if (!f || !f->IsOpen()) {
        f = new TFile(path);
    }
    f->GetObject("IntegratorData",tree);

   Init(tree);
}

IntegratorData::~IntegratorData()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t IntegratorData::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t IntegratorData::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void IntegratorData::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("LB", &LB, &b_LB);
   fChain->SetBranchAddress("BeamEnergy", &BeamEnergy, &b_BeamEnergy);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_run_number);
   fChain->SetBranchAddress("BeamType", &BeamType, &b_BeamType);
   fChain->SetBranchAddress("timesec", timesec, &b_timesec);
   fChain->SetBranchAddress("timeusec", timeusec, &b_timeusec);
   fChain->SetBranchAddress("rawADCCurrent", rawADCCurrent, &b_rawADCCurrent);
   fChain->SetBranchAddress("RunType", &RunType, &b_RunType);
   Notify();
}

Bool_t IntegratorData::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void IntegratorData::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t IntegratorData::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef IntegratorData_cxx
