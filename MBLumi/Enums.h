
namespace PART {
    typedef enum {
        EBA = 0,
        LBA = 1,
        LBC = 2,
        EBC = 3,
    } PartID;
};

namespace PMT {  // From Tile mapping pmt-1 (Remember to add implementations in Utils.C GetPMTString, GetPMTStringExtra, HasLeftRight, IsRightPMT, GetPMTFromString and (GetLaserIndex if necessary) and in functions.h getCellsName)
    typedef enum {
        D5R = 16,
        D5L = 17,
        D6R = 36,
        D6L = 37,
        E3 = 0,
        E4 = 1,
        E4prime = 12,
        D0 = 0,
        D1L = 13,
        D1R = 14,
        D2L = 25,
        D2R = 26,
        D3L = 39,
        D3R = 42,
        BC1L = 3,
        BC1R = 2,
        BC2L = 7,
        BC2R = 6,
        BC3L = 11,
        BC3R = 12,
        BC4L = 17,
        BC4R = 16,
        BC8L = 41,
        BC8R = 40,
        BC9L = 45,
        BC9R = 44,
        BC11L = 9,
        BC11R = 8,
        BC12L = 15,
        BC12R = 14,
        E1 = 12,
        E2 = 13,
        A9L = 37,
        A9R = 38,
        A1L = 1,
        A1R = 4,
        A2L = 5,
        A2R = 8,
        A10L = 47,
        A10R = 46,
        A12L = 7,
        A12R = 6,
        A13L = 11,
        A13R = 10,
        A14L = 21,
        A14R = 20,
        A15L = 29,
        A15R = 28,
        A16L = 41,
        A16R = 40,
        MBTS_in = 4,
        MBTS_out = 12,
    } PMTID;
};

namespace DUMMY {
    typedef enum {
        PED = -998,
        CURR = -999,
        CORR = -997,
    } DUMMYID;
};

namespace PEDESTALS {
    typedef enum {
        MEAN,
        FIT_LINEAR,
        FIT_339197,
        FIT_354124,
        FIT_SECONDEXPO,
        FIT_331020_SECOND_PEDESTAL,
        INTERPOLATION,
        FIT_EXPO,
        FIT_ADDEXPO,
        VDM,
        FIT_THIRDEXPO,
        ZEROES,
        FIT_SECONDPOLY,
        VDM_EXPO,
        VDM_EXPO_wINTERP,
        VDM2017_EXPO_w2INTERP,
        VDM2017_EXPO_w3INTERP,
        VDM5TeV_EXPO_PLUS_LINE_FIT,
        VDM5TeV_LINE_INTERP_PLUS_LINE_FIT,
        INTERPOLATION_TO_LINEAR_FIT,
        CALIB_PED,
    } PEDESTALID;
};

namespace ANCHOR_TYPE {
    typedef enum {
        TRACKS,
        LUCID,
        BFILE_D6,
        BFILE_D5,
    } ANCHOR_TYPEID;
}

namespace METRIC {
    typedef enum {
        RAW,
        SIGNAL,
        CURRENT,
        LUMI,
    } MetricID;
};