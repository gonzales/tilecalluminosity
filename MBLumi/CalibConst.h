
class CalibConst {
    public:

        Run * run;
        int iLB, fLB;
        int fileTracksIndex;
        // float TML_mu = 1.70322;
        // float f_rev = 11245.4315;
        // float n_bunch;
        // float sigma_inel = 80000;
        vector<float> anchor_LBs, anchor_lumi;
        string convConstFilename = "";
        string suffix;
        double extra_factors[4][64][48];
        double extra_TRACKs_factor = 1.;
        int anchor_type;

        double conv_const_EBA_current[64][48], conv_const_EBC_current[64][48];

        CalibConst(Run * the_run, int anchor_type_id, int _iLB, int _fLB, int _fileTracksIndex, string _suffix) {
            run = the_run;
            anchor_type = anchor_type_id;
            iLB = _iLB;
            fLB = _fLB;
            fileTracksIndex = _fileTracksIndex;
            suffix = _suffix;
            // n_bunch = Global::getNBunch(the_run->runNum);

            memset(conv_const_EBA_current, 0, sizeof(conv_const_EBA_current));
            memset(conv_const_EBC_current, 0, sizeof(conv_const_EBC_current));

            for (int part = 0 ; part < 4 ; part ++) {
                for (int mod = 0 ; mod < 64 ; mod ++) {
                    for (int pmt = 0 ; pmt < 48 ; pmt ++) {
                        extra_factors[part][mod][pmt] = 1;
                    }
                }
            }

            if (the_run->runNum == 339205) convConstFilename = "share/testCalib_339205_350LB370.dat";
            if (the_run->runNum == 331085) convConstFilename = "share/TransfValue_331085_tracksmod_laser.dat";
            if (the_run->runNum == 354174) convConstFilename = "share/testCalib_354174_120LB140.dat";

            fstream calibfile; calibfile.open(convConstFilename);
            string cline;
            double c_const, c_error;
            int c_mod, c_pmt;
            char c_part[10];
            while(getline(calibfile, cline)) {

                sscanf (cline.c_str(),"%9s\t%i\t%i\t%lf\t%lf\n", c_part, &c_mod, &c_pmt, &c_const, &c_error);

                if (strcmp(c_part, "EBA") == 0) {
                    conv_const_EBA_current[c_mod][c_pmt] = c_const;
                }

                if (strcmp(c_part, "EBC") == 0) {
                    conv_const_EBC_current[c_mod][c_pmt] = c_const;
                }

            }
        }

        void getBenedettoLumis(int columnToUse);
        void getBFileLumis();
        void calcConstants(vector<vector<vector< float>>> lbCurr_byLB [4], vector<float> lbs);

};