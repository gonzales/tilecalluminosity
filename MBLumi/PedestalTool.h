

namespace PedestalTool {

    void doPedestal_MEAN(Run * run, int part, int mod, int pmt, vector<vector<vector<float> > >  pedLBs, vector<vector<vector<float> > > Ped, vector<vector<vector<float> > >  pedLBs_err, vector<vector<vector<float> > > Ped_err, int doPedestalPlots);
    void doPedestal_CALIB_MEAN(Run * run, int part, int mod, int pmt, vector<vector<vector<float> > >  pedLBs, vector<vector<vector<float> > > Ped, vector<vector<vector<float> > >  pedLBs_err, vector<vector<vector<float> > > Ped_err, int doPedestalPlots);
    void doPedestal_LINEAR(Run * run, int part, int mod, int pmt, vector<vector<vector<float> > >  pedLBs, vector<vector<vector<float> > > Ped, vector<vector<vector<float> > >  pedTimes, vector<vector<vector<float> > >  pedTimes_err, vector<vector<vector<float> > > Ped_err, int doPedestalPlots);
    void doPedestal_EXPO (Run * run, int part, int mod, int pmt, vector<vector<vector<float> > >  pedLBs, vector<vector<vector<float> > > Ped, vector<vector<vector<float> > >  pedTimes, vector<vector<vector<float> > >  pedTimes_err, vector<vector<vector<float> > > Ped_err, int doPedestalPlots);
    void doPedestal_FIT_339197(Run * run, int part, int mod, int pmt, vector<vector<vector<float> > >  times, vector<vector<vector<float> > > Ped, vector<vector<vector<float> > >  times_err, vector<vector<vector<float> > > Ped_err, float pedNum[64][48], float pedDen[64][48], int doPedestalPlots);
    void doPedestal_FIT_354124(Run * run, int part, int mod, int pmt, vector<vector<vector<float> > >  times, vector<vector<vector<float> > > Ped, vector<vector<vector<float> > >  times_err, vector<vector<vector<float> > > Ped_err, int doPedestalPlots);
    void doPedestal_SECONDEXPO(Run * run, int part, int mod, int pmt, vector<vector<vector<float> > >  pedLBs, vector<vector<vector<float> > > Ped, vector<vector<vector<float> > >  pedTimes, vector<vector<vector<float> > >  pedTimes_err, vector<vector<vector<float> > > Ped_err, int doPedestalPlots, int beamDump0_constant1);
    void doPedestal_INTERPOLATION(Run * run, int part, int mod, int pmt, vector<vector<vector<float> > >  pedLBs, vector<vector<vector<float> > > Ped, vector<vector<vector<float> > >  pedTimes, vector<vector<vector<float> > >  pedTimes_err, vector<vector<vector<float> > > Ped_err, int doPedestalPlots);
    void doPedestal_INTERPOLATION_TO_LINEAR_FIT(Run * run, int part, int mod, int pmt, vector<vector<vector<float> > >  pedLBs, vector<vector<vector<float> > > Ped, vector<vector<vector<float> > >  pedTimes, vector<vector<vector<float> > >  pedTimes_err, vector<vector<vector<float> > > Ped_err, int doPedestalPlots);
    void doPedestal_ADDEXPO (Run * run, int part, int mod, int pmt, vector<vector<vector<float> > >  pedLBs, vector<vector<vector<float> > > Ped, vector<vector<vector<float> > >  pedTimes, vector<vector<vector<float> > >  pedTimes_err, vector<vector<vector<float> > > Ped_err, int doPedestalPlots);
    void doPedestal_VDM (Run * run, int part, int mod, int pmt, vector<vector<vector<float> > >  pedLBs, vector<vector<vector<float> > > Ped, vector<vector<vector<float> > >  pedTimes, vector<vector<vector<float> > >  pedTimes_err, vector<vector<vector<float> > > Ped_err, vector<float> LB_LCD_zero, vector<vector<vector<float> > > rawTimes, vector<vector<vector<float> > > rawPed, vector<vector<vector<float> > > rawLBs, int doPedestalPlots);
    void doPedestal_ZEROES (Run * run, int part, int mod, int pmt);
    void doPedestal_SECONDPOLY (Run * run, int part, int mod, int pmt, vector<vector<vector<float> > > pedTimes, vector<vector<vector<float> > >  pedLBs, vector<vector<vector<float> > > Ped);
    void doPedestal_VDM_EXPO (Run * run, int part, int mod, int pmt, vector<vector<vector<float> > > pedTimes, vector<vector<vector<float> > >  pedLBs, vector<vector<vector<float> > > Ped);

    vector<float> getTimes(int mod, int pmt, int pedStart, int pedEnd, vector<vector<vector<float> > >  pedLBs, vector<vector<vector<float> > >  pedTimes, vector<vector<vector<float> > > Ped);

}