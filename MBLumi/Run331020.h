
class Run331020 : public RunLB {
    public:
        int useFirstPedestal;

        void ComputePedestal(int doPedestal, int part, int doPedestalPlots);

        Run331020(int runNum, string runSuffix, string convConstFilename, string laserFileName, string anchorLaserFileName, int cell_type, int doUseFirstPedestal = 1) : RunLB(runNum, runSuffix, convConstFilename, laserFileName, anchorLaserFileName, cell_type) {
            useFirstPedestal = doUseFirstPedestal;
            if (doUseFirstPedestal == 0) pedestalType = PEDESTALS::FIT_SECONDEXPO;
            if (doUseFirstPedestal == 2) pedestalType = PEDESTALS::FIT_THIRDEXPO;
            if (doUseFirstPedestal != 1) {
                if (runNum == 331020) pedestalFileIndex_offset = 4;
                pedStart = getPedLimits().at(2);
                pedEnd = getPedLimits().at(3);
                numberForMean = 1;
                if (runNum == 341615) numberForMean = 3;
                if (runNum == 331020) numberForMean = 5;
            }
        }

        

};
