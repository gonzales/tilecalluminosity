#!/usr/bin/env python

import sys, os
import ROOT

from optparse import OptionParser

def get_comma_separated_args(option, opt, value, parser):
    setattr(parser.values, option.dest, value.split(','))

parser = OptionParser()

parser.add_option('-r','--runNumber', type=int, help='Run number', dest="runNumber")
parser.add_option('-c','--cellsType', type=int, help='Index of the cells', dest="cellsType")
parser.add_option('-s', '--suffix', type='string', help='Suffix of the output', dest = "suffix", default = "")
parser.add_option('-a','--anchor-run', type=int, help='Anchor Run number', dest="anchorRun", default = 0)
parser.add_option('-l','--laser-index', type=int, help='Index for Laser Corrections', dest="laserIndex", default = 0)
parser.add_option('-g','--inGapLaser', type=int, help='Index for inGap Laser', dest="inGapLaser", default = 0)
parser.add_option('--scintillator-index', dest = "scintillatorIndex", type=int, help='Scintillator correction index', default=0)
parser.add_option('--pruning', type=int, help='Pruning factor', dest="pruning", default=1)
parser.add_option('--doPedestal', dest = "doPedestal", action="store_true", help='Recompute pedestal', default=False)
parser.add_option('--doPedestalPlots', dest = "doPedestalPlots", action="store_true", help='Do pedestal plots', default=False)
parser.add_option('--doSACorrection', dest = "doSACorrection", action="store_true", help='Do SA correction', default=False)
parser.add_option('--doEpicTree', dest = "doEpicTree", action="store_true", help='Do save EpicTree', default=False)
parser.add_option('--override-pedestal', type='string', help='Override pedestal method', dest = "overridePedestal", default="")
parser.add_option('--outFile-folder', type='string', help='Folder to store the output BFiles', dest = "outFile_folder", default="BFiles")
parser.add_option('--useTime', dest = "useTime", action="store_true", help='Use time instead of LB', default=False)
parser.add_option('--noLBavg', dest = "noLBavg", action="store_true", help='Dont average over LB', default=False)
parser.add_option('--anchor-file', type='string', help='Use this anchor file', dest = "anchor_file", default="")

(options, args) = parser.parse_args()

ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine(".L MBLumi.C")

ROOT.MBLumi(
                options.runNumber, 
                options.pruning,
                options.doPedestal,
                options.doPedestalPlots,
                options.anchorRun,
                options.cellsType,
                options.laserIndex,
                options.inGapLaser,
                options.doSACorrection,
                options.doEpicTree,
                options.scintillatorIndex,
                options.suffix,
                options.overridePedestal,
                options.outFile_folder,
                options.useTime,
                options.noLBavg,
                options.anchor_file,
            )
